Wireless (Wi-Fi/WLAN)
=====================

The official [WiFi documentation](https://unlimited.ethz.ch/display/itkb/Wi-Fi) explains how to use the wireless networks `eduroam` and `eth` provided by the [Informatikdienste](http://www.id.ethz.ch).


Guest access
------------

Visitors are granted temporary access to the ETH wireless

* Connect your device to the `public`, `public-5` or `eth-guest` wireless SSID
* On the pop-up, click the `Guest Access Wi-Fi` link
* You will be redirected to the [self-registration portal](https://iam.password.ethz.ch/ethzWebcenterPublic/startRegistration.do?redirectURL=https://enter-guest-net.ethz.ch)
* Provide your name and mobile number
* Enter the verification code that was sent to your phone
* Follow the instructions to connect to the wireless network.
* The guest account will provide access until midnight of the same day.


Best practices
--------------

If you are connecting for the first time or you need to re-connect your mobile device use an unloaded access point. This means better not do this in crowded places like the cafeteria, canteen or exteriors. There have been reports of problems when connecting from such places.

Connect your mobile device to the **eduroam** network, which offers access for members of universities from many countries [around the world](https://www.eduroam.org/where/).


Quick guide
-----------

Use the Configuration Assistant Tool (CAT): [cat.eduroam.org](https://cat.eduroam.org/) to setup eduroam, or refer to the [instructions](https://unlimited.ethz.ch/display/itkb/Wi-Fi) for more information.

Manual setup instructions
-------------------------

Note: Manual setup may not work on all devices.

For most devices automatic mode should work and you can leave everything on default settings. However on some mobile devices you may have to specify some of the following settings to connect to the wireless network:

| Name                           | Value                                 |
|--------------------------------|---------------------------------------|
| Network name                   | eduroam                               |
| Wi-Fi security                 | WPA & WPA2 Enterprise                 |
| Authentication                 | Protected EAP (PEAP)                  |
| Anonymous identity             | *leave empty*                         |
| CA certificate                 | *leave empty* / (none) / `ethz.ch`    |
| CA validation                  | Check no CA required / Don't validate |
| PEAP version                   | Automatic                             |
| Phase 2 (inner) authentication | MSCHAPv2                              |
| Username (for employees)       | ETHusername@staff-net.ethz.ch         |
| Username (for students)        | ETHusername@student-net.ethz.ch       |
| Password                       | Network password (WIFI/VPN)           |

Where `ETHusername` is your username from the [[ETH Account|account]].

In case you have special devices that require wireless access, please [[contact us|/services/contact]] to connect them to the IoT network.


Test Wifi/VPN password
----------------------

Should you be unsure about your Wireless/VPN password, you can try to log in on https://sslvpn.ethz.ch/ with username `ETHusername@staff-net.ethz.ch`. In case you're not able to log in, you may try to set a new Wifi/VPN password on [password.ethz.ch](https://password.ethz.ch).


Additional instructions
-----------------------

* [Official ETH WiFi documentation](https://unlimited.ethz.ch/display/itkb/Wi-Fi)
* [WiFi on Linux](/linux/eduroam_wifi_on_linux)
* [WiFi on Android](/network/eduroam_wifi_on_android)
