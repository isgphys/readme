Secure your own device
======================

The main D-PHYS network is different from your network at home or from usual corporate networks.
It has an almost fully open firewall to allow connections to arbitrary ports and services on your device.
If your computer is not configured correctly and exposes vulnerable services to the network,
it is at risk of being attacked from the whole internet.

This page provides a rough checklist what you should do to minimize the attack surface
and greatly reduce the chances that your device will be hacked.

If some of the steps described here do not mean anything to you or if you would like to have
fewer headaches keeping your device secure in an open network, **[[please contact us|/services/contact]]**!
We will help you or place your device in a [[more restricted (firewalled) network|/network#other-networks]],
where it cannot be accessed directly (without VPN) from the internet.

All operating systems
---------------------

- **Always** have your device/software fully updated
- [[Use good passwords|/documentation/how_to_handle_passwords]]
- Never install software from untrusted sources
- Enable your host firewall
- Only expose services (ports) needed

More detailed recommendations can be found on [monster.phys.ethz.ch](https://monster.phys.ethz.ch).

Windows
-------

### Windows Updates

Windows updates are installed automatically. Please check sometimes if everything is installed correctly here: **Settings** > **Update & Security** > **Windows Update**. There should be a green symbol.

It's recommendable to enable these two settings:
**Settings** > **Update & Security** > **Windows Update** > **Advanced Options**, Enable **Receive updates for other Microsoft products...** this is for updating other Microsoft products like MS Office and **Show a notification when your PC requires a restart...** to get an additional notification if a restart is required after installing updates.

### Network and firewall

Check that your network settings are set to **Public**.
Go to **Settings** > **Network & Internet** > **Status** > **Network status**,
go to the network interface **Properties** and set it to **Public**.
This will enable more restrictive firewall settings.

### Windows Security

Go to **Settings** > **Update & Security** > **Windows Security** > **Open Windows Security**,
check that all is green.

### Remote Desktop

If you enable Remote Desktop it will be world wide open. But we block RDP traffic from outside the ETH, therefore you will always need a [VPN](/windows/vpn_windows/) connection to ETH network.

You can restrict it more with a powershell in admin mode.

```powershell
# Allow just current subnet.
Set-NetFirewallRule -DisplayGroup "Remote Desktop" `
          -RemoteAddress "LocalSubnet"

# Allow just current subnet and eth vpn networks
Set-NetFirewallRule -DisplayGroup "Remote Desktop" `
          -RemoteAddress "LocalSubnet", 10.5.0.0/16, 10.6.0.0/16
```


Linux
-----

All example configs and commands are for a most recent Debian installation or derivatives (Debian, Ubuntu, Raspbian)
and may need adaptions for other distributions.

- **Always change default passwords** of default users (root, pi, ...)
- Install packages from trusted repositories only

### Secure SSH access to use [[key-based authentication|/documentation/ssh_keys]] only

Edit `/etc/ssh/sshd_config`:

```
PermitRootLogin without-password
ChallengeResponseAuthentication no
PasswordAuthentication no
AuthorizedKeysFile .ssh/authorized_keys
```

Restart sshd: `systemctl restart sshd.service`

This is the most secure option and will disable password-based authentication.

### Secure SSH access with passwords

Alternatively enable password-based authentication for non-root users and limit the allowed users:

```
PermitRootLogin without-password
ChallengeResponseAuthentication no
PasswordAuthentication yes
AuthorizedKeysFile .ssh/authorized_keys
AllowUsers root alice bob
```

Restart sshd: `systemctl restart sshd.service`

Install [fail2ban](https://www.fail2ban.org) to slow down brute-force attacks:

```bash
apt install fail2ban
```

[[Use good passwords|/documentation/how_to_handle_passwords]]!

### Enable automatic update installation

- Recommended: `apt install aptitude-robot`
- Alternative: Install and configure `cron-apt`

### Secure services and ports

Keep the list of services exposed to the network as small as possible.
Exposed here means services listening on non-loopback interfaces.
To check that use the following command:

```bash
ss -tulpn
```

All sockets listed in column `Local Address:Port` that are NOT listening on
`127.0.0.1` (IPv4 loopback address) or `[::1]` (IPv6 loopback address)
may be exposed to the network. This includes any explicit IP addresses
or all addresses/interfaces, noted as `0.0.0.0` (IPv4) and `[::]` (IPv6).

If some services are needed/exposed, secure them by configuration or firewall.

### Avoid high risk and unencrypted services

The following non-exhaustive list of services/ports are known to be insecure
and should be avoided/protected at all cost:

- Telnet: `23`
- FTP: `20`, `21`
- VNC: `5900+N`
- mDNS: `5353`

See also [SWITCH report of vulnerable services](https://www.switch.ch/fr/security/info/report/vulnerable/).

### Use a host firewall

Additionally or alternatively to securing services, install and configure a host firewall
using either [iptables](https://wiki.archlinux.org/index.php/iptables),
[nftables](https://wiki.archlinux.org/index.php/nftables) or
[ufw](https://ubuntu.com/server/docs/security-firewall).

Very simple example using `ufw` (Uncomplicated Firewall) to allow SSH access only:

```bash
apt install ufw
ufw enable
ufw allow 22
```

Do not forget IPv6.
