Network
=======

Wired network
-------------

### ETH docking network

The ETH docking network is the default network for all computers. It uses the [[802.1x|802 1x]] protocol for authentication. You can use your ETH credentials to get full network access.

### D-PHYS networks

Machines must be [[registered|How to get network access]] to access our D-PHYS networks. Registrations expire 45 days after the last usage of the computer in our network. Machines registered to you can also be directly reactivated on https://registration.phys.ethz.ch/.

#### D-PHYS NAT network with DynDNS

By default, all registered computers will be connected to our NAT networks. These are ETH-internal sub-nets with full access to the internet, but with a firewall that forbids access from outside ETH.

These computers are automatically listed in the dynamic DNS under the name _**sentname**.dhcp-int.phys.ethz.ch_. This allows you to connect to your computer using this name instead of its dynamic IP address. The https://registration.phys.ethz.ch/ page also shows the name sent by your machine, that is used for DynDNS.

#### Other networks

We have a variety of other network types. If your computer hosts a service, that needs to be accessible from everywhere, we can move it to a public network and assign a static IP and hostname. Conversely there are even more restricted networks for special needs (IoT, eXile, etc). Please talk to us for more details.


Wireless Network
----------------

To connect to the wireless network (WiFi) from your mobile devices, please refer to [[/network/wireless]].


VPN (Virtual Private Network)
-----------------------------

To connect to the [[ETH network|network/]] from outside the campus, please refer to [[services/how_to_use_vpn/]].


Security Policy
---------------

All computers connecting to a network on ETH campus have to adhere to the [security policies](https://ethz.ch/staffnet/en/it-services/it-security/awareness/information-security-house-rules.html). This includes that the computers must be free of any viruses/malware/bots and that the access to the network can be blocked if those requirements are not fulfilled.

Please refer to our basic checklist that should help you to [[secure your own device]].
