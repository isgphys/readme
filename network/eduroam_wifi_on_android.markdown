# Eduroam Wifi on Android

In case your Android device does not automatically detect the settings of the eduroam wireless network at ETH, the following screenshot shows the required configuration.

[[!img /media/network/eduroam_android.png size="400x"]]

Android 11 comes with a slightly modified wifi setup:

[[!img /media/network/eduroam_android11.jpg size="400x"]]
