How to get network access
=========================

The access to our D-PHYS network is restricted to registered machines. For most computers the ETH Docking network should be sufficient and can be used without registration.


Did my computer just expire?
----------------------------

Computers not seen on the network for more than 45 days expire automatically. Computers registered and not connected successfully on the same day, also expire on the next day.

Please first visit https://registration.phys.ethz.ch to check if your computer has just expired and where you can directly reactivate it.
If you or your deputy do not see it in the list, please register your computer (see below).


How do I register my computer?
------------------------------

The following information must be provided to register your computer for the D-PHYS network:

* MAC address of your Ethernet interface
* Operating system (Windows/Mac/Linux)
* Hardware type (Laptop/Desktop/Ethernet adapter)
* Two physics accounts to register the computer on: typically one being the user and administrator of the computer and the second a deputy we could contact if the user is unreachable and there's a problem with the machine. The latter is often chosen to be the IT responsible of your group.

We can register your computer if you provide us this information either by sending us a [[/chat]] message in [#helpdesk:phys.ethz.ch](https://m.ethz.ch/#/#helpdesk:phys.ethz.ch) or by sending us an [email](mailto:isg@phys.ethz.ch).

Newly registered computers not seen on the network within the same day will expire automatically on the following day.


Where can I find the MAC address?
---------------------------------

The [MAC address](http://en.wikipedia.org/wiki/Mac_address) is a kind of telephone number which is needed for computers to communicate in a network. It is written as 6 pairs of letters or numbers, like `01:ab:02:cd:03:ef`.

### Windows

Use the command `getmac -v` or `ipconfig /all` in a Command Prompt to see the MAC address. Be sure to pick the wired network interface and not the wireless.

[[!img /media/how_to_get_network_access/ipconfig.png alt="ipconfig.png" size="450x"]]

### macOS

Open _System Preferences, Network, Ethernet, Advanced, Hardware_ to see the MAC address.

If not already done, [[disable automatic 802.1x connection|osx/802_1x]].

### Linux

Enter the command `/sbin/ip address` to see the MAC-address

```
$ /sbin/ip address
link/ether 45:24:c6:7b:f0:da brd ff:ff:ff:ff:ff:ff
           ^^^^^^^^^^^^^^^^^
```

Or on older computers, enter the command `/sbin/ifconfig` to see the MAC-address

```
$ /sbin/ifconfig
eth0      Link encap:Ethernet  HWaddr 00:D0:B7:AB:FD:BF
                                      ^^^^^^^^^^^^^^^^^
```


Why do I need to register my computer?
--------------------------------------

The access to the ETH network is restricted to either known users (when authenticating with 802.1X) or known clients (when registering their MAC address). All other hosts will be redirected to a guest network with limited internet access.

By registering your MAC address you can avoid the 802.1X login with your ETH account. In addition your computer will get an IP address from our D-PHYS network. Given that we (ISG) are responsible for all devices in our network, we require at least two D-PHYS users as contact.

Technically speaking, all Ethernet sockets are configured to be "docking", meaning that there is no fixed VLAN associated. The UTP socket will be dynamically patched to the VLAN corresponding to the MAC address registration (or 802.1X authentication). As a side effect it may happen that some switches do not properly relay the MAC address information of the connected devices. These switches may need to be power-cycled for newly registered devices to be seen correctly.


Which requirements must be fulfilled?
-------------------------------------

The D-PHYS network is different from the networks usually found in companies. There are basically no firewall rules between the public internet and our local network. Therefore most computers are visible and reachable from the outside. This implies that the host must be able to withstand the possible attacks. Computers that are found to be hacked must be taken offline and reinstalled.

* A maintained version of the operating system with all updates installed.
* Strong passwords for all accounts on the computer.
* Unused services should be switched off.


How will the computer get an IP address?
----------------------------------------

We distribute IP addresses, DNS servers etc. by DHCP. When you never touched your network settings, everything will work without your intervention. When you changed the default, be sure to enable DHCP.

### DHCP on Windows

If you are using Windows and would like to access D-PHYS IT services, please refer to the following documentation as well: [[Configuration hints for self-managed Windows Machines|windows/self_managed_windows_machines]]

### DHCP on Linux

Most Linux distributions set up DHCP usage by default. In other cases please consult the manual of your distribution on how to activate DHCP.

You may want to have a look at our [[configuration hints for self-managed Linux machines|linux/self_managed_linux_machines]] if you want to customize DynDNS.
