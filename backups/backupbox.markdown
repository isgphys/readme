BackupBox
=========

Overview
--------

BackupBox allows backups from anywhere as long as a network connection is available. So it's meant for the following types of machines:

* ETH-owned laptops which are not always online and frequently away from the D-PHYS network.
* private laptops: We only back up directories exclusively used for ETH data. We recommend to use the name "ETH-Data" for this directory.

BackupBox is self service and has its own [website](https://backupbox.phys.ethz.ch). Please find all usage and installation instruction over there.
