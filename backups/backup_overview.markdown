Automatic Backup of Managed Workstations and Servers
====================================================

Your data is backed up on a regular schedule. The last 30 days (or even more, on a best effort basis) are backed up on our backup server and are accessible by the user to restore files. This includes

* Backup of Home directories
* Backup of Groupdrives
* Backup of Webshares
* [[Backup of Managed Workstations|Backup of Workstations]]

Access Backups to Restore Data
------------------------------

NB: The following refers to the backups of the home directories on managed workstations. The backups of non-managed computers can be restored from the [BackupBox](https://backupbox.phys.ethz.ch/) and [BackupBox-Lab](https://backupbox-lab.phys.ethz.ch/) web interface.

Most of these backups can be restored by yourself. Connect the network share with the backups and simply copy the files to where you need them.

* [[Restore]]

Restoring Emails from the IMAP folder is a bit more tricky. Please get in touch with us to have emails restored from backup.

Backup Folder Structure
-----------------------

Your personal directory contains 2 subdirectories, each containing folders with the individual backups organized by date and time.

```
  user/my_uid
   |
   |--2014.09.01_210002/    Homedirectory of September, 1st
   |--2014.09.02_210002/           "      of September, 2nd
   |...

  group/
   |
   |--my_share_1
      |--2014.09.01_210002/    my_share_1 of September, 1st
      |--2014.09.02_210002/           "   of September, 2nd
      |...
   |--my_share_2
      |--2014.09.01_210002/    my_share_2 of September, 1st
      |--2014.09.02_210002/           "   of September, 2nd
      |...
   |...
```

Backups are typically done during the night - so the folder named `2014.09.01_210002` contains data of September 1st 2014 at 21:00:02.
