Backups
=======

Who has never deleted or overwritten a file? Lost data because of a computer crash? Hours of work in a project and everything went away... Files stored on our servers are backed up on a daily basis and we offer you an easy way to access them. You can even register your laptop to be backed up by us. Read on to prevent a lot of headache in the next disaster.

Even though our servers are equipped with [Redundant Arrays of Independent Disks (RAID)](http://en.wikipedia.org/wiki/Standard_RAID_levels), data may be damaged in case of a hardware blackout, break-in or human error. To prevent actual data loss in these emergency situations, all data on our servers is backed up to a second set of servers.

Depending on OS and hardware requirements, we use our in-house development [BaNG](https://github.com/isgphys/BaNG) (based on [rsync](http://samba.anu.edu.au/rsync/), for server backups), or BackupBox and BackupBox-Lab (based on [restic](https://restic.net)) to perform the backups.

## Further Reading

* [[Automatic Backup of managed workstations and servers|backup overview]]: restore home, mail and group data
* [[BackupBox]]: optional backup for laptops
* [[BackupBox-Lab]]: optional backup for lab PCs

## Archival

Archival is related to, but different from backup. It is meant as a permanent long-term storage for data that is no longer in active use, typically on tape. We provide archival in the form of [ID's LTS](https://ethz.ch/staffnet/en/it-services/catalogue.html#/detail/252) system. Please see our [[LTS]] page for details.

Don't hesitate to [[contact us|services/contact]] in case you have troubles to find lost files!
