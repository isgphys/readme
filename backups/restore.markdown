Restore data from our backups
=============================

You have read-only access to our backups if you want to restore files from your home or a group share. The folder structure is explained in the [[backup overview]].

Please refer to [[BackupBox]] and [[BackupBox-Lab]] if you want to restore from optional backups of laptops and Lab PCs.

Windows
-------

* press `🪟 Windows` and the `R` key together
* Type `\\restore.phys.ethz.ch\restore`
* Enter your D-PHYS username **`AD\USERNAME`** and your D-PHYS password and select OK

macOS
--------

* Select the **Finder**
* Click in the menu **Go** the point **Connect to server**
* Type in `smb://restore.phys.ethz.ch/restore` and click **Connect**
* In the following dialog, enter your username and password
* You'll get your backup directory mounted and shown on your desktop

Linux
-----

### SMB

On Linux, you may access an SMB server with several interfaces:

* Nautilus, Konqueror, far2l, mc and others are able to connect directly to `smb://restore.phys.ethz.ch/restore`

* Browse backups with ftp-like interface

```sh
smbclient //restore.phys.ethz.ch/restore --max-protocol SMB3 -W AD -U <username>
```

**NOTE** that in some cases you have to use **uppercase _AD_** for the domain if you get a _permission denied_ error
