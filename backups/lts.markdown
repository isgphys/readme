LTS
===

LTS ([long term storage](https://ethz.ch/staffnet/en/it-services/catalogue.html#/detail/252)) is a tape long-term archival system run by Informatikdienste. If you have *cold* data (not in active use any longer) that should still be kept around but will likely not be accessed for years to come, LTS is for you. In a nutshell, it works like this:

- you collect the data in a specific folder on the file server
- we then use a bunch of scripts around the [dar](http://dar.linux.free.fr/) archiver to create a set of 200G archive files that we write onto the LTS ingress share
- since we take care of all data processing, you don't have to worry about the 'limitations' listed on ID's LTS web page.
- LTS then duplicates the archive to a second tape robot so there are always two copies
- when this copy process has finished, you'll be able to verify your archive on our [LTS dashboard](https://ltsdash.phys.ethz.ch/)
- once you/we have made sure the data has been committed to LTS, we can delete the files from the file server
- should you ever need to retrieve this data, we'll restore it for you on the file server

If you have any data that you'd like to have archived on tape, please get in touch!

Note that it doesn't make much sense to archive a couple of GB to LTS. As a rule of thumb, the lower limit for an LTS archive should be around 5 TB.
