Backup of Managed Workstations
==============================

With some exceptions workstations are **not** backed up. The content of your local disks will be lost in case of a disk crash or a new installation. Upgrades or re-installations are done by us with some care about your data but we cannot guarantee that everything will be back after reinstall. So be sure to put your valuable data on the [[file server|storage/]] and read your mail with [[IMAP|mail/]] or [webmail](http://webmail.phys.ethz.ch). File space on the local hard disk (mostly on `/scratch`) is only meant for temporary data that needs fast access.

Now the exceptions of the above rule:

* The data disks of selected Linux workstations are backed up on our backup server. See [[backups/restore]] for some hints on how to restore data.
* Some Windows workstations of the department services and [Firstlab](http://www.first.ethz.ch) have a image copy of their local disks on one of our servers. Ask your local IT administrator in case of troubles.
* We also offer [[BackupBox]] for laptops and lab PCs.
