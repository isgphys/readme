BackupBox-Lab
=============

Overview
--------

Having made good experiences with [[BackupBox]], we now provide [BackupBox-Lab](https://backupbox-lab.phys.ethz.ch) for lab PCs. Backups are triggered on the client (push vs. pull), are encrypted and usually very fast. It is meant for self-managed PCs within the ETH network. If you are planning to backup multiple machines (eg. all measurement machines in your group), please contact us, we will provide you with an impersonal, shareable login.
You can then use [[mail/sieve/]] on this technical account to forward status emails to your own email account.

BackupBox-Lab is self service and has its own [website](https://backupbox-lab.phys.ethz.ch). Please find all usage and installation instruction over there.
