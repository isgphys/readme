Recurring jobs
==============

Recurring jobs can be configured with systemd or cron.
On managed Linux workstations you have the option to use
[systemd timers](https://www.freedesktop.org/software/systemd/man/latest/systemd.timer.html),
which are described here.
Cronjobs are a thing of the past and not suitable for modern multi-user systems.

Run a script every hour
-----------------------

Let's create a systemd timer that will run an existing executable script `~/bin/foo.sh` every hour.

### Create the service unit

```bash
systemctl --user edit --full --force foo.service
```

This opens the editor, paste the following or similar content:

```ini
[Unit]
Description=Run foo
After=network-online.target

[Service]
Type=oneshot
ExecStart=%h/bin/foo.sh

[Install]
WantedBy=multi-user.target
```

Notes: The placeholder `%h` will be expanded to your home directory path.
You may also specify an absolute path.

You can test (start) the service unit using:

```bash
systemctl --user start foo.service
systemctl --user status foo.service
```

### Create the timer unit

```bash
systemctl --user edit --full --force foo.timer
```

With the following content:

```ini
[Unit]
Description=Run foo hourly
After=network-online.target
ConditionHost=|<hostname>

[Timer]
OnCalendar=hourly

[Install]
WantedBy=timers.target
```

Replace `<hostname>` with the hostname of the computer.
You may also specify `ConditionHost` multiple times to configure the timer on multiple hosts.

You can test or start the timer unit using:

```bash
systemctl --user start foo.timer
systemctl --user status foo.timer
```

### Enable the timer unit to activate on boot

To automatically start the timer after reboot:

```bash
systemctl --user enable foo.timer
loginctl enable-linger
```

Note: You need to configure `loginctl enable-linger` on all computers where the timer should automatically
start after a reboot. This will make sure the systemd user service manager will start for your user without
you having to manually log in.

### Listing or removing timers

Show active timers:

```bash
systemctl --user list-timers
```

Show all timers:

```bash
systemctl --user list-timers --all
```

Disable and stop timer:

```bash
systemctl --user stop foo.timer
systemctl --user disable foo.timer
```

### Manual configuration and logs

Systemd user units are stored in your home directory in `~/.config/systemd/user`
and are available on all managed Linux workstations via NFS.

The timer and service configured above will look like this:

```bash
+rdatest@krum:~$ tree .config/systemd/
.config/systemd/
└── user
    ├── foo.service
    ├── foo.timer
    └── timers.target.wants
        └── foo.timer -> /home/rdatest/.config/systemd/user/foo.timer
```

Whenever you manually edit those files, you need to reload systemd:

```bash
systemctl --user daemon-reload
```

To inspect the log:

```bash
journalctl --user -f
```

Run a script immediately after boot
-----------------------------------

To automatically start a long running job once after the computer boots up
create and configure systemd service and timer units as shown above.

But use the following timer section in the timer unit instead of `OnCalendar`:

```ini
[Timer]
OnBootSec=10
```

This will start the timer 10 seconds after boot.

Documentation
-------------

Please refer to the manpages `man 5 systemd.timer` and `man 7 systemd.time` for details.
