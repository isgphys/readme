Personal Groups
===============

We use _personal groups_ to enable a clear-cut distinction between the user's personal files and those shared with others, for instance on a groupshare. In practice this means that any D-Phys user's primary group (gid) has the same name as the username, while the secondary groups reflect his permissions to access resources shared with other members of a group.

Unix permissions
----------------

Whenever you create a file in your home folder, or locally on your computer, it will belong to your user and personal group.

```
 # cd /scratch/somefolder
 # touch somefile
 # ls -l
-rw-r--r-- 1 username username  0 Jan  1 12:00 somefile
```

If you wish to share the folder with a group, you need to change the group-ownership

```
 # chown -R username:groupname /scratch/somefolder
 # ls -l
-rw-r--r-- 1 username groupname 0 Jan  1 12:00 somefile
```

Moreover you may want to make sure that any new file created in this folder will also belong to the group, instead of the user who created the file. This can be done be setting the _group sticky bit_ (SGID) recursively for the folder and contained subfolders

```
 # find /scratch/somefolder -type d -exec chmod -v g+s {} +
```

