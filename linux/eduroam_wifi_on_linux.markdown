Eduroam Wifi on Linux
=====================

In case your Linux computer does not automatically detect the settings of the eduroam wireless network at ETH, the following screenshot shows the required configuration.

[[!img /media/linux/eduroam_linux.png size="400x"]]
