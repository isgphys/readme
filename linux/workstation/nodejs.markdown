Node.js
=======

[nodejs](https://nodejs.org/) is a free, open-source, cross-platform JavaScript runtime environment that lets developers create servers, web apps, command line tools and scripts.

Installation
------------

You can install `node`, `npm` and `yarn` as user on any linux workstation.
Get the current stable [node version](https://nodejs.org/en) and install a pre-built release using `nodeenv`:

```sh
mkdir -p "/scratch/${USER}"
cd "/scratch/${USER}"
nodeenv --node=22.13.1 --prebuilt .nodeenv
source .nodeenv/bin/activate
npm install -g --no-package-lock --no-save yarn
```

You should now have a `nodeenv` environment ready:

```sh
user@host:/scratch/user$ source .nodeenv/bin/activate
user@host:/scratch/user$ node --version
v22.13.1
user@host:/scratch/user$ npm --version
10.9.2
user@host:/scratch/user$ yarn --version
1.22.22
```

Package installation
--------------------

Example to install the angular cli (`ng`):

```sh
user@host:/scratch/user$ npm install -g --no-package-lock --no-save @angular/cli
...
user@host:/scratch/user$ which ng
/scratch/user/.nodeenv/bin/ng
user@host:/scratch/user$ ng --version
19.1.4
```

This will install the [geese](https://www.youtube.com/shorts/xqnCUPUeCJg) (video/audio link).
