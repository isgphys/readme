# Some hints and notes regarding the Gnome desktop

## Gnome settings

There are two places to change Gnome's behavior:

`Settings` <img src="/media/linux/gnome-settings.png" alt="gnome settings" style="width: 40px; align=bottom;"> and `Tweaks` <img src="/media/linux/gnome-tweaks.png" alt="gnome tweaks" style="width: 40px;">.

In addition, the `Extensions` app offers a range of useful helpers. `Launch new instance` might be one to check out. Additional extensions can be found on [Gnome shell extensions](https://extensions.gnome.org/).

## Key bindings

Some of the Gnome key bindings we find most useful:

- `Windows key` + `l` -> screen lock
- `ALT` + `F2` -> launcher
- `CTRL` + `ALT` + `left/right arrow` -> switch work space
