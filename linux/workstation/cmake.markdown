cmake
=====

[CMake](https://cmake.org/): A Powerful Software Build System


System cmake version
--------------------

All Debian workstations will have the same default `cmake` version shipped by the operating system.
Use `cmake --version` to get the currently active version.


Other cmake versions
--------------------

We manage a central installation with a selection of `cmake` versions in our `/opt/software/` NFS mount
available on all managed Linux workstations.

In order to use it, source the corresponding environment variables depending on your required version (`X.Y.Z`):

```sh
source /opt/software/cmake/env/X[.Y[.Z]]
```

This will setup your environment variables to point to the specific `cmake` binary and tools.

To check the currently loaded `cmake` version you can use:

```sh
cmake --version
```

Version missing
---------------

We are ready to install other `cmake` versions on request. Please [[contact us|/services/contact]] in that case.
