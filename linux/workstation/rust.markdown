Rust
====

[Rust](https://www.rust-lang.org/) is a general-purpose programming language emphasizing performance, type safety, concurrency and enforces memory safety.


Cargo
-----

[Cargo](https://doc.rust-lang.org/cargo/) is the Rust package manager.
Cargo downloads your Rust package's dependencies, compiles your packages, makes distributable packages, etc.

Starting with Debian bookworm, we opt for a central installation of the `rustc` compiler and `cargo` package manager,
which we will keep up to date with the default stable rust toolchain.

Upon login on a managed Linux workstation, your environment variables will be setup automatically to include
the rust toolchain binaries located in `/opt/software/rust` in your `PATH` variable.

You can inspect the relevant environment variables using:

```bash
env | grep -E 'PATH|CARGO|RUST'
```

### Build or install

Just run `cargo build` or `cargo install <foo>`, which will use your local `~/.cargo` directory as target.

For research projects, group projects or due to disk space quota limits on home directories,
please redirect larger rust projects onto some other storage medium, such as `/scratch`.
You can do so using the `CARGO_HOME` environment variable, example:

```bash
export CARGO_HOME="/scratch/${USER}/.cargo"
```

### Clean cache

Please regularly clean your cache in `~/.cargo` using:

```bash
cargo cache -i           # print infos about cache directories
cargo cache -a           # autoclean cache directories
rm -r ~/.cargo/registry  # clean registry cache
```

<!-- TODO: unstable feature gc: https://doc.rust-lang.org/nightly/cargo/reference/unstable.html#gc -->

### Manual environment setup

To set up `/opt/software/rust` manually:

```bash
export RUSTUP_HOME=/opt/software/rust/rustup
export PATH="${PATH:+${PATH}:}/opt/software/rust/cargo/bin"
export PATH="${PATH:+${PATH}:}${HOME}/.cargo/bin"
```

### Versions and software

You find older versions using: `ls -la /opt/software/rust`.
If your required version is not available, please [[contact us|/services/contact]].

We also have the option to centrally pre-install other frequently used software or rust crates.


Rustup
------

Preferably, please use our central installation as described above,
but you may also install any `rust` or `cargo` version as user using `rustup`:

```bash
export RUSTUP_HOME="/scratch/${USER}/.rustup"
export CARGO_HOME="/scratch/${USER}/.cargo"
export RUSTUP_INIT_SKIP_PATH_CHECK=yes
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --no-modify-path --quiet -y
. "/scratch/${USER}/.cargo/env"
cargo --version
rustc --version
```
