User privileges on managed Linux workstations
=============================================

Like file access is controlled by [permissions](https://compenv.phys.ethz.ch/linux/basics_1/30_file_permissions/),
there are other actions on Linux such as `reboot`, which require even more elevated privileges.
These are usually only available to the `root` user or using `sudo` (ISG personnel only for security reasons).


Polkit
------

We deploy `polkit` rules (policies) to grant elevated privileges to users for certain actions.
In contrast to systems such as `sudo`, it does not grant root permission to an entire process,
but rather allows a finer level of control of centralized system policy.

Generally all actions require a local seat (physical login on the workstation) and do not work remotely,
unless explicitly mentioned below.

### Reboot

- Local: workstations may be rebooted by any locally logged in user (i.e. on your personal desktop computer).
- Remote: we can enable optional remote reboot (i.e. via SSH) for specific users/groups and workstations on request.

To reboot use the reboot buttons in the display manager, window manager or `systemctl reboot`.

### External disks

To manage external disks (USB storage) you can use the `gnome-disks` (Disks) app, which allows actions such as:

- mount
- unmount
- formatting
- partitioning
- read/write partition images

Some actions are also available from the cli using `udisksctl` (see `man 1 udisksctl`):

```sh
udisksctl mount -b /dev/sdb1
udisksctl unmount -b /dev/sdb1
```

To mount/unmount ISO images you can use the `nautilus` (Files) app or use `udisksctl`:

```
udisksctl loop-setup --file debian-11.7.0-amd64-netinst.iso
udisksctl loop-delete --block-device /dev/loop0
```

### Other actions

Please [[contact us|/services/contact]] should you require other elevated privileges.
Usually this is the case when you see an authentication prompt as root/admin.
