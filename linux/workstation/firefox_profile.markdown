Firefox profile
===============

Due to the switch from Ubuntu to Debian all Firefox profiles need to be migrated once. This is a manual process and ideally happens during the workstation re-installation. We will assist in the process. Read this page for instructions on how to migrate your profile in order to preserve your history and settings if you'd like to give it a go yourself.

By default only Firefox ESR (Extended Support Release) is installed now.
Previously (with Ubuntu) there were two versions of Firefox installed:

- **Firefox ESR**: Extended Support Release (Profile name: `default-esr*`)
- **Firefox RR**: Rapid Release (Profile name: `default-release`)

See also https://support.mozilla.org/en-US/kb/choosing-firefox-update-channel.

Firefox profile migration steps
-------------------------------

After your workstation is migrated to Debian 12 Bookworm,
follow the steps below to migrate your profile and get your browser history and settings back:

### 1. Generate your new profile directory by starting Firefox (ESR) once

You should be greeted by splash screen asking you to setup firefox.
You can close it immediately without doing any changes.
This should have created a profile at `~/.mozilla/firefox/XXXXXXXX.default-esr`.

### 2. Close Firefox (ESR)

### 3. Locate your old profile

Depending on if you were using Firefox RR or Firefox ESR you should have the following profile directories:

- `~/.mozilla/firefox-esr/XXXXXXXX.default-esrNNN`: Old Ubuntu Firefox ESR profile
- `~/.mozilla/firefox/XXXXXXXX.default-release`: Old Firefox RR profile
- `~/.mozilla/firefox/XXXXXXXX.default-esr`: New Debian Firefox ESR profile

Note that `XXXXXXXX` is a unique random string identifying your profile. `NNN` may be a numeric version number.
If you have both old profiles you need to decide which one to import. You may look at the timestamps if in doubt.

### 4. Copy the profile data to the new profile

Choose the right source profile directory as per the previous step, in this example `~/.mozilla/firefox-esr/XXXXXXXX.default-esrNNN` (Firefox ESR Ubuntu) and rsync the data over to the new Firefox ESR profile directory:

```bash
rsync -a --delete ~/.mozilla/firefox-esr/XXXXXXXX.default-esrNNN/ ~/.mozilla/firefox/XXXXXXXX.default-esr/
```

### 5. Start Firefox ESR

You should now have your profile back.

### 6. Downgrade profile (optional, only for Firefox RR)

If you see a message `You've launched an older version of firefox`, which is normal when migrating from Firefox RR,
then please follow these steps:

- Click `Quit`
- In a terminal start Firefox ESR once using the command:

```bash
firefox-esr --allow-downgrade
```

This will downgrade your profile to the version currently used by Firefox ESR.
After that is done once, you can start Firefox normally from now on.

### 7. Delete old profiles

You may delete your old profile directories now if they are no longer needed.
