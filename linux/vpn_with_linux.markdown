VPN with Linux
==============

The central Informatikdienste [recommend](https://unlimited.ethz.ch/display/itkb/VPN) to use the Cisco Secure Client (formerly known as Cisco AnyConnect), but you can also use the native, open-source tools.

### Using the command line

Install `openconnect`, and run the following with the right parameters:

```sh
openconnect -u ETHZNAME@student-net.ethz.ch --useragent=AnyConnect -g student-net sslvpn.ethz.ch
```

or

```sh
openconnect -u ETHZNAME@staff-net.ethz.ch --useragent=AnyConnect -g staff-net sslvpn.ethz.ch
```

### NetworkManager

Make sure the `network-manager-openconnect` and `network-manager-openconnect-gnome` packages are installed. Then configure the VPN via the [NetworkManager](https://wiki.gnome.org/Projects/NetworkManager) GUI (for instance by opening the GNOME Control Center with the `gnome-control-center` command).

The setup can be made in a few steps:

- Network, VPN, +, Multi-protocol VPN client (openconnect)
- Name: ETH Zurich, VPN Protocol: Cisco AnyConnect or openconnect, Gateway: sslvpn.ethz.ch, User Agent: AnyConnect, Add
- Then turn on the new VPN connection: GROUP: staff-net (or student-net if you're a student..), Username: ethzuserid@realm (for example johndoe@staff-net.ethz.ch, Password: ETH VPN (you can check your password at https://sslvpn.ethz.ch/), Login

[[!img /media/linux/openconnect-linux.png size="400x"]]

### Starting the client

Start or stop the VPN connection from the NetworkManager.
