General Overview over Linux at the Department of Physics
========================================================

Distributions Used
------------------

We are using Debian and Debian-based distributions.  The servers tend to have the stable release of Debian while the desktops have mostly a recent LTS release of Ubuntu.

Numbers of Systems
------------------

* ca. 80 servers (very diverse, including virtual Xen-DomU)
* ca. 120 desktop workstations (of which ~50 are CUDA enabled)
* ca. 80 headless workstations (for simulations)
* ca. 20 raspberry pi machines (for display, kiosk, and RDP)

Deployment and Management
-------------------------

We use native deploy and management mechanisms supported in the official Debian as much as possible.  Our scripts provision the configuration files and settings needed to use the automation infrastructure in Debian.  As far as deployment and management is concerned Ubuntu is fully compatible with Debian.

Some of the ingredients:

* TFTP and iPXE netboot using the Debian and Ubuntu kernel/initrd without local modifications
* [preseeding](http://d-i.debian.org/manual/en.i386/apb.html) to support unattended installations
* the `preseed/late_command` parameter triggers configuration and package installation on first boot
* [aptitude-robot](https://github.com/elmar/aptitude-robot) for package management (local development newly contributed to Debian)
* `aptitude-robot`, and [ansible](https://github.com/ansible/ansible) are used for initial setup as well as regular maintenance

Use Cases
---------

* server infrastructure
    * file server: SAN front and back ends (via iSCSI)
    * print, web, mail, LDAP, DHCP, TFTP, etc.
    * Xen and KVM virtualization
* main workstation for scientists
* development for numerical simulations
* number crunchers
* console terminals

Motivation for the Choice of Distribution
-----------------------------------------

Note: at the time of the decision (spring/summer 2000) our know-how was on the Red Hat side.

Debian (in 2000):

* high-quality technical support by community
* continuous upgrades through several releases were very well supported
* fine-grained choice of packages easily possible

Ubuntu (from about 2009):

* first choice among our users on their own laptops; Ubuntu have established themselves as a standard
* technically Debian-compatible (easy to support)
* good choice of desktop software packages

Student Workstations and Exams
------------------------------

* one student cluster (~10 computers) mostly for the convenience of the students (mail, web, Mathematica/Maple/Matlab, etc.)
* no exams (that we know of)

Strengths and Weaknesses of the Distribution
--------------------------------------------

Debian strengths:

* fine-grained package management with good package/version dependency system supported by policy
* upgrades (without re-installations) go smooth over several generations of releases
* responsive community

Debian weaknesses:

* a newcomer is quickly overwhelmed by the size and needs help beyond the first few steps
* no support contract from Debian available: some effort on our own part is expected when dealing with the community

Ubuntu strengths:

* new software readily available with 6 months release cycle
* well curated collection of packages for the typical desktop user
* works nicely for newcomers; people often use Ubuntu as their first choice distribution on the personal laptop
* commercial support available ([Canonical](http://www.canonical.com/))

Ubuntu weaknesses:

* default management tools assume the user can and should reconfigure the computer; this often interferes with automated management; the one-user-per-computer assumption is built in with Ubuntu and requires a lot of effort to be worked around
* quick release cycle means sub-standard quality of some packages (for different packages in each release; most packages are quite OK and the others get fixed within a release or two)
* sponsoring (currently Amazon) means that in the default installation some user information is potentially leaked to external parties; careful configuration work needed to catch the relevant parameters

Note: Our (limited) experience with Ubuntu commercial support was not very good.  We hear it has gotten better lately.
