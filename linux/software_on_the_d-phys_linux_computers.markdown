Software on the D-PHYS Linux Computers
======================================

Here is a list of some software that is available on the D-PHYS Linux workstations.  It is by far incomplete and we install more packages as the need arises.

Currently we run Debian 12, migrated from [Ubuntu 22.04](https://isg.phys.ethz.ch/2024/07/24/linux-workstations-debian/).

Manual Pages, Documentation
---------------------------

* `man man` or [online](https://dashdash.io/)
* `help` (depends on `$SHELL`)
* `whatis command`
* `tldr command`
* `dict wordyoudontunderstand`
* Maybe there's something in `/usr/share/doc`?

Notes on the system configuration
---------------------------------

We also mostly have and use [zRAM](https://en.wikipedia.org/wiki/Zram), `swapon` gives you an overview about it, as well as `cat /sys/block/zram*/comp_algorithm` on the used compression. Generally it's lzo-rle since 20.04.

For a long time we used to have XFS for /scratch* file systems, around 2015, we switched to [btrfs](https://www.kernel.org/doc/html/latest/filesystems/btrfs.html) and thus can support live file system
compression. (There was a time when btrfs did not support swap files). It's [ext4](https://en.wikipedia.org/wiki/Ext4) as of 2024.

[exfat](https://github.com/exfatprogs/exfatprogs) support has been switched to the Samsung implementation for 20.04.

If you want to squeeze out some more, we also have [mimalloc](https://github.com/microsoft/mimalloc)

Details to [mounting](https://readme.phys.ethz.ch/linux/workstation/user_privileges/) disks.

Text Processing and Editors
---------------------------

* [LibreOffice](https://www.libreoffice.org/): a complete office suite with `lowriter` as the text processing tool.  It has its own file formats but can read and write MS Word documents, ASCII texts, and more.
* [Emacs](https://www.gnu.org/software/emacs/): a variant of the classic Emacs extensible editor.  Perfect for text files, program development, and a lot more.
* vi: a classic among Unix editors, comes as `vim`, `nvi`
* mcedit: for fans of Norton Commander
* TeX, LaTeX: professional document processing tool, a classic [Introduction](https://tobi.oetiker.ch/lshort/lshort.pdf)
* Microsoft Office: can be accessed with RDP, `xfreerdp /f /bpp:24 /d:ad /v:winlogin.phys.ethz.ch /u:$USER +fonts` or `wints.igp.ethz.ch`
* [Visual Studio Code](https://code.visualstudio.com/)
* PyCharm

GNOME
-----

If you still get shown old icons, you can use `gnome-tweak`
and select "Yaru" in Appearance.

Alternatively use these commands:

```
gsettings set org.gnome.desktop.interface gtk-theme 'Yaru'
gsettings set org.gnome.desktop.interface icon-theme 'Yaru'
gsettings set org.gnome.desktop.interface cursor-theme 'Yaru'
```

What is useful too is these commands:
`gnome-extensions list` as well as `gsettings list-recursively`.

If you want fractional scaling, run this once:

```
gsettings set org.gnome.mutter experimental-features "['scale-monitor-framebuffer']"
```

Compilers & Interpreters
------------------------

### C, Objective-C and C++

* `gcc` and `g++` from the [GNU Compiler Collection](http://gcc.gnu.org/)
* `clang` [LLVM/clang](http://clang.llvm.org/) (dlang to follow soon)
* `icc` the [Intel C/C++ Compiler](http://software.intel.com/en-us/articles/intel-compilers/)
* `tcc` the [tiny C compiler/interpreter](https://bellard.org/tcc/)
* `musl-gcc` the [musl C library with compiler](https://musl.libc.org/)
* `aocc` the [AMD Optimizing C/C++ Compiler](https://developer.amd.com/amd-aocc/) where applicable

### NVIDIA CUDA Compiler

* where installed, available with `/usr/local/cuda/bin/nvcc` (cudnn is also included)

You will find other CUDA versions according our [documentation](https://readme.phys.ethz.ch/linux/workstation/nvidia_cuda/)

### Fortran

* `gfortran` (Fortran 95) from the [GNU Compiler Collection](http://gcc.gnu.org/)
* `ifort` the [Intel Fortran Compiler](http://software.intel.com/en-us/articles/intel-compilers/) on request

### Julia

* `julia` high-performance programming language for technical computing [Julia](https://julialang.org/)

Local installation as user:

```sh
pip install jill
export PATH=~/.local/bin:$PATH
jill list
jill install --install_dir /scratch/julia-lang
julia
```

If it already exists in `/scratch/julia-lang`:

```sh
ln -s /scratch/julia-lang/julia-<version>/bin/julia .local/bin/jilia
export PATH=~/.local/bin:$PATH
```

### Java

Java software usually don't need an installation. They can be just
downloaded to your `$HOME` or `/scratch/directory` and unpacked, and
run with `java -jar the.jar`

### Python

See [[linux/workstation/python]] for details on how to load our pre-installed Python packages.

Libraries
---------

There are just too many libraries and too diverse needs to provide a useful overview.  Most libraries are part of the system (e.g. OpenGL is part of the X Window system) and as such compiled with the default compiler (i.e. currently, for Debian 12/bookworm GCC version 12.2 in most cases). See the last section to check the state of specific libraries.

Mathematical Tools
------------------

* [Mathematica](http://www.wolfram.com/)
* [Maple](http://www.maplesoft.com/) (to run with GUI run `maple -x`)
* [Matlab](http://www.mathworks.com/) [font too small](https://ch.mathworks.com/matlabcentral/answers/406956-does-matlab-support-high-dpi-screens-on-linux)
* [Octave](https://www.gnu.org/software/octave/) drop in compatible with Matlab scripts
* Comsol needs to be bought
* CST needs to be bought
* [Lumerical/FDTD](https://www.lumerical.com) needs be bought
* [R Studio](https://rstudio.com/) on request, from 20.04 on
* [SageMath](https://www.sagemath.org) on request

Plotting Software
-----------------

* [IDL](https://www.l3harrisgeospatial.com/Software-Technology/IDL) [Online Tutorial](http://www.bu.edu/tech/support/research/training-consulting/online-tutorials/idl/) with [Astrolib](https://github.com/wlandsman/IDLAstro/)
* [GDL](http://gnudatalanguage.sourceforge.net/) (free alternative to IDL)
* [gnuplot](http://www.gnuplot.info/)
* [pyxplot](http://www.pyxplot.org.uk/)
* [supermongo](http://www.astro.princeton.edu/~rhl/sm/)
* [gmt](http://www.soest.hawaii.edu/gmt/)
* [graphviz](https://graphviz.org/)

Graphics Tools
--------------

* [The GIMP](https://gimp.org/tutorials/): tool to create and manipulate pixel oriented graphics files (e.g., JPEG, PNG, PPM, etc.), useful for working with photos, web graphics and more.  Similar uses as Adobe Photoshop
* [inkscape](https://inkscape.org/): Vector drawing software.
* [Scribus](https://www.scribus.net/): WYSIWYG desktop publishing
* xmgr, grace: an XY plotting tool
* xfig: drawing tool, powerful, although with a slightly unusual user interface (only on request)
* [Blender](https://www.blender.org/): 3d/VFX software, use `ctrl-alt-u` to set `/tmp/` to be `/scratch/user/tmp`.
* [QVGE](https://github.com/ArsMasiuk/qvge): Visual Graph Editor

Video Editing
-------------

* [shotcut](https://shotcut.org/)
* [ETH Zurich video templates](https://ethz.ch/services/en/service/communication/video.html)

Scientific Software
-------------------

* [amide](http://amide.sourceforge.net/)
* [cadabra](https://cadabra.science)
* [colmap](https://github.com/colmap/colmap)
* [cloudcompare](http://www.danielgm.net/cc/)
* [cuba](http://www.feynarts.de/cuba/) on request
* [ds9](http://ds9.si.edu/site/Home.html)
* [form](http://www.nikhef.nl/~form/)
* [ffcv](https://ffcv.io) on request
* [geant4](https://geant4.web.cern.ch)
* [groops](https://github.com/groops-devs/groops)
* [hdfview](https://github.com/HDFGroup/hdfview)
* [lie](http://young.sp2mi.univ-poitiers.fr/~marc/LiE/)
* [largetifftools](https://www.imnc.in2p3.fr/pagesperso/deroulers/software/largetifftools/)
* [magma](http://magma.maths.usyd.edu.au/magma/) only on toad
* [macaulay2](http://www2.macaulay2.com/Macaulay2/) on request
* [meep](https://meep.readthedocs.io/en/latest/)
* [mepp2](https://github.com/MEPP-team/MEPP2) on request
* [meshlab](http://www.meshlab.net/)
* [mm3d](https://github.com/zturtleman/mm3d) (import special 3d formats that cloudcompare fails with)
* [nusolve](https://space-geodesy.nasa.gov/techniques/tools/nuSolve/nuSolve.html) on request
* [petitradtrans](https://gitlab.com/mauricemolli/petitRADTRANS/) on request
* [pytorch](https://pytorch.org) on request
* [ROOT](https://root.cern/) (run `source /opt/root/bin/thisroot.sh` to set the correct environment)
* [rtklib](http://www.rtklib.com/)
* [snap](http://step.esa.int/main/download/snap-download/) you can install this one yourself
* [source-extractor](https://github.com/astromatic/sextractor)
* [stellarium](http://www.stellarium.org/)
* [splash](http://users.monash.edu.au/~dprice/splash/)
* [swarp](http://www.astromatic.net/software/swarp)
* [singular](https://www.singular.uni-kl.de/)
* [qgis](https://qgis.org/en/site/)
* [udunits](https://www.unidata.ucar.edu/software/udunits/) on request
* [visit](https://wci.llnl.gov/simulation/computer-codes/visit/)
* [zfp](https://computing.llnl.gov/projects/zfp) on request

Data sources and software
-------------------------

* [google-earth-pro](https://www.google.com/earth/versions/)

Note on usage: some software like Google Earth Pro, just don't get caching files right, you have a remote filesystem $HOME, so run it with `$HOME=/scratch/youruser` to not get into quota problems, and run fast.

Software useful with special hardware
-------------------------------------

* [callisto](http://www.e-callisto.org)
* [drs4eb](https://www.psi.ch/en/drs/software-download)
* [wds](https://bitbucket.org/twavedaq/wavedaq_sw/src/develop/wds)

Remote access
-------------

* [openssh](https://www.openssh.com) This is standard
* [xrdp](https://github.com/neutrinolabs/xorgxrdp) (requires VPN from public internet) on request

Data transfer
-------------

* [rclone](https://github.com/rclone/rclone)
* [rclone-browser](https://github.com/kapitainsky/RcloneBrowser)

Container
---------

Since [Docker](https://www.docker.com/) is a security problem on managed computers, we recommend to use [[documentation/podman]] instead.

Finding Software Packages
-------------------------

You can list all installed packages with `dpkg --list`

If you want to search the whole Debian software archive you can use `apt-cache`, e.g. like this: `apt-cache search emacs`
Alternatively you can use the web interface of [Debian](https://packages.debian.org/)

This command will also find packages which are not installed on our system.  You can check whether a specific package is installed by specifying the package name like such: `dpkg -l emacs`

To list the content of a package use `dpkg -L package`

Another source of software is https://github.com/


Resource Management
-------------------

We have a lot of tools, please use them.

* `htop` - shows you number of CPUs/cores and memory/swap usage
* `ruptime/rload` - shows you hosts and their CPU/MEM/GPU/GPUMEM usage in %, [Homepage](https://github.com/alexmyczko/ruptime)
* `timeout`
* `scr` - [Homepage](https://computing.llnl.gov/projects/scalable-checkpoint-restart-for-mpi)

CUDA enabled machines also have

* `nvtop` - shows you number of GPUs and GPU memory usage

You might want to setup these environment variables:

* `export CUDA_HOME=/usr/local/cuda`
* `export PATH=$PATH:/usr/local/cuda/bin/` (for nvcc)

You might want to setup the directories versioned, if you need a specific version of CUDA/nvcc

PF machines have

* `rload | grep pf-pc`

The guenther cluster also has

* `rload | grep guenther`
* `pestat`
* [SLURM](https://scicomp.ethz.ch/wiki/Using_the_batch_system) batch queuing system

You can use `nice` to run jobs reniced, or `cpulimit -l 50 -- yourjob` to limit cpu usage on your job to 50 %.

Debugging
---------

`strace yourcommand` (-c to count calls)

`ltrace yourcommand`

are often useful to trace system/library calls of some
software.

`bash -x bashscript`

`-x` with bash shows you all lines executing the script
which often is helpful too.

Hardware
--------

* `cat /etc/serial` - shows you the serial number of the computer, if there is LETTER-XX.YY.ZZ, then XX is the year of delivery and YY the month.

Missing Software?
-----------------

If you're missing software, don't hesitate to contact us,
with the following details:

* Computer hostname you work on
* URL of the homepage of the software
* The version of the software you would like to have
