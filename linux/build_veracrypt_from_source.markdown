# Instructions for Building VeraCrypt for Linux

These build instructions use wxWidgets static library for VeraCrypt.

* Download `wxWidgets`:

```sh
mkdir /scratch/git
cd /scratch/git
wget https://github.com/wxWidgets/wxWidgets/releases/download/v3.0.2/wxWidgets-3.0.2.tar.bz2
tar -xjf wxWidgets-3.0.2.tar.bz2
mv wxWidgets-3.0.2 wxWidgets
rm wxWidgets-3.0.2.tar.bz2
```

* Get the VeraCrypt sources:

```sh
git clone git@github.com:veracrypt/VeraCrypt.git veracrypt
```

Make sure the [RSA fingerprint](https://help.github.com/articles/github-s-ssh-key-fingerprints/) is correct:

```sh
SHA256:nThbg6kXUpJWGl7E1IGOCspRomTxdCARLviKw6E5SY8
```

* Configure the wxWidgets static library for VeraCrypt and build it:

```sh
cd veracrypt/src
make clean
make WXSTATIC=1 WX_ROOT=/scratch/git/wxWidgets wxbuild
```

* Build `veracrypt`:

```sh
make WXSTATIC=1
```

* If successful, the `veracrypt` binary should be located in the directory `Main`.

You can now copy the binary to your preferred location, like `~/bin/`:

```sh
cp Main/veracrypt ~/bin/
```

By default, a universal executable supporting both graphical and text user interface (through the switch --text) is built.

On Linux, a console-only executable, which requires no GUI library, can be built using the 'NOGUI' parameter:

```sh
make NOGUI=1 WXSTATIC=1 WX_ROOT=/usr/src/wxWidgets wxbuild
make NOGUI=1 WXSTATIC=1
```
