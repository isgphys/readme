Configuration hints for self-managed Linux Machines
===================================================

Sending Hostname for Dynamic DNS
--------------------------------

If you want your Linux host accessible via dynamic DNS at `<yourhost>.dhcp[-int].phys.ethz.ch`, make sure that `send host-name` is used in `/etc/dhcp/dhclient.conf`.

For more recent versions of ISC's dhclient, the following line in `/etc/dhcp/dhclient.conf` should suffice:

```
send host-name = gethostname();
```

Older versions may need an explicit name there. Please use a name without domain, e.g.

```
send host-name "johns-laptop";
```

If nobody else uses that name, you remotely can access your computer by using e.g. `johns-laptop.dhcp[-int].phys.ethz.ch` (continuing the example from above).
