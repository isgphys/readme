How to create partitions over 2TB on Linux
==========================================

NOTE: This used to be a problem many years ago. Meanwhile it's not one anymore.

### Problems

* fdisk and cfdisk (from the util-linux package in Debian) can't do it. They have overflows at 2TB.
    * fdisk refuses to do it
    * cfdisk claims to do it, but the overflow is visible after a restart of cfdisk
    * Citation: [fdisk doesn't understand GUID Partition Table (GPT) and it is not designed for large partitions. In particular case use more advanced GNU parted(8).](http://linux.die.net/man/8/fdisk)

### Solutions

Install parted

### How-To

#### Short

Use parted and GUID Partition Tables (GPT)

#### Step by step example

```
 # parted /dev/sdb
(parted) mklabel GPT
(parted) mkpart primary ext3 0 100%
(parted) name 1 vg01
(parted) set 1 lvm on
(parted) print

Disk /dev/sdb: 16.5TB
Sector size (logical/physical): 512B/512B
Partition Table: gpt

Number  Start   End     Size    File system  Name  Flags
 1      17.4kB  16.5TB  16.5TB               vg01  lvm

(parted) quit
```

