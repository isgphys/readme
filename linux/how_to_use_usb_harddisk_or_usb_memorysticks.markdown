How to use USB Harddisk or USB Memorysticks
===========================================

On our managed D-PHYS Linux workstations you can just plug in an USB harddisk or memory stick and the system will mount the filesystem automatically.  If you pull it out again it will be automatically unmounted.  If you wrote something on the filesystem please make sure to allow the system to write all data to the device before pulling it out.  I.e., you should either wait 30 seconds or force synchronization with the command `sync`
If you only read from the device there is no need to synchronize before removal of the device.

The device will be mounted somewhere in `/media`
