Using your CD Writer under Linux
================================

First, you need a compatible CD burner. Linux works with SCSI CD-burners, IDE CD-burners (through ide-scsi emulation), and USB CD burners. In all cases, the device appears as a SCSI device. If you can see your device with the command 'cdrecord -scanbus', then you are ready to burn. If so, skip to section 2.

##### Section 1: Configuring your CD writer

Often, people have a IDE cd-writer, but don't have the ide-scsi emulation turned on. This is best done by editing the /etc/lilo.conf on your computer, and adding an entry to your current boot option:

```
image=/boot/vmlinuz-STD-current
  label=linux
  read-only
  root=/dev/device
  append="hdb=ide-scsi"
```

(NOTE: this is just an example, your root partition and CD-writer device might be different. If you have any problems identifying or configuring your CD-writer, contact the SCS Helpdesk).
Once you have configured lilo, re-run /sbin/lilo to install the new bootloader, and reboot.

##### Section 2: Using cdrecord

The output of `cdrecord -scanbus` will look something like this:

```
 # cdrecord -scanbus
Cdrecord 1.9 (i686-pc-linux-gnu) Copyright (C) 1995-2000 Jörg Schilling
Linux sg driver version: 3.1.22
Using libscg version 'schily-0.1'
scsibus0:
        0,0,0     0) 'TOSHIBA ' 'DVD-ROM SD-R1202' '1020' Removable CD-ROM
        0,1,0     1) *
        0,2,0     2) *
        0,3,0     3) *
        0,4,0     4) *
        0,5,0     5)
        0,6,0     6)
        0,7,0     7)
```

The CD writer is the device labeled '0,0,0' in this example. You can now use cdrecord to burn an audio or data CD, using that device number. Here's an example of how to burn an ISO image to a cd, using cdrecord:

```
 # cdrecord -v speed=2 dev=0,0,0 -data cdimage.iso
```

Read the man page for `cdrecord` for more options and examples.

##### Section 3: Making ISO images

You may want to generate a CD image containing data. You can do this with the command `mkisofs`.

```
mkisofs  -r   -o  cdimage.iso  private_collection/
              `--------------' `-----------------'
                       |               |
             write output to   take directory as input
```

You can then use `cdrecord` to write cdimage.iso to a CD.

More information The [CD-Writing How-to](http://www.ibiblio.org/pub/Linux/docs/HOWTO/CD-Writing-HOWTO) is a good place to find more information about writing CDs under linux.

##### Section 4: Reading a CD image

You may already have a CD of which you want to take the CD image.  You do this in two steps.  First you determine the block and volume sizes with

```
isoinfo -d -i /dev/cdrom
```

The _Logical block size_ is usually 2048 and the _Volume size_ depends on how much data is on the CD.  Second you should enter these sizes in the command to read the CD

```
dd if=/dev/cdrom of=cdimage.iso bs=2048 count=<volume-size>
```

##### Section 5: Tips and Tricks

If you want to make sure that your CDs can be read on all possible CD drives it is recommended that you **pad** your CDs.  This goes for making the CD with `mkisofs` and burning them with `cdrecord`.  Just add the option `-pad` to both commands.  It will take up a bit more space on the CD but then any CD drive should read the very same bytes.

You can find out a lot of information about the CD drive and the media you are burning on by issuing the command`

```
cdrecord dev=0,0,0 -atip
```

(of course replace the value for `dev=` as explained above).  If you experience problems with the quality and durability of your burned CDs, the most important information is given by _Manufacturer_.
