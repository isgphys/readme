Setting up an LTSP server for diskless clients
==============================================

Installing LTSP on Ubuntu 16.04 with Debian chroots for 32-bit clients
--------------------------------------------------------------------

`dphys-config-ltsp` has to be run to set up the Debian changeroot
for the LTSP clients to get (the client needs an X server, rdesktop/xfree-rdp,
xymon-client, and ntp). Ubuntu has removed PAE thus doesn't run on
32-bit hardware anymore.

### Kiosk Mode

We use Kiosk Mode, a local user for info displays, web shop orders,
and new account sheets.

### IPv4 vs IPv6

Make sure tftpd and nbd are running with IPv4, /etc/default/tftpd-hpa TFTP_ADDRESS="0.0.0.0:69", sysctl net.ipv6.conf.eno1.disable_ipv6=1 and restarting nbd-server might help.

New install on Ubuntu LTS (applies to 11.04, 12.04, 14.04)
----------------------------------------------------------

To create a basic load balancing, there is this documentation http://ubuntuforums.org/showthread.php?t=935416

Watch out for this one, you might be affected too: http://ubuntuforums.org/showthread.php?t=2222278

Regular workstation installation. Install `ltsp-server`, `ldm-server` and `ltspfs`. On the DHCP server:

```
 # -----------
 # LTSP Server
 # -----------
group {
  next-server 192.33.99.213;
  filename "/ltsp/i386/pxelinux.0";
  include "/etc/dhcp3/clients-diskless-test";
}
```

On the Terminal Server build the chroot by issuing `ltsp-build-client --arch=i386` and add

```
/opt/ltsp/i386  @many(ro,no_root_squash,async)
/var/lib/tftpboot/ltsp/i386     @bootservers(ro,async,crossmnt,no_subtree_check)
/boot   @bootservers(ro,async,crossmnt,no_subtree_check)
```

to `/etc/exports` and run `invoke-rc.d nfs-kernel-server reload`.

We also need a tftp server `aptitude install tftpd-hpa`.

In `/pub/debian-local/configs/isg/` create an `lts.conf` containing

```
[Default]
        LOCALDEV = True
        SOUND = True
        LDM_DIRECTX = True
        X_COLOR_DEPTH = 24
        SCREEN_07 = ldm

 #kiosk terminals
[kiosk]
        LDM_AUTOLOGIN = true
        LDM_USERNAME = kiosk
        LDM_PASSWORD = dsjfgasdjf

 #Windows Terminal Server client
[wts_en]
        SCREEN_08 = rdesktop
        RDP_SERVER = winlogin.phys.ethz.ch
        RDP_OPTIONS     = "-a 32 -x l -z -P -r sound:local -k en-us -r disk:stick=/var/run/drives"
[wts_de]
        SCREEN_08 = rdesktop
        RDP_SERVER = winlogin.phys.ethz.ch
        RDP_OPTIONS     = "-a 32 -x l -z -P -r sound:local -k de-ch -r disk:stick=/var/run/drives"
```

and `dphys-config.list.host` with

```
lts.conf:/var/lib/tftpboot/ltsp/i386/
mathematica-fonts-Type1.tar.gz:/opt/ltsp/i386/usr/share/fonts/X11/Type1/:tar xzf {}
ldm-theme-dphys.tar.gz:/opt/ltsp/i386/usr/share/ldm/themes/:tar xzf {}
logo.png:/opt/ltsp/i386/usr/share/ldm/themes/DPHYS/:
```

For the kiosk user (don't forget pasckiosk and bibkiosk), add

```
kiosk:x:128:65534::/var/kiosk:/bin/bash
```

to `/etc/passwd`,

```
kiosk:$1$SUPERHASH:14734::::::
```

to `/etc/shadow` and copy kiosk's `/var/kiosk/`.

Now run dphys-config and create the NBD boot image: `ltsp-update-image -a i386` (remember to run this command every time you've changed something in the chroot).


Old install on etch/lenny
-------------------------

* [[http://wiki.debian.org/LTSP/Howto]]

### LTSP server root fs

enable alioth LTSP backports:

```
wget http://pkg-ltsp.alioth.debian.org/debian/pkg-ltsp-keyring
apt-key add pkg-ltsp-keyring
```

add

```
deb http://pkg-ltsp.alioth.debian.org/debian etch-ltsp-backports main
```

to `/etc/apt/sources.list`

```
aptitude install ltsp-server ltspfs
ltsp-build-client --arch i386 --dist lenny --mirror http://debian.ethz.ch/debian/ --apt-keys /etc/apt/trusted.gpg  --extra-mirror "http://pkg-ltsp.alioth.debian.org/debian etch-ltsp-backports main"
```

`/etc/exports`:

```
/opt/ltsp/i386       (ro,no_root_squash,async)
/var/lib/tftpboot/ltsp/i386     @bootservers(ro,async,crossmnt,no_subtree_check)
```

`/etc/inetd.conf`:

```
tftp           dgram   udp     wait    root  /usr/sbin/in.tftpd /usr/sbin/in.tftpd -s /var/lib/tftpboot
```

sound support:

```
aptitude install libasound2-plugins
```

`/etc/asound.conf`:


```
pcm.!default {
  type pulse
}
ctl.!default {
  type pulse
}
```

configure client sound output to alsa.

DHCP config `/etc/dhcp3/dhcpd.conf`:

```
group {
    next-server "LTSP-SERVER-IP";
    filename "/ltsp/i386/pxelinux.0";
    include "/etc/dhcp3/clients-diskless-test";
}
```

PXE config `/var/lib/tftpboot/ltsp/i386/pxelinux.cfg/default`:

```
DEFAULT vmlinuz ro initrd=initrd.img root=/dev/nfs ip=dhcp nfsroot=LTSP-SERVER-IP:/opt/ltsp/i386/ vga=794 boot=nfs
```

for local USB devices and VirtualBox `/etc/rc.local`

```
chmod 666 /dev/vboxdrv
chmod 666 /dev/fuse
```

#### client file system: (chroot /opt/ltsp/i386/)

```
passwd root
```

copy amd xorg driver to `/usr/lib/xorg/modules/drivers/amd_drv.so`

`/etc/lts.conf`:

```
 #true for all
[Default]
        LOCALDEV = True
        SOUND = True
        LDM_DIRECTX = False #as long as the X auth issue has not been solved
        X_COLOR_DEPTH = 16
        X_MODE_0 = 1280x1024

 #hardware dependent stuff
[Alix]
        XSERVER = amd

 #monitor specific stuff
[1600]
        X_MODE_0 = 1600x1200

[1280]
        X_MODE_0 = 1280x1024

[1024]
        X_MODE_0 = 1024x768

 #kiosk terminals
[kiosk]
        LDM_AUTOLOGIN = true
        LDM_USERNAME = kiosk
        LDM_PASSWORD = dsjfasdjf

 #actual hosts

[00:0d:b9:0c:ef:c0]
 #HPK F ? Ralph Eichler
        LIKE = Alix
        LIKE = 1280
        HOST_NAME = malta
```

changes to `/usr/share/ltsp/configure-x.sh`  to enable mathematica fonts and screen rotation:

```sh
handle_mathematica_fonts(){
    perl -pi -e 's#dirs\/TrueType"#dirs/TrueType"\n FontPath        "/usr/share/fonts/mathematica/Type1"\nFontPath        "/usr/share/fonts/mathematica/BDF"#' $INPUT_FILE
}

handle_rotate(){
    if [ -n "$X_ROTATE_SCREEN" ];then
            perl -pi -e 's#BoardName#Option         "Rotate" "left"\nBoardName#' $INPUT_FILE
    fi
}

handle_dpms(){
    perl -pi -e 's#Section "Monitor"#Section "Monitor"\n\tOption         "DPMS"#' $INPUT_FILE
    perl -pi -e 's#Section "Screen"#Section "Screen"\n\tOption         "DPMS" "true"#' $INPUT_FILE
}
```

and

```
handle_mathematica_fonts || true
handle_rotate || true
handle_dpms || true
```

adds mathematica font paths and allows us to define `X_ROTATE_SCREEN = True` to use portrait screens

for rdesktop (Windows terminal server) screens on :8 we need to fix the login windows reset problem in Windows Server 2008. Our workaround: extra wrapper script to start rdesktop. In `/usr/share/ltsp/screen.d/`, create `rdw` containing

```sh
 #!/bin/sh
 #
 # remote desktop wrapper script
 # written to supersede the original LTSP rdesktop script in order to solve the Windows 2008 Server logon screen timeout issue
 # (c) 2008 daduke <daduke@daduke.org>
 #
 # This software is licensed under the Gnu General Public License.
 # The full text of which can be found at http://www.LTSP.org/license.txt
 #
 #
 #
 # /opt/ltsp/i386/etc/screen.d/rdesktop
 #
 # Call this through /opt/ltsp/i386/etc/lts.conf like this:
 #           RDP_OPTIONS  = "-a 16"
 #           RDP_SERVER   = w2k3.terminal.server.ip
 #           SCREEN_02    = rdesktop
 #

PATH=/bin:$PATH; export PATH
. /usr/share/ltsp/screen-x-common

RDP_OPTIONS=${RDP_OPTIONS:-"-f"}

RDESKTOP_OPTIONS="${RDP_OPTIONS} -f $* ${RDP_SERVER}"

while :; do
        /usr/bin/rdesktop           \
                  -u "" \
                ${RDESKTOP_OPTIONS}
done
```

and `chmod a+x` it. Modify `/usr/share/ltsp/screen.d/rdesktop` to read

```sh
 #!/bin/sh
 #
 # The following script works for LTSP5 and was tested in Debian etch.
 # Modified 2008 by daduke <daduke@daduke.org>
 # Original rdesktop script by James A. McQuillan,
 # modified for LTSP5 by Colin Osterhout of SERRC (www.serrc.org).
 #
 # This software is licensed under the Gnu General Public License.
 # The full text of which can be found at http://www.LTSP.org/license.txt
 #
 #
 #
 # /opt/ltsp/i386/etc/screen.d/rdesktop
 #
 # Call this through /opt/ltsp/i386/etc/lts.conf like this:
 #           RDP_OPTIONS  = "-a 16"
 #           RDP_SERVER   = w2k3.terminal.server.ip
 #           SCREEN_02    = rdesktop
 #

PATH=/bin:$PATH; export PATH
. /usr/share/ltsp/screen-x-common

/usr/bin/xinit      /usr/share/ltsp/screen.d/rdw \
        -- \
        ${DISP}                     \
        vt${TTY}
```

and add

```
SCREEN_08 = rdesktop
RDP_SERVER = winlogin.phys.ethz.ch
RDP_OPTIONS     = "-x l -r sound:local -r disk:usbstick=/var/run/drives"
```

to `lts.conf`.

    lts.conf options:
                        *****Switch on/off features (and dependant optional options)*****

    |   Variable    |   Possible Values     |   Default value  |       Comment
    ---------------------------------------------------------------------------------------------
    SOUND               Y,y,True,true            unset
     (SOUND_DAEMON)     esd , nasd               esd
    ---------------------------------------------------------------------------------------------
    LOCALDEV            Y,y,True,true            unset
    ---------------------------------------------------------------------------------------------
    DNS_SERVER          A valid IP               unset
     (SEARCH_DOMAIN)    Domainname               unset           required if DNS_SERVER is set
    ---------------------------------------------------------------------------------------------
    CONSOLE_KEYMAP      A valid console keymap   en
    ---------------------------------------------------------------------------------------------
    NBD_SWAP            Y,y,True,true            unset
     (SWAP_SERVER)      nbd server IP            ltsp server
     (NBD_PORT)         A valid nbd portnumber   9572
    ---------------------------------------------------------------------------------------------
    USE_LOCAL_SWAP      Y,y,True,true            unset
    ---------------------------------------------------------------------------------------------
    PRINTER_0_DEVICE    dev (i.e. /dev/usblp0)   unset
     (PRINTER_0_TYPE)   P(parallel), S(serial)   unset           only required for serial,
                        U(USB)                                   others are autodetected
     (PRINTER_0_PORT)   A valid portnumber       9100
     (PRINTER_0_WRITE_ONLY)     Y,N              N
     (PRINTER_0_SPEED)  A baud rate              9600            serial only
     (PRINTER_0_FLOWCTRL) flow control param     S               serial only
     (PRINTER_0_PARITY) Parity setting           N               serial only
     (PRINTER_0_DATABITS) Bitnumber              8               serial only
     (PRINTER_0_OPTIONS) additional options      unset
                         the printer understands
    ---------------------------------------------------------------------------------------------
    USE_XFS             Y,y,True,true            unset
     (XFS_SERVER)       Font server IP           unset
    ---------------------------------------------------------------------------------------------
    XKBLAYOUT           A valid xorg Layout      unset           see xorg documentation for value
    ---------------------------------------------------------------------------------------------
    XKBMODEL            A valid xkb model        unset           see xorg documentation for value
    ---------------------------------------------------------------------------------------------
    XKBRULES            A set of xkb rules       unset           see xorg documentation for value
    ---------------------------------------------------------------------------------------------
    XKBOPTIONS          A set of xkb options     unset           see xorg documentation for value
    ---------------------------------------------------------------------------------------------
    XKBVARIANT          A valid xkb variant      unset           see xorg documentation for value
    ---------------------------------------------------------------------------------------------
    NETWORK_COMPRESSION Y,y,True,true            unset           compress ssh traffic
    ---------------------------------------------------------------------------------------------
    LDM_REMOTECMD       A custom command (path   Xsession        use to override default session
                        on the server)
    ---------------------------------------------------------------------------------------------
    RCFILE_nn           A custom rc file         unset
    ---------------------------------------------------------------------------------------------

                                   *****Overrides for autodetected values*****

    |   Variable    |   Possible Values     |   Default value  |       Comment
    ---------------------------------------------------------------------------------------------
    SCREEN_nn           ldm, sdm, shell,        ldm
                        startx, telnet
    ---------------------------------------------------------------------------------------------
    SERVER              Server IP               ltsp server
    ---------------------------------------------------------------------------------------------
    SYSLOG_HOST         Server IP               ltsp server      add SYSLOGD="-r" to the servers
                                                                 /etc/ltsp/syslogd file first
    ---------------------------------------------------------------------------------------------
    MODULE_nn           Kernel module name      autodetected     use to force loading of specific
                                                                 module
    ---------------------------------------------------------------------------------------------
    XF86CONFIG_FILE     path to a file          autodetected     relative to /opt/ltsp/$ARCH
    ---------------------------------------------------------------------------------------------
    XSERVER             ark                     autodetected
                        ati
                        atimisc
                        chips
                        cirrus_alpine
                        cirrus
                        cirrus_laguna
                        cyrix
                        dummy
                        fbdev
                        fglrx
                        glint
                        i128
                        i740
                        i810
                        imstt
                        mga
                        neomagic
                        newport
                        nsc
                        nv
                        r128
                        radeon
                        rendition
                        riva128
                        s3
                        s3virge
                        savage
                        siliconmotion
                        sis
                        sisusb
                        tdfx
                        tga
                        trident
                        tseng
                        v4l
                        vesa
                        vga
                        via
                        vmware
                        voodoo
    ---------------------------------------------------------------------------------------------
    X_HORZSYNC          min-max MHz             autdetected      use for forcing X_MODE
    ---------------------------------------------------------------------------------------------
    X_VERTREFRESH       min-max Hz              autdetected      use for forcing X_MODE
    ---------------------------------------------------------------------------------------------
    X_COLOR_DEPTH       2, 4, 8, 16, 24, 32     32               set to 16bit to save client mem
    ---------------------------------------------------------------------------------------------
    X_MOUSE_DEVICE      /dev/ttySn              autodetected     serial mice only,
                                                                 all others are autodetected
     (X_MOUSE_PROTOCOL) sunkbd                  unset            serial only (see above)
                        lkkbd
                        vsxxxaa
                        spaceorb
                        spaceball
                        magellan
                        warrior
                        stinger
                        mousesystems
                        sunmouse
                        microsoft
                        mshack
                        mouseman
                        intellimouse
                        mmwheel
                        iforce
                        h3600ts
                        stowawaykbd
                        ps2serkbd
                        twiddler
                        twiddlerjoy
    ---------------------------------------------------------------------------------------------
    X_MOUSE_EMULATE3BTN Y,y,True,true           unset
    ---------------------------------------------------------------------------------------------
    TELNET_HOST         Telnet server IP        ltsp server      for use with SCREEN_nn=telnet
    ---------------------------------------------------------------------------------------------
    XDM_SERVER          XDMCP server IP         ltsp server      for use with SCREEN_nn=startx
    ---------------------------------------------------------------------------------------------
    SSH_OVERRIDE_PORT   Portnumber              unset            for use wiht a dedicated ssh port
    ---------------------------------------------------------------------------------------------
