# Hardware comparison for Agisoft PhotoScan

This page shows a comparison of several CPU types for use with Agisoft PhotoScan.

## Choosing a system configuration

### The four processing stages

- Align Photos (CPU)
- Build Dense Cloud (CPU, GPU)
- Build Mesh (CPU)
- Build Texture (CPU)

Only the "Build Dense Cloud" step is able to utilize the GPU. However, that one step takes longer than all the others combined.

### Optimal CPU-core count / GPU balance

**For a good price-performance ratio** use a minimum of 6-8 CPU cores, add one core for every additional GPU. For Multi-GPU one core must be reserved per GPU. While multi-core CPU and multi-GPU efficiency decreases for every added CPU-core/GPU, multi-core CPU efficiency increases when adding more GPUs:

- multi-core CPU efficiency 50%-92% (depending on step and data)
- multi-core CPU efficiency increases by ~5% per GPU
- with more GPUs, add more CPU cores
- disable one CPU core per physical GPU in the system
- second GPU increases performance by 30-40%
- third GPU increases performance by another 20-25%
- fourth GPU increases performance by another 12-15%
- for GPUs, more is better (2x GTX 970 is better than 1x GTX 980)

### Other performance factors

- operating system
- network processing (cluster)

## CPU comparison

The last 3 columns show the build times in seconds (total and stages 1 and 2) from [anandtech 2017 CPU tests win10 - photoscan 1.3.3 large](https://www.anandtech.com/bench/CPU).

| cpu       | socket | mem       | max-mem | cores | frequency | turbo | cost | total | s1  | s2   |
|-----------|--------|-----------|---------|-------|-----------|-------|------|-------|-----|------|
| i9-7980XE | 2066   | ddr4-2666 | 128     | 18    | 2.6       | 4.2   | 2049 | 2081  | 344 | 1292 |
| i9-7960X  | 2066   | ddr4-2666 | 128     | 16    | 2.8       | 4.2   | 1699 | 2026  | 343 | 1269 |
| i9-7940X  | 2066   | ddr4-2666 | 128     | 14    | 3.1       | 4.3   | 1444 | ?     | ?   | ?    |
| i9-7920X  | 2066   | ddr4-2666 | 128     | 12    | 2.9       | 4.3   | 1189 | ?     | ?   | ?    |
| i9-7900X  | 2066   | ddr4-2666 | 128     | 10    | 3.3       | 4.3   | 999  | ?     | ?   | ?    |
| TR-1950X  | TR4    | ddr4-2666 | 1024    | 16    | 3.4       | 4.0   | 999  | 2374  | 333 | 1507 |
| i7-8700K  | 1151   | ddr4-2666 | 64      | 6     | 3.7       | 4.7   | 399  | 2477  | 598 | 1537 |
| i7-7820X  | 2066   | ddr4-2666 | 128     | 8     | 3.6       | 4.3   | 609  | 2515  | 508 | 1608 |
| TR-1920X  | TR4    | ddr4-2666 | 1024    | 12    | 3.5       | 4.0   | 779  | 2560  | 377 | 1647 |
| R7-1800X  | AM4    | ddr4-2666 | 64      | 8     | 3.6       | 4.0   | 374  | 2769  | 510 | 1809 |
| R7-1700X  | AM4    | ddr4-2666 | 64      | 8     | 3.4       | 3.8   | 334  | 2769  | 537 | 1882 |
| i7-6850K  | 2011v3 | ddr4-2400 | 128     | 6     | 3.6       | 3.8   | 399  | 3261  | 808 | 2020 |
| i7-4960X  | 2011   | ddr3-1333 | 64      | 6     | 3.6       | 4.0   | ?    | ?     | ?   | ?    |

## Other system components

| component      | type        | amount      | capacity total (GB) | cost |
|----------------|-------------|-------------|---------------------|------|
| board MSI X299 | LGA 2066    | 8 ddr4-2666 | 128                 | 352  |
| board MSI X99A | LGA 2011v3  | 8 ddr4-2666 | 128                 | 391  |
| board MSI X399 | TR4         | 8 ddr4-2133 | 128                 | 389  |
| board MSI X370 | AM4         | 4 ddr4-2133 | 64                  | 175  |
| board MSI Z270 | LGA 1151    | 4 ddr4-2400 | 64                  | 185  |
| GPU GeForce    | GTX 1080 Ti | 1 gddr5x    | 11                  | 1200 |
| GPU GeForce    | GTX 1070 Ti | 1 gddr5x    | 8                   | 750  |
| GPU GeForce    | GTX 1060    | 1 gddr5x    | 6                   | 450  |
| memory         | 16G         | 8 ddr4-2666 | 128                 | 1600 |
| memory         | 16G         | 4 ddr4-2666 | 64                  | 800  |
| case           |             | 1           | midi tower          | 150  |
| psu            | corsair RM  | 1           | 550 W               | 105  |
| psu            | corsair RM  | 1           | 650 W               | 115  |
| psu            | corsair RM  | 1           | 750 W               | 130  |
| psu            | corsair RM  | 1           | 850 W               | 180  |
| psu            | corsair RM  | 1           | 1000 W              | 190  |
| psu            | corsair HX  | 1           | 1200 W              | 230  |
| ssd            | m2 2280     | 1           | 512                 | 240  |
| hd             | sata        | 1           | 2000                | 100  |


## Load requirements [W]

Add ~50W for PSU selection.

| cpu       | 1x1080 | 2x1080 | 3x1080 | 1x1070 | 2x1070 | 3x1070 | 1x1060 | 2x1060 | 3x1060 |
|-----------|--------|--------|--------|--------|--------|--------|--------|--------|--------|
| i9-7980XE | 609    | 800    | 1045   | 488    | 663    | 839    | 427    | 545    | 663    |
| i7-7820X  | 534    | 775    | 1019   | 463    | 638    | 814    | 401    | 520    | 637    |
| i7-6850K  | 534    | 775    | 1019   | 463    | 638    | 814    | 401    | 520    | 637    |
| i7-8700K  | 484    | 725    | 969    | 417    | 588    | 764    | 355    | 470    | 587    |
| TR-1950X  | 575    | 815    | 1060   | 503    | 678    | 855    | 442    | 561    | 678    |
| R7-1800X  | 488    | 729    | 974    | 417    | 592    | 768    | 355    | 474    | 592    |

## Example system configurations

All costs are estimated using consumer prices (Mar 2018).

Base hardware:

- case, ssd, hd: 500
- psu: 110 - 230
- board (2066, 2011, TR4): 350 - 390
- board (AM4, 1151): 175 - 185

Configurations:

| #  | vendor | cpu      | gpu(s)  | mem | max | psu [W] | [CHF] |
|----|--------|----------|---------|-----|-----|---------|-------|
| 1  | intel  | i7-8700K | 1x 1080 | 64  | 64  | 550     | 3190  |
| 2  | amd    | R7-1800X | 1x 1080 | 64  | 64  | 550     | 3180  |
| 3  | intel  | i7-6850K | 1x 1080 | 64  | 128 | 650     | 3410  |
| 4  | amd    | R7-1800X | 2x 1070 | 64  | 64  | 650     | 3470  |
| 5  | amd    | R7-1800X | 2x 1080 | 64  | 64  | 850     | 4430  |
| 6  | intel  | i7-7820X | 2x 1070 | 64  | 128 | 750     | 3900  |
| 7  | intel  | i7-7820X | 2x 1080 | 64  | 128 | 850     | 4850  |
| 8  | intel  | i9-7960X | 3x 1070 | 64  | 128 | 1000    | 5790  |
| 9  | intel  | i9-7960X | 3x 1080 | 64  | 128 | 1200    | 7180  |
| 10 | amd    | TR-1950X | 3x 1070 | 64  | 128 | 1000    | 5130  |
| 11 | amd    | TR-1950X | 3x 1080 | 64  | 128 | 1200    | 6520  |

## Tests at ISG (Windows vs. Linux)

We made some tests to figure out what operating system performs better for agisoft photoscan. The used scene, test settings and system configurations are listed below.

- Version: 1.4.1
- Test scene: [monuments](http://www.agisoft.com/datasets/monument.zip)
- Batch jobs: Align Photos, Build Dense Cloud, Build Mesh, Build Texture

### Test system 1

- Board: ASUS X99 Deluxe
- CPU:  i7-5820K 6x3.3 GHz @ 4.7 GHz (over-clocked)
- GPU: ASUS GeForce GTX 980 Ti STRIX
- Memory: 4x4 GB DDR4 @ 2400 MHz
- SSD: Samsung 960 Pro 2 TB

| os           | quality (dense cloud) | total | s1 | s2   | s3  | s4  |
|--------------|-----------------------|-------|----|------|-----|-----|
| Ubuntu 17.10 | Highest (Ultra High)  | 2005  | 35 | 1670 | 111 | 189 |
| Ubuntu 17.10 | Default (High)        | 485   | 31 | 290  | 60  | 105 |
| Ubuntu 17.10 | Default (Medium)      | 240   | 30 | 72   | 46  | 91  |
| Windows 8.1  | Highest (Ultra High)  | 1612  | 28 | 1266 | 119 | 199 |
| Windows 8.1  | Default (High)        | 480   | 22 | 306  | 54  | 97  |
| Windows 8.1  | Default (Medium)      | 244   | 30 | 86   | 42  | 86  |

Test results are similar for medium to high settings. Using ultra high settings, windows takes the lead. It is unclear however if the highly over-clocked system is the reason for this difference, as it was initially optimized for running Windows.

### Test system 2

The [Institute of Geodesy and Photogrammetry](http://www.igp.ethz.ch/) uses photoscan and was in need of additional computing power for their research projects. We helped them out configuring a system for that purpose, which led to this blog post. The result was a dedicated workstation with the specs listed below. We were able to get our hands on this shiny piece of hardware for a few days and ran the same test again using Linux and Windows. The settings were left all at defaults, except dense cloud set to high.

- System: DALCO X299 Workstation black (Tower)
- CPU: Intel i9-7960X 16x2.8 GHz
- GPU: 2x nVidia GTX 1080Ti 11G (GPU PCIe Speed: x16/x16)
- Memory: 64GB DDR4 2666MHz
- SSD: Samsung 960 Evo 500GB M.2 2280

Below are the test results comparing Windows and Linux. This test was finished 70% faster on Linux compared to Windows:

| os           | quality (dense cloud) | total | s1 | s2  | s3  | s4 |
|--------------|-----------------------|-------|----|-----|-----|----|
| Ubuntu 18.04 | Default (High)        | 257   | 24 | 87  | 107 | 40 |
| Windows 10   | Default (High)        | 365   | 20 | 185 | 97  | 63 |

I made some additional tests on Linux (Ubuntu 18.04) using the same settings as above to compare the performance between version 1.3 and 1.4 and using OpenCL:

| os           | quality (dense cloud) | version    | total | s1 | s2 | s3  | s4 |
|--------------|-----------------------|------------|-------|----|----|-----|----|
| Ubuntu 18.04 | Default (High)        | 1.3        | 251   | 19 | 97 | 103 | 30 |
| Ubuntu 18.04 | Default (High)        | 1.3 OpenCL | 233   | 19 | 86 | 98  | 30 |
| Ubuntu 18.04 | Default (High)        | 1.4        | 294   | 35 | 72 | 124 | 61 |

### Conclusion

The performance comparison of Agisoft Photoscan running on Linux vs. running on Windows is inconclusive from these tests. The results may highly depend on the hardware specs, the used settings and the scene to render. But choosing the right operating system may very well result in noticeably shorter render times. It is probably a good idea to make some tests with the actual hardware and if possible using a real scene as test data to get some useful representative results.

An important side note regarding GPU (graphics processing unit): Using consumer grade Geforce Graphics Cards, as listed above, will not allow to use Remote Desktop to run Agisoft Photoscan. If you want to use Remote Desktop to run Agisoft Photoscan, you need enterprise grade Geforce Quadro cards. Of course this comes with its price.

## References

- [anandtech 2017 CPU tests win10 - photoscan 1.3.3 large](https://www.anandtech.com/bench/CPU)
- [Agisoft PhotoScan Multi Core Performance](https://www.pugetsystems.com/labs/articles/Agisoft-PhotoScan-Multi-Core-Performance-709/)
- [Estimating CPU Performance using Amdahls Law](https://www.pugetsystems.com/labs/articles/Estimating-CPU-Performance-using-Amdahls-Law-619/)
- [Agisoft PhotoScan GPU Acceleration](https://www.pugetsystems.com/labs/articles/Agisoft-PhotoScan-GPU-Acceleration-710/)
- [Manual](http://www.agisoft.com/downloads/user-manuals/)
- [Intel CPU specs](https://ark.intel.com)
- [AMD CPU specs](https://en.wikichip.org/wiki/amd/)
- [Digitec Prices 2018-03-01](https://www.digitec.ch/)
- [Power supply calculator](http://www.coolermaster.com/power-supply-calculator/)
