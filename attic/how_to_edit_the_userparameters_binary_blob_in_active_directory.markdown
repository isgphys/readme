How to edit the userParameters binary blob in Active directory
==============================================================

For setting the `TerminalServicesProfilePath` attribute in Active Directory, one has to fight with a weird (and I'm being polite here) encoding. The whole magic is now included into our user creation process, the basic algorithm is just sketched here as a reference (note that each character of plain text is encoded in 3 bytes of binary):

[[!img /media/how_to_edit_the_userparameters_binary_blob_in_active_directory/userparameters.png alt="userParameters.png" size="450x"]]

for each character you want to encode, do:

* split the character's byte into nibbles `xxxx` and `yyyy`
* have a look at `xxxx`. If it's ≤ 9, `control x` equals `001011`, otherwise it's `011010`
* have a look at `yyyy`. Here the bit patterns for `control y` are `001110` (`yyyy` ≤ 9), `011010` otherwise
* if `xxxx`>9: `xxxx` -= 9
* if `yyyy`>9: `yyyy` -= 9
* take the `1110` prefix, `control y`, `yyyy`, `control x` and `xxxx` and glue them all together to yield a 24 bit string
* convert this bit stream to three bytes

[More details here](http://daduke.org/linux/userparameters.html).
