How to install additional serial port boards
============================================

This was tested to work on [trelawney](https://bb.phys.ethz.ch/bb/notes/TRELAWNEY.html) (while it was a Linux box) using a NetMOS PCI serial board.

```
$ lspci | grep -i serial
01:06.0 Serial controller: Unknown device 9710:9835 (rev 01)

$ lspci -v -s 01:06.0
01:06.0 Serial controller: Unknown device 9710:9835 (rev 01) (prog-if 02 [16550])
        Subsystem: LSI Logic / Symbios Logic (formerly NCR): Unknown device 0002
        Flags: medium devsel, IRQ 11
        I/O ports at a000 [size=8]
        I/O ports at a400 [size=8]
        I/O ports at a800 [size=8]
        I/O ports at ac00 [size=8]
        I/O ports at b000 [size=8]
        I/O ports at b400 [size=16]
```

in /etc/init.d/serialext (create this file, Note: this assumes devfs mounted on /dev, for classic /dev or standard udev replace /dev/tts/1 with /dev/ttyS1, etc.):

```
/bin/mknod /dev/tts/1 c 4 65
/bin/mknod /dev/tts/2 c 4 66\\
chown root:dialout /dev/tts/{1,2}
chmod 0660 /dev/tts/{1,2}
setserial -g /dev/tts/1\\
setserial -g /dev/tts/2
/etc/init.d/setserial
```

in /etc/serial.conf (from the setserial package) edit:

```
/dev/tts/1 uart 16550A port 0xa000 irq 11 baud_base 115200 spd_normal
/dev/tts/2 uart 16550A port 0xa400 irq 11 baud_base 115200 spd_normal
```

