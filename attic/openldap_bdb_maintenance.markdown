OpenLDAP Berkeley Database Maintenance
======================================

OpenLDAP uses Oracle's (formerly Sleepycat's) Berkeley Database (BDB) as storage back-end. BDB generates log files which accumulate over the time. Here are some hints (for now only links) how to deal with this issue.

Links
-----

* [How to maintain Berkeley DB (logs etc.)?](http://www.openldap.org/faq/data/cache/738.html)
* [How do I configure the BDB back-end?](http://www.openldap.org/faq/data/cache/893.html)
* [Why isn't check-pointing working?](http://www.openldap.org/faq/data/cache/1162.html)
