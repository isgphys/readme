Configuring Microsoft Remote Desktop on macOS
=============================================

The _Microsoft Windows_ app (formerly called _Microsoft Remote Desktop_) allows to connect to our Windows terminal server, either with a full graphical session, or to launch individual programs from Windows directly on your macOS as [[remoteApp|services/using_remoteapp]].

Installation
------------

* The Microsoft Windows app can be installed through the [AppStore](https://apps.apple.com/ch/app/windows-app/id1295203466)

Start full Windows session
--------------------------

* Click on the plus in the top right corner and choose "Add PC".
* Enter the details of the computer you want to connect.
    - For instance our RDP server `winlogin.phys.ethz.ch`
* Optionally save your D-PHYS username and password using `Add Credentials` in the `Credentials` drop-down.
* Double-click the newly created icon to start the connection.

Start individual apps
---------------------

* Start Microsoft Windows App.
* Click on the plus in the top right corner and choose "Add Workspace".
* Enter `https://winlogin.phys.ethz.ch/RDWeb/Feed/webfeed.aspx` as URL
* Optionally save your D-PHYS username and password using `Add Credentials` in the `Credentials` drop-down.
* Double-click the newly created icons to start the corresponding Windows application.

