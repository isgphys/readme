macOS Command Line Tools
========================

We only present a few of the many command line tools available.

List System Information
-----------------------

```
/usr/bin/sw_vers
```

This shows the version and build number of the installed macOS.

```
/usr/sbin/system_profiler
```

This will list the hardware and software configuration for the macOS workstation.  Among the useful information you can find the type of the CPU, the serial number of the Macintosh, the amount of RAM, the size, type, and serial number of the hard disk(s) and the installed major software packages. This is the command line equivalent of "Apple -> About this Mac".

Open Applications or Files
--------------------------

Applications or files can be opened without any clicks from the command line.

```
open file                    # open file with default application
open -e file                 # open file in TextEditor
open -a Calculator           # open application Calculator
open -a Preview doc.pdf      # open doc.pdf in Preview
open -g file                 # open file in the background with default application
open http://www.example.com/ # open webpage in default browser
open mailto:name@example.com # compose new email with default client
opendiff file1 file2         # open FileMerge (Xcode) with differences between file1 and file2
```

Apple Software Updates
----------------------

The updates for Apple software can be checked for and installed from the menu "Apple -> Software Update" or the command line

```
/usr/sbin/softwareupdate -l           # list all available updates
/usr/sbin/softwareupdate -i <name>    # install specific update
/usr/sbin/softwareupdate -ia          # install all updates
```
