How to improve your macOS workstation
========================================

There are quite a few tools available to help you to get most out of your macOS workstation. Darwin, the macOS base system, is a BSD on Mach Micro-kernel and this gives you the possibility to use nearly all of the typical _Open-Source_ applications written for Linux or BSD. Furthermore, a lot of _Freeware_ for macOS is provided by various people. Lastly, you may also opt for commercial applications like MS Office or the Adobe products.

### User Applications

##### LibreOffice

[LibreOffice](http://www.libreoffice.org) is a free office suite for word processing, spreadsheets, presentations, and more.  It can replace Word, Excel, PowerPoint, and Access.

##### Microsoft Office

The [[documentation/Microsoft365]] subscription offers access to all Microsoft Office applications (Word, Excel, PowerPoint, Outlook, ...).

##### PDF Reader and Writer

Apple's Preview is the standard MacOS application to view and annotate PDF files. If you want to create a PDF file you can do that without additional software using the standard print dialog. If you want you can additionally install Adobe's [Acrobat Reader](http://get.adobe.com/reader/) (proprietary, free software) or the complete [Acrobat Pro](http://www.adobe.com/products/acrobat/) application via the [[documentation/Adobe Creative Cloud]] subscription. [[Skim]] is a very powerful alternative to view and especially to annotate PDFs.

##### Web Browser

Aside from Apple's Safari, you can opt for any alternative web browser: [Mozilla Firefox](http://www.mozilla.com/en-US/firefox/), [Google Chrome](http://www.google.com/chrome/)

##### Thunderbird

Mozilla [Thunderbird](https://www.thunderbird.net) can be used as an alternative email client to Apple Mail.

##### Gimp

[Gimp](http://www.gimp.org/) is a free alternative to Adobe Photoshop, allowing you to work on photos and other graphic files.

##### Cyberduck

[Cyberduck](http://cyberduck.ch/) is a front end to transfer files to and from servers, via various protocols.

##### LaTeX

[[LaTeX]]

### Developer Tools and Unix Ports

##### Command Line Tools

The Apple Command Line Tools are required for most developer environments and can be installed with the `xcode-select --install` command.

##### Homebrew

[Homebrew](https://brew.sh) is a popular package management system for macOS. It provides access to command line tools as well as graphical applications (called `casks`).

##### MacPorts

[[MacPorts]] is another package management system for macOS.

##### MacVim

[Vim](http://www.vim.org/) is the most powerful editor of the world. [MacVim](https://github.com/macvim-dev/macvim/releases) is a graphical port for macOS.

##### Emacs

[Emacs](http://emacsformacosx.com) is one of the most powerful editors.

##### Terminal

[iTerm2](http://www.iterm2.com) is a powerful terminal emulator providing many more features than the standard macOS Terminal.
