Weirdness of macOS mounting Linux Samba share
=============================================

We have the common situation where our file servers are running Linux and the clients with their different operating systems are mounting the existing shares via a given protocol. A possible candidate for this is the cross-platform protocol samba. This works well with Unix and Windows clients, but as all too often, poses problems for clients running macOS. One of the samba settings that turns out to be crucial is whether `unix extensions` are turned on or off.

#### unix extensions off -> macOS can't see or change file permissions

The [common lore](http://www.google.com/search?q=mac%20os%20smb%20unix%20extensions) is that unix extensions should be turned off for macOS clients. As a downside, the Mac user cannot correctly see or change the file permissions on the SMB share. To illustrate this we consider a folder with three empty files having different permissions and run `ls -l` on the Linux file server and in the SMB mount on a Mac client.

##### Linux

```
ls -l
   -r-xr--r-- 1 user staff 0 2011-10-26 09:48 executable.txt
   -r--r--r-- 1 user staff 0 2011-10-26 09:48 readonly.txt
   -rw-r--r-- 1 user staff 0 2011-10-26 10:04 writable.txt
```

##### Mac

```
ls -l
   -rwx------ 1 user staff 0 Oct 26 09:48 executable.txt
   -rwx------ 1 user staff 0 Oct 26 09:48 readonly.txt
   -rwx------ 1 user staff 0 Oct 26 10:04 writable.txt
```

The Linux displays the correct file permissions, whereas the Mac shows identical and wrong permissions for all files. If you create a text file with a script on the mac, it will be executable even though it doesn't have the executable bit on a Linux client. Moreover the Mac user cannot change the permissions, neither with `chmod`, nor with the Finder. One can issue `chmod` on the Mac without error message, but the command has no effect on the permissions of the file on the server. It seems to get simply ignored.

#### unix extensions on -> cannot copy symlinks

When unix extensions are switched on on the SMB server, the Mac can correctly display and change file permissions on the command line. Unfortunately this comes at the price of no longer being able to copy certain symbolic links to the SMB share. The following scenario illustrates the problem.

Let's create, locally on the Mac, a folder containing a file `b.txt` and a symlink `a.txt` to that file.

```
mkdir TestFolder
cd TestFolder
touch b.txt
ln -s b.txt a.txt
ls -l
   lrwxrwxrwx 1 user staff 5 Oct 26 10:33 a.txt -> b.txt
   -rw-r--r-- 1 user staff 0 Oct 26 10:33 b.txt
```

Then, within the Finder, drag'n'drop that folder to the mounted SMB share with enabled unix extensions. The copying fails with the error message

```
     The operation can't be completed because one or more required items can't be found. (Error code -43)
```

A similar error message is produced when copying the folder with `rsync -av`. After some more digging we realized that the alphabetical order matters. If the symlink is called `c.txt` instead of `a.txt`, Finder is able to copy the folder. This indicates that the Finder copies in alphabetical order. When the symlink precedes the actual file it points to, the SMB server seems to complain, that it cannot link to a non-existing file, yielding the above error message in macOS.

#### Conclusion

From the above we must conclude that there is no possible choice for the unix extensions setting in samba that does not lead to issues with SMB shares on macOS.

##### Further reading

* [smb.conf manpages](http://www.samba.org/samba/docs/man/manpages-3/smb.conf.5.html)
* [Apple's documentation of the error code -43](http://support.apple.com/kb/TS2039)
