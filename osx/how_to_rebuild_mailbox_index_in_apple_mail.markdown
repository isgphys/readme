How to rebuild the mailbox index in Apple Mail
==============================================

The Apple Mail application keeps track of all emails by means of an index stored locally on your computer. This, among other things, allows for fast full-text search in all messages. For various reasons it may happen that this index gets corrupted.

Typical symptoms:

* some attachments are displayed as binary code and cannot be opened
* when clicking on one email, the message of a different email is displayed

In most cases the issues can be fixed by telling the mail client to rebuild the mailbox index:

* Select the folder with the broken emails (Inbox or a subfolder)
* In the `Mailbox` menu, click on the last entry `Rebuild`

Depending on the amount of emails in the folder, the rebuild process can take from a few seconds to a couple of minutes. Once it's finished, your emails should again be displayed correctly.

If the problem occurs with many folders, it may be fastest to delete and re-add the email account.
