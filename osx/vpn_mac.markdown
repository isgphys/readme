How to setup VPN on macOS
=========================

Please install the Cisco Secure Client according to the [documentation](https://unlimited.ethz.ch/display/itkb/VPN) of the central IT services.

Open [https://sslvpn.ethz.ch](https://sslvpn.ethz.ch) in your web browser and log in with our ETH username, VPN realm (eg staff-net) and Radius password.

[[!img /media/osx/vpn_mac_1.png size="450x"]]

Click the `Download for macOS` button to download the Cisco installer and run it on your computer. You may also click on the `Instructions` button on the lower right corner for screenshots how to do this.

After the installation, you may get a notification, that the system extension by Cisco was blocked.

[[!img /media/osx/vpn_mac_2.png size="250x"]]

Open the System Preferences, and manually allow the Cisco Socket Filter

[[!img /media/osx/vpn_mac_3.png size="450x"]]

Open your Applications folder, navigate to the Cisco subfolder, and open the `Cisco Secure Client` app.

When prompted, allow the Cisco software to filter your network traffic.

[[!img /media/osx/vpn_mac_4.png size="250x"]]

Enter the secure gateway address: `sslvpn.ethz.ch/staff-net`, click on the `Connect` button and enter your VPN credentials, including the `@staff-net.ethz.ch` realm for the username, as shown below.

[[!img /media/osx/vpn_mac_5.png size="450x"]]

You should now see an icon in the top right menu bar, where you can check the state of the VPN connection, and disconnect when no longer needed.

In particular note that you will have to re-renter your Radius password each time you connect VPN.
