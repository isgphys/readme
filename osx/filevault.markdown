How to encrypt your disk using FileVault
========================================

FileVault allows to encrypt the disk of your macOS computer, thereby preventing people having physical access to your computer from reading your data. This is particularly important for laptops that can easily be forgotten or stolen. FileVault encrypts the whole disk and uses your password to unlock it during the early boot process. Once inside macOS, the user will not notice that his disk is encrypted.

FileVault can be activated anytime and will encrypt the data on your disk in the background. Chances that something can go wrong are tiny, but you should nevertheless make sure to have a recent backup before proceeding.

Go to `System Preferences -> Security & Privacy -> FileVault` and press the button to turn it on.

[[!img /media/osx/filevault_1.png size="450x"]]

Typically you want to choose not to store your private recovery key in iCloud.

[[!img /media/osx/filevault_2.png]]

Write it down on a piece of paper and store it in a safe location.

[[!img /media/osx/filevault_3.png]]

If you use Time Machine you may optionally also encrypt your backups. Unless you are very have sensitive data, or many people having easy access to your backup disk, we suggest to leave the backup unencrypted. This ensures that in the worst case scenario you can always access the data in your backups.
