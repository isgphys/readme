Apple TV
========

We describe how to configure an Apple TV to connect to the ETH wireless network.

Apple Configurator
------------------

* Install the Apple Configurator from the AppStore.
* Start the Apple Configurator from your applications.

Create Profile
--------------

* Create a new Profile in the menu File -> New Profile.
* Give it a name in the _General_ tab.
* [[Download|/media/osx/appletv-certificates.zip]] and extract the required certificates (valid until January 2022).
* Configure _Certificates_ and select all the `*.cer` files you just downloaded.
* Configure _Wi-Fi_ according to the screenshot below. Use your personal ETH wifi account for testing purposes. If you want to use an AppleTV for presentations in your group, we can create a dedicated technical account for authentication or help you connect it to the IoT network.

    [[!img /media/osx/appletv-wifi-1.png size="450x"]]

* Make sure to check all certificates in the _Trust_ tab.

    [[!img /media/osx/appletv-wifi-2.png]]

* Save the profile. Note that this file contains the Wi-Fi username and password in plain-text.

Configure Apple TV
------------------

* Connect your Apple TV with a USB cable to your computer.
* Wipe the Apple TV by selecting it, then right-click and choose Advanced -> Erase all Content and Settings.
* Follow the instructions on the screen and wait for the device to be rebooted.
* Right-click and choose Modify -> Device Name and give your Apple TV a telling name.
* Right-click and choose Prepare and make the following settings:
    - 'Do not enroll in MDM'
    - Network: Wi-Fi
    - Profile: YourAppleTVProfile

Your AppleTV should now be connected to the Wi-Fi and can be used by computers on the same wireless network.
