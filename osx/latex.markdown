LaTeX on macOS
=============


MacTeX
------

[MacTeX](http://tug.org/mactex/) provides a 2GB package containing the TeXLive distribution and the most common graphical front-ends to work with LaTeX. If you don't mind investing about 4GB of disk space, MacTeX is certainly the easiest way to start working with LaTeX on your Mac. You may also try to start with the much smaller [BasicTeX](http://www.tug.org/mactex/morepackages.html) package. The *TeX Live Utility* application can be used to install or upgrade specific TeX packages. In case you have multiple MacTex versions installed, you can pick the default version from the *TeX Distribution* pane in the System Preferences.


TeXShop
-------

[TeXShop](http://pages.uoregon.edu/koch/texshop/) is a popular TeX front-end including an editor, straightforward TeX compilation and a viewer of the generated PDF. It also supports PDF-TeX synchronization to jump back and forth between the LaTeX code and the corresponding location in the PDF. TeXShop can also be configured to use your favorite editor for writing.


BibDesk
-------

[BibDesk](http://bibdesk.sourceforge.net/) is a Mac-only bibliography manager. It allows to keep track of bibliographic references and the associated PDF files or web links. You can search by keyword or group tagged articles with smart folders. It also helps to generate well-formatted citations. All the information is stored in a BibTeX file that can also be used directly with LaTeX.


LaTeXiT
-------

[LaTeXiT](http://www.chachatelier.fr/latexit/) is a small utility that allows to quickly typeset LaTeX equations. The generated PDF can be inserted into any application via drag and drop. This is a handy way to include equations in your Keynote/PowerPoint presentations. Note that you can also drag the equation back onto LaTeXiT to edit it again.

The export format can be defined in the application's preferences. For instance *PDF with outlined fonts* will yield a PDF, but with all characters converted to a vector drawing. The exported formula is therefore independent of the underlying font, fully compatible with most programs and still scalable without loss of image quality.

You can adapt the LaTeX preamble to include your favorite packages either in the preferences, or directly by dragging down the middle bar of the program.

UnicodeIt
---------

[[UnicodeIt]] allows to convert LaTeX expressions to the corresponding Unicode characters.


latexmk
-------

[Latexmk](http://users.phys.psu.edu/~collins/software/latexmk-jcc/) (included in MacTeX) is a command-line tool automating the generation of documents from LaTeX source. It runs all the required commands in the correct order and updates the PDF each time the TeX file is saved.

The following command compiles the TeX file with `pdflatex`, opens the resulting PDF and continually re-compiles everything if the source files change.

```sh
latexmk -pdf -pvc document.tex
```

Common command-line options:

```sh
-pdf                                # generate pdf using pdflatex
-pdflatex='pdflatex -synctex=1'     # explicit pdflatex command to enable pdfsync
-C                                  # cleanup: remove all generated files including pdf
-bibtex                             # also run bibtex
-pv                                 # run file previewer
-pvc                                # run file previewer and continually recompile on change
-r ~/.latexmkrc_custom              # run with different rc file
```

The settings can also be configured through `~/.latexmkrc`, for instance

```perl
$pdf_mode = 1;                      # Generate pdf using pdflatex
$bibtex_use = 1;                    # Use bibtex if the .bib file exists
$pdf_previewer = 'open';            # Use default PDF viewer to open files
$clean_ext = 'pdfsync synctex.gz';  # Also remove pdfsync files on clean
$pdflatex = 'pdflatex -synctex=1 --interaction=nonstopmode -file-line-error';
```
