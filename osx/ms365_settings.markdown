Microsoft 365 settings on macOS
===============================

We document some settings to enhance the configuration of your [[Microsoft 365|documentation/microsoft365]] products, including Office, on macOS. The similar settings for Windows are documented [[here|windows/ms365 settings]].


Privacy Settings
----------------

In its default configuration, the Office programs send various data to Microsoft, as explained in their [overview of privacy controls](https://docs.microsoft.com/en-us/deployoffice/privacy/overview-privacy-controls) and [Mac privacy preferences](https://docs.microsoft.com/en-us/deployoffice/privacy/mac-privacy-preferences).

### Sending of diagnostic data

The following Terminal command disables sending diagnostic data to Microsoft

```sh
defaults write com.microsoft.office DiagnosticDataTypePreference ZeroDiagnosticData
```

### Sending of your content for connected experiences

Some features of the Office suite, called [connected experiences](https://docs.microsoft.com/en-us/deployoffice/privacy/connected-experiences), may send your content to Microsoft servers, in order to analyze it and provide specific services. The following Terminal command disables such features to protect the privacy of your data.

```sh
defaults write com.microsoft.office OfficeExperiencesAnalyzingContentPreference -bool false
```
