Manage role of external display on macOS
===========================================

When you attach a second display to your Mac, you can configure it to be used in two ways

* Multiple Displays: to extend your desktop and work on two monitors.
* Mirrored Displays: to show the same desktop on all displays, useful for instance when giving a presentation using a beamer.

The role of the display can be changed in `System Preferences -> Displays` by toggling the checkbox `Mirror Displays`.

[[!img /media/osx/osx_multidisplay.png size="450x"]][[!img /media/osx/osx_mirrordisplay.png size="450x"]]
