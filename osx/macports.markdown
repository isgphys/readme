MacPorts
========

[MacPorts](http://www.macports.org/) is a project designed to bring many of the open-source unix tools to the Macintosh computers.

##### Installation

Make sure that Apple Xcode is installed on your computer. Then simply [download](http://www.macports.org/install.php) and install the MacPorts package for your version of macOS. You may need to accept the Xcode license agreement as root before installing some packages through MacPorts using `sudo xcodebuild -license accept`.

##### Usage

MacPorts is a command-line tool. The commands need to be run as root or prepended by `sudo`. A detailed documentation can be found at [guide.macports.org](http://guide.macports.org ). If you prefer a graphical user interface, have a look at [Pallet](http://trac.macports.org/wiki/MacPortsGUI).

| Command                         | Description                                                         |
|---------------------------------|---------------------------------------------------------------------|
| `port selfupdate`               | Update MacPorts and get the latest listing of the repository        |
| `port search htop`              | Search for _htop_ in the repository                                 |
| `port info htop`                | Show information for _htop_ package                                 |
| `port contents htop`            | List the files installed by the _htop_ package                      |
| `port install htop`             | Install _htop_ package and all its dependencies                     |
| `port uninstall htop`           | Uninstall _htop_ package                                            |
| `port variants vim`             | List available variants of package vim                              |
| `port -v installed vim`         | Show detailed info about installed package, including which variant |
| `port installed`                | List all installed packages                                         |
| `port echo requested`           | List requested packages, not installed through dependencies         |
| `port outdated`                 | List installed packages for which an update is available            |
| `port upgrade htop`             | Upgrade _htop_ package to the latest version                        |
| `port upgrade outdated`         | Upgrade all outdated packages                                       |
| `port -f clean --all installed` | Remove downloaded temp files to free disk space                     |
| `port -f uninstall inactive`    | Remove old versions of installed packages                           |
| `port select --list pip`        | List all installed pip versions (similarly for python, ipython etc) |
| `port select --set pip pip34`   | Select pip 3.4 as default version to start when running pip         |
| `port doctor`                   | Check for common issues in the user's environment (version 2.4+)    |
| `port reclaim`                  | Reclaim disk space (rm inactive ports and tmp files) (version 2.4+) |

MacPorts installs the packages into `/opt/local`. You may need to add the folders to your PATH environment variables

```sh
export PATH=/opt/local/bin:/opt/local/sbin:$PATH
export MANPATH=/opt/local/share/man:$MANPATH
```

##### Compiling other software using MacPorts libraries

Because MacPorts does not write its libraries into the standard system folders, you may encounter error messages when trying to compile certain programs from source. Often one can use additional flags with `./configure` to explicitly provide the paths to the header files and libraries. Another possible solution is to populate the shell environment variables with the relevant paths:

```sh
export LDFLAGS='-L/opt/local/lib'
export CPPFLAGS='-I/opt/local/include'
export LD_LIBRARY_PATH=/opt/local/lib
export LD_INCLUDE_PATH=/opt/local/include
```
