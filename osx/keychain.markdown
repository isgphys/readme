Keychain Access Password Storage
================================

macOS can store passwords, certificates and other sensitive information and provides the **Keychain Access** application to manage them. It can be found in `/Applications/Utilities/`.

Removing entries with wrong or outdated passwords
-------------------------------------------------

It may happen that your computer cannot connect to a given service because it uses a wrong or outdated password stored in the keychain. Use the search box in the top right corner to filter the password entries (e.g. the `pia` server for the ID printing service). Then select the items in the lower table, and press backspace to delete them. The next time you try to connect to the service you should be prompted to re-enter your username and password.

[[!img /media/osx/keychain_access.png size="x400"]]
