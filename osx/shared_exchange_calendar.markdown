Shared Exchange Calendar
========================

This guide shows how to configure a shared Microsoft Exchange Calendar on macOS. Please note that this service is hosted by ETH ID (IT-Services), for the official documentation please refer to [ETH Email and Calendar](https://unlimited.ethz.ch/pages/viewpage.action?pageId=16451437).

From the _Apple_ -> _Preferences..._ menu choose _Internet Accounts_ and select _Exchange_ as account type:

[[!img /media/osx/mac_shared_calendar_1.png size="450x"]]

Sign in using your [[ETH Account|account/]], then _Configure manually_:

[[!img /media/osx/mac_shared_calendar_2.png size="450x"]]

We need to sync at least **Calendar** and **Contacts**, check the other boxes only if needed:

[[!img /media/osx/mac_shared_calendar_3.png size="450x"]]

Open the **Calendar** and from _Calendar_ -> _Preferences..._ menu choose _Exchange_ -> _Delegation_:

[[!img /media/osx/mac_shared_calendar_4.png size="450x"]]

Click on the `+` (plus) symbol to add a calendar to access, click in the _Users_ box and start typing the calendar/user name. It will now search for a list of calendars matching the keyword. From the the drop-down menu choose the appropriate calendar:

[[!img /media/osx/mac_shared_calendar_5.png size="450x"]]

Check the _Show_ box and the calendar should appear in your Calendar App:

[[!img /media/osx/mac_shared_calendar_6.png size="450x"]]
