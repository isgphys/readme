macOS Print Settings for ETH Printing Service
============================================

Print in color or black & white
-------------------------------

[[!img /media/osx/id_printer_color_mode.png size="450x"]]

If you don't see _Printer Features_ in the list, you probably didn't install the proper drivers, as explained in the first step of our [[installation instructions|printing/how_to_add_an_id_printer_on_osx]].

Print using a specific paper tray
---------------------------------

[[!img /media/osx/id_printer_paper_tray.png size="450x"]]

If you don't see additional trays in the list, you probably didn't configure the proper _Option Tray_, as explained in our [[installation instructions|printing/how_to_add_an_id_printer_on_osx]].

Save custom settings as preset
------------------------------

Custom print settings can be saved as named preset. This allows for quick access of different color modes and single or double page printing. By default macOS will reuse the preset of the last print job, thereby providing a way to override the default settings.


[[!img /media/osx/id_printer_custom_presets.png size="450x"]]
