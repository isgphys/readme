Magic Keys used by Mac Hardware
===============================

### During boot

* **Cmd-V** Verbose startup (text console)
* **Cmd-S** Single user mode
* **T** Export internal disk for target mode
* **C** Boot from CD-ROM
* **N** Boot from Network
* **D** Download and run diagnosing software over the network (even wireless can be used)
* **Opt** Boot device selection
* **Cmd-Opt-O-F** Enter OpenProm prompt
* **Cmd-Opt-P-R** Flush OpenProm nvram

### During operation

* **Cmd-Shift-3** Create screenshot, puts a file on the desktop
* **Cmd-Shift-4** Create screenshot with selection, puts a file on the desktop; click space to capture a window
* **Ctrl-Cmd-Shift-3** Create screenshot, puts it into the clipboard
* **Ctrl-Cmd-Shift-4** Create screenshot with selection, puts it into the clipboard, click space to capture a window
* **Cmd-Alt-Esc** End program
* **Shift-Alt-Cmd-Q** logoff immediately
