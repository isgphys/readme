Skim
====

[Skim](http://skim-app.sourceforge.net) allows to view and annotate PDF documents.

Features
--------

### Annotations

You can highlight text and add notes. In contrast to Apple's Preview, the annotations are stored as extended attributes without altering the PDF file itself. This ensures that saving notes is quick even for large documents. The text highlights and notes can be exported to a text file for further studying or processing.

The [SkimNotes](http://sourceforge.net/apps/mediawiki/skim-app/index.php?title=Interaction_with_Skim#SkimNotes_Command_Line_Tool) command line tool allows to automate the conversion of the Skim notes and includes an agent other programs can connect to and access the notes.

### Full-screen

Besides the presentation mode, Skim offers a full screen mode for comfortable reading.

### Auto-reload

Skim can recognize when a PDF file is updated on the disk and reload it. For this to work, make sure `Check for file changes` is checked in the `Sync` tab of Skim's preferences.

If you don't want to be prompted for every file, you can enter the following command to activate auto-reload globally.

```sh
defaults write -app Skim SKAutoReloadFileUpdate -boolean true
```

### TeX-PDF synchronization

Skim allows to Shift-Command-click on a location in the PDF and jump to the corresponding line in the TeX, and the other way round. This may require a little bit of configuration at three locations: Skim, the TeX compiler and your text editor.

#### Sync

Skim has predefined settings for the most common editors in the `Sync` preferences.

#### TeX

To enable TeX-PDF synchronization, you can compile with `pdflatex -synctex=1` or include `\usepackage{pdfsync}` in the header of your TeX document.

#### Editor

In Vim you can define a keyboard shortcut to jump to the location in the PDF corresponding to the TeX line under the cursor. Putting the following in your `.vimrc` assigns this mapping to the `F3` key.

```vim
au filetype tex map <F3> :w<CR>:silent !/Applications/Skim.app/Contents/SharedSupport/displayline <C-r>=line('.')<CR> %<.pdf %<CR>
```

Further instructions and configurations for other environments can be found in the [TeX-PDF Synchronization Wiki](http://sourceforge.net/apps/mediawiki/skim-app/index.php?title=TeX_and_PDF_Synchronization).
