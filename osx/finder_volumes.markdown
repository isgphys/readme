Add Volumes to Finder Sidebar
=============================

The `/Volumes/` folder on macOS contains all mounted file shares. If you need a quick access to connected groupshares, you can add this folder to the Finder sidebar on the left.

* With the Finder active, open the `Go` menu and select `Go to Folder...`
* Enter `/Volumes` in the dialog, then press Enter.

    [[!img /media/osx/finder_volumes_1.png size="450x"]]

* Drag-n-drop the Volumes folder to the Finder sidebar

    [[!img /media/osx/finder_volumes_2.png size="450x"]]
