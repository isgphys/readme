How to Prevent Apple Mail from Downloading Remote Images
========================================================

By default Apple Mail will automatically download images in HTML formatted mail messages.  This allows the sender of the mail to track you when you view the message.  It is a technique often used by spammers to verify addresses.  If you want more privacy you can forbid Apple Mail to automatically download remote images.

From the _Apple_ -> _Preferences..._ menu choose _Viewing_ and then make sure to tick off the check box for the remote images.

[[!img /media/osx/no_remote_images_in_mail.png alt="Disable Remote Image Loading"]]

You can still load the images on demand for each mail message once you are confident it is from a legitimate sender.
