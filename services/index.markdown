ISG Services
============

The IT Services Group (ISG) of the [Department of Physics (D-PHYS)](http://www.phys.ethz.ch/) maintains and manages the IT infrastructure of the Physics Department.  All members of the department (employees, students, guests) as well as other people (inside or outside of ETH) may use these services.  Our support is targeted primarily on the needs of the D-PHYS.  Others can use the services on an "as is" basis, as long as they are not stretching our resources too much.  In this way we hope to support academic cooperation.

Many of our services are open to everyone, but others require authentication by a [[physics account|account/]]. Please note that we may terminate an account temporarily or permanently if we suspect any abuse.

Some of our services cover basic infrastructure needs available to the whole department while others address individual requirements in groups or institutes.

## Hardware purchase process

As of Jan 1 2011 the hardware purchase process has been changed in order to give professors full control over their IT funds. The [Informatikkoordinator](http://www.phys.ethz.ch/phys/dep/gremien/informatikkommission) in your institute acts as your local contact person.

## News and announcements

We post announcements and notifications on our [blog](https://isg.phys.ethz.ch/). Please [subscribe](https://isg.phys.ethz.ch/subscribe/) to our email newsletter or join our chat rooms below to stay in touch.

**New: Join our chat rooms get updates from ISG instantly!**

- [#isg-news:phys.ethz.ch](https://m.ethz.ch/#/#isg-news:phys.ethz.ch) - IT announcements and news
- [#isg-status:phys.ethz.ch](https://m.ethz.ch/#/#isg-status:phys.ethz.ch) - current IT system status (problems)

## List of Services

A non-exhaustive list of our IT services can be found [here](https://www.phys.ethz.ch/services/physics-isg.html).

Some other services we provide:

- TLS/SSL [[web/certificates]]
- Directory services: [[account/LDAP]], Active Directory
- [[User accounts|account]]
- [[Group accounts|account/groups]]
