Our mission is to help you do your work
===============================

Feel free to contact us in the case of problems, questions, or special wishes.

[[!img /media/helpdesk1.jpeg]]

### Offices

HPT H 6 - H 9 <br/>
Auguste-Piccard-Hof 1 <br/>
Campus Hönggerberg <br/>
ETH Zürich <br/>
CH-8093 Zürich <br/>

### E-Mail (Ticketing system)

[[isg@phys.ethz.ch]]

Please include the **hostname of your computer**, your **phone number** and **office location**.

### Phone

+41 44 633 26 68

### Chat

Join our [#heldpesk](https://m.ethz.ch/#/#helpdesk:phys.ethz.ch) room to [[chat]] with us.

* either using our [element.phys.ethz.ch](https://element.phys.ethz.ch/#/room/#helpdesk:phys.ethz.ch) web client
* or open it in your favorite [matrix client](https://m.ethz.ch/#/#helpdesk:phys.ethz.ch).

### Business hours

Usually somebody's around between 0700 and 1700.

Scheduled group meetings are Monday 1000-1100 and first Thursday of each month 1000-1100.

### How to find our office

#### using public transport

* Use [VBZ](https://www.stadt-zuerich.ch/vbz) lines 37, 80 or 69 to ETH Hönggerberg or the [direct bus](http://www.ethz.ch/about/location/hoengg/index_EN) from the main station or ETH Zentrum.
* Go north (towards Affoltern) and turn right after the _Polybuchhandlung_.
* Enter the _HPT_ building, and go up to the _H_ floor.
* Look for the office with the _ISG Helpdesk_ sign.

#### with a car

* Drive into the _Physik-Besucher_ garage — choose one of the two decks.
* Leave the garage through the _Hauptausgang_ and turn right.
* Enter the _HPT_ building, and go up to the _H_ floor.
* Look for the office with the _ISG Helpdesk_ sign.

Find us on [Google Maps](https://www.google.ch/maps/place/47%C2%B024'31.8%22N+8%C2%B030'32.4%22E/@47.408824,8.508986,19z/data=!3m1!4b1!4m2!3m1!1s0x0:0x0?hl=de) or on [map.search.ch](http://map.search.ch/8049-zuerich/wolfgang-pauli-str.16).

See also our [main page](https://www.phys.ethz.ch/services/physics-isg.html) on https://www.phys.ethz.ch/.
