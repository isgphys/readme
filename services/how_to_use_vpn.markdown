VPN
===

Using a [Virtual Private Network](https://en.wikipedia.org/wiki/Virtual_private_network) (VPN) allows you to access the [[ETH network|https://readme.phys.ethz.ch/network/]] from outside the campus. From wherever your computer is connected to the internet, once you activate VPN, it is as if your computer was on the ETH campus. Because all internet traffic flows through ETH, it is best to maintain the VPN connection only as long as it is needed.

### Situations where VPN can be useful

* To access scientific journals from outside ETH campus
* To access resources restricted to the ETH intranet
* To have full internet access while using the `Public` wireless at ETH
* To have full internet access at certain public HotSpots (e.g. train stations)

### How to setup VPN

The setup depends on your operating system. Please refer to the [VPN instructions](https://unlimited.ethz.ch/display/itkb/VPN) by the Informatikdienste for more details.

* [[How to setup VPN on Windows|windows/vpn_windows]]
* [[How to setup VPN on macOS|osx/vpn_mac]]
* [[How to setup VPN on Linux|linux/vpn_with_linux/]]
