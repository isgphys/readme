Managed macOS Workstations
==========================

We provide fully managed macOS workstations that come preconfigured with several applications. We take care of software updates and make backups of your local data.

Login
-----

You need a D-PHYS account and special rights to be able to log in to our managed macOS workstations. Please [[get in touch|services/contact]] with us if you need access.

Home
----

You D-PHYS home is automatically mounted on login and for instance available through the Dock icon next to the Downloads folder. This is the best place to store all important files, as you can [[access them|storage/access]] from anywhere.

Additionally each user has a local home on every Mac he logs on. This is to store all the settings and caches, but also the files on your desktop etc. Please note that you will only have access to these files when working on that specific Mac.

Email
-----

Follow our instructions to [[configure Apple Mail|mail/how_to_use_email_with_apple_mail]] or head over to [webmail](https://webmail.phys.ethz.ch).

Documentation
-------------

Various topics are [[documented|osx]].
