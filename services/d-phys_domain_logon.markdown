D-PHYS Domain Logon
===================

In our environment the D-PHYS Domain Logon is done against a Microsoft Active Directory Domain consisting of several servers called Domain Controllers (DC).

D-PHYS Domain Logon has some advantages like:

* **Roaming Profile**, which saves the personal settings of the user environment on the profile and can be used on every Windows workstation which is also in the D-PHYS Domain.
* **No local user accounts**, it is not anymore necessary to create local user accounts on the workstations and keep them in synchronization.
* **automatic share connection**, the home drive and any group drives you have access to will be connected automatically and always have the same drive letter for every user in the group.
