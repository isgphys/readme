IT at D-PHYS
============

When joining D-PHYS (or IGP/D-BAUG), please consider attending our 1 h introductory lecture "[IT at D-PHYS](https://compenv.phys.ethz.ch/)".

**Please also subscribe to our [newsletter](http://nic.phys.ethz.ch/subscribe/) and join our [Matrix chat](https://readme.phys.ethz.ch/chat/) to stay in touch!**

Links

* [Best Practices for Scientific Computing](http://arxiv.org/abs/1210.0530)
* [Good Enough Practices in Scientific Computing](https://arxiv.org/abs/1609.00037)
