Workstations
============

The ISG operates a large number of workstations running [[Linux|linuxws]], [[macOS|macws]] and [[Windows|windowsws]]. We call them _Managed Workstations_. You need a physics [[account/]] to use them.  Your files are automatically on our fileservers and [[backed up|backups/]] on a regular basis. Please note the general [[services/usage policies]].

One public room is equipped with Linux workstations:

* In the room [HIT E 12](https://www.ethz.ch/services/de/service/raeume-standorte-transporte/raeume-gebaeude/orientierung/gebaeude.html?args0=HIT) you'll find some computers running Linux and a wide range of applications like mail clients, word processors, TeX and scientific applications.

* A second room [HIT F 21](https://www.ethz.ch/services/de/service/raeume-standorte-transporte/raeume-gebaeude/orientierung/gebaeude.html?args0=HIT) exists, which is managed by ID services. More rooms could be found with the [room information database](https://ethz.ch/staffnet/en/service/rooms-and-buildings/roominfo.html).
