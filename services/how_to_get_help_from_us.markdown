ISG Helpdesk
============

If you have any IT related problems, you can [[services/contact]] our helpdesk. Furthermore, if you have a specific issue on your screen, we might be able to help you remotely, using TeamViewer.

TeamViewer
----------

TeamViewer is a cross-platform remote desktop solution (Linux, Mac and Windows) that is installed on all of our managed workstations. If you're machine is not managed by us, you can [download](http://www.teamviewer.com/en/download/index.aspx) the software and install it on your machine if you'd like us to help you remotely. Once installed, launch `teamviewer` and [[services/contact]] us by phone to tell us the two numbers (ID and password) you can find in the `Wait for session` tab. We can then remotely take a look at your desktop. Rest assured that you will always have full control and can close the remote connection at any time. We cannot and will not look at your screen without you asking us to.

[[!img /media/how_to_get_help_from_us/win_teamviewer3.png]]

How to start TeamViewer on our managed Windows workstations:

[[!img /media/how_to_get_help_from_us/win_teamviewer1.png]]
[[!img /media/how_to_get_help_from_us/win_teamviewer2.png]]
