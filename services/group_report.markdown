Group Reports
=============

The amount of used disk space is steadily increasing, as people accumulate more and more data. It can become non-trivial to keep an overview of where the largest data is hiding. This is especially the case for group shares, where several people have access, and files are rarely cleaned up after the departure of a group member. In some cases not even the list of group members is kept updated.

Of course we can't decide for you which folders are important and which files can safely be deleted. Nevertheless we can help you to get an overview of how much disk space your group is using, and how it is distributed among the folders of your group shares. We gather all the relevant information in a report, which can be sent to the group responsible on a monthly basis.

A group report includes information about your group's disk quota and the disk space usage distribution among the top ten largest folders. It also shows the list of the current group members and a link to a web page, on which all group members can interactively navigate the folders of the group share and see their sizes.

Sample report
-------------

```
==========[ Group disk quota ]==========

Used diskspace:    318 GB (59.6%)
Soft quota limit:  467 GB
Hard quota limit:  534 GB

====[ Groupshare data distribution ]====

0.1%                 1%                 10%               100%
[####################|###################|################---]  74.2% = 236 GB   grpshare
[####################|###################|#######------------]  25.8% =  82 GB   labshare

===[ Top folders total space usage ]====

0.1%                 1%                 10%               100%
[####################|###################|######-------------]  21.8% =  69 GB   labshare/Scratch
[####################|###################|####---------------]  17.5% =  56 GB   grpshare/Data
[####################|###################|##-----------------]  13.3% =  42 GB   grpshare/Backup
[####################|##################-|-------------------]   8.1% =  26 GB   grpshare/Papers
[####################|#################--|-------------------]   7.5% =  24 GB   grpshare/Theses
[####################|###############----|-------------------]   5.9% =  19 GB   grpshare/Projects
[####################|##############-----|-------------------]   5.0% =  16 GB   grpshare/Students
[####################|##########---------|-------------------]   3.4% =  11 GB   grpshare/Secret
[####################|#########----------|-------------------]   3.1% = 9.9 GB   labshare/Lab
[####################|########-----------|-------------------]   2.8% = 8.9 GB   grpshare/Code

The following folders were omitted because they belong to a different group:
  - /grpshare/Secretary ( 2.1 GB belonging to group 'grpsecr')
This may lead to differences between this report and the online version.

===========[ Group members ]============

abe       │  Axel Beckert
daduke    │  Christian Herzog
esh       │  Elmar Heeb

```

How to get it
-------------

Please contact us if you'd like to get a report for your group share.

Detailed explanation
--------------------

### Group disk quota

Note that the quota information is gathered at the level of the filesystem. It includes files owned by your group in all possible locations, sometimes even outside of the listed group shares. This may lead to discrepancies with the reported disk usage for the group share.

* Used disk space: Total size of all the files belonging to your group and its percentage relative to the hard quota limit.
* Soft quota limit: If your usage exceeds the soft limit, you start receiving warning emails, that you are over quota.
* Hard quota limit: If the hard limit is reached it becomes impossible to write to the share.

### Groupshare data distribution

If the group owns several shares, we plot the disk space used by each of them.

### Top folders total space usage

Logarithmic plot showing the size and paths of the ten largest folders. Percents are computed with respect to the total size of all the folders owned by the group and are independent of the available disk space or quota. We take into account all first-level subfolders that belong to your group and determine their size, independently of who the contents belong to. If a first-level subfolder belongs to a different group, it is excluded, and listed as such below the graph.

### Group members

List of current group members, ordered by username. Contact ISG if you want any changes to be made to this list.

### Online graph

The report includes a link to a web page with an interactive sunburst visualization of the groupshare disk usage. The access is restricted to the current members of the group which can use their physics account to log in. Each graph represents a single groupshare, with all its subfolders, including those belonging to a different group. The angular width represents the relative size of the folder. The different rings are the subfolder levels. The sole purpose of the colors is to differentiate the elements. The user can click on each folder to enter it, and click on the central region to navigate back to the parent folder. You can play with a [working example](https://duc.zevv.nl/demo.cgi?cmd=index&path=/usr/share) on the [DUC homepage](https://duc.zevv.nl/).
