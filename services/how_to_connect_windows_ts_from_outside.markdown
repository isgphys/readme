How-to connect to the Windows Remote Desktop Server (RDS) from outside of the D-PHYS networks with a Windows computer
======================================================================================================

You have three possibilities for accessing the Windows Remote Desktop Server (RDS) from outside of the D-PHYS networks:

## Primary possibility

* First establish a **VPN connection**, more information is described [here](https://sslvpn.ethz.ch).
* Then connect to winlogin.phys.ethz.ch with your Physics User Account (AD\username) with a remote desktop client (e.g.mstsc.exe).

## Alternative possibilities

First alternative possibility:

* Using a **SSH tunnel** with builtin ssh client see [[here|documentation/ssh_tunnel#Windows-RDP-via-SSH-tunnel]].

Second alternative possibility:

* Using a **SSH tunnel** with PuTTY client, see the following guide:

* First you need to install [PuTTY](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html) the free SSH client for Windows.

* **Start PuTTY** from your Start Menu (Enter putty to the search field of your Start Menu and hit `ENTER`).
* Navigate to **Connection > SSH > Tunnels** in the config tree at the left side of the PuTTY window.

    [[!img /media/how_to_connect_windows_ts_from_outside/puttyrdprdr2.jpg]]

* Enter the value **9000** to the field **Source port**, and **winlogin.phys.ethz.ch:3389** to the **Destination** field and activate the option **IPv4**.

    [[!img /media/how_to_connect_windows_ts_from_outside/puttyrdprdr3.jpg]]

* Click the **Add** button to finally add the tunnel configuration.

    [[!img /media/how_to_connect_windows_ts_from_outside/puttyrdprdr4.jpg]]

* Navigate to the **Session** entry at the top of the config tree.

    [[!img /media/how_to_connect_windows_ts_from_outside/puttyrdprdr1.jpg]]

* Enter the value `USERNAME@login.phys.ethz.ch` to the field **Host Name (or IP address)**. (Replace USERNAME with your real D-PHYS user account name) Also enter the string **login.phys.ethz.ch** to the field **Save Sessions**.

    [[!img /media/how_to_connect_windows_ts_from_outside/puttyrdprdr5.jpg]]

* Click the **Save** Button to save the newly created session settings.

    [[!img /media/how_to_connect_windows_ts_from_outside/puttyrdprdr6.jpg]]

* Double click the newly saved session **login.phys.ethz.ch** in the **Saved Session List** and enter your D-PHYS password. You should now reach a command prompt on the linux server. Minimize the SSH session window but do not close it, we only need it to stay in the background.
* Start the **Remote Desktop Connection Client** from your Start Menu (enter _Remote Desktop_ to the search field of your Start Menu and click on the Search result **Remote Desktop Connection**). Enter **localhost:9000** to the field **Computer:**

    [[!img /media/how_to_connect_windows_ts_from_outside/puttyrdprdr7.jpg]]
    [[!img /media/how_to_connect_windows_ts_from_outside/puttyrdprdr9.jpg]]

* Login with your normal D-PHYS username **AD\USERNAME** (replace USERNAME with your real D-PHYS user account name). You should now reach your Remote Desktop Session.

* After you've disconnected or logged out your remote desktop session you can close the SSH session as well. For this focus your SSH session window and type `logout<ENTER>` at the given command prompt. The SSH session window should have been close now as well as the SSH Tunnel to forward the RDP connection.
