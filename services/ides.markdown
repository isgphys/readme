ETH IT Shop (ex IDES)
=====================

ETH members can purchase and download licensed software through the [IT Shop](http://itshop.ethz.ch).

Add New Software
----------------

You can request missing software to the IDES store at https://www.softwareinfo.ethz.ch/suggesting-a-new-it-shop-product/
