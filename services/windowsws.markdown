Managed Windows Workstations
============================

We provide fully managed Windows workstations that come pre-configured with several applications.

## Login

You need a D-PHYS account to log in to our managed Windows workstations. To get a physics account, take a look at [here](/account/policy)

After first login it will create a [roaming user profiles](http://msdn.microsoft.com/en-us/library/bb776897.aspx), that you have all settings on every machine.

## Home

Your D-PHYS home is automatically mounted on login and for instance available through the `H:` Drive.

This is the best place to store all important personal files, as you can [access them](/storage/access) from anywhere.

## Group Shares

Your D-PHYS group shares, where you have access rights, are automatically mounted on login and for instance available through the `P:` Drive.

This is the best place to store all important group files, as you can [access them](/storage/access) from anywhere.

## Printing

Feel free to install the printers you need as described [here](/printing/how_to_add_an_id_printer_on_windows).

## Email

You have the choice to use [Webmail](https://webmail.phys.ethz.ch), [Thunderbird](/mail/how_to_use_email_with_thunderbird) or [Outlook](/mail/how_to_use_email_with_outlook) as your mail Client.

## Software

Some office and science software packages are pre-installed and ready for use. A lot of optional apps can be installed through [Chic](https://chic.phys.ethz.ch). Send us an email if you need anything else.

## First configuration steps

Here are few first steps for configuration your Windows client

### UI Language

The Default language of our Windows installation is German. You can change it to [English](/windows/windows10/change_display_or_keyboard_language) with few clicks.

### Change Default Apps

You want open some documents with different applications? There are few [ways](/windows/windows10/set_default_program) to do this.

### Enable auto lock screen

In the default settings, windows locks your screen automatically. If you want change this setting, follow [this](/windows/windows10/edit_lockscreen) manual.


For other Windows-specific topics refer to our [[audience/Windows User]] read me.
