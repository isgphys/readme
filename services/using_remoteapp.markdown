Using Windows RemoteApp
=======================

RemoteApp is a virtual application solution that allows users to run Windows-based applications regardless of what operating system they are using. It allows users to launch virtual applications from a server that appears on their computer as if it is installed locally, but in reality, is running on a remote server.

[[!img /media/windows/remoteapp/01.jpg size="450x"]]

How to use for one-time/temporary access
----------------------------

This will open RemoteApp temporary on your computer.

* go to [winlogin.phys.ethz.ch](https://winlogin.phys.ethz.ch)
* login with your d-phys account `ad\your_dphys_username`
* choose an application and save this file on your local computer
* run the downloaded file
* Select the checkbox `Dont't ask me again for remote connections from this publisher` and choose `connect`

    [[!img /media/windows/remoteapp/02.png size="300x"]]

* enter your credentials `ad\your_dphys_username`

How to use for regular/permanent access
---------------------------

You can add the RemoteApps permanently to your start menu with these steps.

#### Windows

See the instructions [here](/windows/how_to_remoteapp)

#### macOS

See the instructions [[here|osx/configuring_microsoft_remote_desktop_on_mac]]
