Usage Policies
==============

A number of regulations are put forth by ETH to guide the usage of computers and information security in general.

* [Acceptable Use Policy for Information Technology ("BOT")](https://rechtssammlung.sp.ethz.ch/Dokumente/203.21.pdf)
* [Directive on Information Security](https://rechtssammlung.sp.ethz.ch/dokumente/203.25.pdf)
* [IT Guidelines and IT Baseline Protection Rules](https://rechtssammlung.sp.ethz.ch/dokumente/203.23.pdf)
* [Directive on Inventory and Classification of information](https://rechtssammlung.sp.ethz.ch/Dokumente/203.28.pdf)
* [Directive on Logging, Evaluation and Monitoring](https://rechtssammlung.sp.ethz.ch/Dokumente/203.29.pdf)

* Further guidelines and code of conducts
    - [[linux/How to handle long running jobs]]
    - [[Recommendation for the Use and Care of Data on Computers]]
