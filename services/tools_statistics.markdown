Tools & Statistics
==================

Tools
-----

We use many different tools to manage our infrastructure. Some of them are publicly available and may give you some information about the state of our services:

* [BaNG - Backup Web Front-end](https://backup.phys.ethz.ch) (Code on [GitHub](https://github.com/isgphys/BaNG))
* [Mailman](https://lists.phys.ethz.ch) Mailing list management
* [ISG Ticket System](https://rt.phys.ethz.ch/rt/)
* [Show Members of Group](https://account.phys.ethz.ch/group/info) or use `members group`
* [First Operating Team](https://admin.phys.ethz.ch/firstlab/)

Some tools outside of our control:

* [NetCenter](https://www.netcenter.ethz.ch/)
* [IDES Shop](https://itshop.ethz.ch/)
* [OnTheHub](https://onthehub.com/)
* [Doodle@ETHZ](https://ethz.doodle.com)
* [Print + Publish](https://www.print-publish-sb.ethz.ch/)
* [Archiv Zeitgeschichte](https://onlinearchives.ethz.ch/)
* [Hochschularchiv](https://archivdatenbank-online.ethz.ch/)
* [e-pics](https://www.e-pics.ethz.ch/)
* [GEODATA2USE](https://geodata2use.ch/) and [GEOVITE](https://geovite.ethz.ch/)
* [eduroam setup](https://cat.eduroam.org/)
* [HPC Cluster System Status](https://scicomp.ethz.ch/wiki/System_status)
* [Collaborative whiteboard](https://wb.ethz.ch) hosted on D-ARCH servers available to all ETH users
* [Open Source](https://ethz.ch/en/industry/researchers/licensing-software/open-source-software.html) at ETHZ

Statistics
----------

In order to offer our customers the best possible service, we collect and monitor a wide range of statistical data:

* [ISG fun facts](https://isg.phys.ethz.ch) (right column)

### File Server

* Storage statistics for [file servers](https://admin.phys.ethz.ch/lvm2FS/) and [hypervisors](https://admin.phys.ethz.ch/lvm2HV/) (Code on [GitHub](https://github.com/daduke/vmchart))
