Recommendation for the Use and Care of Data on Computers
========================================================

#### Background

Increasingly, a large amount of research data is stored solely on computers.  This data represents a considerable value mostly in the form of time and other resources used to acquire it.  It is therefore important that proper care is taken to ensure the integrity, availability, and safeguard against loss of the data.

Most research at ETH is financed entirely or partially by taxpayers money.  The result of this research belongs to ETH.  Commercial use of research results outside of ETH requires a contract according to the rules and regulations of ETH.  Collaboration with industry partners is a typical case.  In that case the contract will regulate the handling of data.  The recommendations below apply to the case where the research is done within ETH.

#### Individual Accounts / Integrity of Data

All modern Operating System use individual accounts and permission settings on files and directories.  The ownership and permissions make sure that one owner of an account cannot accidentally or intentionally affect the data of another account.  Individual account can be made part of a group which allows collaboration by sharing data within the group.  A reasonable default setting allows the owner read/write, the group read-only, and everybody else no access.  Individual accounts shall not be shared, i.e., the password shall only be known to the person owning the account.

Make sure that the data is properly backed up.  Temporary data which can be easily recreated may not be backed up, but everything else should be backed up daily or at least weekly.  The Department of Physics maintains some file servers which are backed up daily.  All members of the D-PHYS are invited to use these servers.

The integrity of the data can usually be ensured by the permission system of the operating system (read-only or no-access).  For additional safety a cryptographic signature can be produced to accompany a file.  The file can remain in clear text.  This may be beneficial for data to be archived.

To make sure that the data remains available even if the owner of the file is not at hand, common data shall be stored with group read permissions.  Occasionally, it may be necessary that an administrator will access a file.  The administrator password shall be known either to a small group of trusted people or to a single trusted person with a copy of the password kept in a safe place in case of an emergency. Make sure that access to the data does not depend on the availability of a single person.

Data shall be stored in commonly known file formats or the file format shall be documented properly.  This is especially important for data that is intended to be archived for later reference.

#### Confidential Data

If you need to store confidential data on a computer you may consider encrypting the file.  However, be aware that there are at least two risks to encrypted files.  Firstly, if an encrypted file becomes damaged, there is no way to recover the original file even in part. Secondly, if the passphrase protecting the file gets lost (e.g., because the single person knowing the passphrase is not at hand) there is no access to the original file.  Instead of encrypting a file it may be better to store the data in a separate place (and make sure that a proper backup is arranged).

In any case make sure that access to the data does not depend on a single person.  For encrypted files you may either deposit the passphrase in a safe place or you may use public key encryption to encrypt the file with multiple keys.

#### Private Data

ETH allows the use of its IT infrastructure for private use as long as regular ETH work is not adversely affected.  Be aware that an administrator may gain access to your private data in the course of regular maintenance.  If you want to prevent that, we recommend that you buy some storage device (e.g., an USB memory stick) to keep your private data separate.  If you choose to encrypt your private data the same risks apply as mentioned above.

#### Mobile Computers

During the last few years laptops have become increasingly common due to their convenience.  They are especially useful for presentations and for field work.  However they pose additional risks for the loss of data.  There is a higher probability that a mobile computer gets stolen.  Additionally, they may not be at the right place when access is needed.

You should therefore make sure that all relevant data is regularly copied to some file server which is readily accessible and which has a working backup strategy set up.  You should also be aware that with mobile computers it is the responsibility of the owner to make sure that the operating system and applications are up-to-date.  Computers which are used in different environments suffer from an additional risk of being attacked by malicious hackers.

#### Private Computers

Analogous to the case for private data, private computers may be used at ETH as long as they produce no negative impact on the regular work and infrastructure of ETH.  Additionally, you need to make sure that no ETH data is solely stored on private computers.  In general, you should use ETH equipment for ETH work.  If you use your private computer for ETH related work, make sure that all ETH data is regularly copied over to an appropriate file server much like with mobile computers mentioned above.

#### Summary

* Keep your personal account private, do not share your password
* Use group accounts and/or proper permissions to share data between individuals
* Single Point-of-Failure Computer: make sure all data is stored in places where proper backup is working
* Single Point-of-Failure Human: make sure that administrative passwords can be retrieved in case of an emergency
