Windows Remote Desktop Server Login
=============================

We operate a Windows 2022 Remote Desktop Server for D-PHYS and D-BAUG IGP users. It allows also to login from a non-Windows computer and use Windows operating system with the remote desktop application.

The intention of this Remote Desktop Server is to offer native Windows applications for users who are working on a non-Windows system.

General Notes
-------------

* Printers have to be configured once according to the [[readme document|printing/how_to_add_an_id_printer_on_windows/]].
* There is the possibility to change your keyboard layout in the systray area.
* We have installed a basic set of applications:
    * MS Office
    * MS Visio
    * MS Project
    * Adobe Applications (Acrobat Pro...)
    * FileMaker
    * Firefox
    * Thunderbird
    * VLC Player
    * 7-ZIP
    * Putty
    * WinSCP
    * The Gimp
    * Chrome
    * Edge
    * Element
    * Citrix Workspace App
    * Mathtype
    * Corel Draw
    * MsysGIT/TortoiseGIT
    * Inkscape
    * TightVNCViewer
    * VS Code Editor
    * and some more
    * [please contact us](mailto:isg@phys.ethz.ch) if you think you're missing an application of general interest.

Using Windows  RemoteApp
-------------

We support Windows RemoteApp for Windows and Mac clients. You can launch virtual applications from the server that appears on your computer as if it were installed locally, but in reality, they are running on the remote server.

See [here](/services/using_remoteapp) for instructions about how to use it.


RDS login from Windows
------------------

To connect to the Remote Desktop Server, just follow these simple steps:

* Run `mstsc.exe`
* Enter computer name: `winlogin.phys.ethz.ch`
* Enter the D-PHYS username `ad\your_dphys_username`

If needed, you will find [here](/windows/connect_to_terminalserver) more details.

RDS Login from macOS
-------------------

See [[here|osx/configuring Microsoft Remote Desktop on Mac]] for details.

Login from Linux/Unix
---------------------

You can also access the Windows Remote Desktop Server from any regular Linux computer using xfreerdp. Just run this command:

```bash
xfreerdp /d:ad /v:winlogin.phys.ethz.ch /u:${USER} \
    /sec:tls /cert-ignore /rfx /compression /f /bpp:24 +fonts
```

* Exit full-screen: Ctrl-Alt-Enter

Consult the manpage of xfreerdp (`man xfreerdp`) for different command-line options.

Login from outside of the D-PHYS network
----------------------------------------

The Windows RDS Server is only reachable from inside the D-PHYS network but you can tunnel your Remote Desktop session with VPN or SSH.

### Connect by VPN

The recommended and easiest method is to start [[VPN|services/how_to_use_vpn]] before connecting to our Remote Desktop Server.

### Alternative method using ssh tunnel

The details are explained in our [[documentation/ssh tunnel#windows-rdp-via-ssh-tunnel]] documentation.
