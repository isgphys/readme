Hostnames of our services
=========================

These are the official hostnames you should use when accessing one of our services. All other names could change without notification.

<!-- markdownlint-disable MD056 -->

| Hostname                                           | Service Description                               | More information                            |
|----------------------------------------------------|---------------------------------------------------|---------------------------------------------|
| readme.phys.ethz.ch                                | D-PHYS IT Documentation                           |                                             |
| isg.phys.ethz.ch                                   | D-PHYS ISG Website/News                           | [[ISG|services/contact]]                    |
| login.phys.ethz.ch                                 | Linux Terminal Server                             | [[Readme|documentation/ssh]]                |
| winlogin.phys.ethz.ch                              | Windows Remote Desktop Server                     | [[Readme|services/windows_terminal_server]] |
| mail.phys.ethz.ch                                  | Outgoing mailserver                               | [[Readme|mail/how_to_use_email]]            |
| imap.phys.ethz.ch                                  | Mail over IMAP                                    | [[Readme|mail/how_to_use_email]]            |
| pop.phys.ethz.ch                                   | Mail over POP3                                    | [[Readme|mail/how_to_use_email]]            |
| webmail.phys.ethz.ch                               | Access to your mail over the web                  | [[Readme|mail/how_to_use_email]]            |
| home.phys.ethz.ch                                  | Access to your home directory                     | [[Readme|storage/access]]                   |
| group-data.phys.ethz.ch                            | Access to groupdrives                             | [[Readme|storage/access]]                   |
| restore.phys.ethz.ch                               | Backup                                            | [[Readme|backups/]]                         |
| debian.ethz.ch, ftp.ch.debian.org                  | [Debian](http://www.debian.org/) Mirror           |                                             |
| wiki.phys.ethz.ch                                  | Wikis                                             |                                             |
| registration.phys.ethz.ch                          | Reactivate network registration of your computers | [[Readme|network/]]                         |
| sql.phys.ethz.ch                                   | SQL Server (database only)                        |                                             |
| sqlweb.phys.ethz.ch                                | SQL Server (for webshares and websites)           | [[Readme|web/webshares]]                    |
| fm.phys.ethz.ch                                    | FileMaker Server                                  |                                             |
| time1.ethz.ch, time2.ethz.ch                       | NTP Timeserver                                    |                                             |
| ldap1.phys.ethz.ch                                 | LDAP Authentication Cluster Server 1              | [[Readme|account/ldap]]                     |
| ldap2.phys.ethz.ch                                 | LDAP Authentication Cluster Server 2              | [[Readme|account/ldap]]                     |
| ldap3.phys.ethz.ch                                 | LDAP Authentication Cluster Server 3              | [[Readme|account/ldap]]                     |
| admin.phys.ethz.ch                                 | User and MAC Address Administration               |                                             |
| password.phys.ethz.ch, passwort.phys.ethz.ch       | Change password of your D-PHYS account            |                                             |
| account.phys.ethz.ch                               | Manage your D-PHYS account                        |                                             |
| [influxdb.phys.ethz.ch](https://influxdb.phys.ethz.ch) | InfluxDB Service                              | [[Readme|documentation/influxdb]]           |
| [grafana.phys.ethz.ch](https://grafana.phys.ethz.ch)   | Grafana Service                               | [[Readme|documentation/grafana/]]           |
| matrix.phys.ethz.ch                                | Matrix homeserver                                 | [[Readme|chat]]                             |
| chat.phys.ethz.ch                                  | Web-based chat client (Matrix/Element)            | [[Readme|chat]]                             |
