D-PHYS Lab Reservation System
=============================

In order to assist with the logistical challenges of post-lockdown lab revival, we have installed a lab reservation system that allows to avoid contacts by making sure labs and offices are only staffed within acceptable limits. The web-based interface is called [Nemo](https://github.com/usnistgov/NEMO), developed by [NIST](https://www.nist.gov/) and already in use in [FIRST](https://first.ethz.ch/).

Our Nemo instance can be found at [reservation.phys.ethz.ch](https://reservation.phys.ethz.ch).

This is the nemo user documentation. Help for local Nemo admins can be found [[here|nemo_admin]].

For the regular lab user, the two most important Nemo functions are **reservations** and **lab use**. Both can be accessed via the _Calendar_ icon on the Nemo front page.

Rooms/experiments (or _Tools_ as they're called in Nemo) are ordered by building/floor/room and can be browsed in the left column of the calendar tool:

[[!img /media/nemo_cal1.png size="450x"]]

It is important to understand the logic when navigating this tree: the checkboxes in front of each experiment select which items are shown in the calendar while a click on an experiment's name (rendering it gray) determines which is your selected experiment for making reservations. This allows you to quickly reserve experiments while having an overview of your group's tool situation. Having selected the right experiment, you can then **reserve** it by simply selecting the appropriate time slot in the calendar.

Now your group will be able to see that you made this reservation during which time this tool will be unavailable to them. The second important step is logging **tool usage**, which also takes care of the problem of being alone in labs with 'dangerous' equipment (liquid gases etc). Select _Tool control_ from the top menu to get the following interface:

[[!img /media/nemo_cal2.png size="450x"]]

When you're ready to use the experiment on-site, click 'Start using this tool'. Make sure to select the correct tool in the left column.

Once you're finished performing your measurements, log out of the experiment again:

[[!img /media/nemo_cal3.png size="450x"]]
