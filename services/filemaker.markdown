FileMaker Databases
===================

Installing the FileMaker client
-------------------------------

Users of non-managed computers are requested to download and install FileMaker Pro 19.x from [IT Shop](https://itshop.ethz.ch/). Alternatively, you may use the software on our [Windows RDP Server](/services/windows_terminal_server/).

Connecting to our FileMaker server
----------------------------------

The name of our FileMaker server is `fm.phys.ethz.ch`. You may have to enter it manually when you first start your client software, to see the list of available databases.

NB: Even though the screenshots were taken on a Windows client, the same procedure also applies to macOS. But the menu is called "**Ablage**" instead of "**Datei**".

Choose "**Datei**" -> "**Hosts**" -> "**Hosts anzeigen...**"

[[!img /media/filemaker/filemaker18_remoteoeffnen_1.png size="400x"]]

add a host at the plus sign and fill in `fm.phys.ethz.ch` as **Internet-Adresse Host**.

[[!img /media/filemaker/filemaker18_remoteoeffnen_2.png size="400x"]]

**Choose** your desired database from the list and press "**OK**" button

[[!img /media/filemaker/filemaker18_remoteoeffnen_3.png size="400x"]]

Finished
