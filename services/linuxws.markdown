Managed Linux Workstations
==========================

[[!img /media/dphys-tux.gif]]

Our Managed Linux Workstations are now based on [Debian/GNU Linux](http://www.debian.org/).
You can find an [[overview of the most important changes|/linux/workstation/debian#changes]]
since our previous release with Ubuntu.

Several Readme offer more information:

[[!map pages="linux/workstation/*"]]
* [[linux/recurring_jobs]]
* [[linux/how_to_handle_long_running_jobs]]
* [[linux/software_on_the_d-phys_linux_computers]]
* [[linux/how_to_use_usb_harddisk_or_usb_memorysticks]]
* [[restore home or groupshare on linux|backups/restore]]

The general [[services/workstations]] page shows you the locations of the publicly accessible rooms equipped with our Linux workstations.
