Other chat & collaboration software
===================================

At the D-PHYS we use [[Element]]/[[Matrix]] for internal (and external) communication (see [[/chat]]). Some alternatives are listed here.

Microsoft Teams
---------------

Informatikdienste provides Microsoft Teams, which tries to achieve the same goal but which we neither recommend nor support in D-PHYS (details [here](https://unlimited.ethz.ch/pages/viewpage.action?pageId=29236528)). Also read the accompanying [[license agreement|documentation/microsoft365]].

Zoom
----

A video conferencing solution. See [[here|documentation/zoom]] for details.
