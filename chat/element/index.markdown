Element
=======

While there are many [clients](https://matrix.org/clients/) available free to use to connect to our Matrix homeserver,
we recommend to use the flagship app [Element](https://element.io/) which has the most [features](https://element.io/features).
Please read on below to get started connecting with Element or refer to the official [help](https://element.io/help).

[[!img media/chat/element_full_example.png size="450x"]]

First steps
-----------

- Customize your [[notification|/chat/element#notifications]] levels across the app and per room
- Reduce the notification level of chatty rooms like the Helpdesk to 'Mentions & Keywords'.
- Keep level 'Use default' for the News and Status room, where only moderators are allowed to send messages.
- Explore more public rooms in the [public room directory](https://element.phys.ethz.ch/#/directory) via the 'Explore rooms' button next to the search field.

[[!img media/chat/element_explore.png]]

It shows all published rooms on our server. Explore or add other servers using the drop-down menu:

[[!img media/chat/element_public_room_directory.png size="450x"]]

- Create direct message (DM) chats (`+` next to **People**)
- Create rooms for group chats (`+` next to **Rooms**)
- [[Set up encryption|/chat/element/e2ee]] after joining your first encrypted room

Note that after having created a private room, you will have to invite people into this room.
In certain cases a public room might make sense (e.g. to publicly distribute information about a software project) - in this case,
please read on below ([[rooms|/chat/element/#rooms]]).

If you want to invite a number of people to a room, please see [[here|addmembers]].

Documentation
-------------

- [[Connect with Element (Web, Desktop, App)|/chat/element/connect]]
- [[Connect with Element Android|/chat/element/android]].
- [[Element FAQ|/chat/element/faq]] (frequently asked questions)
- [[End-to-end encryption|/chat/element/e2ee]]
- [[Element security|/chat/element/security]]
- [[Invite many users to join a room or group|/chat/element/addmembers]]
- [[Use spaces|/chat/element/spaces]] (groups)
- [[Use integrations|/chat/element/integrations]] (widgets)
- [[Moderate rooms|/chat/matrix/moderation]]

Notifications
-------------

Element has intelligent notifications. Customize notifications to suit your priorities.
Receive notifications whenever anyone mentions your name, and define keywords to trigger if you're following a specific topic.

Element allows you to customize your notifications at two levels: across the app and per room.
You can configure how you will be notified for given events by default in the Notifications section of your Settings
(accessible from the drop down menu next to your name in the top left corner of the web/desktop app).

The per room notification settings can be:

- **Use default**: Use the defaults configured in account settings
- **All messages**: Get notifications on all messages
- **Mentions & Keywords**: Only get notified about messages containing your name or keywords
- **None**

[[!img media/chat/element_notifications.png]]

### How to mention someone

If you have an important message for someone or simply want them to know that your message in a room is for them, you can mention them by name.
Just start typing their name and use tab-completion or alternatively start typing the `@` symbol, followed by their name and select the corresponding user.
The special `@room` notification target can by used by room moderators to send a notification to all members.

Keyboard shortcuts
------------------

You can use keyboard shortcuts to use Element. Go to **Settings** > **Help & About** > **FAQ** > **Keyboard Shortcuts** or use the shortcut:

`Ctrl`/`Cmd` + `/`

To use commands just start typing a `/` and a list of available commands will show up. Example usage:

```
/rainbow I love rainbows
```

To send emojis in Element (Web, Desktop), type a colon `:` followed by the name of the emoji. Use the arrow keys to select one. Example:

```
:thumbsup:
```

Hint: If you hover over a message in a room, choose **React** (smiley icon), which will present an emoji keyboard with their names.

Text formatting (Markdown)
--------------------------

Element uses [CommonMark](https://commonmark.org/) to format text.
Refer to the [CommonMark Help](https://commonmark.org/help/) to get an overview of the syntax or follow the [tutorial](https://commonmark.org/help/tutorial/).

Rooms
-----

To create a room, click in the plus (`+`) symbol next to rooms (see overview `3`). set a name and an optional topic.
Select **Make this room public** if you want to create a public room that is joinable by anyone in the Matrix network without an invitation (requires to set a [[room address (alias)|/chat/matrix/#room-aliases]]).

**If you create room addresses, please use a reasonable prefix!**
We recommend to use your group abbreviation, followed by a dash (`-`), followed by your preferred name.
Example: `#isg-status:phys.ethz.ch`

At this point it is also possible to disallow users on other homeservers from joining the room.
**This cannot be changed later and is not recommended by ISG!**
Please [[contact us|/services/contact]] for more info and alternatives.

Each room has a globally unique ID like `!aTvivYGclTcDSRxPjp:phys.ethz.ch` (see *Settings* > *Advanced* > *Internal room ID*).

Rooms are decentralized, which means they could possibly exist on many servers. This depends on whether users from other domains joined the room.
Permissions in the room are controlled by the room itself, depending on what permissions you assign to other users.
This also means that if you lose access to a room or demote yourself, even server admins (ISG) won't be able to help you.

### Security & Privacy

Rooms are either **private** (only invited users may join) or **public** (anyone may join freely, even users from other homeservers).
To configure the room, click on the 3 dots that appear next to the room name (or right-click) and then **Settings**:

You may also modify who can read the room history. By default a newly joined user can read the whole history.

In encrypted rooms newly joined users cannot decrypt past messages.

### Roles & Permissions

Every action in a room (like sending messages or inviting users) requires a permission level. Permission levels are represented by a number between `0-100`. By default 3 named roles are defined with the following permission levels:

- Default: `0`
- Moderator: `50`
- Admin: `100`

The room creator is automatically set as a room admin (level `100`). By default, a user who joins the room is set to level `0`.
By default only room admins can edit these settings in **Settings** > **Roles & Permissions**.

For example if you would like to have a moderated room, where not anyone by default can send messages, set a power level of `1` or greater in **Permissions** > **Send messages**.

For important rooms we recommend to ensure that you have multiple users set as admin (level `100`). This is just in case some admin looses access to their Matrix account and to prevent loosing the ability to change room settings in that case. If there is at least one local (`@user:phys.ethz.ch`) admin in the room, then server admins (ISG) can set another local user as admin.

### Modifying power levels

Any user can grant other users elevated power levels up to his own level. Any user who has a power level higher than another user can lower the other users power level. Any user can lower his own power level.

Please note that these rules also mean, that an admin cannot lower the powerlevel of another admin or remove another admin from the room. And admin must do this themself before they leave ETH/D-PHYS. Therefore please clarify and communicate such responsibilities in advance.

Open the user list in the right pane to view power levels. Select a username to modify the power levels:

Either select a named role from the drop-down menu or set a custom power level in the highlighted box. The user shown here has been effectively muted in this room by a negative power level of `-1`. Room admins may also click on **Mute** under **Admin Tools**, which has the same effect. They may also kick or ban users. All users can ignore other users, which hides all messages from that user for you.

### How to make rooms public and discoverable

To create a new public room, see above ([[rooms|/chat/element/#rooms]]). To change an existing room from private to public, first always make sure it is not an encrypted room. A public room should never use encryption. Then go to **Room Settings** > **Security & Privacy** > **Who can access this room?** and set it to public: **Anyone who knows the room's link, apart from guests**. The room is now public. Anyone who knows the room ID can join it.

To make it easier to share the room with others or link to it on a website, we can create a room alias in the form of:

```
#group-roomalias:phys.ethz.ch
```

Create an alias in **Room Settings** > **General** > **Local Addresses**:

[[!img /media/chat/riot_room_alias_create.png size="450x"]]

To make the room discoverable we can publish it in the public room directory.
Go to **Published Addresses**, select your new address as the **Main address** and publish it:

[[!img /media/chat/riot_room_alias_publish.png size="450x"]]

[[Share a link|/chat/matrix#share-links]] to your room alias.

### How to shut down (delete) a room

Since rooms are decentralized, this is not entirely possible. However, you can shutdown a room cleanly, which will make it inaccessible and will allow our server to purge it from its database:

- Make the room private (invite only)
- Unpublish the room (if it was published)
- Remove all local addresses (aliases)
- Kick all users or ask them nicely to leave
- Leave the room
