# Spaces

Spaces are a new feature in Matrix/Element to group rooms and build communities.
Read more in the [Spaces blast out of beta](https://element.io/blog/spaces-blast-out-of-beta/) blog post.

[[!img /media/chat/element_spaces1.png alt="Element spaces" size="300x"]]

Forming hierarchies of spaces as shown above is still in beta test with some features missing.
We recommend sticking with top-level spaces for now.

Read more about [[spaces|/chat/matrix#spaces]].

## Types

There are 2 types of spaces:

- **Public space**: Open for anyone, best for communities
- **Private space**: Invite only, for teams or for yourself to organize rooms

## Creating spaces

Click on the `+` (plus) icon in the space panel on the left and choose a public or private space:

[[!img /media/chat/element_spaces3.png alt="Element spaces" size="260x"]]

### Public spaces

Public spaces can be joined by anyone without an invite. They also require to set an address
in the form of `#group-spacealias:phys.ethz.ch` like it is done for public rooms.

### Private spaces

If you choose a private space, you will be asked who you are working with - just ignore this
and continue with **Managing spaces** below.
We recommend not to try inviting people via email address, since this may not work as expected.

## Restricted rooms

Within a private space it is now possible to have rooms that restrict access to **space members**.
This means members of your space can join without an invite and all others need an explicit invite.

Read more about [[restricted rooms|/chat/matrix#restricted-rooms]].

## Managing spaces

To add rooms and invite people, right click on the space and select **Manage & explore rooms**.
This will open the space view, from where you can **add** rooms and **invite** others.

You can add existing rooms or directly create new rooms in the space.
This will by default create a restricted room (visible to space members):

[[!img /media/chat/element_spaces4.png alt="Element spaces" size="260x"]]

If you add an existing room and would like to set the access to restricted,
open **Room settings** > **Security & Privacy** > **Access** and select **Space members**.

[[!img /media/chat/element_spaces5.png alt="Element spaces" size="400x"]]

### Room upgrades

Switching rooms to restricted access currently requires a new room version.
Upgrading an existing room will result in a new room, the old room and messages will be hidden.
Existing members can automatically be invited to the new room.
The older room and messages can be accessed via **Room Settings** > **Advanced** > **Room version**.

[[!img /media/chat/element_spaces2.png alt="Element spaces" size="400x"]]

Read more about [[room versions|/chat/matrix#room-versions]].

## Migrating communities

The old communities are hidden, but community admins can migrate them to spaces.

Open **Settings** > **All settings** > **Preferences** > **Communities** > **Show my communities**
and click on **Create Space**, which will guide you through the migration process:

[[!img /media/chat/element_spaces6.png alt="Element spaces" size="400x"]]

For further steps see **Managing spaces** above.

## Need help?

Join [#matrix:phys.ethz.ch](https://m.ethz.ch/#/#matrix:phys.ethz.ch), we are happy to assist.
