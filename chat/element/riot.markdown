# Domain change

## Old name: Riot

Maybe you have missed it, Riot is now called **Element**.
As [announced last year](https://isg.phys.ethz.ch/2020/10/09/update-on-matrix-chat/),
the old domain `riot.phys.ethz.ch` has been shut down.

For any kind of problem we are happy to assist you,
preferably please contact us in the dedicated
[D-PHYS Matrix/Element support room](https://m.ethz.ch/#/#matrix:phys.ethz.ch)

## New name: Element

Continue chatting here: [element.phys.ethz.ch](https://element.phys.ethz.ch)
