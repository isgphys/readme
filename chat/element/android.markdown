Element (Android)
=================

[Element](https://github.com/vector-im/riotX-android) is an Android Matrix client.

[<img src="/media/chat/googleplay.svg" alt="Get it on Google Play">](https://play.google.com/store/apps/details?id=im.vector.app)
[<img src="/media/chat/fdroid_40px.png" alt="Get it on F-Droid">](https://f-droid.org/packages/im.vector.app/)

Signing in
----------

Select: **I already have an account**

[[!img /media/chat/element_android_signin0.jpg size="450x"]]

Select a server: **Sign in with Matrix ID**

[[!img /media/chat/element_android_signin1.jpg size="450x"]]

Enter your [[MXID|/chat/matrix/#terms]] and your D-PHYS password  
Our homeserver is auto-discovered: `https://matrix.phys.ethz.ch`

[[!img /media/chat/element_android_signin2.jpg size="450x"]]

Select: **Sign In**

[[!img /media/chat/element_android_signin3.jpg size="450x"]]

Verify your login using one of the available methods.


Verifying the device via QR-Code
--------------------------------

Your Element (Web, Desktop) should now show an unverified login.

Click **Review**:

[[!img /media/chat/riotx_riot_verify1.png ]]

In Element (Android) select **New Sign In (Complete Security)**:

[[!img /media/chat/riotx_signin6.png size="450x"]]

In Element (Android) select **Scan their code**:

[[!img /media/chat/riotx_signin7.png size="450x"]]

Scan the QR Code shown in Element (Web, Desktop):

[[!img /media/chat/riotx_riot_verify2.png size="450x"]]

Confirm the green shield in Element Android:

[[!img /media/chat/riotx_signin8.png size="450x"]]

Confirm the green shield in Element (Web, Desktop):

[[!img /media/chat/riotx_riot_verify3.png size="450x"]]
