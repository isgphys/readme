Frequently asked questions (FAQ)
================================

### This room is annoying, how to control notifications?

If you receive unwanted notifications for some room (e.g. our Helpdesk room), you can control the verbosity level in the room settings:

[[!img media/chat/riot_notifications.png size="300x"]]

See also [Element notifications](https://readme.phys.ethz.ch/chat/element/#notifications)

### Why are my messages not encrypted?

This means your messages do not use [[end-to-end encryption (e2ee)|/chat/element/e2ee]].
Your connection to our server is still secure and [[encrypted using TLS|/chat/element/security/#encryption]].

### Remove an admin from the room who left ETH

This is not possible due to how the Matrix protocol works.
Room admins power levels can only be lowered by themselves.
An admin cannot change the power level of another admin or remove another admin.
Please clarify and communicate such responsibilities in advance.
See also [[/chat/element/#modifying-power-levels]].

If a user left ETH and their D-PHYS Account is blocked, then their Matrix account is locked
and they cannot effectively access the room anymore.
However, if the user's account is later reactivated (should they return or due to collaboration),
they would have access again. If you have security concerns we recommend then to create a new room.
