Security
========

Element is similar to email regarding security precautions you should take (see [Use email with care](https://monster.phys.ethz.ch/en/#/use_email_with_care)).
Anyone on the Internet can contact you, send you spam and [viruses](https://monster.phys.ethz.ch/en/#/prevent_malware_infection) or try a [phishing attack](https://monster.phys.ethz.ch/en/#/spot_phishing_emails).

However, Element has advanced features to confirm your correspondent's identity, which makes it much more secure than email.

Identity
--------

The 'displayname' is an arbitrary name that users can set as they wish. This is the name you will see in Element's message history.
The 'MXID' (Matrix ID) is better suited to confirm an identity. It has the following format:

```
@localpart:domain
```

You can check it by hovering over the avatar (picture) next to the name in the message history.
Click on the avatar, which will open the right sidebar with additional information about that user:

[[!img /media/chat/riot_security_identity.png size="450x"]]

For D-PHYS users, the following rules apply:

- 'localpart' is the D-PHYS Account name
- 'domain' always is `phys.ethz.ch`

There is still the risk that the account was hijacked (guessed or stolen password).
To further increase security, see [[user verification|/chat/element/e2ee/#verifying-other-users-optional]].

Encryption
----------

Our server uses latest Transport Layer Security (TLS) standards. This encrypts all traffic from your client to the server.
You may still see `Send a message (unencrypted)...`. This means that the message you send will not be using [[end-to-end encryption (e2ee)|/chat/element/e2ee]].
If [[e2ee|/chat/element/e2ee]] is enabled (the default, starting from Element version 1.6.0), your messages and files are encrypted before they leave your device, and stay encrypted until they reach the other participants' devices. End-to-end encrypted messages can only be read by the participants in the conversation.

Federation (where is my data stored?)
-------------------------------------

See [[how federation works|/chat/matrix/#federation]]. Rooms are [[decentralized|/chat/element/#rooms]] and could be synced to other Matrix homeservers.
As long as all participants in a room are on our server (domainpart of MXID = `phys.ethz.ch`), the message history is on our server only.
If D-PHYS external participants (from other domains) join the room, the message history will be synced with the homeservers that holds their account.
You can control that by setting the room to **invite only** and carefully choose who may join.
