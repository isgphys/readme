Add members in Element
======================

How to invite many users to join a room or space
------------------------------------------------

If you want to invite a large number of users into rooms or a space, the fastest way to do that is to prepare a list with all D-PHYS usernames in the following format:

```
@username1:phys.ethz.ch
@username2:phys.ethz.ch
@username3:phys.ethz.ch
```

You can then copy paste that list into the invite field.

If you want to generate such a Matrix ID user list based on an LDAP group (groupshare), you can use the following command on any D-PHYS managed Linux workstation (for example `login.phys.ethz.ch`):

```bash
members <groupname> | sed "s,\ ,\n,g" | awk '{print "@" $1 ":phys.ethz.ch"}'
```

Just replace `groupname` with the LDAP group

How to generate a member list from an existing room
---------------------------------------------------

To do this you need to use the Client-Server API using an `access_token`.
Please see [[How to use the Client-Server API|/chat/matrix/api]] on how to obtain an access token.

Then you need the room ID. Go to **Room settings** > **Advanced** > **Internal Room ID**:

[[!img /media/chat/riot_room_id.png size="450x"]]

Now using `curl`, your access token and the room ID, we can query the HTTP API to get our member list:

```bash
curl -s -X GET -H "Authorization: Bearer ${token}" 'https://matrix.phys.ethz.ch/_matrix/client/r0/rooms/!GYTZNIMhOdEHvEzGrR:phys.ethz.ch/members?membership=join&not_membership=leave' | grep state_key | grep -oE '@[^:]+:[^"]+'
```
