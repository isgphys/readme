Connect to Matrix with Element
==============================

There are several ways in which you can use Element. In all cases:

- Log in using: `@username:phys.ethz.ch`
- Where `username` is your [D-PHYS account](https://account.phys.ethz.ch/info)
- Use your D-PHYS password

Note: All Matrix clients should automatically discover the correct homeserver,
which is `phys.ethz.ch`. The direct server URL is `https://matrix.phys.ethz.ch`.

See [[Set up encryption|/chat/element/e2ee/#set-up-encryption]] for the next steps
after you connect to an end-to-end encrypted room for the first time.

Web app
-------

The easiest way to use Element is to point your (modern) web browser
(last 2 major versions of Chrome, Firefox, and Safari on desktop OSes)
to [element.phys.ethz.ch](https://element.phys.ethz.ch).

Note: The web app does not support searching in encrypted rooms, use the desktop app instead.

Optionally you can [configure](https://github.com/vector-im/element-desktop#user-specified-configjson)
your app using our [settings](https://element.phys.ethz.ch/config.json).

Mobile app
----------

Get Element from
[Google Play](https://play.google.com/store/apps/details?id=im.vector.app) or
[App Store](https://itunes.apple.com/us/app/vector.im/id1083446067):

[<img src="/media/chat/googleplay.svg" alt="Get it on Google Play">](https://play.google.com/store/apps/details?id=im.vector.app)
[<img src="/media/chat/appstore.svg" alt="Download on the App Store">](https://itunes.apple.com/us/app/vector.im/id1083446067)

See also [[Connect with Element Android|/chat/element/android]].

Desktop app
-----------

There is a standalone [desktop app](https://element.io/get-started) for Windows, macOS and Linux.

We ship it pre-installed on our managed [[Linux|/services/linuxws]] and Windows workstations.
On macOS you can install the app yourself (no admin rights needed).

Labs
----

Element has some [experimental features](https://github.com/vector-im/element-web/blob/develop/docs/labs.md)
called 'Labs', which are for advanced users and developers. By default these features are hidden.
When enabled, they will appear in **Settings** > **Labs**.

Warning: **These features may break or get removed at any time without notice! Use at your own risk.**

A Labs enabled Element Web instance can be used at [labs-element.phys.ethz.ch](https://labs-element.phys.ethz.ch).
Alternatively you can [configure](https://github.com/vector-im/element-desktop#user-specified-configjson)
your desktop app using our [labs settings](https://labs-element.phys.ethz.ch/config.json).

The following configuration snippet enables the labs features menu item:

```json
{
    "show_labs_settings": true
}
```

### Maths support

One of these features is 'Render LaTeX maths in messages', which uses [[KaTeX|/documentation/katex]] (LaTeX subset) under the hood.
To write LaTeX use the `$` character as delimiter: Inline-math is wrapped in single-`$`'s and blocks in double-`$`'s.

**This feature is enabled by default in all [D-PHYS Element](https://element.phys.ethz.ch) installations.**

On self-managed Element Desktop installations you have two options to enable it:

- Use the [D-PHYS Element Labs configuration](https://labs-element.phys.ethz.ch/config.json) (see above) and turn the feature on manually in **Settings** > **Labs**
- Use the [D-PHYS Element configuration](https://element.phys.ethz.ch/config.json), which has that feature enabled only.

The following configuration snippet force-enables the labs feature:

```json
{
    "features": {
        "feature_latex_maths": true
    }
}
```
