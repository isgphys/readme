# Integrations

Integrations are bots, bridges and widgets that you can add to a room in matrix.

## Integration manager

Our previous integration manager at `dimension.mbot.ethz.ch` is retired,
because the upstream project has been archived.

Please use commands/invite/kick as shown below.

## Bots & bridges

See [[bots|/chat/matrix/bots]] & [[bridges|/chat/matrix/bridges]].

## Widgets

A widget is an external web application that is embedded into your room as an iframe.
Any website could be embedded as a custom widget, although sometimes it is restricted by security settings.

To add a custom widget, type `/addwidget <url>`.

### Experimental widgets

**Collaborative online whiteboard and notepad**:

- [Create whitebophir whiteboard (simple, server-side storage)](https://c5.test-lxd.phys.ethz.ch/random)
- [Create excalidraw whiteboard (advanced, end-to-end encrypted)](https://c6.test-lxd.phys.ethz.ch/)
- [Create etherpad notepad](https://c7.test-lxd.phys.ethz.ch/)
- Copy the URL from your browsers address bar
- Select the room where you would like to add the whiteboard
- Open the integration manager and add a custom widget
- Paste the URL into the *Widget URL* field
- Set a widget name and click add

[[!img /media/chat/riot_integration_manager_whiteboard_widget.png size="450x"]]
