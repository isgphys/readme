End-to-end encryption
=====================

Element offers true end-to-end encrypted (E2EE) communication, meaning no-one else can eavesdrop on your conversations, not even server admins. Element uses the [best end-to-end encryption](https://matrix.org/blog/home/#privacy) available today.

Encryption is kept friendly with features like secure key backup, which allow you to recover your encrypted data even if you lose or break a device. Advanced features like verification highlight if a user's account may be compromised.

### What is Key Backup?

When key backup is enabled, your device will maintain a secure copy of its keys on our server. To ensure those keys can only ever be accessed by you, they are encrypted on your device, with a key that you either store yourself, or secure with a passphrase and upload to our server. It is important to understand that to protect your privacy your keys will never touch our systems unencrypted.

### What is a 'device'?

For historical reasons, when we say 'device' we don't mean your phone or your laptop - you actually create a new 'device' each time you log in on Matrix (and destroy it again when you log out).

### What does it mean to verify or trust a device in Element?

Element uses trust to represent an additional layer of security within the app, over and above username and password authentication.

If somebody is sending messages as Alice, we know that they have access to Alice's account - either they've logged in with Alice's username and password, or they're using a logged in session, perhaps on Alice's phone.

Usually, that somebody is going to be Alice. Unfortunately, in the real world, passwords can be guessed or sniffed and phones can be stolen. Element's trust mechanism is designed to mitigate this.

In Element, you can see every device that has joined an encrypted conversation. If a new and unexpected device joins, you can use device verification to check that it's really Alice. And if you suspect that a trusted device has fallen into the wrong hands, you can revoke that trust and remove its access to the ongoing encrypted conversation.

What is new in Element 1.6.0 (cross-signing)?
------------------------------------------

Instead of verifying and trusting all devices of your conversation partners, you just have to verify and trust other persons (accounts).
On the other hand, each person verifies and trusts their own devices.

With the update, new direct messages will be encrypted by default. Element will also suggest to enable encryption if you create a new private room.
If you do not want that, just unselect **Enable end-to-end encryption** on the **Create a private room** dialog to turn off encryption.
If the room to be created is intended to be a public room, then do not use encryption.
End-to-end encryption can never be disabled once it is enabled.

Key storage and Security Phrase/Key
-----------------------------------

End-to-end encryption in Element has to manage many encryption keys. All these keys are stored securely on our server.
In order to do that, an additional password is required to encrypt the key storage. The two passwords for Element are named as follows:

- **Account password**: This is your D-PHYS password
- **Security Phrase**: Another secure password

There is an additional safety net called the **'Security Key'**, which you can use to restore access to your encrypted messages if you forget your Security Phrase.
We recommend to keep a copy of it somewhere secure, like a password manager or even a safe.

**Important note: Should you forget your 'Security Phrase' and lose your 'Security Key', your encrypted conversations could be lost!
Not even server admins can help you in that case.** 🙅

Resetting the server-side key backup
------------------------------------

Please refer to the procedure in the [element blog](https://element.io/blog/resetting-the-server-side-key-backup/).

### Secure passwords and password managers

The two passwords mentioned above should be different for maximum security and at the same time they should also be secure.
Please read our guide on [[how to handle passwords|/documentation/how_to_handle_passwords/]] to learn methods for creating secure passwords that are also easy to remember.
If you have not used a [[password manager|/documentation/how_to_handle_passwords/#using-a-password-manager]] so far, we highly recommend it.
It can be used as a virtual safe on your computer to store all your passwords securely (using an encrypted file).
Remember to also make a backup of this encrypted password store.

Set up encryption
-----------------

When joining en encrypted room for the first time, you are asked to set up encryption.
The first step is to set your 'Security Phrase':

[[!img /media/chat/riot_e2ee_boot1.png size="450x"]]

After confirming the password, it will show your Security Key:

[[!img /media/chat/riot_e2ee_boot2.png size="450x"]]

Copy it and store the key in a safe place as mentioned above or as suggested on the next screen:

[[!img /media/chat/riot_e2ee_boot3.png size="450x"]]

Self-verification
-----------------

Should you have used Element on other devices without logging out, you will be asked to verify these sessions:

[[!img /media/chat/riot_e2ee_verify1.png ]]

You can try that right now or just click **Later** (recommended).

You may also review and cleanup your existing sessions in **Settings** > **Security & Privacy** > **Sessions**.
The example below will delete and old session, that is no longer needed.
**If this session is still in use on another device, it will be logged out immediately.**

[[!img /media/chat/riot_e2ee_verify2.png size="450x"]]

To verify your other sessions, select a **Room** > **Members** > **Yourname**:

[[!img /media/chat/riot_e2ee_verify3.png ]]

Here we just have one active session, that is already verified (green).
Should you have unverified sessions, they will be marked as untrusted (red):

[[!img /media/chat/riot_e2ee_verify4.png ]]

To verify the session, click on **Not trusted** and follow the on-screen instructions on both devices.
There are several methods for verification to choose from:

- [[QR-code|/chat/element/android/#verifying-the-device-via-qr-code]] (recommended with Mobile Apps)
- Emoji (recommended with Web, Desktop)
- Text

An emoji verification flow may look as follows:

[[!img /media/chat/riot_e2ee_verify5.png size="450x"]]

Compare the presented emojis on both devices and confirm if they match.
If they do not match, someone could be eavesdropping on your conversation (please [[contact us|/services/contact]]).

[[!img /media/chat/riot_e2ee_verify6.png size="450x"]]

If all is good, you will be presented a **VERIFIED** with a green shield:

[[!img /media/chat/riot_e2ee_verify7.png ]]

See also [[How to verify a device via QR-Code|/chat/element/android/#verifying-the-device-via-qr-code]]

Using end-to-end encryption
---------------------------

End-to-end encryption (e2ee) is enabled by default for new direct messages (DMs) and suggested (by default on) for new group conversations (rooms).
Encryption on existing DMs or rooms will not be enabled automatically. To enable it go to **Room Settings** > **Security & Privacy** > **Encryption** and enable **Encryption**:

[[!img /media/chat/riot_e2ee_encrypt1.png size="450x"]]

Depending on the encryption setting and the verification status, you will be presented a black, green or red shield next to the avatar:

[[!img /media/chat/riot_e2ee_encrypt2.png size="450x"]]

- **green**: Encrypted, user verified, all sessions trusted
- **red**: Encrypted, user verified, some **untrusted** sessions (this account could be compromised!)
- **black**: Encrypted, user unverified
- **no shield**: Unencrypted

Verifying other users (optional)
--------------------------------

Should you want to use e2ee with the highest possible security and privacy, you can verify other user accounts.
This is best done on another secure communication channel, where you are confident to verify the correct person.
The verification flow is similar to **Self-verification** (see above).
There should be a video available once the update is live.

To initiate it, **select a Room** > **Members** > **Another user** and click on verify:

[[!img /media/chat/riot_e2ee_encrypt3.png ]]
