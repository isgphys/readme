Chat & collaboration
====================

In order to facilitate secure real-time communication within D-PHYS and with the outside world,
we provide a [Matrix](https://matrix.org/) chat infrastructure.
The service can be used from modern web browsers (up-to-date versions of Firefox, Safari or Chrome based browsers),
mobile apps (Android, iOS) and standalone desktop applications (macOS, Linux, Windows).
Learn [[why we use Matrix|/chat/matrix/why]].

Matrix (protocol/network)
-------------------------

Matrix is an open source project that publishes the [Matrix open standard](https://matrix.org/docs/spec) for secure, decentralized, real-time communication.
Our Matrix [homeserver](https://github.com/matrix-org/synapse) for the domain `phys.ethz.ch` hosts your Matrix ID (`@id:phys.ethz.ch`) similar to an email address,
your account settings and secure [[end-to-end encryption|/chat/element/e2ee]] key backups, stores a copy of your rooms history and connects you to the whole Matrix network
including any other [bridged networks](https://matrix.org/bridges/) like IRC, Slack, Discord, Telegram and many more.

[[!img /media/chat/networks.png size="450x"]]

To access our homeserver you need [a Matrix client](https://matrix.org/clients/), similar as with email where you need a mail client like Thunderbird or Outlook.
Our recommendation is to use [[Element|/chat/element]], which is a glossy and feature-rich JavaScript app running entirely in your browser.
Examples for more clients (beta) include
[fluffychat](https://fluffychat.im/) (maths support),
[weechat-matrix](https://matrix.org/docs/projects/client/weechat-matrix),
[gomuks](https://matrix.org/docs/projects/client/gomuks),
[Quaternion](https://matrix.org/docs/projects/client/quaternion),
[Nheko](https://matrix.org/docs/projects/client/nheko-reborn)
or
[Fractal](https://matrix.org/docs/projects/client/fractal).
Talk directly to the HTTP+JSON [Client Server API](https://matrix.org/docs/api/) or use your preferred [SDK](https://matrix.org/sdks/).

Element (client)
----------------

Connect to our Matrix server using Element Web:

- Point your browser to [element.phys.ethz.ch](https://element.phys.ethz.ch/)
- Log in using your Matrix ID: `@username:phys.ethz.ch`
- Where `username` is your [D-PHYS account](https://account.phys.ethz.ch/info)
- Use your D-PHYS password

See also [[Connect with Element (Web, Desktop, App)|/chat/element/connect]].

Public rooms
------------

After the first login you will be auto-joined to these rooms:

- [#lobby:phys.ethz.ch](https://m.ethz.ch/#/#lobby:phys.ethz.ch) - discover others that are using Matrix
- [#isg-news:phys.ethz.ch](https://m.ethz.ch/#/#isg-news:phys.ethz.ch) - announcements and news
- [#isg-status:phys.ethz.ch](https://m.ethz.ch/#/#isg-status:phys.ethz.ch) - current IT system status (problems)

These rooms provide an excellent way for us to connect with you and inform instantly.
The News room contains an [RSS bot](https://m.ethz.ch/#/@rss:mbot.ethz.ch)
that sends all announcements from our [blog](https://isg.phys.ethz.ch/),
and provides an alternative to our [newsletter](https://isg.phys.ethz.ch/subscribe/).
If you do not wish to participate in these rooms, opt-out at any time by leaving.

**Please join** our spaces:

- [#isg:phys.ethz.ch](https://m.ethz.ch/#/#isg:phys.ethz.ch) - D-PHYS ISG managed rooms
- [#d:phys.ethz.ch](https://m.ethz.ch/#/#d:phys.ethz.ch) - D-PHYS Community rooms
- [#computing:phys.ethz.ch](https://m.ethz.ch/#/#computing:phys.ethz.ch) - D-PHYS Computing (Linux, CUDA)

[[Spaces|/chat/element/spaces/]] appear as icons in the left sidebar and group some rooms together.
They can be very helpful to organize large amounts of rooms or to discover new ones.
All rooms listed here will be grouped into those spaces once you join the space.

**Please join** our other rooms:

- [#matrix:phys.ethz.ch](https://m.ethz.ch/#/#matrix:phys.ethz.ch) - help & news for Matrix & Element
- [#influxdb:phys.ethz.ch](https://m.ethz.ch/#/#influxdb:phys.ethz.ch) - help & news for InfluxDB & Grafana
- [#web:phys.ethz.ch](https://m.ethz.ch/#/#web:phys.ethz.ch) - help & news for web hosting
- [#linux:phys.ethz.ch](https://m.ethz.ch/#/#linux:phys.ethz.ch) - about Linux workstations & software
- [#gpu:phys.ethz.ch](https://m.ethz.ch/#/#gpu:phys.ethz.ch) - about GPU & CUDA usage
- [#helpdesk:phys.ethz.ch](https://m.ethz.ch/#/#helpdesk:phys.ethz.ch) - for all other IT requests & help

Customize your [[notification|/chat/element#notifications]] levels across the app and per room.
Reduce the notification level of chatty rooms like the Helpdesk to 'Mentions & Keywords'.
Keep the default for the News and Status room, where only moderators are allowed to send messages.

More public rooms can be found in our [public room directory](https://element.phys.ethz.ch/#/directory)
via 'Search' > 'Public rooms'.

Documentation
-------------

The documentation for Matrix/Element is split into several pages.
Please read on in the following order to get started:

- [[Connect with Element (Web, Desktop, App)|/chat/element/connect]]
- [[Connect with Element Android|/chat/element/android]]
- [[Matrix|/chat/matrix]]
- [[Element|/chat/element]]
- [[Element FAQ|/chat/element/faq]] (frequently asked questions)
- [[Element end-to-end encryption|/chat/element/e2ee]]
- [[Element security|/chat/element/security]]
- [[Invite many users to join a room or group|/chat/element/addmembers]]
- [[Use spaces|/chat/element/spaces]] (groups)
- [[Integrations|/chat/element/integrations]] (widgets, bridges, bots)
- [[Matrix bots|/chat/matrix/bots]]
- [[Matrix bridges|/chat/matrix/bridges]]
- [[Use the Matrix Client-Server API|/chat/matrix/api]]
- [[Moderate rooms|/chat/matrix/moderation]]
- [[Other chat & collaboration software|other]]

Please note
-----------

- **It is not a file server**! While you can upload and share files, please do so with reason. Use it to share a PDF or graph with your colleagues, but do not send research data - that's what our [[group shares|storage]] are for.
- To chat with D-PHYS external people, you need to invite them using their Matrix ID (`@username:domain`). Except for Matrix users on the ETH homeserver (`staffchat.ethz.ch`), where you can use the search function as usual.

Features
--------

Matrix offers an extremely wide range of features and connectivity. The following list is currently implemented on the D-PHYS server and is considered to be stable:

- Basic chat functionality (instant messaging, push notifications)
- Private and public rooms (explore in the [public room directory](https://element.phys.ethz.ch/#/directory))
- Media uploads (3 GiB **initial** quota per user, max. 100 MiB per upload)
- Search the D-PHYS & ETH user directory (username, display name, email, phone number)
- End-to-end encryption and cross-signing ([[instructions|/chat/element/e2ee]])
- Federation with "the internet" and other ETH homeservers
- Integrations: widgets, bridges & bots ([[instructions|/chat/element/integrations]])
- Bots ([[gitlab integration|/chat/matrix/bots#gitlab]], [[reminders|/chat/matrix/bots#reminders]], RSS feeds and [[more|/chat/matrix/bots]])
- [[Webhook bridge|/chat/matrix/bridges#hookshot]] to send notifications via HTTP requests
- [[IRC bridge|/chat/matrix/bridges#heisenbridge]] to use Matrix as an IRC bouncer
- [[Slack bridge|/chat/matrix/bridges#slack]] or puppeting bridge
- Migrate Slack workspace to Matrix ([export your workspace](https://slack.com/intl/en-ch/help/articles/201658943-Export-your-workspace-data) and [[contact us|/services/contact]])
- Labs features enabled [Element Web](https://labs-element.phys.ethz.ch/) or [configure your Desktop App](https://labs-element.phys.ethz.ch/config.json)
- Native maths support (labs feature) [[enabled in D-PHYS Element|/chat/element/connect#maths-support]]
- Direct (1:1) voice/video calls
- Group web-conferencing using [[Jitsi|chat/jitsi]] (rooms with more than 2 members)
- Screen sharing (e.g. for helpdesk)
- [[Spaces|/chat/element/spaces]] (to form communities and teams or to organize rooms)

### Other features are either not tested as much or known to be buggy

- Shared whiteboard [[integrations|/chat/element/integrations]] (widget)

### Roadmap (probably in this order)

- ISG made bots

### Disabled features

- Add email or phone numbers to your profile (automatic discovery via LDAP)
- Invite users via email (but you may very well search by email)
