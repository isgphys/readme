# Bridges

For the full list of available bridges and to get help, join [#bridges:mbot.ethz.ch](https://m.ethz.ch/#/#bridges:mbot.ethz.ch).
New bridges and updates will be posted in this room.


## Hookshot

The source code for the bridge is available at [matrix-hookshot](https://github.com/matrix-org/matrix-hookshot) on GitHub.
Currently enabled features:

- Webhooks (inbound, outbound)

For a list of commands see the help:

```
!hookshot help
```

### Inbound webhooks

This section covers inbound webhooks.

Base URL: `https://hookshot.mbot.ethz.ch/webhook/<hook>`

1. Invite [hookshot](https://m.ethz.ch/#/@hookshot:mbot.ethz.ch) (`@hookshot:mbot.ethz.ch`) to your **unencrypted** room
2. Give the bot permissions (Moderator)
3. Send the message `!hookshot webhook <name>` (replace `<name>`)
4. The bridge will send you an URL in a private admin room (DM)

The bridge accepts POST or PUT requests. See [docs](https://matrix-org.github.io/matrix-hookshot/latest/setup/webhooks.html#webhook-handling) for details or the example below.

#### Example usage

Example JSON payloads:

```json
{"text":"Hello world!"}
{"text":"Hello **world**!"}
{"text":"Hello world!","html":"Hello <b>world</b>!"}
{"text":"Hello world!","username":"Jay Doe"}
```

Example using [httpie](https://httpie.io/):

```bash
http PUT "<url>" <<<'<payload>'
```

Example using `curl`:

```bash
curl --header "Content-Type: application/json" --data '<payload>' "<url>"
```

#### Remove webhooks

In the room where webhooks were configured (not admin room):

- List webhooks: `!hookshot webhook list`
- Remove webhook: `!hookshot webhook remove <name>`

### Outbound webhooks

**Experimental, deleting outbound hooks is not implemented. Use it wisely!**

1. Create a new unencrypted room
2. Use similar steps as for inbound webhooks. See `!hookshot help` for usage and refer to the [docs](https://matrix-org.github.io/matrix-hookshot/latest/setup/webhooks.html#outbound-webhooks).


## Webhook API (Grafana, Slack)

Base URL: `https://webhooks.mbot.ethz.ch/webhook/<api>/<hook>`

This API translates incoming webhooks into generic webhooks (hookshot). The source code is available on [ETH Gitlab](https://gitlab.ethz.ch/isgphys/webhook-to-matrix-hookshot) or on [GitHub](https://github.com/rda0/webhook-to-matrix-hookshot).

1. Create a generic webhook using hookshot (see above)
2. Use the `<hook>` URL part from hookshot with the base URLs listed below

Optional (but recommended): Disable URL Previews (in Room > Settings). Otherwise URL Previews may generate error
messages when previewing links sent by the webhook which require authentication.

### Grafana webhook notification

Base URL: `https://webhooks.mbot.ethz.ch/webhook/grafana/<hook>`

In [Grafana](https://grafana.phys.ethz.ch) ([Docs](https://readme.phys.ethz.ch/documentation/grafana/#sending-alerts-to-matrix)):

1. Create a contact point: **Alerting** > **Contact points** > **Add contact point**
2. Add a **Name** and select **Integration**: `Webhook`
3. Paste the url into the **URL** field

Optional: Select another notification template using a query parameter: `?template=<template>`.
Refer to the [configuration section](https://gitlab.ethz.ch/isgphys/webhook-to-matrix-hookshot#configuration)
for details.

### Grafana Slack notification (deprecated for Grafana)

**Do not use for Grafana alerts.** Use Grafana webhook notification instead.

Base URL: `https://webhooks.mbot.ethz.ch/webhook/slack/<hook>`

1. Create a contact point: **Alerting** > **Contact points** > **Add contact point**
2. Select **Integration**: `Slack`
3. Paste the url into the **Webhook URL** field
4. Optional: Set **Optional Slack settings** > **Username** to `Grafana`


## Heisenbridge

A bouncer-style Matrix IRC bridge. Can be used as an alternative to existing bridges using [matrix-appservice-irc](https://github.com/matrix-org/matrix-appservice-irc) which already bridge many [IRC networks](https://matrix-org.github.io/matrix-appservice-irc/latest/bridged_networks.html) to Matrix.

The source code is available at [hifi/heisenbridge](https://github.com/hifi/heisenbridge) on GitHub.

1. Start a DM with [heisenbridge](https://m.ethz.ch/#/@heisenbridge:mbot.ethz.ch) (`@heisenbridge:mbot.ethz.ch`)
2. Type `help` or `<command> -h` to get started


## Slack

Talk to [rda](https://m.ethz.ch/#/@rda:phys.ethz.ch) in [#bridges:mbot.ethz.ch](https://m.ethz.ch/#/#bridges:mbot.ethz.ch)
if you want to bridge your Slack channels to Matrix.

A fully functioning bridge can be tested in [#isg-slack-bridge:phys.ethz.ch](https://m.ethz.ch/#/#isg-slack-bridge:phys.ethz.ch).
