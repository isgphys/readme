# Room moderation

Please refer to [[our Element readme page|/chat/element/#rooms]] to learn the basics about room permissions and moderators.

## Moderating unwanted content or spam

If you are the administrator of a large room or if your room is public, you have some tools to remove unwanted content or block spammers.
Element provides an interface for some basics, like kicking or banning users.

Please refer to the [official documentation about room moderation](https://matrix.org/docs/guides/moderation) for more information.
It includes an example on how to use server ACLs to block malicious servers from interacting with a room (just be careful not to lock yourself out).
To learn more about ACLs or blocking other servers, refer to:

- [Banning servers from rooms with Server ACLs](https://matrix.org/docs/guides/moderation#banning-servers-from-rooms-server-acls)
- [Matrix specification about Server ACLs](https://matrix.org/docs/spec/client_server/latest#server-access-control-lists-acls-for-rooms)

## Locking down a public room (D-PHYS only)

This will make a room accessible from the D-PHYS homeserver only. Servers which were already participating in that room over federation will not receive any new events.

**Warning: The steps below cannot be undone. Be careful when deploying server ACLs. It is very well possible to completely lock a room. It would be lost forever.**

We will now use the developer tools in Element (Web/Desktop) to send a custom state event to the room. Open the developer tools by entering the following command in Element:

```
/devtools
```

[[!img /media/chat/riot_devtools_open.png size="450x"]]

Now select **Send Custom Event**:

[[!img /media/chat/riot_devtools_send_custom_event.png size="450x"]]

- Click on the red **Event** button in the lower-right corner to switch to **State Event** mode
- As **Event Type** set: `m.room.server_acl`
- Add the following **Event Content**:

```json
{
    "allow": [
        "phys.ethz.ch"
    ],
    "allow_ip_literals": false,
    "deny": [
        "*"
    ]
}
```

The window should now look like this:

[[!img /media/chat/riot_devtools_send_server_acl_phys_only.png size="450x"]]

- Click **Send**

You should now get a confirmation message that the event was sent:

[[!img /media/chat/riot_devtools_event_sent.png size="450x"]]

You can later review the settings. Just open the devtools again and select **Explore Room State** > **`m.room.server_acl`**:

[[!img /media/chat/riot_devtools_view_state_event_server_acl.png size="450x"]]
