# Bots

For the full list of available bots and to get help, join [#bots:mbot.ethz.ch](https://m.ethz.ch/#/#bots:mbot.ethz.ch).
New bots and updates will be posted in this room.

To manage bots `/invite` or `/kick` them like regular users.
They respond to `!commands` or react to patterns or words.

Most of them support encryption, but it can be unreliable.
It is probably a bad idea anyway to include a bot in your encrypted conversations.


## Maubot

Most of them are documented on the [maubot GitHub page](https://github.com/maubot/maubot)
or on [t2bot.io](https://t2bot.io/) (a public integration network for Matrix).


## Gitlab

A bot for showing Gitlab activity in your Matrix room and for interacting with your Gitlab repositories.

### Getting notifications in your room

Apply the following steps:

1. Invite [gitlab](https://m.ethz.ch/#/@gitlab:mbot.ethz.ch) (`@gitlab:mbot.ethz.ch`) to your room.
2. Log in to Gitlab and open your repository's webhooks settings.
3. For the URL, put `https://mbot.ethz.ch/_matrix/maubot/plugin/gitlab/webhooks?room=!your_room_id`. Your room ID can be found on the "Advanced" tab of your room settings in Element.
4. For the Secret Token, put `mbot`
5. Check off the triggers you'd like to have in your room and click "Add webhook".

To later remove the bot from your room, delete the webhook in Gitlab and kick the bot from the room.

### Managing your repositories with the bot

The bot has several commands to do things like create issues which require access to your Gitlab account.

1. Invite [gitlab](https://m.ethz.ch/#/@gitlab:mbot.ethz.ch) (`@gitlab:mbot.ethz.ch`) to a private chat.
2. Log in to Gitlab and go to your account settings. Once there, click "Access Tokens" on the left side.
3. Generate a new access token with a name and optional expiration date. Ensure the token has all available scopes assigned. These are required so that it can create issues and such on your behalf.
4. In your private chat with the bot, say `!gitlab login your_access_token`
5. If successful, say `!gitlab help` to see what the bot can do.

<!-- markdownlint-disable MD024 -->

## Reminders

- Supports recurring reminders, cron-style reminders, room reminders

### Usage

- Invite [reminder](https://m.ethz.ch/#/@reminderbot:mbot.ethz.ch) (`@reminderbot:mbot.ethz.ch`) to your room
- Type `!help reminders` or have a look at the [readme](https://github.com/anoadragon453/matrix-reminder-bot/blob/master/README.md#usage)
- For room reminders set the appropriate power level (Moderator)


## Lunch

A [maubot](https://github.com/maubot/maubot) plugin for the canteen lunch menus at ETH Zurich.

* Show lunch menu (optional canteens filter)
* Set up recurring reminders to post the lunch menu

Gitlab: https://gitlab.phys.ethz.ch/isgphys/maubot-ethzlunch

### Usage

- Invite [lunch](https://m.ethz.ch/#/@lunchmenu:phys.ethz.ch) (`@lunchmenu:phys.ethz.ch`)
- Show menu: `!lunch menu` or `!lunch menu [canteens]` (or one of the aliases such as `!hunger`)
- German menus: `!lunch config language de`
- Reminder: `!lunch remind 11:00` (and optionally 👍️ it to get pinged)

It shows a selection of canteens on Hönggerberg by default. If you are interested in other canteens:

- List them all: `!lunch canteen`
- Configure your selection: `!lunch config canteen <canteens>`  (example: `all` or `poly,food market,fusion`)

For details see:

- `!lunch`
- `!lunch config`
- `!lunch help`


## arXiv

A [maubot](https://github.com/maubot/maubot) plugin that gives arXiv summaries
([ETH gitlab](https://gitlab.ethz.ch/iyanmv/arxivbot), [GitHub](https://github.com/iyanmv/arxivbot)).

- Invite [arxiv](https://m.ethz.ch/#/@arxiv:phys.ethz.ch) (`@arxiv:phys.ethz.ch`) to your room
- Paste URLs of arXiv abstracts (https://arxiv.org/abs/XXX) to your room
- The bot will respond with Date, Title, Authors, Abstract and PDF link.


## Stickers

Note: **Service is disabled** due to lack of an integration manager.

Create your own sticker packs to share with others (or to keep to yourself).
This requires an [[integration manager|/chat/element/integrations]] which supports custom sticker packs.

### Usage

- Invite [stickers](https://m.ethz.ch/#/@stickers:mbot.ethz.ch) (`@stickers:mbot.ethz.ch`) to a private chat.
- Say `!stickers newpack` and follow the directions.
- Open the [[integration manager|/chat/element/integrations]]  and edit your sticker packs.
- Paste the URL the bot gave you into the "Add Sticker Packs" box.
- Click "Add Sticker pack"

## Emailbot

A bot that posts messages to rooms when an email is received.
Ideal for uses where a short message is desired in a chat room when a newsletter goes out.

**This bot can only receive emails from within the ETH network**, this includes:

- mails from @phys.ethz.ch or @ethz.ch mail addresses
- mailing lists ([lists.phys.ethz.ch/](https://lists.phys.ethz.ch/))
- direct mails from computers or IoT devices on the ETH network

It is still possible to route external emails to the bot via a D-PHYS mailbox or alias.
Please [[contact us|/services/contact]] to request a mailbox/alias (if not already existing).

### Usage

- Invite [emailbot](https://m.ethz.ch/#/@emailbot:mbot.ethz.ch) `@emailbot:mbot.ethz.ch` to your Matrix room.
- Get your room's internal ID (for instance, `!ZkngAyfszzfCqwNZUd:phys.ethz.ch` which is `#test:phys.ethz.ch`).
- Contact [rda](https://m.ethz.ch/#/@rda:phys.ethz.ch) `@rda:phys.ethz.ch` (via DM) to set the appropriate `allow_from` rule for your room ID.
- Send an email to `<room id without !>_<domain>@emailbot.mbot.ethz.ch` (eg: `ZkngAyfszzfCqwNZUd_phys.ethz.ch@emailbot.mbot.ethz.ch`).
- See the message the bot posts (click on *view* to see the whole message).
- Optional: create an [[email forward|/mail/sieve]] from a [mailbox](https://webmail.phys.ethz.ch/)
  or subscribe to a [mailing list](https://lists.phys.ethz.ch/) using the email address from above.
