Matrix
======

**Matrix** is an **open standard** for interoperable, decentralized, real-time communication over IP. It can be used to power Instant Messaging, VoIP/WebRTC signaling, Internet of Things communication - or anywhere you need a [standard HTTP API](https://matrix.org/docs/spec/client_server/latest) for publishing and subscribing to data whilst tracking the conversation history.

Matrix defines [the standard](https://matrix.org/docs/spec), and provides [open source](https://github.com/matrix-org) reference implementations of Matrix-compatible Servers, [Client SDKs](https://matrix.org/sdks/) and [Application Services](https://matrix.org/docs/guides/application-services) to help you create new communication solutions or [extend the capabilities](https://matrix.org/bots/) and [reach of existing ones](https://matrix.org/bridges/).

[[!img /media/chat/bridges.png size="450x"]]

Want to know who is behind it and why they create such a useful project for the public for free? Read on in the official [matrix.org FAQ](https://matrix.org/faq).

Documentation
-------------

- [[Use the Client-Server API|api]]
- [[Get an access token|api]]
- [[Moderate rooms|moderation]]

How does it work?
-----------------

Each user connects to a single server, this is their **homeserver**. Users are able to participate in **rooms** that were created on any Matrix server since each server **federates** with other Matrix servers. This means you can talk to anyone on any server. It also means you can host your own server, giving you control over all of your data. Self hosting also gives you the ability to customize your server to fit your needs including giving you the ability to bridge to other chat networks (such as IRC, XMPP, Discord, Telegram, etc) or to host bots.

Each message that is sent in a room is synchronized to all of the other servers that participate in that room. If one server goes offline, everyone else in the room can continue talking. Once that server comes back online it will be sent all of the messages that it missed while it was down.

[[!img /media/chat/distributed_architecture.png size="450x"]]

Did we mention it is secure? Your private conversations can be secured by end to end encryption so the server has no idea what you are talking about.

See the [Introduction](https://matrix.org/docs/guides/introduction) or the recommended [Guides](https://matrix.org/docs/guides/) by [matrix.org](https://matrix.org/) if you are interested to learn more.

Terms
-----

A few important [terms](https://matrix.org/faq/#definitions) to know are:

- **homeserver**: Each account in the Matrix federation is associated with a single homeserver. The software running at this server stores the history and account information for that user. Homeservers synchronize message history with other homeservers.
- **federation**: Federation means that separate instances of a service communicate - the best example of this is email servers, in which it's possible to send mail between difference service providers. For Matrix, this means that data about rooms and message history is shared between servers of participating users.
- **Element**: Element is a popular matrix client developed by the core matrix.org team.
- **MXID**: Matrix user IDs (MXID) are unique user IDs. They are in the format `@username:phys.ethz.ch`.
- **3PID**: Third-party IDs (3PIDs) are IDs from other systems or contexts, such as email addresses, social network accounts and phone numbers.
- **sigil**: Sigils refer the symbols uses at the beginning of many matrix identifiers. For example `@` is used for users, `#` for rooms and spaces.
- **space**: Spaces (groups) are collections of rooms.
- **room**: A room is a fundamental building bock of the matrix architecture: events are typically sent in the context of a room. A room is a conceptual place where users can send and receive events. Events are sent to a room, and all participants in that room with sufficient access will receive the event.

Homeserver (synapse)
--------------------

We host a [Synapse Matrix Homeserver](https://github.com/matrix-org/synapse) for the domain `phys.ethz.ch`.
It holds user account data, message history, media uploads, room aliases and participates in the Matrix federation network.

Clients
-------

To connect to the Matrix federation, you will use a client. At the D-PHYS we recommend [[Element|/chat/element]]. [These are some of the most popular Matrix clients](https://matrix.org/clients/) available today, and more are available at [try-matrix-now](https://matrix.org/docs/projects/try-matrix-now/). To get started using Matrix, see [[How to connect using Element|/chat/element/connect]]. To see more clients in a features matrix, see the [Clients Matrix](https://matrix.org/clients-matrix).

Clients connect you to a homeserver to send/receive messages. Your homeserver stores a copy of the message history of every room you participate in. Homeservers connect to other homeservers on the internet and synchronize the message history of federated rooms.

Federation
----------

Federation is the process by which users on different servers can participate in the same room. As soon as a Matrix user from another homeserver joins your room (either via an invitation to a private room or by joining a public room), the corresponding homeservers will start to sync the message history of that room. If one of the participating homeservers is offline, the users on other homeservers can continue talking in that room. This is called decentralization.

The [Matrix Traveler (Voyager)](https://voyager.t2bot.io/#/) bot maps the currently known universe in a [public graph](https://voyager.t2bot.io/#/graph).

Users
-----

Each client is associated with a user account, which is identified in Matrix using a unique **user ID**. This ID is namespaced to the homeserver which allocated the account and has the form:

```
@localpart:domain
```

Events
------

All data exchanged over Matrix is expressed as an **event**. Typically each client action (e.g. sending a message) correlates with exactly one event. Each event has a `type` which is used to differentiate different kinds of data. The special top-level namespace `m.` is reserved for events defined in the Matrix specification - for instance `m.room.message` is the event type for instant messages. Events are usually sent in the context of a **Room**.

Rooms
-----

A room is a conceptual place where users can send and receive events. Events are sent to a room, and all participants in that room with sufficient access will receive the event. Rooms are uniquely identified internally via **Room IDs**, which have the form:

```
!opaque_id:domain
```

There is exactly one room ID for each room. Whilst the room ID does contain a domain, it is simply for globally namespacing room IDs. The room does NOT reside on the domain specified.

Room Aliases
------------

Each room can also have multiple **Room Aliases**, which look like:

```
#room_alias:domain
```

A room alias "points" to a room ID and is the human-readable label by which rooms are publicized and discovered.

Spaces
------

Spaces are a new feature in Matrix to group rooms.
Under the hood spaces are rooms with a special type `m.space` to distinguish them from regular messaging rooms.
This means spaces (unlike the old communities) are fully decentralized like rooms.

Spaces can contain rooms and members. Since spaces are just rooms, spaces can also contain sub-spaces.
This allows to organize rooms in a tree structure.

The details are outlined in [MSC1772](https://github.com/matrix-org/matrix-doc/pull/1772).

Restricted rooms
----------------

Within a private space it is now possible to have rooms with a new type of join rule `restricted`.
A restricted room is like a private room (only invited people can join) and additionally allows to grant
access to the room based on the membership of spaces. This means you can now create a private team space
and invite teammates to your space. Your teammates will then be able to join all restricted rooms that are
part of your space without an invite. This greatly simplifies group management when you have a lot of rooms.

Since this introduces some protocol changes, restricted rooms require a new room version
([9](https://github.com/matrix-org/matrix-doc/pull/3375)).
This means you will have to upgrade your existing rooms to get this feature.

The details are outlined in [MSC3083](https://github.com/matrix-org/matrix-doc/pull/3083).

Room Versions
-------------

Rooms are central to how Matrix operates, and have strict rules for what is allowed to be contained within them. Rooms can also have various algorithms that handle different tasks.To allow rooms to be improved upon through new algorithms or rules, "room versions" are employed to manage a set of expectations for each room. New room versions are assigned as needed.

Room version 9 (required for restricted rooms) is not stable yet and may not be supported by all servers.
If your room has members from other servers you should check their server version.
To do that invite the rsvc (phys.ethz.ch) bot to your room by typing `/invite @rsvc:phys.ethz.ch`
and check the server versions in your room with `!servers`.
All joined servers should at least have the version `Synapse 1.43.0`.

Share links
-----------

To share links outside of a Matrix client (i.e. on a website)
use our URL redirecting service at [m.ethz.ch](https://m.ethz.ch).
It contains all Element Web instances at the ETH that we know about
and allows people to join using their preferred client.

You can use links that are generated in Element and replace the domain `matrix.to` with `m.ethz.ch`.
