How to use the Matrix API
=========================

You can use the Matrix [Client-Server API](https://matrix.org/docs/spec/client_server/latest) to send messages, control rooms or anything else. It uses standard HTTP + JSON objects.

To get started you need an access token. The simplest way to get that is to open Element in a private (incognito) window in your web browser or just use your currently open Element.
Go to **Settings** > **Help & About** > **Advanced** > **Access Token `<click to reveal>`** and copy your access token:

[[!img /media/chat/matrix_access_token.png size="450x"]]

**Warning**: This token grants full access to your Matrix account. **Handle it as safely as you would handle your password!**

Do not logout, just close the browser window or leave it open. Logging out will invalidate your access token.

Open a terminal on a Linux/Mac Computer or use [[ssh|/documentation/ssh]] to access one of our Linux machines remotely.
First put the access token into a variable named `token`. Make sure it has a space in front of `token`. This will prevent your access token from leaking into the bash history:

```bash
 token=YOURveryLONGaccessTOKENhere.........
```

The token is now stored in a shell variable for reuse in the following commands.

Example usage
-------------

Using `curl`:

```bash
curl -s -X GET -H "Authorization: Bearer ${token}" 'https://matrix.phys.ethz.ch/_matrix/client/r0/profile/@rda:phys.ethz.ch/displayname'
```

Output:

```json
{
    "displayname": "rda"
}
```

Using `httpie`:

```bash
http GET 'https://matrix.phys.ethz.ch/_matrix/client/r0/profile/@rda:phys.ethz.ch/avatar_url' Authorization:"Bearer ${token}"
http PUT 'https://matrix.phys.ethz.ch/_matrix/client/r0/profile/@rda:phys.ethz.ch/avatar_url' Authorization:"Bearer ${token}" avatar_url="mxc://phys.ethz.ch/59a4f8b387241cf460d553dee989d4ba10356251"
```

or a media upload:

```bash
http POST 'https://matrix.phys.ethz.ch/_matrix/media/r0/upload?filename=morpheus.png' Authorization:"Bearer ${token}" Content-Type:"image/png" < Pictures/avatars/morpheus.png
```

or to post JSON data:

```bash
http POST 'https://matrix.phys.ethz.ch/_matrix/client/r0/rooms/!ZkngAyfszzfCqwNZUd:phys.ethz.ch/invite' Authorization:"Bearer ${token}" <<<'{"user_id":"@rda:phys.ethz.ch"}'
```

or to sent an `m.room.message` using python:

```python
#!/usr/bin/env python3

import requests

access_token = 'YOURveryLONGaccessTOKENhere'
room_id = '!ZkngAyfszzfCqwNZUd:phys:phys.ethz.ch'
url = 'https://matrix.phys.ethz.ch/_matrix/client/r0/rooms/' + room_id + '/send/m.room.message'
headers = {'Authorization': ' '.join(['Bearer', access_token])}
data = {
    'body': 'hello matrix',
    'format': 'org.matrix.custom.html',
    'formatted_body': 'hello <b>matrix</b>',
    'msgtype': 'm.text'
}

r = requests.post(url, json=data, headers=headers)
```

where `format` and `formatted_body` can be omitted to send plain-text only messages.

Get a list of room members
--------------------------

```bash
curl -s -X GET -H "Authorization: Bearer ${token}" 'https://matrix.phys.ethz.ch/_matrix/client/r0/rooms/!GYTZNIMhOdEHvEzGrR:phys.ethz.ch/members?membership=join&not_membership=leave' |
grep state_key | grep -oE '@[^:]+:[^"]+'
```

How to invalidate and access token
----------------------------------

Just logout. Or go to **Settings** > **Security & Privacy** > **Sessions** and delete the corresponding active session, which will also invalidate the access tokens.
