Secure and privacy aware video conferencing with Jitsi
======================================================

One of the most important tools in current times is video conferencing. You might be aware of the multitude of privacy and security problems in the popular Zoom software. Well, so are we, so we started looking for a local, secure and privacy aware alternative.

A promising and popular Open Source video conferencing solution is [Jitsi](https://jitsi.org/). So we set up a [Jitsi server at D-PHYS](https://jitsi.phys.ethz.ch) for all its members to use. It offers audio and video calls that do not use any non-ETH servers.

It's not perfect of course, but we routinely use it for ISG-internal group meetings (10 participants) and a lot of 1:1 calls. In many cases, you don't even need to install any software since it runs in any WebRTC-capable browser (Chrome/Chromium, Firefox, Edge, Brave, Safari).

Usage
-----

- **Desktop**, **Notebook**: point your browser to [jitsi.phys.ethz.ch](https://jitsi.phys.ethz.ch)
- **Mobile** devices, install the [Jitsi Meet](https://jitsi.org/downloads/) app

Create a new meeting. Then distribute the URL to your friends or colleagues.

Hints
-----

- Chrome-based browsers seem to work best, but we've also been using recent versions of Firefox and Safari.
- on some platforms the window size of the browser does affect call quality. If you have issues, try shrinking the browser window (your computer needs to decode a highly-compressed video feed for every participant, which requires a lot of CPU performance)
- on lower-end Android applets the Jitsi app might not be up to the task and can cause audio problems
- Jitsi is not meant for very large audiences. It should work for at least 15 participants, but for larger crowds you might hit a limit at some point
