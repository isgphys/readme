ISG D-PHYS IT Documentation
===========================

This is the source of our documentation at [readme.phys.ethz.ch](https://readme.phys.ethz.ch).


Spellchecking
-------------

One can use `aspell` in markdown mode to check the spelling of all files. We add non-standard words in a personal dictionary `.dict.spl` to complement the default word list.

Run non-interactively to list possible spelling mistakes.

```sh
find . -name '*.markdown' -exec sh -c 'aspell list --mode=markdown --personal="$PWD"/.dict.spl < {}' \;
```

Use interactive mode to take actions on spelling mistakes.

```sh
find . -name '*.markdown' -exec aspell check -x --mode=markdown --personal="$PWD"/.dict.spl {} \;
```
