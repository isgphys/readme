How to use SSH
==============

We provide [Secure Shell](http://en.wikipedia.org/wiki/Secure_shell) logins to our workstations.  Either use

```
ssh <username>@<hostname>
```

if you know a specific hostname where you have login rights or use

```
ssh <username>@login.phys.ethz.ch
```

for a generic login.  You will need your username and password of the [[Physics account|account/]].

In case of a restrictive firewall, for instance in an hotel, you can also connect to `login.phys.ethz.ch` via SSH on the ports 80 or 443.

Linux and macOS workstations have SSH installed in all current default installations. For Windows we suggest [Putty SSH](http://www.putty.org/) as an SSH client.

Configuration
-------------

We recommend the following client settings in your `~/.ssh/config`:

```
HashKnownHosts no
ForwardAgent yes
ForwardX11 yes

ControlPersist 60s
ControlMaster auto
ControlPath ~/.ssh/ssh-%h-%p-%r
```

The last 3 variables enable the sharing of multiple sessions over a single network connection,
which will speed up subsequent connections to the same host (i.e.: makes tab completion usable for ssh/scp commands).
See also `man 5 ssh_config`.

To override this and start a new session in a new connection:

```bash
ssh -o ControlMaster=no -o ControlPath=none user@host
```

Debugging tips
--------------

Try to use ssh with the '-vvv' option for verbose output.

If your ssh login to our managed Linux workstations works, but takes several seconds to connect, make sure your files have the correct access permissions:

```
ssh <username>@login.phys.ethz.ch
chmod g-w ~/
chmod 700 ~/.ssh
chmod 644 ~/.ssh/config
chmod 600 ~/.ssh/authorized_keys*
```

Explanation: SSH is strict regarding permissions and tries to make sure that only you can access the files. If your home folder gives the group write access, SSH needs to make sure that you are the only member in this group. This lookup of the group members can slow down the initial connection.

Further Reading
---------------

* [[How to use SSH keys|ssh keys]]
* [[How to propagate your editor, language and other choices to remote machines via ssh|How to propagate your editor language and other choices to remote machines via ssh]]
* [OpenSSH](http://www.openssh.org/)
* [[services/Usage Policies]]
