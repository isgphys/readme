# How to handle passwords

Good passwords are important – but they are only truly secure if they are handled correctly.

## General guidelines

- Use a unique password for every account
- Use a password generator for very secure passwords
- Never write down a password
- Never share your password with anyone
- Do not accept passwords from others

## Use good passwords

- The longer and more unusual your password, the more secure it is.
- Passwords with >= 12 characters (incl. >= 3 character classes) are secure
- Character classes are:
    * lower-case letters `a-z`
    * upper-case letters `A-Z`
    * numbers `0-9`
    * punctuation characters <code>\\`_~!@#$%^&*()+={}[]\|;:"\<\>,.?/</code>
    * all other characters (white-space/unprintable)

## Do not use weak passwords

- Rows of characters on a keyboard such as `asdfghjkl` or `1234567890`
- Words that appear in a dictionary such as `secret1` or `P4ssw0rD`
- Words and numbers from your personal environment such as your name or that of your partner or pets or dates of birth

## Real life tips to manage your passwords

Use a password manager with a very secure master password. See below how to build such a password, which is easily rememberable. This will be the only single password you have to remember - ever. Therefore it is crucial to use a secure password as explained under "Use good passwords". I recommend to use even more characters as the minimal password length mentioned in this how-to. The longer the more secure it is.

Generate unique passwords for all your accounts and store them securely in the password manager.

You can later retrieve them using your master password and copy/paste them. Some password managers also interact with your web browser and offer to 'type in' passwords on websites automatically.

## How to build an easy rememberable secure password

To create a password from any string of at least 12 characters:

- Take a sentence you can remember easily. For example: **I**n **J**une **2017** **I** **w**ill **c**omplete **m**y **d**octorate.
- Form a password from the initial letters, special characters and numbers: `IJ2017Iwcmd.`

To create a word group password:

- Think of randomly chosen common words that you can link with a personal mnemonic or story, e.g. **Zermatt, fisher, complex, rainbow** or **Dog, tempo, eerie, typing**
- Separate the words with a special character, e.g. `?` or `-`
- Form your password: `Zermatt?fisher?complex?rainbow` or `Dog-tempo-eerie-typing`

## Using a password manager

With an encrypted password safe, your passwords are stored in an encrypted database. The decryption is performed with what is known as a master password. In other words, this means the following:

- You only have to remember one password to gain access to all your passwords
- The master password has to be extremely secure (see **Use good passwords**)
- If you lose the master password, you lose all your passwords

## Vaultwarden (ISG-hosted password manager)

We host [Vaultwarden](https://github.com/dani-garcia/vaultwarden), a [Bitwarden](https://bitwarden.com/) compatible password manager featuring a web front-end, device sync and the possibility to share passwords within a group. Please read [our documentation](https://readme.phys.ethz.ch/documentation/vaultwarden/) on how to get started with Vaultwarden.

## KeePassXC (standalone password manager)

**KeePassXC** is a community fork of KeePassX, a native cross-platform port of the KeePass Password Safe, with the goal to extend and improve it with new features and bugfixes to provide a feature-rich, fully cross-platform and modern open-source password manager. In a typical scenario, you'd use [Polybox](https://polybox.ethz.ch/) as the sync back-end in KeePassXC.

- [Features](https://github.com/keepassxreboot/keepassxc/wiki/KeePassXC-Changelog)
- [Screenshots](https://keepassxc.org/screenshots)
- [Download](https://keepassxc.org/download)
- [Documentation](https://github.com/keepassxreboot/keepassxc/wiki)

Notable features:

- Command line interface
- Stand-alone password and passphrase generator
- Password strength meter
- Merging of databases
- Automatic reload when the database was changed externally

### Installation

**On D-PHYS managed workstations:**

If KeePassXC is not installed on your D-PHYS managed workstation, please [[contact us|services/contact/]]. You can also use a portable version for [Windows](https://keepassxc.org/download#windows) or for Linux.

**On unmanaged/private workstations:**

Use the binaries/installers or distribution-specific packages from [keepassxc.org](https://keepassxc.org/download). If there is no packaged version for your distribution available, you can build it from source.


## Additional information about password security

- [ETHZ Information Security Awareness Campaign](https://monster.phys.ethz.ch)
