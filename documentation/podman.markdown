Podman
======

The Pod Manager tool, [podman](https://podman.io) is a daemon-less container engine. It is similar to docker, but relies on the fork/exec model instead. It can therefore be used without root rights.


Basic usage
-----------

Podman cannot be used directly on the NFS home. Instead, the container storage must be moved to a local disk.

```sh
SCRATCHDIR="/scratch/$(whoami)"
mkdir -p $SCRATCHDIR
chmod 700 $SCRATCHDIR

mkdir -p ~/.config/containers
cat > ~/.config/containers/storage.conf << EOF
[storage]
driver = "overlay"
graphroot = "$SCRATCHDIR/containers/storage"

[storage.options.overlay]
ignore_chown_errors = "true"
mount_program = "/usr/bin/fuse-overlayfs"
EOF
```

With this config you should be able to run simple containers.

```sh
podman run -ti --rm docker.io/library/debian bash
```

To clean up or reset the setup, you can delete the local container storage files.

```sh
podman system reset
```

If you need advanced features, like multiple users inside the container, please get in touch, as we may have to enable the subuid mappings for you.

### NVidia GPU support

Upon request, we can configure the container support for NVidia GPUs. An additional `--device` parameter then automatically enables `nvidia-smi` and other commands inside any container.

```sh
podman run -ti --rm --device=nvidia.com/gpu=all docker.io/library/debian nvidia-smi
```
