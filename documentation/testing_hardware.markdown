Testing Hardware
================

There are a few things you can do to test your hardware.

#### Testing RAM

A rather thorough test is possible with [Memtest86](http://www.memtest86.com/).  For ISG administrators this test is available via the net-boot menu.

#### Testing Hard-disks

First of all you have to find out what type of harddisk you are using.  When running Linux (e.g., Knoppix if you don't have a running Linux installation on the box) you can look it up in the /proc filesystem.  E.g., for first IDE harddisk (hda, master on the first IDE port) look at the file /proc/ide/hda/model .  You will find something like `IC35L040AVVN07-0`.  Enter this model name in a [Google search](http://www.google.com/) to find some data sheets and testing software made by the manufacturer (in the example mentioned, a [IBM/Hitachi Deskstar 120GXP with 40GB](http://www.hgst.com/hdd/desk/ds120gxp.htm)).

Some often used test programs for hard-disks:

* [Drive Fitness Test](http://www.hgst.com/hdd/support/download.htm#DFT) by IBM/Hitachi Global Storage Technologies.  ISG administrators find it under ~isg/tests/Hardware-Tests or in the net-boot menu
* [SeaTools Diagnostic Suite](https://www.seagate.com/support/downloads/seatools/) by Seagate Technology.  ISG administrators find it under ~isg/Hardware-Tests or in the net-boot menu
* some drive info: `smartctl -a -d ata /dev/discs/disc0/disc` (ide=ata, scsi, 3ware)
* You can also use the tool [badblocks](https://manpages.debian.org/stable/e2fsprogs/badblocks.8.en.html) to do either read-only or read-write checks.

#### Testing CPUs

* cpuburn is a tool to stress the CPU.  If lockups occur it may indicate problems with cooling or just sub-standard CPUs.  See the [cpuburn package](http://packages.debian.org/cpuburn) for details.
* **WARNING: improperly cooled CPUs may be destroyed by these tests. Watch carefully and know what you are doing**.
* Note: observe the temperature (e.g. in /proc/acpi/thermal_zone/THRM/temperature).  It should not go beyond 70 degrees Celsius
* According to our Hardware supplier Dalco the CPU is OK when it runs k7burn for one hour.
