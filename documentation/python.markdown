Python
======

The *Python Ecosystem* modules of our [Basics of Computing Environments for Scientists](https://compenv.phys.ethz.ch) lectures explain various concepts in detail.

Keywords: pip, virtualenv, venv, pyenv, pipenv, poetry, pip-review, pipx
