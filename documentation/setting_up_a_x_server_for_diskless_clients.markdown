Setting up a X server for diskless clients
==========================================

Describes setup for diskless X clients based on alix boards from pcengine.ch

the server needs tftp and NFS

settings for NFS (/etc/exports):

```
/opt/alixboot       (ro,no_root_squash,async)
```

settings for tftp (pxelinux.cfg/default):

```
DEFAULT vmlinuz ro initrd=initrd.img root=/dev/nfs ip=dhcp nfsroot=192.33.96.202:/opt/alixboot/
```

Changes to a standard Debian (etch) in `/opt/alixboot`:

Create tmpfs directories

```
mkdir /tmp.rmdsk /etc.rmdsk /var.rmdsk
chmod 777 /tmp.rmdsk
```

`/etc/fstab`:

```
proc            /proc           proc    defaults          0 0

sysfs           /sys        sysfs  defaults               0 0
tmpfs           /dev/shm    tmpfs  defaults,noatime       0 0
devpts          /dev/pts    devpts defaults,noatime       0 0

tmpfs           /tmp.rmdsk  tmpfs  defaults,noatime       0 0
tmpfs           /var.rmdsk  tmpfs  defaults,noatime       0 0
tmpfs           /etc.rmdsk  tmpfs  defaults,noatime       0 0

unionfs         /etc        unionfs dirs=/etc.rmdsk=rw:/etc=nfsro           0 0
unionfs         /var        unionfs dirs=/var.rmdsk=rw:/var=nfsro           0 0
unionfs         /tmp        unionfs dirs=/tmp.rmdsk=rw:/tmp=nfsro           0 0
```

this mounts writable tmpfs overlays onto the read-only NFS export

`/etc/mtab`:

```
tmpfs /lib/init/rw tmpfs rw,nosuid,mode=0755 0 0
proc /proc proc rw,noexec,nosuid,nodev 0 0
sysfs /sys sysfs rw,noexec,nosuid,nodev 0 0
udev /dev tmpfs rw,mode=0755 0 0
tmpfs /dev/shm tmpfs rw,nosuid,nodev 0 0
devpts /dev/pts devpts rw,noexec,nosuid,gid=5,mode=620 0 0
rootfs / rootfs ro 0 0
```

```
rm /etc/rcS.d/S40networking
```

prevent dhcpclient from messing with the network config

`etc/inittab`:

```
6:23:respawn:/sbin/getty 38400 tty6
kk:2345:respawn:X -query pomfrey.ethz.ch
```

start X and point it to a (powerful) X application server

`/etc/mkinitrd/modules`:

```
nfs
nfs_acl
lockd
```

these are needed in the initrd to mount NFS /
then remake initrd: `dpkg-reconfigure linux-image-..`

`etc/default/rc`:

```
RAMRUN=yes
RAMLOCK=yes
```

for initial writability in /var

```
ln -s /tmp/ usr/share/X11/xkb/compiled
```

needed for X - keyboard doesn't work w/o
