How to use keychain for ssh and gpg
===================================

You can use keychain to re-use ssh-agent and/or gpg-agent between logins. In this example we show how you can add two ssh keys (`ed25519` and `RSA`) and your gpg key to keychain.

First make sure you already created your [[ssh keys|/documentation/ssh_keys]] and [[gpg keys|documentation/how_to_create_an_openpgp_gnupg_key]]. Then run the following command in your terminal:

```bash
eval $(keychain --nogui --eval --agents ssh,gpg --quick [ssh_key_filenames ...] [gpg-key-id])
```

For example if we have 2 ssh keys (`id_ed25519` and `id_rsa`) and a gpg key id (`8F3FB7BD7A6BF71D`):

```bash
eval $(keychain --nogui --eval --agents ssh,gpg --quick id_ed25519 id_rsa 8F3FB7BD7A6BF71D)
```

Automatically loading keychain on login
---------------------------------------

You can place this line in you `~/.profile` to automatically load your keychain for a login shell. Just add the option `--quiet` to hide any messages printed to your terminal.

GPG key cache expiration
------------------------

To control how long it takes until your `GPG` key expires and you have to enter your password again, create the file `~/.gnupg/gpg-agent.conf` and set these two parameters:

```
default-cache-ttl 86400
max-cache-ttl 604800
```

Using these settings, the cache will be valid for 24 hours. Each time a cache entry is accessed, the entry's timer is reset. The cache may be valid for a maximum of 7 days.
