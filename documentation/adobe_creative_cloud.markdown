Adobe Creative Cloud
====================

ETH License
-----------

Adobe has forced ETH to move from impersonal serial numbers to Adobe Cloud accounts, linked to your full name and personal `@ethz.ch` email address. All previous installations have stopped working in November 2019. ETH encourages users to switch to one of the many [alternatives to Adobe software](https://www.softwareinfo.ethz.ch/alternatives-to-adobe-software/). You may also consider using the free [Adobe Reader](https://acrobat.adobe.com/us/en/acrobat/pdf-reader.html) to open PDFs, as many don't need the features of the paid Acrobat Pro. If you really need to keep using Adobe software, you have to use the new personalized license. The detailed usage conditions and disclaimer regarding the Adobe Cloud will be visible when ordering the software in the IT Shop.

Yearly "renewal"
----------------

The license is only valid for one year, which means you'll have to buy a new one every 12 months. Note that you don't have to reinstall the software itself, you only need a new valid license.

Installation
------------

* Use your personal ETH account to order the product _Adobe Creative Cloud All Apps CC DC_ from the [IT Shop](https://itshop.ethz.ch/).
* Open the _Adobe Creative Cloud_ application
* Enter your ETH username, followed by `@ethz.ch` as email address. Do not use an email alias or any other domain (eg `@phys.ethz.ch`).

    [[!img /media/adobe_creative_cloud/adobe_creative_cloud_01.jpg size="400x"]]

* You will be redirected to a login page where you have to enter your ETH credentials as requested.

    [[!img /media/adobe_creative_cloud/adobe_creative_cloud_02.jpg size="400x"]]

* This should activate your Adobe license and allow you to install the software you need.

    [[!img /media/adobe_creative_cloud/adobe_creative_cloud_03.jpg size="400x"]]


Fix for "Activation Failed" message in Adobe Acrobat
----------------------------------------------------

There are some cases where Adobe Acrobat shows repeated an "**Activation failed**" message although it was properly activated before. To solve this problem, Adobe Support Team recommends to add an entry in Windows Registry.

Open a `cmd` Command Prompt as administrator, then copy/paste this command into it for adding the registry key:

    REG ADD "HKLM\SOFTWARE\WOW6432Node\Adobe\Adobe Acrobat\DC\Activation" /v IsNGLEnforced /d 1 /t REG_DWORD /f

Alternatively you can add this registry key with regedit.exe (be sure to run it "as administrator").

 Add a new DWORD (32-bit): `IsNGLEnforced` Data `1` under this key `HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Adobe\Adobe Acrobat\DC\Activation`

[[!img /media/adobe_creative_cloud/acrobat_IsNGLEnforced.png size="400x"]]


Fix common issues using the Adobe CC Cleaner Tool
-------------------------------------------------

In case the Creative Cloud application is not working as expected, it may help to completely wipe all previous installations of Adobe software using one of the Adobe Cleaning Tools and re-install the package from the IT-Shop.

* [CC Cleaner Tool](https://helpx.adobe.com/creative-cloud/kb/cc-cleaner-tool-installation-problems.html) for Photoshop, Illustrator, Indesign, etc.
* [Acrobat Cleaner Tool](https://www.adobe.com/devnet-docs/acrobatetk/tools/Labs/cleaner.html) for Adobe Acrobat DC


Modify the language
-------------------

* In the preferences you can change the language in which the programs should be installed.

    [[!img /media/adobe_creative_cloud/adobe_creative_cloud_04.jpg size="400x"]]


Adobe Software on our Windows Terminal Server
---------------------------------------------

For the occasional user, we have pre-installed the common Adobe applications on our [[services/Windows Terminal Server]]. You still need at least a free Adobe ID to use the [[shared license|adobe creative cloud sdl]].
