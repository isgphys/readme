# KaTeX

[KaTeX](https://khan.github.io/KaTeX/) is a fast math typesetting library for the web. It uses JavaScript for client-side rendering.

This guide shows how to include KaTeX from a CDN and automatically render all latex content on your page. This is done using the `auto-render.js` script [included in KaTeX](https://github.com/Khan/KaTeX/tree/master/contrib/auto-render).

If you do not want to use a CDN, you can [download](https://github.com/khan/katex/releases) the files and host them on your page.

## Example usage

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>

### Inline

```
This $\int\limits_1^\infty \frac{1}{k}\,dk$ is inline.
```

This $\int\limits_1^\infty \frac{1}{k}\,dk$ is inline.

### Separate paragraph

```
$$f(x) = \int_{-\infty}^\infty \hat f(\xi)\,e^{2 \pi i \xi x} \,d\xi$$
```

$$f(x) = \int_{-\infty}^\infty \hat f(\xi)\,e^{2 \pi i \xi x} \,d\xi$$

<script>
  renderMathInElement(
      document.body,
      {
          delimiters: [
              {left: "<math>", right: "</math>", display: false},
              {left: "$$", right: "$$", display: true},
              {left: "$", right: "$", display: false},
              {left: "\\[", right: "\\]", display: true},
              {left: "\\(", right: "\\)", display: false},
          ]
      }
  );
</script>

## How to use KaTex on your page

Add the following inside the HTML `<head></head>` section:

```html
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>
```

Add the following (to the end) inside the HTML `<body></body>` section:

```html
<script>
  renderMathInElement(
      document.body,
      {
          delimiters: [
              {left: "<math>", right: "</math>", display: false},
              {left: "$$", right: "$$", display: true},
              {left: "$", right: "$", display: false},
              {left: "\\[", right: "\\]", display: true},
              {left: "\\(", right: "\\)", display: false},
          ]
      }
  );
</script>
```

This script will parse the whole HTML body and render everything enclosed in the defined delimiters. It also defines some custom delimiters.

### Example HTML files

You can find some examples [here](https://people.phys.ethz.ch/~rda/katex/)
