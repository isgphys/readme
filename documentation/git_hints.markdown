Git Hints
=========

Configure your personal git usage
---------------------------------

Configure git with (substituting the contact info with your specifics):

```sh
git config --global user.name "Full Name"
git config --global user.email "myemail@example.com"
```

We think that color coded diffs and status reports help greatly, therefore we configure:

```sh
git config --global color.ui "auto"
```

If you prefer SVN-like shortcuts, also configure the following aliases:

```sh
git config --global alias.st status
git config --global alias.co checkout
git config --global alias.ci commit -v
```

How to create a central git repository on one of our group drives
-----------------------------------------------------------------

If all your contributors do have a D-PHYS account and access to the same group drive, you can host your Git repository on that group's drive.

The following can be executed on any of our managed Linux machines, or on the terminal server `login.phys.ethz.ch`.

```sh
cd ~<group name>/[<some optional path>/]
mkdir <your project>.git
cd <your project>.git
git init --bare --shared=group
```

Use the repository as git remote
--------------------------------

You can access the repository from anywhere via `login.phys.ethz.ch`

```
git clone <your D-PHYS user name>@login.phys.ethz.ch:/home/<group name>/[<some optional path>/]<your project>.git
```

Git on Windows
--------------

Git on Windows is often used via [Cygwin](http://www.cygwin.com/). Nevertheless the most popular SSH client for Windows is Putty. Putty provides a program called `plink.exe` which can be used as command-line SSH client. Unfortunately this program does not seem to work properly together with `git`.

In case you get error messages like

```
error: cannot run ssh: No such file or directory
fatal: unable to fork
```

or

```
fatal: The remote end hung up unexpectedly
```

explicitly setting the environment variable `GIT_SSH` to Cygwin's OpenSSH client may help:

```
export GIT_SSH=/usr/bin/ssh
```

See also [git push using PuTTY on Windows fails (fatal: The remote end hung up unexpectedly)](http://stackoverflow.com/questions/6681918/git-push-using-putty-on-windows-fails-fatal-the-remote-end-hung-up-unexpectedly).
