Documentation
=============

* [[ACL]] (Access Control Lists)
* [[Adobe Creative Cloud]] apps and [[shared license|adobe creative cloud sdl]] on our Remote Desktop Server
* [[Ansible]]
* [[Attic]] - potentially outdated documentation that we keep for future reference
* [[Chat]]
* [[Elasticsearch]]
* [[GNU Screen]]
* [[GPG Keys|how_to_create_an_openpgp_gnupg_key]]
* [[Grafana]]
* [[How to propagate your editor language and other choices to remote machines via SSH]]
* [[InfluxDB]]
    * [[How to backup and restore InfluxDB|InfluxDB backup restore]]
* [[KaTeX]]
* [[Keychain for SSH and GPG|keychain]]
* [[Locales]]
* [[SSH]]
    * [[SSH Examples|SSH Samples D-PHYS Workstation to D-PHYS Workstation]]
    * [[SSH Keys]]
* [[Vagrant]]
* [[Vim]]

Version Control
---------------

* [[Git Basics]]
* [[Git Hints]]
* [[Git Advanced Hints]]
* [[Gitignore]]
* [[Subversion]] (SVN)

Development & Packaging
-----------------------

* [[Python]]
* [[Python for Windows|windows/development/python]]
* [[Jetbrains EDU account]]
* [[Notes on Linux Package Management]]
* [[Packaging Software for Debian]]
* [[Managing a Software Package with the GNU Autotools]]
* [[Managing Perl Code]]
* [[Debian Version Numbers]]

Additional documentation
------------------------

* [[How to handle passwords]] - tips about passwords and password managers
* [[How to install Debian from Debian]] - Re-install a Debian based distribution on a running system

Use the search to find even more topics or head to the documentation for each operating system

* [[Windows]]
* [[macOS|osx]]
* [[Linux]]

External documentation

* [Debian Documentation](http://www.debian.org/doc/)
    - [How to Secure Debian](https://www.debian.org/doc/manuals/securing-debian-manual/)
