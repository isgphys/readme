Free JetBrains Educational Account
==================================

As member of ETH you can benefit from a free JetBrains account for using all JetBrains IDEs for example PyCharm Pro.

- Open the [JetBrains website](https://www.jetbrains.com/community/education/#students) and apply for a **student and teachers license**.

- Fill in the form.
 **Important:** in field "Email address" use **`ETH-USERNAME@ethz.ch`** and **not** the physics email address!

- After filling this form, you will get an email from JetBrains which contains a link for confirming the account creation.

When this step is complete you can use this account for login in PyCharm Pro after first start.
