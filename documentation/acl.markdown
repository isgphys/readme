Access Control Lists (ACL)
==========================

ACLs offer a more flexible way to control file and directory rights than the usual owner-group-other schema in Unix. We use them on some group shares for example. In a nutshell, with ACLs you can set separate permissions for every single user.

Please use ACLs only if really necessary. By design, group shares are meant to be accessible by the whole group, as the research data should remain accessible, even if individual members leave the group.

Note that the following commands should be executed on one of our managed Linux workstations, for instance `login.phys.ethz.ch`. You will need to use a different set of tools to view and modify ACLs, depending on how the share is mounted. In most cases the NFSv4 documentation should apply.


How to detect ACLs
------------------

If `ls -l` looks something like this

```
drwxrws---+ 4 daduke ast 4.0K 2011-10-28 10:41 test
```

(note the `+` sign at the end), then the directory in question has ACL rules applied.


How to view and modify ACL over NFSv4
-------------------------------------

```
nfs4_getfacl test        # get current acls
nfs4_setfacl -e test     # edit acls in editor
```

### Adapting NFSv4 ACLs from POSIX ACLs

To use bare usernames, they must now be qualified with a domain — for username `johndoe` (and pretty much every D-PHYS user) this would now be `johndoe@phys.ethz.ch`
You can still use numeric IDs, as before, without resolving them to a domain.
For the familiar POSIX permissions shorthands `R`,`W` and `X` exist, to avoid the complexities of the additional granularity offered by NFSv4.
Both these features only work when first setting the entry. To modify, you must resolve the result and adjust accordingly.

For example, to add an ACL entry for user `johndoe` with `rwx` permissions , you would run:

```
nfs4_setfacl -a A::johndoe@phys.ethz.ch:RWX /path/to/file
```

To modify this in-place, to take away the `W` permissions:

```
nfs4_getfacl /path/to/file
D::OWNER@:x
A::OWNER@:rwatTcCy
A::1001:rwaxtcy # this is the resolved user id of the user in the previous example
A::1002:rwaxtcy
A::GROUP@:rwatcy
A::EVERYONE@:rtcy

nfs4_setfacl -m A::1001:RWX A::johndoe@phys.ethz.ch:R /path/to/file
```

The special `OWNER@`, `GROUP@` and `EVERYONE@` shorthands are equivalent to the `user`, `group`, and `other` fields in basic POSIX permissions.
Always make sure that the result, especially if using any of the aliases (user@domain and the RWX shortcuts), reflects your original intentions.

```
nfs4_getfacl /path/to/file
D::OWNER@:x
A::OWNER@:rwatTcCy
D::1001:wa
A::1001:rtcy
A::1002:rwaxtcy
A::GROUP@:rwatcy
A::EVERYONE@:rtcy
```

For more details, please check the manual pages `man 5 nfs_acl` and `man nfs4_setfacl`.

And please be aware that, while NFSv4 does offer many more features, it is still possible to access these shares using NFSv3, samba, or directly over the filesystem, where they will be translated to POSIX ACLs, as best as that is possible. Usually as the least permissive intersection of features of both.

```
phd-san-gw2:~# getfacl /path/to/file/on/actual/filesystem
getfacl: Removing leading '/' from absolute path names
# file: path/to/file/on/actual/filesystem
# owner: janedoe
# group: isg
user::rw-
user:johndoe:r--
user:janedoe:rwx
group::rw-
mask::rwx
other::r--
```


How to view and modify ACL over NFSv3
-------------------------------------

If a share is mounted via NFSv3, you can display ACLs by running

```
getfacl test
```

which will show

```
 # file: test
 # owner: daduke
 # group: ast
 # flags: -s-
user::rwx
user:schmid:rwx
user:kovac:rwx
user:amaraa:rwx
user:geersv:rwx
group::---
mask::rwx
other::---
```

To modify ACLs you just need something like

```
setfacl -m u:daduke:rw test
```

to grant `read` and `write` permission on file `test` to user `daduke` and

```
setfacl -x u:daduke test
```

to remove these permissions again. If you need more,

```
man setfacl
```

is your friend.

There is also a nice GUI that we have installed on our Linux workstations: `eiciel`. Start it, open the file or directory you'd like to modify and then edit the users' permissions.
