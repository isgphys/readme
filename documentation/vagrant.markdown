Vagrant
=======

[Vagrant](https://www.vagrantup.com) is a Ruby program to automate the installation and setup of virtual machines in [VirtualBox](https://www.virtualbox.org) (or other providers). It allows to quickly setup an environment for testing purposes or software development. Their [documentation](https://www.vagrantup.com/docs) contains many more details.


Installation
------------

Note: on our managed Linux workstation Vagrant is already installed.

* [Download](https://www.virtualbox.org/wiki/Downloads) and install VirtualBox.
* [Download](https://www.vagrantup.com/downloads.html) and install Vagrant package.


Preparation
-----------

When using VirtualBox you need to re-direct the path where virtual machines are stored outside of your home to the local disk.

```sh
VBoxManage setproperty machinefolder /scratch/some_path
VBoxManage list systemproperties | grep 'Default machine folder'
```

Start by adding an image of a preconfigured virtual machine to your system. You can find our collection of compact Vagrant boxes at https://share.phys.ethz.ch/~isg/vagrant/.

```sh
vagrant box add <boxname> <path/to/vm.box>
```

I.e., if you want to test out Debian Bookworm you would do

```sh
vagrant box add bookworm 'https://share.phys.ethz.ch/~isg/vagrant/bookworm64-core.box'
```

The box will be copied to `~/.vagrant.d/`. To get a list of the boxes that you have available use

```sh
vagrant box list
```

You should remove old boxes you no longer need to free disk space

```sh
vagrant box remove <boxname>
```

You can find more boxes on the internet at sites like http://www.vagrantbox.es/.


Creating and Using a Vagrant Box
--------------------------------

Next you have to create an initial `Vagrantfile` — a configuration file — inside some directory

```sh
mkdir /path/to/new/project
cd /path/to/new/project
vagrant init <boxname>
```

You may optionally edit the `Vagrantfile` to customize it to your needs.

To start the box use

```sh
vagrant up
```

To connect to the box once it is started use

```sh
vagrant ssh
```

You will be logged in without password as the user `vagrant`.  Use `sudo` to issue commands as root.  The directory `/vagrant` corresponds to the directory where the `Vagrantfile` is.

To stop the virtual machine use

```sh
vagrant halt
```

To delete the virtual machine use

```sh
vagrant destroy
```


Configuration Management
------------------------

Vagrant reaches its full power in combination with a configuration management system, like [[ansible]], to automate the installation of packages and deployment of settings in the virtual machine.
