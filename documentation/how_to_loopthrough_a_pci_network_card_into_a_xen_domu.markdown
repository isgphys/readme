How to loopthrough a PCI network card into a Xen DomU
=====================================================

If you have two network cards in your Xen Dom0 and want to use one inside a DomU, you need to do the following:

### Choose a currently unused network card

* The command `lspci` from the package `[pciutils](http://packages.debian.org/pciutils)` helps to find the right network card: `lspci | fgrep Ethernet`
* Write down the PCI Bus ID of the network card you are planning to loopthrough.

```
2/0/0 root@dom0:pts/7 [~] # lspci | fgrep Ethernet
06:00.0 Ethernet controller: Intel Corporation 631xESB/632xESB DPT LAN Controller Copper (rev 01)
06:00.1 Ethernet controller: Intel Corporation 631xESB/632xESB DPT LAN Controller Copper (rev 01)
3/0/0 root@dom0:pts/7 [~] #
```

### Edit /boot/grub/menu.lst

* Add `pciback.hide=($PCIBUSID)` to the Xen Kernel **Default** Options line (that one which looks like commented out with a single hash sign), so that `update-grub` adds it to every xen kernel as option automatically. Add `pciback.verbose_request=1` for debugging, too.

```
 ## Xen Linux kernel options to use with the default Xen boot option
 # xenkopt=console=tty0 pciback.hide=(06:00.1) pciback.verbose_request=1
```

### Run update-grub

* Call `update-grub` on the Dom0 to generate a new menu.lst which then will have the newly added Xen kernel options added to each (existing as well as new) xen kernel entry.

### Adapt network configuration inside the DomU

* Edit `/etc/network/interfaces` and add the same configuration as for the to be replaced network interface and a new interface name with a number one higher than highest one ever used until then. See [[#Footnotes]] below.

### Shutdown the DomU to which the PCI card should be loopthrough-ed

### Edit /etc/xen/$DOMU.cfg

* Comment out any former network configuration (calling of bridging scripts, MAC addresses, IP addresses, DHCP, etc.) for the connection you want to replace by the loopthrough-ed network card.
* Add `pci = [ '$PCIBUSID' ]`

```
 #
 #  Networking
 #
 #dhcp = 'dhcp'
 #vif  = [ 'mac=00:16:3E:AB:CD:EF' ]
pci = [ '06:00.1' ]
```

### Reboot the Dom0

* If everything works fine, the DomU should now see the network card. Check with `lspci`. It should look like this: `00:00.0 Ethernet controller: Intel Corporation 631xESB/632xESB DPT LAN Controller Copper (rev 01)`
* You also should have a **new** network interface (check with `ifconfig -a` which has replaced the formerly used one in case udev is used inside the DomU and has the default configuration. If you have configured it before as described, it should have the same network connection as before.
