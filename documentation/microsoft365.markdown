Microsoft 365 Subscription (Office 365)
=====================================

ETH users have access to the Microsoft 365 subscription, also known as Office 365, for MS Windows, MS Office, [[Teams|chat/other]] & various other software products by Microsoft.


License
-------

ETH has decided to move from the anonymized accounts to the personal `username@ethz.ch` logins. Your full name and username will be shared with Microsoft.

The detailed license agreement can be found in the IT Shop and will be shown before ordering.

In particular it implies that _no classified information_ is allowed to be stored in the Cloud. This typically includes (among others):

* research findings before publication
* research data subject to contractual confidentiality with third parties
* sensitive personal or financial data
* evaluation reports in the recruitment process

The user and/or information owner (ie professor) will be held responsible.

See [here](https://unlimited.ethz.ch/display/itkb/Data+protection+and+privacy) for more information about data protection and privacy.

The license allows the products to be used at home, but only for work-related matters. If you want to use Microsoft Office for private documents, you need to buy a personal license.

Your currently installed and licensed Microsoft products will continue working until the license has to be renewed (usually once a year) at which point you'll have to change to the new personal license. For managed workstations we can still install the desired Microsoft software packages for you, but you may have to enter your license upon first start.


Setup
-----

### Order License

* Go to [itshop.ethz.ch](https://itshop.ethz.ch) and log in with your ETH account.
* In the left navigation, click on _Service Catalog_, then _Cloud Subscription_ (below _Identity and Access_), and finally _Microsoft Cloud Access_.
* Please DO READ the license agreement and ponder its implications.
* After submitting the request, wait for the confirmation mail.

Please refer to the [PDF](https://unlimited.ethz.ch/download/attachments/36254186/CSC_Anleitung_Bestellung_M365_Cloud_Subscription.pdf?version=1&modificationDate=1678277413689&api=v2) of Informatikdienste for detailed instructions and screenshots.


### Install Office on macOS

* On macOS you can freely download all Microsoft Office programs directly from the App Store
    - [Word](https://apps.apple.com/ch/app/microsoft-word/id462054704?l=en&mt=12)
    - [Excel](https://apps.apple.com/ch/app/microsoft-excel/id462058435?l=en&mt=12)
    - [PowerPoint](https://apps.apple.com/ch/app/microsoft-powerpoint/id462062816?l=en&mt=12)
    - [Outlook](https://apps.apple.com/ch/app/microsoft-outlook/id985367838?l=en&mt=12)
* When opening Office for the first time, sign in with your `username@ethz.ch` and ETH password.

### Install Office on Windows

* Go to the [Office Site](https://portal.office.com/account/?ref=MeControl#)
* Login with your credentials `username@ethz.ch` and ETH password
* Click on `Install Office`
* Run the downloaded file and follow the install instruction
* Configure [Enhance settings for Microsoft 365](/windows/ms365_settings)

### Check on which devices Office is activated

* Go to the [Office Site](https://portal.office.com/account/?ref=MeControl#)
* Login with your credentials `username@ethz.ch` and ETH password
* Choose My Account -> View apps & devices
* Expand Devices
* Now you see the list of the activated devices with your account. If you exceed the max. count of activations, you can sign off from older unused devices here.


Further instructions
--------------------

See [here](https://unlimited.ethz.ch/display/itwdb/Microsoft+365) for further instructions.
