Vaultwarden Password Manager
============================

For our users we provide an instance of the [Vaultwarden](https://github.com/dani-garcia/vaultwarden) password manager. It allows you to store all your passwords in a secure location and sync them to your mobile and desktop devices. One master password encrypts all other passwords (think of it like a safe), so you need to remember just this one master password and can have a different password for each service. Vaultwarden is based on the [Bitwarden](https://bitwarden.com/) protocol, so you can use all Bitwarden-compatible clients to manage and access your passwords.

Vaultwarden also supports [Passkeys](https://readme.phys.ethz.ch/documentation/vaultwarden/?updated#passkeys-support) and TOTP-based MFA like the one ETH uses.

## Important

- make sure your master password is [really good](https://xkcd.com/936/). It should be **at least** 12 characters long.
- if you forget your master password, you'll lose access to all your stored passwords. So either
    - store your master password in a secure location on a piece of paper or
    - set up [emergency access](https://vault.phys.ethz.ch/#/settings/emergency-access) within Vaultwarden.
- we **strongly** recommend always using your unique **`<login>@phys.ethz.ch`** email address and not some `firstname.lastname@phys.ethz.ch` or other alias.
- if you're uber paranoid, you can switch the key derivation function from `PBKDF2` to `Argon2id` in the [security settings](https://vault.phys.ethz.ch/#/settings/security/security-keys) at the expense of higher memory consumption.

## Individual use

If you'd just like to use Vaultwarden to manage your personal passwords, follow a few easy steps:

### Create your Vaultwarden account

- go to the Vaultwarden [web front-end](https://vault.phys.ethz.ch).
- click `Create account`.
- provide your **D-PHYS** email (we limit registrations to `@phys.ethz.ch` addresses) and name.
- choose your master password. The longer the better.
- click `Create account`.
- verify your email.
- you can now use the Vaultwarden [web front-end](https://vault.phys.ethz.ch) or

### Install clients

- install the [Bitwarden client](https://bitwarden.com/download/) or the [browser extension](https://bitwarden.com/download/#downloads-web-browser) on your devices.
- register your client by providing the D-PHYS email address you used above.
- under `Logging in on:`, click on `self-hosted`.
- use `https://vault.phys.ethz.ch` for `Server URL`.
- enter your master password.
- make sure to visit `File` -> `Settings` to configure your client according to your preferences.

### Filling your password database

If you already have your passwords in electronic form, go to `File` -> `Import data` and check if your file format is supported. If it isn't, check your previous password store for more export options.

If you're starting 'from scratch', we recommend adding each password the next time you're using it. This allows you to also change it to something unique right away.

### Passkeys support

You might have heard about [Passkeys](https://fidoalliance.org/passkeys/), a new authentication technology that is meant to be more secure than plain old passwords. It is based on established principles like [public-key cryptography](https://en.wikipedia.org/wiki/Public-key_cryptography) and [challenge-response](https://en.wikipedia.org/wiki/Challenge%E2%80%93response_authentication) and effectively eliminates the danger of [password phishing](https://en.wikipedia.org/wiki/Phishing) since your private key will never leave your device(s). Since Passkeys' introduction at the end of 2022, it was basically only usable within the walled gardens of Apple's, Google's and Microsoft's ecosystems and you could not sync your Passkeys cross-platform. With Vaultwarden, this is now possible. If you're interested, here's what you need:

- a recent version of Firefox (tested), Chrome (tested) or Safari (untested)
- install the Bitwarden [browser extension](https://bitwarden.com/download/#downloads-web-browser) from your extension repository
- watch this [short video](https://bitwarden.com/blog/bitwarden-launches-passkey-management/) to see how it works
- do a dry run on the [Passkeys test site](https://www.passkeys.io/)
- add a Passkey on any web site that supports it and store it in Vaultwarden

### URL filtering

When using Vaultwarden at ETH, you'll face a small dilemma: some website require your ETH login, while others want the D-PHYS one. This can be sorted using URI filters. In a nutshell:

- create a password entry `ETHZ`
    - enter your ETH login + password
    - in the `URI 1` tab, click on the ⚙️ icon and choose `Regular expression`
    - in the URI field, enter `^https:\/\/(?!.*phys\.).*ethz\.ch.*$` exactly like that. This will force detection of all `ethz.ch` websites that are *not* `phys`.
- create a password entry `D-PHYS`
    - enter your D-PHYS login + password
    - in the `URI 1` tab, click on the ⚙️ icon and choose `Regular expression`
    - in the URI field, enter `^https:\/\/.*\.phys\.ethz\.ch.*$` exactly like that. This will force detection of all `phys.ethz.ch` websites.

This URL detection should work fine, but might not be perfect. If you encounter a URL that's not correctly identified, please let us know.

## Vaultwarden in a (research) group

If you'd like to share passwords within your group, Vaultwarden offers the `organization` feature which allows just that. Each user can still have personal private vaults.

Please contact us to have your organization created.

### Becoming part of an organization

Adding someone to an organization has to be initiated by the organization owner. Please ask them to invite you to join.

### Managing an organization

If you're the owner of an organization and would like to add users, log in to the Vaultwarden [web front-end](https://vault.phys.ethz.ch) and go to `Organizations`. You can then use the `Members` tab to invite, confirm and kick users. Some notes:

- invite new users via their `@phys` email address. Note that if they've already created a Vaultwarden login, you need to invite the same email address they've used (not some alias).
- check the 'Collections` tab to control the new user's access rights to your vaults.
- once a new user has joined your organization, you'll have to confirm them. This is done via the `⋮` menu on the right side of the user entry.
- note that members will neither expire nor be automatically removed from your organization. The organization owner is responsible to perform regular user cleanup. Basing an organization on an existing LDAP group is not possible for rather complicated cryptography reasons.

### Sharing passwords in an organization

Each organization can have multiple collections (think folders). When adding a new password that you'd like to share within your organization, the `Ownership` attribute allows you to grant access to your organization and the `Collections` checkboxes let you choose the 'folder'.

For example, a research group might have a collection `Vendors` that stores passwords of vendor websites and is shared with (almost) everyone in the group, while collection `Credit cards` is only available to the Prof, secretaries and PostDocs.
