Adobe Creative Cloud Shared Licenses
====================================

This documentation refers to the shared license (SDL) on our RDP servers. Please refer to the [[Adobe Creative Cloud]] documentation for all matters regarding the normal, individual licenses.

Adobe ID
--------

If you want to use Adobe applications on one of our Remote Desktop servers [[**winlogin.phys.ethz.ch**|services/windows_terminal_server]] (for D-PHYS user) or [**wints.igp.ethz.ch**](https://wiki.phys.ethz.ch/!igp/global/it/wints) (for D-BAUG IGP user), you **have to Sign In** with an _Adobe ID_.

If you **already have** ordered a [[payed _Adobe ID_|documentation/adobe_creative_cloud/]] from IT Shop or have a private one, you don't need to create an additional ID, just use the **existing** _Adobe ID_ for log in.

Create an Adobe ID
------------------

If you **don't have** an _Adobe ID_ you have first to **create** it. This can be done for free. Just follow the registration process according these screen shots.

* After login to the Remote Desktop server, start an Adobe application eg. Adobe Acrobat Pro.

* You will be shown a _Sign In_ page. Click on `Get an Adobe ID` to start the registration process for a free _Adobe ID_.

    [[!img /media/adobe_creative_cloud/adobe_creative_cloud_sdl_01.png size="400x"]]

* Fill in the form.

* **Important:** Use a valid email address.

    [[!img /media/adobe_creative_cloud/adobe_creative_cloud_sdl_02.png size="400x"]]

* Adobe will send you an email to the address you entered. Wait for it and click on the verification link. After this step, click on the blue button `Check again and continue` and you should be logged in successfully.

    [[!img /media/adobe_creative_cloud/adobe_creative_cloud_sdl_03.png size="400x"]]

* Now you are able to use the Adobe applications. Sometimes you will be prompted again to enter the email address and password. So please remember these credentials for later use.
