SSH Samples D-PHYS Workstation to D-PHYS Workstation
====================================================

We have the following machines for our examples:

* **berlin** - our linux workstation running OpenSSH
* **paris** - another linux workstation running OpenSSH

## Simple login to another machine

```
beat@berlin:~$ ssh paris
beat@paris's password:

beat@paris:~$
```

We type ssh and the destination host on a command prompt of berlin and after typing in our password, we could work on paris.

## First time login to another machine

```
beat@berlin:~$ ssh paris
The authenticity of host 'paris (129.132.189.68)' can't be established.
RSA1 key fingerprint is 98:3d:f9:34:bc:64:e2:68:00:3f:35:b2:66:e9:20:ee.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'paris,129.132.189.68' (RSA1)
```

to the list of known hosts. The first time we login to another machine, ssh asks us if the machine specific key should be added to your _known_hosts_. Every time you login to this machine, ssh will check, if the host key of paris has changed and would warn you if this happens. This ensures, that you are connecting to the right machine and that no one has replaced it.

## Tunneling X11

We run now a program which opens an new window:

```
beat@berlin:~$ ssh -X paris
beat@paris's password: password

beat@paris:~$ xclock
```

The window appears automatically on you desktop. You won't need to fiddle around with `$DISPLAY`, xhost or MIT-Cookies - just start your program and everything works well. Even more: the whole communication between paris and berlin is encrypted by ssh!

I hear you say "nice thing - but is this all?". No. ssh is also able to do file transfers. There are two programs to copy files:

## scp

We get a file from the remote machine:

```
beat@berlin:~$ scp paris:/path/to/filea /path/to/fileb
```

We copy a file to the remote machine:

```
beat@berlin:~$ scp /path/to/fileb paris:/path/to/filea
```

The syntax of scp is nearly the same as that of the standard _cp_. You may use relative or absolute paths and additionally you prepend the source or destination host separated with a colon. You may also use wildcards:

```
beat@berlin:~$ scp paris:file* .
```

or copy a directory structure:

```
beat@berlin:~$ scp -r paris:myfiles/ .
```

## sftp

sftp is an alternative file transfer through ssh. Everyone familiar of using a command line _ftp_-client will love it.

```
beat@berlin:~$ sftp paris
sftp> ls
-rwxr-xr-x    1 beat     dep         40848 Jun 27 09:01 filea
-rwxr-xr-x    1 beat     dep         40848 Jun 27 09:01 fileb
drwxr-xr-x    2 beat     dep           512 Jun 27 09:01 myfiles
sftp> get filea
Fetching /home/beat/filea to filea
sftp> put fileb
Uploading fileb to /home/beat/fileb
sftp> rm filea
Removing /home/beat/filea
sftp> quit
beat@berlin:~$
```

## Piping data

Users familiar with the concept of UNIX Pipes will love ssh. ssh forwards STDIN, STDOUT and STDERR from and to the target machine. You may use ssh to execute a command on the remote machine and process the output on the local workstation. But see the examples:

```
beat@berlin:~$ ssh paris ls > filelist
```

Output of ls is written to the file file list.
Or copy a bunch of files:

```
beat@berlin:~$ ssh paris "cd /; tar cf - bin" | tar xvf -
bin/
bin/ae
bin/bash
...
```

## Creating SSH keys

When using ssh a lot, it becomes annoying to type your password each time. Luckily ssh offers authentication based on keys.

Create an SSH key (see [[Generate a new SSH key|ssh keys#generate-a-new-ssh-key]]).

Because _berlin_ and _paris_ have the same home directory from the file server, you should be now able to login without a password:

```
beat@berlin:~$ ssh paris
Enter passphrase for /home/beat/.ssh/id_ed25519:

beat@paris:~$
```

You are now now longer asked for you password, but for your passphrase to unlock you keys.

## Adding your keys to ssh-agent

Add your key to the `ssh-agent` (see [[Use an SSH agent|ssh keys#use-an-ssh-agent]])`:

```
ssh-add
Enter passphrase for /home/beat/.ssh/id_ed25519:
Identity added: /home/beat/.ssh/id_ed25519 (beat@berlin)
```

Now you are able to connect to paris without typing password or passphrase:

```
beat@berlin:~$ ssh paris
beat@paris:~$
```
