SSH tunnel
==========

Often a given service is restricted to only the local machine, as it may be a security issue to open it for public connections from the whole internet. Common examples are the Remote Desktop Protocol (RDP) and a local web server for Jupyter. In order to access such services from outside, one has to make use of SSH tunnels. Not only does SSH allow to tunnel connections securely over the public network, it will also encrypt all network traffic in transit. We will cover local port forwarding here, where SSH is used to "forward" a port, that is only available on a given host, to any other remote host. For a more detailed explanation, refer to [A Visual Guide to SSH Tunnels: Local and Remote Port Forwarding](https://iximiuz.com/en/posts/ssh-tunnels/)

Local Port Forwarding
---------------------

Let's assume we start a local Python web server on a D-PHYS Linux workstation like that:

```bash
user@workstation:~$ python3 -m http.server 9000 --bind 127.0.0.1
Serving HTTP on 127.0.0.1 port 9000 (http://127.0.0.1:9000/) ...
```

The server now listens on the loopback interface (`127.0.0.1`) and will only be reachable on the local computer:

```bash
user@workstation:~$ ss -tulpn | grep 9000
tcp   LISTEN 0      5             127.0.0.1:9000       0.0.0.0:*    users:(("python3",pid=49323,fd=3))
```

In order to connect to the server from another computer over the network, we can use an ssh tunnel.
On the other computer establish an ssh tunnel using the following command:

```bash
+user@laptop:~$ ssh -L 9001:localhost:9000 user@workstation
user@workstation:~$
```

This opens a local port `9001` on the laptop and forwards it to port `9000` on the remote workstation.
You may also use the same port number on both sides. This is just for clarity of the example.

You can now access the remote Python server directly in the browser on your laptop via `http://127.0.0.1:9001/`.


Linux xrdp via SSH tunnel
-------------------------

Some Linux workstations provide an [xrdp](https://www.xrdp.org/) service for graphical remote login.
This is an on-demand service and needs to be requested by the hardware owner by [[contacting us|services/contact]].

For security reasons the service listens on the loopback interface only and is not exposed directly to the network.
Use an ssh tunnel to connect to it from anywhere:

```bash
+user@laptop:~$ ssh -L 13389:localhost:3389 user@workstation
```

The RDP port 3389 of the remote workstation will be forwarded to the custom port 13389 on your local computer. Connect your RDP client to the local forwarded port using the following commands.

### Windows

```sh
mstsc /v:localhost:13389
```

### Linux

```sh
xfreerdp /bpp:24 /v:localhost:13389 /u:<username> /clipboard +fonts
```

Where `<username>` must be replaced with your D-PHYS username.

### macOS

```sh
open "rdp://full%20address=s%3Alocalhost:13389"
```

Or open for instance the Microsoft Windows app and connect to `localhost:13389`.


Windows RDP via SSH tunnel
--------------------------

We offer a [[services/Windows Terminal Server]] for remote access with RDP. If you connect from outside the ETH network, you need to open an appropriate SSH tunnel beforehand, depending on the operating system of your computer.

### Windows

Open `cmd` by typing in the start menu and paste the following command to open an SSH tunnel.

```bash
ssh -l yourloginname -C -L 3390:winlogin.phys.ethz.ch:3389 login.phys.ethz.ch sleep 60
```

Replace `yourloginname` with your D-PHYS username. Let the command prompt window open. Then open the `Remote Desktop Connection` application and connect to `127.0.0.1:3390`. Make sure to use `ad\your_dphys_username` as username.

You can also use Putty instead the ssh client, see [[here|services/how_to_connect_windows_ts_from_outside]].

### Linux

```bash
ssh -l yourloginname -C -f -L 3389:winlogin.phys.ethz.ch:3389 login.phys.ethz.ch sleep 60
rdesktop -d AD -x l -z -k en-us -x 0x80 -g 1280x1024 localhost
```

Where `yourloginname` must be replaced with your D-PHYS username.

### macOS

Open `/Applications/Utilities/Terminal.app` and paste the following command to open an SSH tunnel.

```bash
ssh -l yourloginname -C -f -L 3389:winlogin.phys.ethz.ch:3389 login.phys.ethz.ch sleep 60
```

Replace `yourloginname` with your D-PHYS username. Then open the Microsoft Windows application and connect to `127.0.0.1`. Make sure to use `ad\your_dphys_username` as user name. Further screenshots are found in our [[Remote Desktop on macOS|osx/configuring_microsoft_remote_desktop_on_mac]] documentation.
