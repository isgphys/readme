How to Sign an OpenPGP/GnuPG Key
================================

If you do not have an OpenPGP key yet please follow the instructions in [[How to create an OpenPGP/GnuPG key|How to create an OpenPGP GnuPG key]].  There you may also want to review your configuration in `$HOME/.gnupg/gpg.conf`

Normally, two people should cross sign their keys.  I.e., both should do both steps as shown below.

If you need to quickly find your key ID you can use the following command:

```
gpg --list-keys <email-address>
```

You should also have this key ID as the default in `$HOME/.gnupg/gpg.conf` .

Substitute `<MY-KEY-ID>` below with your own key ID and `<OTHER-KEY-ID>` with that of the person who's key you are signing.

Have Your Key Signed by Someone Else
------------------------------------

Get the fingerprint of your key:

```
gpg --fingerprint <MY-KEY-ID>
```

The last 16 digits (some use 8) of the fingerprint are used as the key ID.  You can use the fingerprint to verify the key over the phone or in a face-to-face meeting.  In the later case you may want to have a printout:

```
gpg --fingerprint <MY-KEY-ID> | lpr
```

The other person should either know you well enough or verify your identity by checking some form of official ID document (passport, etc.).  Be prepared to provide such proof of identity.

Once they have signed your key you will obtain the signatures via mail or they will have uploaded them to a key server.  If you got the signature via mail you can import them as follows:

```
gpg --import signature.asc
```

(replace `signature.asc` with the file you got as an attachment in the mail)

If they uploaded the key to the key server you can obtain the new signature with

```
gpg --keyserver-options no-self-sigs-only --recv-key <MY-KEY-ID>
```

Sign Someone Else's Key
-----------------------

Have the other person give you the fingerprint of their key and make sure you verify their identity (e.g., by looking at their passport).

### Using gpg

Get the key

```
gpg --recv-key <OTHER-KEY-ID>
```

Alternatively import the key as file

```
gpg --import <PUBLIC-KEY-FILE>
```

Sign the key

```
gpg --sign-key <OTHER-KEY-ID>
```

Upload the signed key to the key servers

```
gpg --send-key <OTHER-KEY-ID>
```

Alternatively export it back to a file and give it back

```
gpg -a --export <OTHER-KEY-ID> > <PUBLIC-KEY-FILE-SIGNED>
```

### Using caff

Alternatively one can use `caff` to sign keys ([caff](https://wiki.debian.org/caff) is part of the [signing-party](http://packages.debian.org/source/stable/signing-party) Debian package).

The configuration of caff is in `$HOME/.caffrc` and should look like:

```
$CONFIG{'owner'} = q{My Real Name};
$CONFIG{'email'} = q{my.address@example.com};
$CONFIG{'keyid'} = [ qw{<MY-KEY-ID>} ];
```

Replace the values with your own.

You can then sign with the command:

```
caff <OTHER-KEY-ID>
```

This will lead you through a series of questions.  Make sure you check the fingerprint.  Once you signed the key you can say `save` at the gpg prompt.  caff will then send out the signature by mail.

While mail is the preferred way to give back the signature (it will reach the recipient directly and make sure that the mail address is correct at the same time), the other person may want you to upload their signed key to the key servers.  You can do this as follows:

```
gpg --send-key <OTHER-KEY-ID>
```
