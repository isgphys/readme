How to add tests to our monitoring
==================================

### Standalone Tests reporting to Big Brother/Hobbit/Xymon via TCP

Check what ever you want, and after checking it simply connect to your favorite BigBrother server, usually at TCP port 1984 and send you test results in the following form:

```
status[+<minutes>] <host>.<service> <color> <date> - <state>
<message>
```

where:

* `<minutes>`: Optional timespan in minutes for how long this state will be valid. (Usually is around 1.5 times of the check interval.) There must be a plus sign in between the number of minutes and the word "status", if given. Default value is five minutes.
* `<host>`: The (short) hostname of the host being tested with this test. This may not necessarily be the host sending the message.
* `<service>`: The (short) name of the service being tested with this test.
* `<color>`: The color the test resulted in. Unless writing software for managing BigBrother servers this should only be one of either red, yellow or green.
* `<date>`: Usually the (chomped) output of `date`.
* `<state>`: A word, usually in CAPSLOCK, concisely describing the situation, e.g. for a temperature test the states could be OK or NORMAL (for green), WARM (for yellow), BOILING (for red), but can also state additional situations as ERROR (e.g. in connection with red, yellow or perhaps even purple) or NOTHING TO REPORT (e.g. in connection with green)
* `<message>`: An arbitrary, maybe multi-line message which can include HTML code and therefore images (like RRD stats, BigBrother/Hobbit/Xymon icons for single components of the test), external links, log file contents, etc. Only HTML entities do not work well. Use ISO-Latin-1 8-bit characters instead. (Will be truncated by default a 4kB (BB), in our environment at 1MB.)

Does not have to a local test, e.g. one host can test several other hosts and then report to the BigBrother/Hobbit/Xymon server.

### External Tests run by the local client every 5 Minutes

Work similar, but will be called every five minutes from the local BigBrother client and has to be configured per host in `$BBHOME/etc/bb-bbexttab`. Will get some configuration data from environment variables such as:

* `$BBHOME`: Home of the user `bb` and the base directory of the BigBrother client installation
* `$BBTMP`: Directory for temporary files.
* `$BB`: Location of the BigBrother client binary.
* `$BBDISP`: List of possibly several BigBrother servers to report to.

Shell scripts usually source `$BBHOME/etc/bbdef.sh` to get all configurations.

At the end, the script or program should call

```
$BB $BBDISP "$LINE"
```

where `$LINE` is the data which will be send to the servers by the BigBrother client. It has the same syntax and features as for standalone tests except that the optional validity doesn't make as much sense as for standalone tests.
