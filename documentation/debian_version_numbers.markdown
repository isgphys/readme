Debian Version Numbers
======================

The version number of a package contains several valuable pieces of information which we summarize below. For further details refer to [Debian policy manual - Version numbering](https://www.debian.org/doc/debian-policy/ch-controlfields.html#s-f-Version).

Simple generic examples
-----------------------

These examples show the necessary syntax

* Native package (Debian package = Upstream software): 123
    * Package version = Upstream version
* Non-native package: 123-3
    * 123 = Upstream software version
    * 3 = Debian revision, increments if only the packaging has changed between two package releases.
    * Means: Upstream version is 123 and it's the third Debian package based on that upstream version.
* Package with epoch, e.g. if upstream changed from date based to major.minor versioning
    * Old upstream version: 2013.06.06
    * New upstream version: 1.0
    * Problem: 1.0 < 2013.06.06
    * Solution: Add a (debian-specific) epoch:
        * Old package version (example): 2013.06.06-4 (upstream version + debian revision + implicit epoch of "0", i.e. 0:2013.06.06 is equivalent)
        * New package version: 1:1.0-1 (epoch of 1 + upstream version + debian revision)

Debianized upstream version number conventions
----------------------------------------------

* "~" sorts before the empty string:
    * `1.0~beta1 < 1.0`
    * `2.1~~pre < 2.1~alpha < 2.1~beta < 2.1~rc` (Mozilla style pre-releases)
* Dashes allowed in non-native upstream versions, last dash separates Debian revision:
    * 2013-06-06-3:
        * 2013-06-06 = Upstream version
        * 3 = Debian revision

Generic suffix conventions (native and non-native packages)
-----------------------------------------------------------

Suffixes on package versions can contain further information about a package. All these things are rather conventions, but expected in some places:

* `-<x>.<y>`: Non-maintainer upload (NMU) of a non-native package
    * 123-2.1 = First non-maintainer upload based on the maintainer's 123-2 package
    * 124-0.1 = Non-maintainer upload of a new upstream version
* `+nmu<n>`: Non-maintainer upload (NMU) of a native package
    * 123+nmu3 = Third non-maintainer upload based on package version 123
* `~bpo<x>0+<y>`: Backport
    * 123-3~bpo70+2 = Backport of package version 123-3 to Debian 7.0, second backport package
* `+b<n>`: Binary rebuild without source changes (so called binary non-maintainer upload = BinNMU), i.e. due to updated build-dependencies
    * 123-4+b2 = Second rebuild of source package 123-4  without source changes
* `build<n>`: Binary rebuild without source changes, Ubuntu style
    * 123-4build2 = Second Ubuntu rebuild of source package 123-4  without source changes
* `+deb<x>u<y>`: Stable update
    * 121-3+deb7u2 = Second stable update of the package 121-3 in Debian 7
    * 121-3+deb70u2 = dito (less common variant)
* `ubuntu<n>`: Package changed by Ubuntu compared to the package in Debian
    * 123-3ubuntu4 = Fourth Ubuntu revision of Debian's 123-3 package
    * 124-0ubuntu1 = Ubuntu packaged a newer upstream release before Debian, but based on the according Debian package in a way that Debian's first package may still supersede Ubuntu's package, i.e. 124-1 > 124-0ubuntu1

Full example
------------

2:3.6.19-1~bpo70+1+b1

* 2 = epoch, provided to allow mistakes in the previous package numbers to be left behind. (dpkg-/apt-specific)
* 3.6.19 = Upstream software version (often major.minor.micro version number)
* 1~bpo70+1+b1 = Full debian revision
    * 1 = Original debian revision to allow multiple packages of the same upstream version, increments if only the packaging has changed between two package releases.
    * ~bpo70 = bpo = backport, 70 = to Debian 7 aka Wheezy
    * +1 = Backport revision, increments if only the backport packaging has changed between two backport package  releases.
    * +b1 = First binary rebuild without source changes
