Subversion (SVN)
================

Side note
---------

Based on our own experience we recommend Git instead of Subversion nowadays if there's no strong preference for any versioning system. See the following README pages for more information.

* [[Git Basics]]
* [[Git Hints]]
* [[Git Advanced Hints]]

Differences to CVS
------------------

While CVS is file based and sees no relationship between files except in which directory they reside, Subversion (SVN) sees a project as a whole thing with one global state and not the states of many single files. So revision numbers are global for a whole project. One revision means a consistent state over all files. Another revision number specific difference is that revision numbers are integers and start with revision 1 at import.

Another effect of Subversion's global view is, that also directories have revisions, that files can be moved around or copied with their whole history without fiddling around in the repository itself. Then also file properties (MIME type, svn:ignore for directories, arbitrary, project-specific properties, etc.) are versioned in the repository, too.

Create a repository for your project
------------------------------------

If you collaborate within a group of people, all participants should have read and write access.  Nobody else should have write access.

### Create it on one of our group drives

If all your contributors do have a D-PHYS account and access to the same group drive, you can host your Subversion repository on that's group's drive. To allow it to be used on all our Linux workstations independent of the Subversion version being installed, you need to configure some backwards compatibility:

```
ssh login.phys.ethz.ch svnadmin create --pre-1.5-compatible /home/<group name>/[<some optional path>/]<your project repository>
```

You then can always use

```
svn+ssh://login.phys.ethz.ch/home/<group name>/[<some optional path>/]<your project repository>
```

as URL for your repository.

### Create it at the default location (if you are the server administrator)

```
svnadmin create /var/lib/subversion/[<group directory>/]<your project repository>
```

General Subversion Usage
------------------------

### Import project

```
cd ..
svn import -m "Initial import" <your old project directory> <access-scheme>://<server>/[<group directory>/]<your project repository>
```

`<access-scheme>` may be one of `http`, `https`, `svn`, `svn+ssh` or `file` or even a self defined access scheme of the form `svn+<your personal system to access the repository>`.

A project may already exist if you are joining a group.  You will be told the project name.

### Web Interface

ISG D-PHYS does not provide web interfaces for Subversion repositories.

### Checkout project

```
svn checkout <access-scheme>://<server>/[<group directory>/]<your project repository> [<your new project directory>]
```

(Shortcut: `co` instead of `checkout`)

All steps up to here are necessary to (create and) start working with a Subversion project.

### Update Project from Repository

```
cd <your project directory>
svn update
```

(Shortcut: `up` instead of `update`)

### Show Local Status and Changed Files

```
svn status
```

(Shortcut: `stat` or `st` instead of `status`)

#### Show Locally Changed Files only

```
svn status --quiet
```

(Shortcut: `-q` instead of `--quiet`)

Typical use: `svn st -q`

#### Show Updates available in the Repository

```
svn status --show-updates
```

(Shortcut: `-u` instead of `--show-update`)

Typical use: `svn st -u`

### Show Locally Made Changes

```
svn diff
```

Typical use: `svn diff <filename>`

#### Show Changes Between Local Copy and Revision x

```
svn diff -r<x>
```

#### Show Changes Between Revisions x and y

```
svn diff -r<x>-<y>
```

### Commit changes to the Repository

```
cd <your project directory>
svn commit -m "Commit message (e.g. what you changed)"
```

(Shortcut: `ci` instead of `commit`)

If you changed a file, added or removed one, renamed something, changed properties, create an directory (see all below), etc. you have to commit it to the repository so the change is known there and so others can see it. Otherwise only you can see the changes in your locally checked out copy.

### Revert Changes Locally Made to a File

```
svn revert <filename>
```

### Add new file or directory

```
svn add <new file or directory>
```

If you add a whole directory, all files and subdirectories are added automatically unless they are ignored by Subversion (e.g. Emacs backup files ending with `~`) or you give the `-N` option:

```
svn add -N <new directory>
```

### Remove a File or Directory

```
svn remove <file or directory>
```

or

```
svn delete <file or directory>
```

(Shortcuts: `rm` or `del`)

Note: the project still keeps information about a removed file or directory in case you want to check out an old version.

### Create a Directory

```
svn mkdir <directory>
```

In comparison to CVS which cares only about files, Subversion also cares about directories, so if you want to create a new directory in you project, can either create it on the filesystem and then add it (see above) or just let Subversion create and add it at once.

### Copy or Move Files

```
svn copy <source> <target>
svn move <source> <target>
```

(Shortcuts: `cp` and `mv` or `rename`)

More information
----------------

If you need a quick reference for one Subversion sub-command, e.g. update, use

```
svn help update
```

The same counts for `svnadmin`:

```
svnadmin help create
```

For more read the O'Reilly Subversion book, which is available online at its most current version at http://svnbook.red-bean.com/nightly/en/index.html
