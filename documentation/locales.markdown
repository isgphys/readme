Locales
=======

With [locale](http://en.wikipedia.org/wiki/Locale) you can set the language, country, regional settings for formatting (numbers, dates and time, currency, etc.), character encoding, and more.  Here are some simple hints to make locales work.

System Settings
---------------

Note: on managed Linux workstations this is done by the [[ISG|services/contact]].  If you experience problems please [[services/contact]] us.  On your own standalone Linux workstation or laptop you may want to set up locale yourself.  The commands assume a Debian-like distribution (e.g. Ubuntu).  All of this may already have been set during installation if you chose the appropriate options.  The configuration commands have to be done as root (root shell or via sudo).

Make sure the `locales` package is installed.  Choose at least the `en_US.UTF-8` locale.  Add others according to your desires.

```
apt-get install locales
```

If you want to change the supported locales afterwards you can do

```
dpkg-reconfigure locales
```

### Choose the default locale for the entire system

Check whether the default locale is already set to a reasonable value

```
cat /etc/default/locale
```

If necessary, set the default locale (`en_US.UTF-8` is a reasonable value)

```
echo 'LANG="en_US.UTF-8"' > /etc/default/locale
```

If all else fails you can always to the fallback value `C`

```
echo 'LANG=C' > /etc/default/locale
```

User Accounts
-------------

Each user can set the locale for their session or even per process.  To find out which locales are available on the system use

```
locale -a
```

On the managed workstations we typically configure some English, German, and depending on the software release French and Italian too.  We strongly recommend to use one of the `utf8` locale.

To set a new locale for a shell use

```
export LANG=de_CH.UTF-8
```

If you want to start a single application with a different locale you can use

```
LANG=de_CH.UTF-8 some_application
```

On the managed workstations we usually install the English version of a software and add other languages only on demand.  If you think an application should be available in a language other than English please [[services/contact]] us.  Not all applications have been translated to your language of choice so you may be out of luck (or have to contribute a translation yourself).
