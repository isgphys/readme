# InfluxDB backup and restore

The databases are backed up every night using the scripts provided on the page below.

- [How to backup and restore InfluxDB](https://influxdb.phys.ethz.ch/backup_restore.html)

This guide shows how to backup and restore `InfluxDB` (v.1.5+) using the legacy method.
If you want to migrate your data to our InfluxDB instance follow the procedure to make a full backup
and provide the location of your data to us. You can download the scripts here:

- [backup-influxdb](https://influxdb.phys.ethz.ch/backup-influxdb)
- [restore-influxdb-full](https://influxdb.phys.ethz.ch/restore-influxdb-full)
- [restore-influxdb-database-online](https://influxdb.phys.ethz.ch/restore-influxdb-database-online)

This guide assumes the backups are stored at the following location:

```bash
/var/backups/influxdb
```

## Full InfluxDB backup

```bash
backup-influxdb
```

If authentication is enabled, provide the admin user credentials in the file `~/.influx_admin`:

```bash
export INFLUX_USERNAME="admin"
export INFLUX_PASSWORD="secret"
```

## Restore

### Single database (online) restore

```bash
influx -execute "drop database <db>"
restore-influxdb-database-online <backupdir> <db> [<new_db>]
```

Manually grant privileges shown (adapt db name if `<new_db>` is used).

### Full (offline) restore

```bash
systemctl stop influxd
rm -r /var/lib/influxdb/data
rm -r /var/lib/influxdb/meta
rm -r /var/lib/influxdb/wal
restore-influxdb-full <backupdir>
systemctl start influxd
```

## Manual backup/restore

Here are a some example commands for manual backup/restore.

### Legacy method (preferred)

```bash
BACKUPDIR="/var/backups/influxdb"
DATE="$(date +%Y%m%d-%H%M%S)"
umask 077

# Full backup:

influxd backup "${BACKUPDIR}/${DATE}"
influxd backup -database _internal "${BACKUPDIR}/${DATE}"
# Repeat the next line for all databases:
influxd backup -database <db> "${BACKUPDIR}/${DATE}"

# Full restore (offline):

systemctl stop influxdb
rm -r /var/lib/influxdb/*
systemctl start influxdb
systemctl stop influxdb
influxd restore -metadir /var/lib/influxdb/meta "${BACKUPDIR}/<date>"
influxd restore -db _internal -newdb _internal -datadir /var/lib/influxdb/data "${BACKUPDIR}/<date>"
# Repeat the next line for all databases:
# `-newdb` required in >1.6, see bug https://github.com/influxdata/influxdb/issues/10072
influxd restore -db <db> -newdb <db> -datadir /var/lib/influxdb/data "${BACKUPDIR}/<date>"
chown -R influxdb:influxdb /var/lib/influxdb/*
systemctl start influxdb

# Single database restore (online):

influx -execute "drop database <db>"
influxd restore -online -db <db> "${BACKUPDIR}/<date>/data/<db>"
influx -execute "grant <priv> on <db> to <user>"
```

### Portable method

```bash
BACKUPDIR="/var/backups/influxdb"
DATE="$(date +%Y%m%d-%H%M%S)"
umask 077

# Backup:

influxd backup -portable "${BACKUPDIR}/${DATE}"

# Restore:

influx -execute "drop database <db>"
influxd restore -portable -db <db> "${BACKUPDIR}/<date>"
influx -execute "grant <priv> on <db> to <user>"
```
