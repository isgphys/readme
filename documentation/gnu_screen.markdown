GNU Screen
==========

[Screen](http://www.gnu.org/software/screen/) is a Unix tool that can be thought of as a window manager for the shells inside your terminal. It allows to handle several virtual consoles within a single terminal window.

Furthermore it can provide a persistent console when working remotely with ssh. If you use ssh to work remotely on a computer, you often want your processes to keep running even when you disconnect or loose the network connection. Screen allows to create persistent shells, which you can detach and re-attach any time, to continue your work where you left off (see the example below).

Alternatives:

- [tmux](https://tmux.github.io) (for usage with background/compute jobs, see [[/linux/workstation/resource_control/#background-or-compute-jobs]])
- [mosh](https://mosh.org)

Main Commands
-------------

From the command-line

```
screen -S name    # start a new screen session with a given name
screen -ls        # list open screen sessions
screen -r name    # re-attach given screen session
```

From within a screen session

```
Ctrl-a d          # detach current screen session
Ctrl-a c          # create new window
Ctrl-a n / p      # switch to next/previous window
Ctrl-a 3          # switch to window number 3
Ctrl-a Ctrl-a     # toggle between current and previous window
Ctrl-a S          # split display horizontally
Ctrl-a |          # split display vertically
Ctrl-a Q          # remove split displays
Ctrl-a Tab        # jump to next display region
Ctrl-a X          # remove current display region
Ctrl-a k          # kill current window
Ctrl-a x          # lock the screen
Ctrl-a ?          # display help with list of shortcut keys
```

The screen utility can be customized through `~/.screenrc`.

### Example: ssh + screen

```
ssh remotehost.phys.ethz.ch     # connect to the remote host
screen -S work                  # start a screen session on the remote host
./some_long_computation.sh      # do you work, for instance by running computations
Ctrl-a d                        # detach the screen session, and close the ssh connection
```

When you later reconnect to that host, you can reconnect to that screen session, see what kind of output was produced while you were away, and continue your work

```
ssh remotehost.phys.ethz.ch     # reconnect
screen -r work                  # re-attach screen session
```
