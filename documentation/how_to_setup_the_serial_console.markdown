How to setup the serial console
===============================

Serial console access allows you to manage, monitor and interact with your system over a serial console.
Your system's BIOS, bootloader (GRUB), Xen, Linux and login access must each be individually configured for serial console access.

It is not strictly necessary to have each component fully functional, but it can be quite useful!

Serial Console BIOS configuration
---------------------------------

Refer to your Hardware vendor's documentation for capabilities and procedures to enable BIOS serial redirection.

We will use the serial port **com1**.

Serial Console GRUB configuration
---------------------------------

Adding the following two lines to your GRUB configuration file, typically either in `/boot/grub/menu.lst` or `/boot/grub/grub.conf` depending on your distro, will enable GRUB serial output.

```
serial --unit=0 --speed=57600 --word=8 --parity=no --stop=1
```

```
terminal --timeout=10 serial console
```

Please refer to the GRUB documentation for further information.

Serial Console Xen configuration
--------------------------------

In order to configure Xen serial console output, it is necessary to add a boot option to your GRUB config.

```
kernel /xen-3.2.1-i386.gz **xencons=tty**
```

Serial Console Linux configuration
----------------------------------

To enable Linux output at boot time, add the parameter `console=ttyS0,57600` to your kernel GRUB line. Under Xen, this might be:

```
module /vmlinuz-2.6.26-1-xen-686 root =/dev/mapper/vg0-root ro **console=ttyS0,57600**
```

to enable output over ttyS0 at 57600 baud.

Serial Console Login configuration
----------------------------------

Logging in to Linux via serial console requires specifying a login prompt be started on the serial port.

To automatically start a login prompt over the serial port, add the line:

```
T0:23:respawn:/sbin/getty ttyS0 57600 vt100
```

to `/etc/inittab`. Run **telinit q** to force a reload of your inittab and start getty.

To enable root logins, add **ttyS0** to `/etc/securetty` if not already present.
