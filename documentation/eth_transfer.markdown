ETH Transfer
============

[ETH Transfer](https://ethz.ch/en/industry/transfer.html) is the technology transfer office of ETH Zürich and the contact for all aspects of proprietary rights.

Members of ETH who want to publish their work as open source project need to follow certain [guidelines](https://ethz.ch/en/industry/researchers/licensing-software/open-source-software.html).

You can find open source developed at or with ETH Zurich [here](https://search.library.ethz.ch/primo-explore/search?query=any,exact,open%20source,AND&tab=default_tab&search_scope=data_archive&sortby=date&vid=DADS&lang=en_US&mode=advanced&offset=0).
