Transfer Browser Bookmarks
===========================

Many people use bookmarks in their browser to keep track of all the pages they need to visit. When switching computers or working remotely, it may be important to transfer the bookmarks. This can easily be achieved by exporting them to HTML on one computer, transferring the file, and importing them on the other machine. Please refer to the detailed instructions, depending on your browser.

* [Firefox](https://support.mozilla.org/en-US/kb/export-firefox-bookmarks-to-backup-or-transfer)
* [Chrome](https://support.google.com/chrome/answer/96816?hl=en)
* [Safari](https://support.apple.com/guide/safari/import-bookmarks-and-history-ibrw1015/mac)
* [Internet Explorer](https://support.microsoft.com/en-us/help/4469351/internet-explorer-move-favorites-to-a-new-pc)
