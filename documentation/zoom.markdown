Zoom Video Conferencing
=======================

ETH users have access to the [Zoom](https://ethz.ch/services/de/it-services/katalog/multimedia/video-konferenz/zoom.html) software for virtual meetings and video conferences.

Log in on [ethz.zoom.us](https://ethz.zoom.us) using the `username@ethz.ch` address. The participants can use the web browser or [download](https://ethz.zoom.us/download) the zoom client.

Please refer to the [Legal information regarding Zoom](https://ethz.ch/services/de/it-services/katalog/multimedia/video-konferenz/zoom/rechtlich.html) for details regarding data privacy and safety.
