Packaging software for Debian
=============================

* [Backporting](https://backports.debian.org/Contribute/)
* Debian mirror
* [Official Debian developer information](http://www.debian.org/devel/)
* Setting up environment variables: `DEBEMAIL="$USER@phys.ethz.ch"` and `DEBFULLNAME="$(finger $USER |grep Name |sed -e 's,.*Name: ,,' | iconv -f ISO-8859-1 -t UTF-8)"`
