How to enable your TPM module
=============================

Some features in modern Operating Systems need a security chip.

One of this security chip is called [Trusted Platform Module (TPM)](https://en.wikipedia.org/wiki/Trusted_Platform_Module).

There are two types of TPMs. An additional Chip on your board (TPM) or one in your CPU (fTPM).

## Check if your TPM is already enabled

Let us check, if our TPM is not already enabled.

* in Windows -> Open the `Start Menu` and type `tpm.msc`. Now you should sea your TPM and some information, when not you need enable it.

## DISCLAIMER FIRST

CHANGES IN YOUR BIOS SETTINGS **CAN BREAK YOUR COMPUTER** AND YOU CAN'T START IT UP AGAIN

the ISG can't fix it, when you don't remember what you have changed and a clean installation will be necessary.

## Enter BIOS

Enter the BIOS could you do it in many ways.

Please ask your computer vendor if you don't know how you can do it.

Usually you can boot into the bios with hitting the keys `F2`, `F10`, `F12`, `Delete` or `Enter`.

When fastboot is enabled there is no option to press a key, but you can enter it from Windows.

* Click the Start menu and select Settings.

* Select Update and Security.

* Click Recovery.

* Under Advanced startup, click Restart now. Your computer will now restart.

* Select Troubleshoot.

* Choose Advanced options.

* Select UEFI Firmware Settings.

* Click Restart to restart the system and enter UEFI (BIOS).

## Enable TPM or fTPM in BIOS

There are few points where the setting can be.

Maybe you need enter the advanced look first.

Keywords in the BIOS can be `TPM`, `fTPM`, `Firmware TPM`, `Trusted Platform Module`, `PTT`, `Intel PTT`

* Search for a Security Panel and take a look about the TPM Settings. In the most case are here some settings.

* If you can't find maybe there is something in the `Advanced Setting` -> `PCH-FW Configuration`
