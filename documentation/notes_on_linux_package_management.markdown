Notes on Linux Package Management
=================================

* [[Debian Version Numbers]]

debsums : check md5 sums of installed debian packages
-----------------------------------------------------

Sometimes it can be useful to make sure the binary on a system has not been tampered with and corresponds indeed to the one provided by the package. Debsums allows to compare the md5 sum of the installed files with those provided by the debian package.

```
debsums <packagename>    # check all files installed by given package
debsums -c               # check all non-config files of all packages and only report the modifed ones
```

apt-file : list or search package contents
------------------------------------------

```
apt-file update                  # update the local database of package contents
apt-file search /usr/bin/htop    # list all packages providing the given file
apt-file list htop               # list all files contained in the given package
```

For installed packages one can also use `dpkg -S <filename>` to show which package installed a given file.

apt-cache policy : list priorities of available package versions
----------------------------------------------------------------

`apt-cache policy <packagename>` provides information about the selection process of the package versions available for installation.

The output includes the version that is currently installed, the one of a possible update candidate and whether a package version is pinned. It displays a version table with the priorities of package sources as well as those of individual packages.

Example:

```
 # apt-cache policy drbd8-utils
drbd8-utils:
  Installed: 2:8.3.11-0ubuntu1
  Candidate: 2:8.3.11-0ubuntu1
  Package pin: 2:8.3.11-0ubuntu1
  Version table:
     2:8.4.3-0ubuntu0.12.04.1 991
        500 http://ubuntu.ethz.ch/ubuntu/ precise-updates/main amd64 Packages
 *** 2:8.3.11-0ubuntu1 991
        500 http://ubuntu.ethz.ch/ubuntu/ precise/main amd64 Packages
        100 /var/lib/dpkg/status
```

The priority in front of each package (500 or 100 in the example above) is the priority advertised by the package in that repository or location. The number at the end (namely 991) is the actual pin priority being placed on the package. This is like a critical threshold that has to be surpassed for a package to be considered a candidate for installation. The package with newer version number 8.4.3 is ignored because its priority (500) is lower than the pin priority (991).

Pinning
-------

In specific cases it may be required to force a certain version or origin of a package and prevent the installation of any upgrades. This can for instance be achieved through package pinning in _apt_. In practice this boils down to creating a config file in `/etc/apt/preferences.d/` with a regexp to match the package names, which version to pin and the priority. Further [documentation](https://manpages.debian.org/stable/apt/apt_preferences.5.en.html) can be found in `man apt_preferences`.

### Pinning by Package Version

Example:

```
Package: /samba|smb|wbclient|winbind/
Pin: version 2:3.6.19-1~bpo70+1
Pin-Priority: 991
```

Use `apt-cache policy <packagename>` and `apt-get upgrade` to check that no updates would be installed for the requested packages.

### Pinning by Package Origin

Use `apt-cache policy` (without further parameter) to figure out what identifies an APT repository.

Example:

```
 # apt-cache policy
Package files:
 100 /var/lib/dpkg/status
     release a=now
[…]
 500 http://stat.ethz.ch/CRAN/bin/linux/ubuntu/ precise/ Packages
     release v=12.04,o=CRAN,a=precise,n=precise,l=CRAN,c=
     origin stat.ethz.ch
[…]
 500 http://ubuntu.ethz.ch/ubuntu/ precise/main amd64 Packages
     release v=12.04,o=Ubuntu,a=precise,n=precise,l=Ubuntu,c=main
     origin ubuntu.ethz.ch
```

Say we want to make sure that packages from CRAN only get used if they were requested explicitly. So we want to pin all packages from that APT repository lower than the default (which is usually 500 or 990 depending on the configuration), so we pin them to at most 499.

You can pick any x=y value pair from the above `release`  which is unique for that APT repository (or mirrors thereof):

* o = Release Origin/Vendor (e.g. Debian, Ubuntu, Grml, CRAN, ETHZ, Opera Software ASA, LP-PPA-ppa-name, "Google, Inc.", etc.)
* v = Release Version (6.0, 12.04, etc.)
* a = Archive/Suite (stable, testing, unstable, etc.)
* n = Code name (wheezy, squeeze, sid, etc.)
* c = Component (main, contrib, non-free, universe, restricted, multiverse, etc.)
* l = Label (Debian-Security, Debian Backports, Google, etc.)

Example on how to avoid Ubuntu packages being upgraded to CRAN-provided packages unless explicitly requested:

```
Package: *
Pin: release o=CRAN
Pin-Priority: 499
```

Use `apt-cache policy` (without further parameter), `apt-cache policy <packagename>` and `apt-get upgrade` to check that no updates would be installed for the requested packages.
