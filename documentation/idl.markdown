IDL: Interactive Data Language
==============================

[IDL](https://www.l3harrisgeospatial.com/Software-Technology/IDL) is a commercial software for scientific data visualization. The GNU Data Language [GDL](https://github.com/gnudatalanguage/gdl) is an open-source alternative.


Installation
------------

The IDL software is available in the [IT Shop](https://itshop.ethz.ch/).


Troubleshooting
---------------

### Failure to acquire window rendering context / Unable to acquire device context

If IDL crashes with this error message when drawing using certain image routines, you may try to disable hardware rendering:

    IDL> PREF_SET, 'IDL_GR_X_RENDERER', 1, /COMMIT

[Details](http://www.exelisvis.com/Support/HelpArticlesDetail/TabId/219/ArtMID/900/ArticleID/5294/5294.aspx)
