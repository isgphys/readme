How to Create an OpenPGP/GnuPG Key
==================================

Note: [OpenPGP](http://en.wikipedia.org/wiki/Pretty_Good_Privacy) is the name of the standard and [GnuPG](http://www.gnupg.org/) (command line name `gpg`) is the name of the free implementation most people use.

At the time of writing we recommend that you use a key length of 4096 bits.  It is generally agreed that 1024 is too short by now and 2048 is OK.  4096 bits will be good for a bit longer though.

Follow these steps:

GnuPG Configuration
-------------------

Choose some configuration for GnuPG.  If you already have some configuration in `$HOME/.gnupg/gpg.conf` you may want to review those settings.  Otherwise you can generate some reasonable defaults as follows:

```
mkdir -p $HOME/.gnupg
chmod u=rwx,go= $HOME/.gnupg

cat > $HOME/.gnupg/gpg.conf << 'EOF'
personal-digest-preferences SHA256
cert-digest-algo SHA256
default-preference-list SHA512 SHA384 SHA256 SHA224 AES256 AES192 AES ZLIB BZIP2 ZIP Uncompressed
keyid-format long
keyserver hkp://keyserver.ubuntu.com
EOF
```

Read `man gpg` to learn about the options.

We recommend to use 16 digit key IDs (the `keyid-format long` above).  You may also encounter the abbreviated version with just the last 8 digits.

Key Generation
--------------

Then generate the actual key:

```
gpg --full-gen-key
```

Choose `(1) RSA and RSA (default)` as the kind of key and 4096 as the key size.  A key without expiry date is convenient (if a bit dangerous if you are the paranoid kind).  Give the name and e-mail as you would like them to appear in the key.  You may choose a comment (like `work` or `private`) or leave it empty.  Make sure you use a **good passphrase** (just make it long as in 20 characters or more, e.g. a few words you can remember).

This will generate a new key for you which is identified by an 16 digit key ID (something like `0123456789ABCDEF` if you chose the configuration recommended above).  Use this key ID below where it says `<KEY-ID>`.

You should add this key as the default key to you GnuPG configuration:

```
echo "default-key <KEY-ID>" >> $HOME/.gnupg/gpg.conf
```

Additional Names/Addresses
--------------------------

If you want to add more e-mail addresses (e.g., to use the key for both work and private) you can do it like this:

```
gpg --edit-key <KEY-ID>
...
command> adduid
```

and give the new real name, e-mail, and optionally a comment.

You may add extra names/e-mail at any time later.  However, for people to sign your key you will want to have all relevant UIDs already be part of the key.  If indeed you add an additional UID later you will need to ask people to sign your key again for the new UID.

Upload to Key Server
--------------------

Send your key to the key servers (the important ones are connected):

```
gpg --send-key <KEY-ID>
```

This will make it more convenient for others to find your key.

Collect Signatures
------------------

Meet other people you want to exchange signed or encrypted messages and continue with [[how to sign an OpenPGP/GnuPG key|how to sign an OpenPGP GnuPG key]]

If you get a chance you should attend some [key signing parties](http://en.wikipedia.org/wiki/Key_signing_party).

Trust
-----

To trust your own key and to encrypt messages (without a trust warning) to other identities you have checked and signed:

```
gpg --edit-key <KEY-ID>
...
gpg> trust
Please decide how far you trust this user to correctly verify other users' keys
(by looking at passports, checking fingerprints from different sources, etc.)

  1 = I don't know or won't say
  2 = I do NOT trust
  3 = I trust marginally
  4 = I trust fully
  5 = I trust ultimately
  m = back to the main menu

Your decision? 5
Do you really want to set this key to ultimate trust? (y/N) y

gpg> q
```

Use the keychain
----------------

You can use `keychain` to re-use ssh-agent and/or gpg-agent between logins.

See [[How to use the keychain for ssh and gpg|documentation/keychain]]
