How to use SSH keys
===================

First we need to create our SSH key pair to use this feature. Later we can add our public key to the list of authorized keys to grant access using our key. Optionally we can use an SSH authentication agent to securely remember our key so we won't have to reenter the passphrase each time we login to a host.

Generate SSH key-pair (ed25519)
-------------------------------

First we need to generate an SSH key-pair. We will use the `ed25519` algorithm, which is both fast and secure.

Open a terminal and enter or paste the text below. Replace the last parameter with your email address (preferably your D-PHYS mail address).

```
ssh-keygen -t ed25519 -C "email@example.com"
```

This creates a new ssh key pair:

```
Generating public/private ed25519 key pair.
```

When prompted to enter a file name, press **Enter**. This accepts the default file location.

```
Enter file in which to save the key (/home/rda/.ssh/id_ed25519): [Enter]
```

At the prompt, **type a secure passphrase**. For more information on secure passphrases see [[How to handle passwords]]. You can use an authentication agent so that you won't have to re-enter your passphrase every time you use your SSH keys (see [[Use an SSH agent|ssh keys#use-an-ssh-agent]] below).

```
Enter passphrase (empty for no passphrase): [Type a passphrase]
Enter same passphrase again: [Type passphrase again]
```

Now let's see what we have created:

```
user@host:~$ cd ~/.ssh
user@host:~/.ssh$ ls
id_ed25519  id_ed25519.pub
```

The SSH key pair consists of two files - a public key and a private key. The private key must be protected (it's like a key to open a door), the public key should be distributed to all machines you like to log in without a password.

Continue with the steps below ([[Add your SSH key to the list of allowed keys|ssh keys#add-your-ssh-key-to-the-list-of-allowed-keys]]) to add your key to the list of allowed keys.

Generate RSA key-pair (optional)
--------------------------------

You may optionally also want to create another key-pair using the `RSA` algorithm. `RSA` is still recommended as a gold standard (strong using a sufficiently large key length and wide compatible). The `ed25519` algorithm may not be compatible with all old clients.

Use a key length of `4096` and replace the last parameter with your email address. Follow the same steps as for the `ed25519` key (see above):

```bash
ssh-keygen -t rsa -b 4096 -C "email@example.com"
```

This will create two more files:

```bash
user@host:~$ ls ~/.ssh/id_rsa*
/home/rda/.ssh/id_rsa  /home/rda/.ssh/id_rsa.pub
```

## Add your SSH key to the list of allowed keys

To allow authentication using our key, we add our public key to the list of allowed keys (`~/.ssh/authorized_keys`) in our `home` directory:

```bash
user@host:~$ cat ~/.ssh/id_ed25519.pub > ~/.ssh/authorized_keys
```

If you also generated an RSA key-pair:

```bash
user@host:~$ cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
```

This will allow us to login using our key on all D-PHYS managed workstations and servers, where we are also allowed to login using our D-PHYS Account password.

How to prefer the newer ed25519 key
-----------------------------------

If you have both `ed25519` and `RSA` keys, you probably want to prefer the faster `ed25519` key. By default ssh uses the `rsa` key first.

To do that, add the following lines to your ssh config (`~/.ssh/config`):

```
IdentityFile ~/.ssh/id_ed25519
IdentityFile ~/.ssh/id_rsa
IdentitiesOnly yes
```

This will only offer those 2 keys in the listed order, any other keys loaded in `ssh-agent` will not be offered.

Login using the SSH key
-----------------------

Connect to the host (`ssh <username>@<host>`) and enter your SSH key passphrase:

```
user@host:~$ ssh rda@login.phys.ethz.ch
Enter passphrase for key '/home/rda/.ssh/id_ed25519': [Type your passphrase]
```

Use an SSH agent
----------------

Our workstations automatically run the `ssh-agent` which can hold your keys. The agent is running as long as you are logged in and allows logging in on another workstation without typing your passphrase. In case the agent doesn't know your keys yet, you can load them with `ssh-add`.

Add an SSH key:

```
user@host:~/.ssh$ ssh-add
Enter passphrase for /home/rda/.ssh/id_ed25519: 
Identity added: /home/rda/.ssh/id_ed25519 (email@example.com)
```

List the agent's keys:

```
user@host:~$ ssh-add -l
256 SHA256:qmV/Q9Xqly06wFrNtVjNswEMAAydR9rPeut0BDQy4Zk email@example.com (ED25519)
```

Clean out all keys:

```
user@host:~$ ssh-add -D
All identities removed.
```

Use the keychain
----------------

You can use `keychain` to re-use ssh-agent and/or gpg-agent between logins.

See [[How to use the keychain for ssh and gpg|documentation/keychain]]
