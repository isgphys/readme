How to fix a broken Xen DomU
============================

Some Xen DomU issues we ran into and how we fixed them.

How to fix "clocksource/0: Time went backwards"
-----------------------------------------------

* Add the following command to /etc/rc.local `echo "jiffies" > /sys/devices/system/clocksource/clocksource0/current_clocksource`
* See also http://bugzilla.xensource.com/bugzilla/show_bug.cgi?id=1098

SNMP Monitoring Problems
------------------------

* Were caused by spurious second default routes on DomUs with two DHCP-configured network interfaces.
* See [Debian Bug #507452](http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=507452) and [RT#32615](https://rt.phys.ethz.ch/rt/Ticket/Display.html?id=32615).

How to fix a broken Xen DomU created with xen-tools 3.9
-------------------------------------------------------

### In the Dom0

Replace "sda" by "xvda" in the DomU configuration file:

```
root@phd-xenhost:/etc/xen/ # diff alohomora alohomora2
16c16
< root        = '/dev/sda2 ro'
---
> root        = '/dev/xvda2 ro'
18,19c18,19
<                   'phy:/dev/vg0/alohomora-swap,sda1,w',
<                   'phy:/dev/vg0/alohomora-disk,sda2,w',
---
>                   'phy:/dev/vg0/alohomora2-swap,xvda1,w',
>                   'phy:/dev/vg0/alohomora2-disk,xvda2,w',
```

### In the DomU

Replace "sda" by "xvda" in `/etc/fstab` and "tty1" by "hvc0" in `/etc/inittab`:

```
root@phd-xenhost:/mnt/ # diff tmp/etc/inittab tmp2/etc/inittab
54c54
< 1:2345:respawn:/sbin/getty 38400 tty1
---
> 1:2345:respawn:/sbin/getty 38400 hvc0
root@phd-xenhost:/mnt/ # diff tmp/etc/fstab tmp2/etc/fstab
5,6c5,6
< /dev/sda1 none swap sw 0 0
< /dev/sda2 / ext3 noatime,nodiratime,errors=remount-ro 0 1
---
> /dev/xvda1 none swap sw 0 0
> /dev/xvda2 / ext3 noatime,nodiratime,errors=remount-ro 0 1
root@phd-xenhost:/mnt/ #
```

