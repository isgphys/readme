How to propagate your Editor, Language and other Choices to Remote Machines via SSH
===================================================================================

Preparations on the Server-Side
-------------------------------

To allow users on to propagate environment variables when using SSH, the `AcceptEnv` directive must be used on the server-side

* Add a line `AcceptEnv EDITOR` to your `/etc/ssh/sshd_config`
* Either let the sshd reload its configuration or restart the sshd service: `systemctl restart sshd.service`

### Optional: keep environment variables when working with sudo

When working with `sudo` most environment variables get reset by default. In order to keep specific env variables, add the following line to your `/etc/sudoers` configuration:

```
Defaults env_keep += "EDITOR"
```

Configuration on the Client-Side
--------------------------------

* Each user who wants to use these features should add the line `SendEnv EDITOR` to his `$HOME/.ssh/config` file, either to the section of the host where he wants to propagate the environment variables or to the `Host *` section if the environment variables should be propagated to every host.
* In case you haven't had a `$HOME/.ssh/config` file before, start with the following content saved at that location:

```
Host *
    SendEnv EDITOR
```

Other common Environment Variables to propagate via SSH
-------------------------------------------------------

### GIT

It happens that several people connect to a remote machine with the same admin or root account. You can forward the name and email to be used for git commits by sending the corresponding `GIT_AUTHOR_NAME`, `GIT_AUTHOR_EMAIL`, `GIT_COMMITTER_NAME` and `GIT_COMMITTER_EMAIL` environment variables:


```
Host *
    SendEnv EDITOR GIT_AUTHOR_* GIT_COMMITTER_*
```

### Language and sorting

Append `LANG LC_*` to the `SendEnv`/`AcceptEnv` lists to forward your local language and collation settings.

### TERM

If you want to use all of your terminal's features (like e.g. 256 color support) remotely, add the environment variable `TERM` to the lists above. But be careful if remote hosts don't know about your terminal's features, e.g. if you connect to some older machine with `TERM` set to `xterm-256-color` or so.
