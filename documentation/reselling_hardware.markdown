Reselling hardware
==================

When a group wants to sell or donate old hardware that belongs to ETH, the following [rules](https://ethz.ch/staffnet/de/finanzen-und-controlling/investitionsgueter-inventar-.html) must be followed. In particular the paragraph 8 of the "Wegleitung für die Inventarführung an der ETHZ".
