WeeChat IRC Client
==================

[WeeChat](https://weechat.org) is a modern, terminal-based IRC client.


Installation
------------

```
apt install weechat weechat-plugins
```


Configuration
-------------

Commands to join a chat channel:

```
/server add somename irc.example.com/6697
/connect somename
/join #somechannel
/nick somenick
```

Below you find some lines that can be useful to customize your `~/.weechat/irc.conf`.

```
buffer_switch_autojoin = on
buffer_switch_join = on
highlight_channel = "$nick"
highlight_pv = "$nick"
nick_mode = prefix
server_buffer = merge_with_core
smart_filter_mode = "+"
nick_prefixes = "y:lightred;q:lightred;a:lightcyan;o:lightgreen;h:lightmagenta;v:yellow;*:lightblue"
autoreconnect_delay_max = 600
ban_mask_default = "*!$ident@$host"
whois_double_nick = off
nicks = "<nick>,<nick>_,<nick>__,<your_username>"
ssl_priorities = "NORMAL:-VERS-SSL3.0"
username = "<your_username>"
somename.addresses = "irc.example.com/6697"
somename.ssl = on
somename.autoconnect = on
somename.autojoin = "#somechannel"
```

### Plugins

There are plenty of useful plugins to customize weechat, for instance

* buffer.pl: show sidebar with list of buffers
* notify.py: notifications for highlights and private messages

that can be loaded as follows

```
/script load <scriptname>
```

If you encounter any errors, you may need to install some dependencies first:

```
apt install weechat-scripts
apt install python-notify python-notify2
```


Usage
-----

It's best to start weechat in a dedicated [[GNU screen]] session:

```
screen -S weechat
weechat
```

which you can then detach with **Ctrl+A** -> **Ctrl+D**, and re-attach later `screen -d -r weechat`.


Relay / Proxy
-------------

Weechat can act as small irc server to which other clients can connect. This allows to connect GUI and mobile clients to the same weechat back-end to access all irc channels under the same username. When using this you should ensure that the relay is encrypted with an SSL certificate to avoid plain-text traffic over the internet. As this is an advanced topic we only sketch the important steps for the power users.

The following weechat commands activate the IRC proxy

```
/relay add ssl.irc.example 8001                 # activate ssl-encrypted irc relay for the server on port 8001
/set relay.network.passwort <randompassword>    # define a password for relay connections
/relay sslcertkey                               # (re-)load the
/save                                           # save configuration changes
```

Example snippet to execute when renewing Let's Encrypt certificates to update and reload the file in weechat:

```sh
cat /etc/letsencrypt/live/example.com/privkey.pem \
    /etc/letsencrypt/live/example.com/fullchain.pem > \
    /home/johndoe/.weechat/ssl/relay.pem
chown johndoe:johndoe /home/johndoe/.weechat/ssl/relay.pem
printf '%b' '*/relay sslcertkey\n' > /home/johndoe/.weechat/weechat_fifo_*
```

## Notifications

This example shows how to configure separate notification rules per channel. It will set the following notification settings:

- notify about messages in all channels (global server setting)
- always notify in `#important-channel`
- never notify in `#crowded-channel`

```
/set weechat.notify.irc.example message
/set weechat.notify.irc.example.#important-channel all
/set weechat.notify.irc.example.#crowded-channel none
```

Possible options:

Notify level | Level of messages added to hot-list
------------ | ----------------------------------
`none`       | (none)
`highlight`  | highlight + private
`message`    | highlight + private + message
`all`        | highlight + private + message + low

### Highlights

To enable highlights, when a user sends a message important for all users like `*: important msg for all...` set the following parameter:

```
/set weechat.look.highlight_regex ".*\*:.*"
```

### Set extra highlights on buffer

You can force highlight using a regular expression with the buffer property `highlight_regex`. For example to force the highlight on all messages in the current buffer:

```
/buffer set highlight_regex .*
```

The buffer property `highlight_regex` is not saved in configuration.
You can easily save it with the script `buffer_autoset.py`:

```
/script install buffer_autoset.py
/help buffer_autoset
# highlight all messages in channel
/buffer_autoset add irc.example.#somechannel highlight_regex .*
# no hilights in channel `monitoring` by nick `xymon`
/buffer_autoset add irc.example.#monitoring hotlist_max_level_nicks_add xymon:0
```

Save all options:

```
/save
```

### Filter

Enable smart filter for `join/part/quit` messages on a specific channel (example: `#openldap`):

```
/filter add irc_smart_openldap irc.freenode.#openldap irc_smart_filter *
```

You can hide only join or part/quit with following options:

```
/set irc.look.smart_filter_join on
/set irc.look.smart_filter_quit on
```

You can setup delay (in minutes), only filter if a nick did not speak during last 5 minutes:

```
/set irc.look.smart_filter_delay 5
```

You can use regex to filter any message. For example to filter all server messages for connecting/exiting clients:

```
/filter add irc_filter_connect_disconnect * * \*\*\* Client (connect|exit)ing: 
```

Get help:

```
/help filter
```
