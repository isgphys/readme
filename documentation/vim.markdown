Vim
===

Vim is one of the most powerful text editors. It is at the same time light and highly customizable and exists on virtually every platform. Unfortunately it comes with a steeper learning curve than other text editors. One reason is that vim is a **modal** editor which distinguishes :

* Insert mode to type or edit text
* Command mode to run commands
* Visual mode to select text with the cursor.

##### Most important shortcuts to switch between the modes

* `ESC` switch to command mode
* `i` switch to insert mode
* `v` switch to visual mode character-wise
* `V` switch to visual mode line-wise

##### Essential commands

* `:q`  quit when no changes where made
* `:q!` quit discarding any changes made
* `:w`  write/save current document
* `:w!` write/save current document even though it's read-only
* `:wq` write/save current document and quit (same as `ZZ`)
* `:e file` open file
* `:42` jump to line number 42
* `/pattern` search forward for pattern
* `?pattern` search backward for pattern
* `n` show next search result
* `N` show previous search result
* `u` undo last change
* `ctrl-r` redo last change

##### Movement commands

* `h,j,k,l` behave just like arrow keys left, down, up, right
* `0` go to first column
* `^` go to first non-blank character
* `$` go to end of line
* `w` go to start of following word
* `e` go to end of word under cursor
* `gg` go to first line
* `G`  go to last line
* `g,` go to last modification
* `%` jump to matching counterpart (parenthesis, if else, ...)
* `*` search forward for word under cursor
* `#` search backward for word under cursor

##### Editing commands

* `o` new line below cursor, and switch to insert mode
* `O` new line above cursor, and switch to insert mode
* `dd` delete line under cursor (and copy it to register)
* `yy` yank line under cursor into register = copy
* `p`  put contents of register = paste
* `x`  delete character under cursor (and copy it to register)
* `dw` delete word under cursor (and copy it to register)
* `cw` change word = delete word under cursor, and switch to insert mode
* `A`  append at end of line
* `I`  insert at beginning of line
* `~` toggle case of letter under cursor (lowercase <-> uppercase)
* `J` join the current line with the next one
* `>>` add level of indentation
* `<<` remove level of indentation

##### Advanced commands

* `:set invnu` toggle line numbers
* `ctrl-x ctrl-f` auto-complete filename (when in insert mode)
* `:%s/old/new/g` replace old by new in current document
* `:g/pattern/d` delete all lines matching pattern
* `:g!/pattern/d` delete all lines not matching pattern (same as `:v/pattern/d`)
* `:bn`, `:bp` switch to next/previous open buffer

##### .vimrc

Vim can be configured through the `$HOME/.vimrc` file. Below we list some entries that can be useful to have in the `.vimrc`

```vim
"General
syntax on           "syntax highlighting
set nocompatible    "enable all vim features
set autoindent      "automatic indentation
set showmatch       "show matching parenthesis
set ruler           "show current position at the bottom
set number          "show line numbers

"Tabs
set expandtab       "convert all tabs to spaces
set tabstop=4       "use tabs of 4 spaces
set shiftwidth=4    "indents are 4 spaces wide

"Search
set incsearch       "incremental search
set wrapscan        "continue search from top when reaching the end
set ignorecase      "search is not case-sensitive
set smartcase       "search case-sensitive for uppercase patterns
set hlsearch        "search pattern highlighting
```
