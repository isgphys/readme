Notes about migrating from ISO-8859-x to UTF-8
==============================================

See also [[locales]]

From daduke
-----------

just some notes of the difficulties I encountered when migrating my account from ISO to UTF-8

* first off: locale. It's a mess and I cannot provide a thorough tutorial here, but make sure that `LANG`, `LC_CTYPE` and/or `LC_ALL` contain something with `.UTF-8` at the end.
* next: terminal program. aterm can't do UTF-8, Eterm can't either, so use `urxvt`, `uxterm` or `gnome-terminal`
* terminal codepage: even w/ a UTF-8 capable terminal you sometimes still have to TELL the terminal to actually use UTF-8
* window manager: most WMs or desktop environments have their own language/i18n setting. Make sure those are set to UTF-8

From XTaran
-----------

* Use uxterm instead of xterm (both in the Debian/Ubuntu package xterm, uxterm is just a wrapper script)
* Use urxvt (Debian/Ubuntu packages rxvt-unicode*) instead of rxvt
* On Debian/Ubuntu (as root), use `update-alternative --config x-terminal-emulator` to set the default terminal emulator which most applications call to an UTF-8 capable terminal emulator (see comments above)
* At least on Debian/Ubuntu edit `/etc/environment` to set the global default locales.
* On Debian/Ubuntu (as root), use `dpkg-reconfigure locales` to configure which Locales should be generated.
* On Debian/Ubuntu install and use the package `utf8-migration-tool`.
