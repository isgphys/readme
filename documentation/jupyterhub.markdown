JupyterHub
==========

The ETH is offering JupyterHub instances for lectures that want to make use of Jupyter Notebooks in their teaching or exercises.

* [LET JupyterHub](https://ethz.ch/staffnet/en/teaching/academic-support/it-services-teaching/teaching-applications/jupyterhub.html) overview
* [LET JupyterHub with Moodle](https://wp-prd.let.ethz.ch/usingjupyterhubwithmoodle/) technical documentation


Data handling
-------------

For most use cases, [connecting a git repository](https://wp-prd.let.ethz.ch/usingjupyterhubwithmoodle/chapter/git-repository/) should be enough to bring the lecture notes, exercises and small amounts of data into JupyterHub. Note that our D-PHYS groupshares can not be accessed from inside JupyterHub. Alternatively you can make use of your [[personal or group homepage|web/how_to_get_a_personal_homepage]] to make slightly larger data files available via the web server, to that they can be downloaded from inside the Jupyter notebooks.
