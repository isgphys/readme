How to Use Email with Pine (IMAP)
=================================

#### Configuring Pine for IMAP

**M**enu, **S**etup, **C**onfig `inbox-path={imap.phys.ethz.ch}INBOX`

**S**etup, Collection **L**ists, **C**hange `Path: {imap.phys.ethz.ch}[] ^X`

#### Configuring Pine for IMAP over SSL

Pine since version 4.30 knows about SSL and most installations should support it. With such a pine, you may use:

```
inbox-path={imap.phys.ethz.ch/ssl}INBOX
Path: {imap.phys.ethz.ch/ssl}[]
```
