Exchange Online
===============

ETH Informatikdienste are migrating all email accounts from their on-prem Exchange server to the Microsoft Cloud. The **D-PHYS** mail server is **unaffected** and remains with all its data in the ETH server room.

Please be advised that currently ETH does not back up your mailboxes in Exchange Online.

Migrating your email client
---------------------------

**If your `@ethz.ch` email is being forwarded to your `@phys.ethz.ch` account, you don't have to do anything.**  _(To review or change your email forward settings, go to [https://www.password.ethz.ch](https://www.password.ethz.ch) -> `Self Service` -> `Manage forward`)._

With the exception of Outlook on Windows, if your email client connects to the ETH Exchange mailbox, it must be re-configured after the migration. This means you'll have to delete your `@ethz.ch` email account and recreate it with the new server settings. This is also affects all your mobile clients (phones, tablets etc). You’ll find links on how to update popular clients below.


| Mail client        | English                                                          | German                                                             |
| -------------      | ---------                                                        | --------                                                           |
| Thunderbird        | [EN](https://unlimited.ethz.ch/display/itkb/Thunderbird)         | [DE](https://unlimited.ethz.ch/display/itwdb/Thunderbird)          |
| Outlook (Windows)  | [EN](https://unlimited.ethz.ch/display/itkb/Outlook+on+Windows)  | [DE](https://unlimited.ethz.ch/display/itwdb/Outlook+auf+Windows)  |
| Outlook (macOS)    | [EN](https://unlimited.ethz.ch/display/itkb/Outlook+on+macOS)    | [DE](https://unlimited.ethz.ch/display/itwdb/Outlook+auf+macOS)    |
| Apple Mail (macOS) | [EN](https://unlimited.ethz.ch/display/itkb/Apple+Mail+on+macOS) | [DE](https://unlimited.ethz.ch/display/itwdb/Apple+Mail+auf+macOS) |
| Mail (iOS, iPadOS) | [EN](https://unlimited.ethz.ch/display/itkb/iOS+and+iPadOS)      | [DE](https://unlimited.ethz.ch/display/itwdb/iOS+und+iPadOS)       |
| Android Apps       | [EN](https://unlimited.ethz.ch/display/itkb/Android+Apps)        | [DE](https://unlimited.ethz.ch/display/itwdb/Android+Apps)         |
| Mutt               | [EN](https://people.math.ethz.ch/~michele/mutt-oauth2-outlook)   |                                                                    |
| Alpine             | [EN](https://unlimited.ethz.ch/display/itkb/Alpine)              | [DE](https://unlimited.ethz.ch/display/itwdb/Alpine)               |

If you encounter any issues during/after the migration, check ID's [FAQ](https://unlimited.ethz.ch/display/itkb/Exchange+Online+FAQ) for known problems.

The new **Exchange webmail** URL is: [https://outlook.office.com](https://outlook.office.com)


Spam Filter (Exchange Online Protection)
----------------------------------------

* [ETH ID documentation](https://unlimited.ethz.ch/spaces/itkb/pages/248222163/Cloud-Mailfilter+EOP)
    - Quarantine: [https://security.microsoft.com/quarantine](https://security.microsoft.com/quarantine)
    - [Personal spam settings](https://outlook.office.com/mail/options/mail/junkEmail)
