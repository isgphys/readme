Fix Outlook problems
====================

## Outlook Sync problems

When Outlook do not sync all your Email folders for new mails or [webmail](https://webmail.phys.ethz.ch) / your mobile device have not the same items like outlook.

Then you have a sync issue in your Outlook application. Here are two little steps to fix this problem.

### Disclaimer

To prevent sync problems. Use at least as possible different mail clients and close your mail program when you leave your workplace.

### Locate the sync issue

When you are thinking there is something strange with your mail synchronization. Let's check it in your Outlook.

* Go to the FolderView: Bottom left a click on the three dots -> then click on `Folder`

* Go to your mailbox where you have the problem.

* Take a look for the folder `Sync Issues` -> If you have a problem, you will find some messages what are not syncing now. Go to the first error message to find out where the problem is.

Sometimes are here tonnes of messages and if you can't find the first message go to the fix

### Fix the problem

#### IMAP folders with special characters

Outlook allows you to use some characters that are not allowed in IMAP. Also use just one of these characters to name your IMAP folder.

* `A-Z`
* `a-z`
* `0-9`
* `-`
* `<space>`

other characters can make problem.

Find the Folder from the step before or Rename all folders with special characters

#### Two Inbox folder

Sometimes can it happen, that you have two Inbox folders. One called `Inbox` and one `Posteingang`. This can happen when you change your language in Outlook.

To fix this problem you will need delete the cache folder in your local storage at `%LOCALAPPDATA%\Microsoft\Outlook\[YOUR Cachefile]`. But be sure that your your mailbox is in sync with the webmail (take a look at [Locate the sync issue](#locate-the-sync-issue)), if not you will lose some data.

## Prevent Outlook from sending `WINMAIL.dat` attachments

Some users have problems with Outlook because the recipients, for instance Apple Mail users, do not see the whole mail content but only a `winmail.dat` attachment. This can be caused by Outlook sending emails in Rich Text Format (RTF).

To prevent this issue you should change your text format to `Plain Text` or `HTML`.

In Outlook go to the settings in **File** > **Options** > **Mail** and change the following settings in **Message format** and **Compose messages**:

- **Compose messages in the format**: `Plain Text` or `HTML`
- **When sending messages in Rich Text format to internet recipients**: `Convert to Plain Text` or `Convert to HTML`

[[!img /media/windows/outlook/outlook_disable_rtf_1.png size="450x"]]

[[!img /media/windows/outlook/outlook_disable_rtf_2.png size="450x"]]

The preferred setting should be `plain text` because email is only standardized for plain text.
HTML formatting may look different in every client because it is not standardized.
Rich text format (RTF) is Outlook specific and should never be used.

If you still have the problem add the following key in the registry:

```ini
[HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\Outlook\Preferences]
"DisableTNEF"=dword:00000001
```
