How to Use Email with Outlook
=============================

Note that Microsoft's [New Outlook](https://support.microsoft.com/en-us/office/outlook-for-windows-the-future-of-mail-calendar-and-people-on-windows-11-715fc27c-e0f4-4652-9174-47faa751b199) does *NOT* work with the D-PHYS mail server. It also sends your D-PHYS password to Microsoft which is a severe security issue. Use the 'old' Outlook (bundled with Office) instead.

Automatic configuration
-----------------------

Before you start Outlook, go to `Control Panel` by searching for it.

change the **View by** to `Small icons` and open the "Mail | E-Mail" setting.

[[!img /media/how_to_use_email_with_outlook/mail5.png size="450x"]]

Under **E-mail Accounts** can you add an Mail account.

[[!img /media/how_to_use_email_with_outlook/mail6.png size="450x"]]

[[!img /media/how_to_use_email_with_outlook/mail7.png size="450x"]]

This will be configured only by entering your First- and Lastname (Displayname), D-PHYS E-mail Address and Password. Outlook checks automatically for the correct server names and encryption settings.

[[!img /media/how_to_use_email_with_outlook/mail3.png size="450x"]]

[[!img /media/how_to_use_email_with_outlook/mail4.png size="450x"]]

Manual configuration
---------------------

For manual configuration, take a look at: [[mail/how_to_use_email/#technical-details]]
