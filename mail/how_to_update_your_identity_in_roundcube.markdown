How to update your identity in Roundcube
========================================

When you start using Roundcube webmail for the first time, you should update your personal information inside Roundcube. To this end

* log in (https://webmail.phys.ethz.ch/roundcube/)
* go to Personal Settings -> Identities and choose your default identity
* add a Display name, other fields if necessary
* save
