How to Use Email with Mutt (IMAP)
=================================

### About mutt

Mutt ([man page](https://manpages.debian.org/stable/mutt/mutt.1.en.html)) is a free, very powerful and highly configurable text only e-mail client which can be easily used over an [[SSH|documentation/ssh]] connection or inside a [[screen|documentation/gnu screen]]. All settings are made in a configuration file which is located by default at `~/.muttrc`. The home page of mutt as well as a [very comprehensive mutt reference](http://www.mutt.org/#doc) can be found at http://www.mutt.org/.

### mutt and D-PHYS

To use mutt with our IMAP server from Linux workstations managed by us, you need no further configuration changes.

mutt's IMAP implementation is not the fastest, so see below for speeding up IMAP with mutt by enabling mail header caching.

#### mutt and D-PHYS from home or private laptop

If you want to use mutt with our IMAP server from your personal linux box, you have to add the following lines to your `~/.muttrc`:

```
set folder="imaps://$USER@imap.phys.ethz.ch"
set spoolfile="imaps://$USER@imap.phys.ethz.ch/INBOX"
```

You need to replace `$USER` by a hard-coded value representing your D-PHYS username.

#### Optimizing your mutt configuration

There are a few settings which depend on your personal environment, but can speed up mutt or make your mutt sessions even more comfortable.

##### Local IMAP Header Cache

You can use a local file to cache mail headers when using IMAP, so mutt does not need to fetch them every time you want to search through the subjects. Depending on how you use our workstations, one of two different locations should be used:

* If you nearly always use the same workstation, a file on the local disk should be used for best performance. Add the following line to your `$HOME/.muttrc`: `set header_cache="/scratch/.mutt_header_cache_$USER"`
* If you often work on different workstations which share the same home directory (e.g. in our student's pool), you should use a location in your home directory to share the cache across login sessions on different workstations. Add the following line to your `$HOME/.muttrc`: `set header_cache="$HOME/.mutt_header_cache"`
* If you use mutt with our IMAP server from another computer than our workstation, other locations maybe more useful, but the second one should do it nearly everywhere.

The next time you start mutt or reload the configuration file, the configured file will be created with the appropriate rights. For details and more options see the [header_cache entry](http://www.mutt.org/doc/manual/#caching) in the mutt manual.
