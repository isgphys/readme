How to Use Email
================

Our mail server supports many ways to access your mail.

Web based Mail Client
---------------------

Simple access to your mail from any web browser

* https://webmail.phys.ethz.ch/roundcube/

See also [[How to update your identity in Roundcube]]

IMAP based Mail Clients
-----------------------

We recommend the following mail clients:

* [[How to Use Email with Thunderbird]]
* [[How to Use Email with Apple Mail on macOS|How to Use Email with Apple Mail]]
* [[How to Use Email with Apple Mail on iOS/iPadOS|How to Use Email with Apple Mail iOS]]
* [[How to Use Email with Outlook]]
* [[How to Use Email with Outlook on macOS|How to Use Email with Outlook on Mac]]

For some other mail clients we keep the documentation just for reference:

* [[How to Use Email with Mutt (IMAP)|How to Use Email with Mutt IMAP]]
* [[How to Use Email with Pine (IMAP)|How to Use Email with Pine IMAP]]

Frequently Asked Questions
--------------------------

* [[How to automatically forward email or change address book|mail/how to automatically forward email]]
* [[How to setup your own spam filter rules|spamfilter]]
* [[How to configure mail filtering with sieve|sieve]]

Technical Details
-----------------

In a nutshell

* Incoming mail server: imap.phys.ethz.ch
* Outgoing mail server: mail.phys.ethz.ch

or with more technical details for advanced users:

| Hostname             | Protocol | Port    | Notes                                                                        |
|----------------------|----------|---------|------------------------------------------------------------------------------|
| webmail.phys.ethz.ch | HTTPS    | 443     | HTTP over SSL, always encrypted                                              |
| mail.phys.ethz.ch    | SMTP     | 25, 587 | TLS available, use the submission port 587 when ports 25 and 465 are blocked |
| mail.phys.ethz.ch    | SMTPS    | 465     | SMTP over SSL, always encrypted                                              |
| imap.phys.ethz.ch    | IMAP     | 143     | STARTTLS encryption required                                                 |
| imap.phys.ethz.ch    | IMAPS    | 993     | IMAP over TLS/SSL, always encrypted                                          |
| pop.phys.ethz.ch     | POP3     | 110     | STARTTLS encryption required                                                 |
| pop.phys.ethz.ch     | POP3S    | 995     | POP3 over TLS/SSL, always encrypted                                          |
