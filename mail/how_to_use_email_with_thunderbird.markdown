How to Use Email with Thunderbird
=================================


Automatic configuration
-----------------------

If you start Thunderbird for the first time, it will be automatically configured  only with entering your D-PHYS username and emailadress. Thunderbird will check for the correct server names and encryption.

[[!img /media/how_to_use_email_with_thunderbird/tb2.png size="450x"]]

[[!img /media/how_to_use_email_with_thunderbird/tb3.png]]

[[!img /media/how_to_use_email_with_thunderbird/tb4.png]]


Manual configuration
---------------------

In case the automatic configuration does not work, you can setup it manually according these screenshots:

You will be asked, whether to import settings from some other mail client.

[[!img /media/how_to_use_email_with_thunderbird/t1.png alt="Thunderbird (1 of 8)"]]

Chances are that this is your only mail client so there is nothing to import and you just click "Next".  If you already use Thunderbird for some other mail account you open "Account Settings..." from the menu and then choose "Add Account..."

[[!img /media/how_to_use_email_with_thunderbird/t2.png alt="Thunderbird (2 of 8)"]]

Choose "Email account" and click "Next".

[[!img /media/how_to_use_email_with_thunderbird/t3.png alt="Thunderbird (3 of 8)"]]

Enter your full name and your email address.

[[!img /media/how_to_use_email_with_thunderbird/t4.png alt="Thunderbird (4 of 8)"]]

Choose IMAP as the type, imap.phys.ethz.ch as the incoming server and mail.phys.ethz.ch as outgoing server.

[[!img /media/how_to_use_email_with_thunderbird/t5.png alt="Thunderbird (5 of 8)"]]

Incoming and outgoing user name are both your login name.

[[!img /media/how_to_use_email_with_thunderbird/t6.png alt="Thunderbird (6 of 8)"]]

You can choose any account name that you like.

[[!img /media/how_to_use_email_with_thunderbird/t7.png alt="Thunderbird (7 of 8)"]]

You can check your settings and finish the configuration.

[[!img /media/how_to_use_email_with_thunderbird/t8.png alt="Thunderbird (8 of 8)"]]

You will then be asked for the password to read mail.  This finishes the configuration and you can start reading your mail.

Adding ETH address book
----------------------

You can add the ETH address book in Thunderbird.
Just follow [these instructions](https://unlimited.ethz.ch/spaces/itkb/pages/283251869/Thunderbird+address+book+ETH+Active+Directory).
