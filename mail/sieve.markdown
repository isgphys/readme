Mail filtering with Sieve
=========================

The D-PHYS mail server supports mail filtering with [Sieve](http://sieve.info). You can edit your rules in the [Roundcube](https://webmail.phys.ethz.ch/roundcube/) webmail.

* The rules only apply to new, incoming mails and not those delivered in the past.
* All changes must explicitly be saved to take effect.
* So-called _filter sets_ can be used to group multiple filters.
* Use the `+` button to add new filters or filter sets.
* Filters (and filter sets) can be toggled on and off using the cogwheel on the bottom.
* The order of the filters can be changed with drag'n'drop.


Vacation Auto-Reply
-------------------

The following shows how to configure an automatic out-of-office reply.

[[!img /media/mail/webmail_sieve_vacation.png size="450x"]]


Email Forward
-------------

The following rule forwards all messages to a different email address.

[[!img /media/mail/webmail_sieve_forward.png size="450x"]]


File mail into folders
----------------------

The following example automatically files all messages from Alice and Bob into the Friends folder.

[[!img /media/mail/webmail_sieve_fileinto.png size="450x"]]

You may also wish to discard all mails matching a given subject or sender domain. The following automatically moves them to the Junk folder and marks them as read.

[[!img /media/mail/webmail_sieve_junk.png size="450x"]]


Raw editing for power users
---------------------------

Power users may want to edit the sieve rules directly as plain text.

* Select the filter set you want to edit
* Click on the cogwheel on the bottom and select `Edit filter set`
