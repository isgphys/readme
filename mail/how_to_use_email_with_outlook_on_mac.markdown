How to use email with Outlook on Mac
====================================


Do not use the "New Outlook" interface
--------------------------------------

Microsoft Outlook has started rolling out the so-called "New Outlook" interface and is prompting users with a button to switch to it. Even though it looks like only a change of interface, it also modifies the inner workings and supported features of Outlook. In particular the "New Outlook" is not fully compatible with ETH on-prem Exchange.

We strongly recommend our users to stick to the "Old Outlook" for the time being.


Do not use "Sync with Microsoft Cloud"
--------------------------------------

Together with the "New Outlook" interface, Microsoft is prompting users to sync their messages and contacts to the Microsoft Cloud. When enabled, what happens under the hood is a privacy nightmare forbidden by ETH usage policy. In particular the emails (for instance of your D-PHYS account) will not longer be fetched with IMAP by your computer, but directly from hosts in the "Microsoft Cloud" - using your personal username and password. So Microsoft will have access to your clear-text password and all your emails.

When adding new accounts, always uncheck "Sync with Microsoft Cloud". And whenever prompted to enhance the experience with "Microsoft Cloud" click "Sync directly with IMAP" instead. (Beware that the cloud sync can not be disabled for some other email providers, like Gmail. So when you add your Google account to Outlook on Mac, all your Gmail data will be synced to the Microsoft cloud.)

[[!img /media/osx/outlook_sync_cloud_1.png size="450x"]]
[[!img /media/osx/outlook_sync_cloud_2.png size="450x"]]


Adding D-PHYS mail in Outlook
-----------------------------

* Open Outlook -> Preferences -> Accounts
* Click on the button to add another account
* Configure the settings as follows (replacing `johndoe` with your D-PHYS username)

[[!img /media/osx/outlook_1.png size="450x"]]
[[!img /media/osx/outlook_2.png size="450x"]]
[[!img /media/osx/outlook_3.png size="450x"]]
[[!img /media/osx/outlook_4.png size="450x"]]


As Outlook on macOS does currently support neither ActiveSync nor CalDav, we don't know of any way to integrate our D-PHYS [[Calendar|calendar/]]. If you wish to access email and calendar services from within Outlook, feel free to use the [ETH Exchange](https://unlimited.ethz.ch/pages/viewpage.action?pageId=16451437) server instead.
