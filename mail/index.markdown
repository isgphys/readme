E-Mail
======

Webmail
-------

Most owners of a D-PHYS [[account/]] have access to an E-Mail. You can for instance access it through a web interface.

https://webmail.phys.ethz.ch.


Configuration
-------------

Jump to our [[Readme|how_to_use_email]] to learn how to configure your mail client.

### Mail filtering (forwards, vacation/out-of-office auto-replies)

You can define custom mail filtering rules with [[Sieve]].

### Spam filtering

You can tweak the [[spam filtering|spamfilter]] rules for your account.


Mailing Lists
-------------

We host [[mailing lists|mailman]] to facilitate the email communication inside research groups and other communities.


Big Attachments
---------------

Use the [SWITCHfilesender](https://www.switch.ch/services/filesender/) or [Polybox](https://polybox.ethz.ch) services if you need to send or receive large attachments.


Lost Email
----------

The mailserver is backed up on a regular basis. Please contact us to restore lost email from the backups.
