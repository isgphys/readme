D-PHYS Spam Filter & Virus Scanner
==================================

Overview
--------

Every mail that goes through our mail server must pass a spam filter and virus checker.

Our spam filtering obtains daily updates from official and third-party rule sets. In addition we have over 1000 hand-written rules to catch even more unwanted messages. All messages identified as spam are moved to the `SpamBox` folder of your mailbox, where they are kept for 45 days before being deleted.


How to spot Spam and Phishing?
------------------------------

The typical Spam is easy to spot by a human (but often less so by the algorithms) and ranges from simple advertisements to fake lottery wins or inheritances.

Unfortunately ETH also receives a lot of well-crafted and highly targeted Spam:

* Phishing mails that lure you to some web page with a login to steal your username and password
* Dubious "scientific" journals and conferences that want your money
* CEO fraud messages from a fake supervisor's email that ask you to urgently shop gift cards

Please always remain wary when receiving attachments from unknown sources. Don't blindly trust emails asking for personal data, money or redirecting you to a web page asking your login information.


Why do I receive Spam?
----------------------

Even though a very high percentage is filtered out, some unwanted mails will still reach your inbox. Despite all the effort, our spam filtering can never compete with the detection of high-volume hosters like Gmail or Outlook. Moreover ETH receives a lot of targeted spam and phishing that is very hard to counter. Sadly it's a never-ending cat and mouse game and too many users keep falling for the fake messages so that Spam remains profitable.

In general we don't want to be overly aggressive with spam filtering. We believe that it's better to receive the occasional spam in the inbox, than to have false positives where non-spam messages get moved to the SpamBox.


How much Spam is "normal"?
--------------------------

An average D-PHYS mail user receives

* 10.6 emails every day:
    - 5.1 messages from inside ETH
    - 4.1 messages from outside ETH
    - 1.4 messages flagged as Spam

In addition, another 11.8 messages were rejected by our mail server and 3.5 messages greylisted.

Only a tiny fraction of the ~4 external emails you receive every day should be unwanted spam messages.


What to do with the Spam I receive?
-----------------------------------

In most cases, simply ignore and delete the message. If you want to inform us about a particularly nagging or dangerous spam, please forward it **as attachment** to `isg@phys.ethz.ch`. Note that a simple forward does not suffice, as all valuable data contained in the email header will be lost.

If you receive a lot of spam, please contact us. We will ask you to create a dedicated `Spam4ISG` mailbox folder to collect all unwanted messages. We can then cross-check the messages inside and use them to improve our filtering.


Configuration
-------------

The spam filtering rules can be customized in the [webmail](https://webmail.phys.ethz.ch/roundcube/) interface, by going to Settings -> Junk.

[[!img /media/mail/webmail_spam_rules.png size="450x"]]

The supported **Address Rules** are:

* accept mail from a given sender and never mark it as spam (whitelist)
* reject mail from a given sender and always mark it as spam (blacklist)

Under **General Settings** you can define the _Score threshold_ (default: 5), which is the required number of spam points for a message to be moved to the `SpamBox`. Try to decrease the value if you get a lot of spam in your inbox, but beware that this also increases the risk of false positives, meaning that non-spam messages may also end up in your `SpamBox`.

Any changes must be confirmed with the **Save** button.
