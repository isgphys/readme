How to use email with Apple Mail on iOS/iPadOS
==============================================

Add a new D-PHYS email account
------------------------------

* Open the `Settings` app, scroll down to `Apps` and navigate to the `Mail` section
* Click on `Accounts`, then `Add Account`
* Select `Other`, then `Add Mail Account`
* Enter your full name, email and D-PHYS password

    [[!img /media/mail/apple_mail_ios_new_account_1.png size="450x"]]

* Keep the default `IMAP` selected and configure the incoming and outgoing mail servers, as shown below. Note that you must provide your D-PHYS username and password for both to be able to receive and send emails.

    [[!img /media/mail/apple_mail_ios_new_account_2.png size="450x"]]


Check settings for sending mail
-------------------------------

In case you have problems sending emails you should check the settings of the outgoing mail server. In particular your D-PHYS username and password must be entered correctly, as you will not be able to send emails without proper authentication.

* Open the `Settings` app, scroll down to `Apps` and navigate to the `Mail` section
* Click on `Accounts`, then select your D-PHYS mail account
* Click on your D-PHYS mail address
* Click on `SMTP` just below `outgoing mail server`, and once more on the `primary server`
* Check that your settings are similar to the screenshot below. You may want to re-enter your password to make sure it's correct.

    [[!img /media/mail/apple_mail_ios_outgoing_server.png size="450x"]]
