Create account on lists.phys.ethz.ch
====================================

Given that mailing lists may be public and used by external users, the service is not linked to the D-PHYS or ETH accounts and requires an independent password. Even though we have already created user accounts for the existing mailing list owners and moderators, all other users must follow the steps below to create an account.

* Open [lists.phys.ethz.ch](https://lists.phys.ethz.ch) and click on **Sign Up** in the top right corner.

* Enter your account info. If you have a D-PHYS account, we recommend to use `username@phys.ethz.ch` as e-mail address, and your D-PHYS login as username. The password should ideally be separate, and can be reset in case you forgot it.

[[!img /media/mail/mailman/signup_1.png size="550x"]]

* The service will send you a message to the entered email address, in order to make sure you are the owner of that email, as it will grant you ownership to the linked mailing list subscriptions.

[[!img /media/mail/mailman/signup_2.png size="550x"]]

* Click on the verification link you received by mail and press the button to confirm the email.

[[!img /media/mail/mailman/signup_3.png size="550x"]]

* Next you can sign in with your mailing list account and username.

[[!img /media/mail/mailman/signup_4.png size="550x"]]

* Open the account preferences.

[[!img /media/mail/mailman/signup_5.png size="550x"]]

* Enter your name and time zone.

[[!img /media/mail/mailman/signup_6.png size="550x"]]

### Manage additional email addresses

* You may optionally link additional emails to your mailman account. This is useful if you also want to send messages using any of your email aliases.

[[!img /media/mail/mailman/signup_7.png size="550x"]]

* Only one of your email addresses can be marked as primary, but the choice does not really matter.

[[!img /media/mail/mailman/signup_8.png size="550x"]]
