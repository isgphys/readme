Mailing lists
=============

We provide mailing lists on [lists.phys.ethz.ch](https://lists.phys.ethz.ch).


Typical Tasks
-------------

### Create/Delete mailing list

If you would like to have a mailing list for your group, please [[contact us|services/contact]]. Please also let us know if you no longer need an existing mailing list and want us to delete it.

### Password management

Do not share your mailing list password with anyone! The mailing lists no longer have a single admin password that needs to be shared among owners. Instead, each person has its own and private password that grants access to all the mailing lists owned by that person. Pick a strong password only for you and keep it to yourself. If you want to grant admin access as to another person, please only configure the email address and let him/her create a personal account with that email address.

### Subscribe to a mailing list

Most of our mailing lists are not meant to be subscribed by individuals. Instead their members are managed by the mailing list owners. If you nevertheless want to send a subscription request, feel free to create an account and subscribe to a given mailing list. In most cases you will have to wait for the owner to accept your request of membership.

Similarly, if you want to unsubscribe from a mailing list, you need to first log in with your account, or directly contact the mailing list owner.

### Other Tasks

* [[Create account]]
* [[Moderation]]


Terminology
-----------

Some terms used on the [lists.phys.ethz.ch](https://lists.phys.ethz.ch) web page may require an explanation.

### Mailman, Postorius, Hyperkitty

[Mailman](https://docs.mailman3.org) is the name of the open-source software we use to host mailing lists. It's divided into several components with individual names and icons.

* [Postorius](https://lists.phys.ethz.ch/postorius/lists/) is the web interface to the core mailing list functionalities, like configuration, membership management and moderation of incoming messages.
* [Hyperkitty](https://lists.phys.ethz.ch/hyperkitty/) is the archive of all previous messages sent to the mailing list.

### Owners, moderators, administrators, members, non-members

* Owners have full admin rights on a mailing list. They can change the settings, manage members and moderate new messages.
* Moderators have restricted rights on a mailing list and can only moderate new messages.
* Administrators are the people that are either owner or moderator of a mailing list.
* Members are the people that have been subscribed to a mailing list and receive its messages.
* Non-members are the people that require some special settings, without necessarily being a member of the mailing list. The typical example is to allow someone to send messages, without being moderated.

### Accept, Discard, Ban, Reject

A mailing list administrator may receive notifications about pending subscription requests or held messages waiting for approval. Such requests can be responded with different actions.

* Accept: allow the request
* Discard: refuse the request, but without notifying the sender. Spam messages should always be discarded, to prevent sending out even more unwanted messages to often faked senders.
* Ban: refuse the request and dis-allow the sender from future requests. This should be avoided, as the spamming address is usually used only once and better rejected centrally on our mail server.
* Reject: refuse the request and inform the sender with an optional rejection reason. Only useful if the sender is an actual person.
