Moderation on lists.phys.ethz.ch
================================

Typically new messages to a mailing list are not automatically accepted and sent to all members. Instead they are placed in a hold queue while waiting for a list owner or moderator to decide on their fate.


Moderation of held messages
---------------------------

* Open `Held messages` in the mailing list settings.
* Select the check box of individual messages or all
* Press the appropriate button for your action
    - `Accept`: allow the message to be posted to the mailing list
    - `Reject`: refuse the message and inform the sender with a rejection reason. This is only useful when the sender is an actual person (and not a spammer).
    - `Discard`: refuse the message without notifying the sender. Spam messages should always be discarded, to prevent sending out even mote unwanted messages to often faked senders.

[[!img /media/mail/mailman/moderation_1.png size="550x"]]


Automatic moderation of messages from specific senders
------------------------------------------------------

### Default action for all members and non-members

The `Message acceptance` settings of a mailing list allow to control how to handle new messages. It allows to differentiate messages from members (that are subscribed to the mailing list) and non-members (all other senders).

Consider the example of a mailing list for a research group. You typically want only members to be able to post to the mailing list without moderation, while simply discarding all other messages. For other mailing lists you may consider enabling the `Emergency Moderation` flag thereby forcing to place all new messages on hold.

[[!img /media/mail/mailman/moderation_2.png size="550x"]]

### Default action for specific non-members

Another common scenario is to have a mailing list, where someone who is not a member of that list, should still be able to post without being moderated. You then need to add a new `non-member` with `default processing` as moderation action, to automatically accept postings from this sender address.

[[!img /media/mail/mailman/moderation_3.png size="550x"]]

You may also click on the non-member's address to pick a different moderation action, like putting on hold or discarding directly.

[[!img /media/mail/mailman/moderation_4.png size="550x"]]
