How to automatically forward Email
==================================

Forwarding Email works differently depending on the mail server that is used.  For people at the ETH Zurich D-PHYS there are two important mail servers that you are likely to use: for our department the D-PHYS mail server `mail.phys.ethz.ch` and for services managed centrally by the Informatikdienste (ID) the student/staff mail server that is run by the Basisdienste of ID for all of ETH.  If you are an employee of the department, your mail should automatically be delivered to the D-PHYS account.  If you are a student (diploma or PhD) or an apprentice (Lernende), you probably have two distinct e-mail addresses that you may want to combine.

Therefore, here we describe the two most common uses for students (and _Lernende_): from ETH to D-PHYS and from D-PHYS to ETH.

**NB: do not forward in both directions at the same time!**

### From ETH to D-PHYS (or somewhere else)

* Log in to https://password.ethz.ch/ using your ETH Account
* Select **Self Service** > **Manage Forward** from the menu
* Fill your D-PHYS (or other) address into the field **Forward** > **Recipient**

### Change ETH address book entry to show the D-PHYS address

* Set a forward to your D-PHYS address (see above)
* Select **Self Service** > **Modify User Data**
* Choose your D-PHYS address in the **email** drop down menu
* Wait a couple of days until the address book is updated

### From D-PHYS to ETH (or somewhere else)

See [[sieve]].
