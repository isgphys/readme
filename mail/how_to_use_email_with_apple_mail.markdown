How to use email with Apple Mail on macOS
=========================================

Disable Junk Filter
-------------------

We suggest to disable Apple Mail's junk filter (`Preferences -> Junk Mail ->` Uncheck `Enable junk mail filtering`), as it typically yields many false positives and we have our own spam filter on the D-PHYS mailserver.

Add a new D-PHYS email account
------------------------------

When you first start Apple Mail, you will automatically be presented with a dialog to add an account. If you are already using Apple Mail, go to `Preferences -> Accounts` and click the `+` in the lower left corner to add a new account. Select `Add Other Mail Account`.

[[!img /media/osx/configure_apple_mail_1.png]]

Next you will be prompted for your full name, email address and D-PHYS password.

[[!img /media/osx/configure_apple_mail_2.png]]

In the next window, fill in your D-PHYS username and password, select IMAP as account type and enter `imap.phys.ethz.ch` as incoming, and `mail.phys.ethz.ch` as outgoing mail server.

[[!img /media/osx/configure_apple_mail_3.png]]

You should now be able to read and send emails from Apple Mail.

Check Settings for Receiving Mail
---------------------------------

This section helps you to check your settings. This can be useful if you have already set up your D-PHYS mail account but experience problems. Open the Apple Mail Preferences and select the `Accounts` Tab. Your settings should more or less correspond to what is shown below.

[[!img /media/osx/apple_mail_settings_1.png size="450x"]]

[[!img /media/osx/apple_mail_settings_2.png size="450x"]]

[[!img /media/osx/apple_mail_settings_3.png size="450x"]]

Check Settings for Sending Mail
-------------------------------

If you have trouble sending emails please compare your settings to the screenshots below. First you need to click `Edit SMTP Server List` from the `Outgoing Mail Server Account` drop-down menu.

[[!img /media/osx/apple_mail_settings_4.png size="450x"]]

The description is arbitrary, but the outgoing server name is `mail.phys.ethz.ch`. Also make sure that your D-PHYS username and password are correct.

[[!img /media/osx/apple_mail_settings_5.png size="450x"]]

Further Reading
---------------

* [[How to Use Email]]
* [[osx/How to Prevent Apple Mail from Downloading Remote Images]]
