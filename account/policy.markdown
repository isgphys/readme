D-PHYS Account Policy
=====================

There are multiple ways to obtain a D-PHYS Account, and the account comes in several flavors. Most accounts [[expire|account expiration]].


Account types
-------------


### Employee

This is the standard account type for any D-PHYS staff member. It grants the holder the whole range of D-PHYS IT services, including (but not limited to) email, storage, group membership. When entering D-PHYS, this account will be automatically created for you and you will get your account form on your first day at the very latest. It is valid as long as you have an employment contract with ETH.

* Get account: when joining D-PHYS, you should already have your account. If that didn't work, please get in contact.
* What to do when account expires: if you still need D-PHYS services, [switch to collaborator](https://account.phys.ethz.ch/migrate/2collab) or contact us.


### ETH Account holder

Even if you're not a staff member of D-PHYS but have a valid ETH Account, you are eligible for a D-PHYS Account with a limited set of services, like storage and group membership. The account is valid for 6 months and can be renewed as long as the ETH Account is valid.

* Get account: fill out our [account form](https://account.phys.ethz.ch/new/from_nethz)
* What to do when account expires: as long as your ETH Account is valid, [renew](https://account.phys.ethz.ch/extend) it. Afterwards, if you still need D-PHYS services, [switch to collaborator](https://account.phys.ethz.ch/migrate/2collab) or contact us.


### Collaborator

This account type is meant for external collaborators and long-term guests with no formal ETH affiliation. They will have to name a D-PHYS professor as their host and can use most of our services. The account is valid for 12 months and can be renewed as long as the host agrees.

* Get account: contact us by email
* What to do when account expires: [extend](https://account.phys.ethz.ch/extend) online


### Light

A light account offers a very basic set of services (mainly access to different web sites) and requires a D-PHYS contact person. It is valid for 12 months and can be renewed online.

* Get account: contact us by email
* What to do when account expires: extend [online](https://account.phys.ethz.ch/extend)
