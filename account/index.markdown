# Account

At the [Department of Physics (D-PHYS)](https://www.phys.ethz.ch/) you usually have two accounts:

- **ETH Account**: Provided to all ETHZ members
- **D-PHYS Account**: Provided to users with D-PHYS affiliation

Please read our [[account policy|policy]] page on how to obtain and extend D-PHYS accounts. All [[services|services/]] inside the Department of Physics use the same login and password, called "D-PHYS account" or "physics account".

## ETH Account

The **ETH Account** is given by the [ETH Rektorat](http://www.rektorat.ethz.ch/) to every student and by ETH human resources to every employee at ETHZ. You should get this login and password by regular mail to your home address. Please ask your secretary in case of problems.

This account is required to access your @ethz.ch (or @student.ethz.ch) [email](https://mail.ethz.ch), the [wireless network](/network/wireless), [VPN](http://www.vpn.ethz.ch), licensed software via [IT Shop](http://itshop.ethz.ch) and several other ETH-wide services. Just to make things even more interesting, there are two passwords associated with each ETH Account: the LDAP/AD password and the Radius password. The following list gives you an idea of which one to use:

| Service         | Link / Docs                                                                                                                                                                                                             | Password | Description                 |
|-----------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|-----------------------------|
| Network         | [[network/]]                                                                                                                                                                                                            | Radius   | Wired network               |
| Wireless        | [[network/wireless]]                                                                                                                                                                                                    | Radius   | Wireless (Wi-Fi) network    |
| VPN             | [vpn](https://unlimited.ethz.ch/display/itkb/VPN), [webvpn](https://sslvpn.ethz.ch/)                                                                                                                                    | Radius   | Virtual Private Network     |
| Printing        | [[printing/]]                                                                                                                                                                                                           | LDAP     | Web-/network printing       |
| Main Website    | [ethz.ch](https://www.ethz.ch/)                                                                                                                                                                                         | LDAP     | ETH Websites (\*.ethz.ch)   |
| ETHIS (SAP)     | [ethis.ethz.ch](https://ethis.ethz.ch/)                                                                                                                                                                                 | LDAP     | ETH Info and Support System |
| ETH E-Mail      | [mail](https://unlimited.ethz.ch/pages/viewpage.action?pageId=16451437), [webmail](http://outlook.office.com)                                                                                                           | LDAP     | @ethz.ch, @student.ethz.ch  |
| IT Shop         | [itshop](https://itshop.ethz.ch/)                                                                                                                                                                                       | LDAP     | Licensed Software Catalog   |
| HPC Clusters    | [id sis hpc](https://scicomp.ethz.ch/)                                                                                                                                                                                  | LDAP     | EULER, Leonhard             |
| Password        | [password](https://password.ethz.ch)                                                                                                                                                                                    | LDAP     | Set password, account info  |
| ID/ITS Helpdesk | [FAQ](https://ethz.ch/staffnet/en/it-services/service-desk/support.html), [services](https://www.ethz.ch/services/en/it-services/catalogue.html), [help](https://www.ethz.ch/services/en/it-services/service-desk.html) | LDAP     | ETH Account specific help   |

The passwords can be changed at [password.ethz.ch](https://password.ethz.ch).

Note: Some ETH services need [[MFA|/account/mfa]] ([[Multi-Factor Authentication|/account/mfa]]).

## D-PHYS Account

The **D-PHYS Account** (or **Physics Account**) is given by [[ISG|services/contact]] to students and members of the Department of Physics, either working for the administration or a particular institute (including guests) and to other departments receiving [[services/]] provided by [ISG D-PHYS](https://www.phys.ethz.ch/services/physics-isg.html).

This account is required to access your @phys.ethz.ch [[mail/]], network file [[storage/]], provides the login for our [[services/workstations]], [[servers|/services/hostnames_of_our_services/]], [[websites|web/]] and several other services:

| Service       | Link / Docs                                                                                 | Description                 |
|---------------|---------------------------------------------------------------------------------------------|-----------------------------|
| Host Login    | [[services/workstations]]                                                                   | Managed workstations        |
| SSH Login     | [[/documentation/ssh]], [[computing/]]                                                      | Managed servers             |
| Windows Login | [[services/windows terminal server]]                                                        | Windows Terminal Server     |
| D-PHYS E-Mail | [[mail/]], [webmail](https://webmail.phys.ethz.ch/)                                         | @phys.ethz.ch               |
| File Storage  | [[storage/]]                                                                                | Personal home, Group shares |
| Groups        | [[/account/groups]]                                                                         | Group/Access management     |
| Backups       | [[backups/]]                                                                                | Daily backups               |
| Web services  | [[web/]]                                                                                    | Websites (\*.phys.ethz.ch)  |
| Public HTML   | [homepage how-to](/web/how_to_get_a_personal_homepage/)                                     | Personal/Group Homepages    |
| Chat          | [[chat]], [#helpdesk chat room](https://m.ethz.ch/#/#helpdesk:phys.ethz.ch)                 | Matrix/Element chat server  |
| Calendar      | [[calendar/]]                                                                               | Calendar, address book      |
| Password      | [password](https://account.phys.ethz.ch)                                                    | Set password, account info  |
| ISG Helpdesk  | [FAQ](/), [[services/]], [help](/services/contact/)                                         | IT Support for the D-PHYS   |

The password can be changed at [account.phys.ethz.ch](https://account.phys.ethz.ch/passwd).

Account settings
----------------

* [Change your password](https://account.phys.ethz.ch/passwd) (see also [[documentation/How to handle passwords]])
* [[Install a mail forward or vacation (out of office) auto-reply|mail/sieve]]
* [See your account information](https://account.phys.ethz.ch/info)

Is the D-PHYS account the same as the ETH account?
----------------------------------------------------

No. There is a physics account and an [ETH](https://unlimited.ethz.ch/display/itkb/ETH+user+account) account. Not everyone has both of them and different things need different logins.

What about email?
-----------------

As a D-PHYS employee, you will also get access to a D-PHYS email address. It's often easiest to create a [[forward|mail/sieve]] from, or to, the ETH address.

How to recover a lost password?
-------------------------------

In case you forgot your password, we can reset it and give you a new one. For obvious security reasons this cannot be done over the phone or by email. You need to come to our [[offices|services/contact]] and bring an ID card. To help you create secure passwords you can easily remember and how to handle them securely, please see [[documentation/How to handle passwords]].

Can I keep the account after I leave the D-PHYS or ETH?
------------------------------------------------------

See [[account expiration]].
