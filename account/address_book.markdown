Address Book
============

ETH Directory binding
---------------------

ETH Zurich offers an AD/LDAP directory with all employees and students. Please refer to the [documentation of ID](https://unlimited.ethz.ch/display/itkb/Mailbox) and search `Address query` on that page, if you want to include it as address book in your mail client. In particular there's a detailed [documentation for Thunderbird](https://unlimited.ethz.ch/spaces/itkb/pages/283251869/Thunderbird+address+book+ETH+Active+Directory).


Phonebook
---------

Looking for someone in the Physics Department? Search by Name, Phone Number, Group or Room in our phonebook.

* [D-PHYS Phonebook](http://phonebook.phys.ethz.ch/)
* [ETH People Search](http://www.ethz.ch/people/index_EN)
