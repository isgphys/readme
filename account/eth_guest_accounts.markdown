ETH Guest Accounts
==================

Please refer to the official documentation regarding the rules of ETH guest accounts.

* [ETH guests overview](https://ethz.ch/staffnet/en/employment-and-work/guests.html)
* [ETH guest categories](https://ethz.ch/staffnet/en/employment-and-work/guests/guest-categories.html)
* [ETH guest authorizations](https://ethz.ch/staffnet/en/employment-and-work/guests/general-overview-of-authorisations.html)
    - in particular the PDF linked on the bottom contains a matrix with the list of allowed services for each guest category
