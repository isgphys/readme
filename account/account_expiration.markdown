Expiration of D-PHYS accounts
=============================

* the ETH and D-PHYS account are [[separate|account]], but often linked to each other. The ETH account of scientific staff remains valid for roughly 6 months after the end of the contract. The D-PHYS account typically expires at the same date than the ETH account.
* every account holder will be informed in advance by email before any action is taken
* once you get notified about the account expiration, check our [D-PHYS account portal](https://account.phys.ethz.ch) for possible actions to take. Some [[account types|policy]] allow for an easy extension of the validity. Otherwise you may need to get in touch with us.
* on the date of expiration, accounts will be blocked, not deleted - no data will be purged
* a blocked account can no longer access data or emails on D-PHYS servers. Therefore the user should copy the relevant files before the account expires.
* the account holder can register a follow-up e-mail address. The menu link [Follow-up Email](https://account.phys.ethz.ch/email/followup) will appear on our [account portal](https://account.phys.ethz.ch) 3 months before the account expires. After account expiration, an automatic notification message will be shown to anybody sending an e-mail to the blocked account.
* the [personal homepage](https://people.phys.ethz.ch/) will be set offline automatically. However, if your page contains useful information which is related to your work/studies at ETH Zurich and you think it should remain online, please [contact ISG D-PHYS](mailto:isg@phys.ethz.ch). Please be aware that after the account has been blocked it will not be possible for you to update it anymore.

Depending on the type of your account, different rules may apply, which you can read about in our [[account policy|policy]]. Several types allow for an easy extension of the account validity. All available actions are listed on the D-PHYS account portal.

* https://account.phys.ethz.ch
