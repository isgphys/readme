D-PHYS user authentication
==========================

What is LDAP
------------

All [[D-PHYS accounts|account]] are centrally managed in ISG's [LDAP](https://de.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol) servers. These servers are used to authenticate all [[IT services|services]] that can be used with a D-PHYS account. User groups can be created to limit access on certain services to specific users.

LDAP at D-PHYS
--------------

We run a 3-way cluster of replicating LDAP servers:

`ldap{1,2,3}.phys.ethz.ch`

Since LDAP is a service of such central importance, a cluster of 3 redundant nodes lets us sleep better at night.

Technical details
-----------------

Connection URL (use all three to get maximum availability)

```
ldap(s)://ldap1.phys.ethz.ch, ldap(s)://ldap2.phys.ethz.ch, ldap(s)://ldap3.phys.ethz.ch
```

* only LDAP protocol v3 is supported
* baseDN: `ou=people,dc=phys,dc=ethz,dc=ch`
* DN is based on attribute `uid`, attribute `uniqKey` doesn't exist any more
* several other attributes are gone too, contact us for details
* ports: `389` -> `ldap://` + `StartTLS`, `636` -> `ldaps://` (TLS)
* encryption: A secured connection via [TLS](https://en.wikipedia.org/wiki/Transport_Layer_Security) or [Kerberos](https://en.wikipedia.org/wiki/Kerberos_(protocol)) is enforced. (Minimum [TLSv1.2](https://tools.ietf.org/html/rfc5246))
* preferred method of connection: Use `ldap://` (port `389`) using [`StartTLS`](https://www.ietf.org/rfc/rfc2830.txt) extended operation, which is LDAPv3's standard mechanism for enabling TLS (SSL) data confidentiality protection. Otherwise use `ldaps://` (port `636`, [deprecated in OpenLDAP](http://www.openldap.org/faq/data/cache/605.html), but still possible).

#### TLS Certificate

The TLS certificates of the LDAP servers are issued/signed by [DigiCert Inc](http://cacerts.digicert.com/DigiCertGlobalRootCA.crt)

#### Restrict Access in Apache web server

To configure the Apache web server to authenticate using a D-PHYS Account, you need to modify your `.htaccess` file, `<Directory>` block or `<Location>` block in the server configuration to look like the following:

```
AuthType Basic
AuthName "ETH D-PHYS Account"
AuthBasicProvider ldap
AuthLDAPURL "ldap://ldap1.phys.ethz.ch ldap2.phys.ethz.ch ldap3.phys.ethz.ch/ou=people,dc=phys,dc=ethz,dc=ch?uid?one?(&(objectClass=dphysUser)(!(blocked=yes)))" TLS
AuthLDAPGroupAttribute memberUid
AuthLDAPGroupAttributeIsDN off

# To restrict access to some user(s), add a line like this for each user
# (replace `<username>` with the D-PHYS user name):
Require ldap-user <username>

# To restrict access to some group(s), add a line like this for each group
# (replace `<groupname>` with the D-PHYS group name):
Require ldap-group cn=<groupname>,ou=groups,dc=phys,dc=ethz,dc=ch

# To grant access to all D-PHYS users, add the following line:
Require valid-user
```

#### Apache web server configuration

If you host your own Apache web server, additionally the following configuration in the server-context is recommended. This has to be configured server wide and will have an effect on all `<VirtualHost>`, `<Directory>` and `<Location>` blocks in the configuration.

```
LDAPTrustedGlobalCert CA_BASE64 /etc/ssl/certs/ca-certificates.crt
LDAPVerifyServerCert On
LDAPTrustedMode TLS
```

The configuration file under [[ldap#linux-configuration]] is required as well and will force it's CA settings (`TLS_CACERT`, `TLS_REQCERT`) over the settings configured in `apache2` (`LDAPTrustedGlobalCert`, `LDAPVerifyServerCert`). This is undocumented, may be a bug and could change in the future. So setting both configurations to secure values is recommended.

#### Linux configuration

To configure software on Linux, which is using the [OpenLDAP](http://www.openldap.org) libraries `libldap`, like `apache2` or `ldap-utils`, the following configuration is required/recommended:

Contents of `/etc/ldap/ldap.conf`:

```
URI ldap://ldap1.phys.ethz.ch ldap://ldap2.phys.ethz.ch ldap://ldap3.phys.ethz.ch
BASE dc=phys,dc=ethz,dc=ch
# TLS certificates (needed for GnuTLS)
TLS_CACERT /etc/ssl/certs/ca-certificates.crt
TLS_REQCERT demand
```
