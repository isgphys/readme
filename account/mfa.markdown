Multi-Factor Authentication (MFA)
=================================

As of the second half of 2022, various web services using the [[ETH Account|/account/#eth-account]]
will need multi-factor authentication (MFA).

Affected services and set-up instructions
-----------------------------------------

For a list of affected services and set-up instructions,
please refer to the official article
([de](https://unlimited.ethz.ch/x/TxmJBQ) or [en](https://unlimited.ethz.ch/x/WwZaBg))
in the [ETH IT Knowledge Base](https://unlimited.ethz.ch/display/itkb/).

Alternative software
--------------------

You may freely choose to use alternative software packages without the need for MFA,
which are either more secure or do not need (MFA) authentication:

- MS Teams: [[Matrix/Element Chat|/chat]], [[Jitsi|/chat/jitsi]]
- Zoom: [[Matrix/Element Chat|/chat]], [[Jitsi|/chat/jitsi]]
- Microsoft 365: [LibreOffice](https://www.libreoffice.org/)
- Adobe Creative Suite: [alternatives](https://www.softwareinfo.ethz.ch/alternatives-to-adobe-software/)
- Google Workspace: [ETH Polybox](https://polybox.ethz.ch)

Alternative authenticator apps
------------------------------

In addition to the authenticator apps recommended by ETH,
the following open-source alternatives can be used:

- Windows, Linux, macOS: [KeePassXC](https://keepassxc.org/)
- Android: [Aegis](https://getaegis.app/), [KeePassDX](https://www.keepassdx.com/)
- CLI: [totp-cli](https://yitsushi.github.io/totp-cli/)

TL'DR instructions
------------------

The [MFA registration](https://www.password.ethz.ch/qrcode/) page will show the secret as qr-code and plain-text.

Add the secret to your authenticator or password manager:

- Aegis: Add (+) > Scan QR code
- KeePassXC: Right-click entry > TOTP > Set up TOTP, paste. Copy: Ctrl-T
- KeePassDX: Open entry, Edit, Set up TOTP (clock+passwd icon) > paste ([wiki](https://github.com/Kunzisoft/KeePassDX/wiki/OTP))
