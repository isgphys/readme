# D-PHYS groups

D-PHYS groups control storage access permissions (see [[unix permissions|linux/personal groups]] and [[documentation/acl]]) and may serve as a [[groupshare|storage]] for large amounts of group related storage space.

Every D-PHYS [[user|account]] may be member of any number of such groups. The group membership is managed by ISG D-PHYS or the respective group owner directly.

## How to get group membership/access

Please get in touch with the owner of the group directly and ask to be given access. He may be able to give you access immediately or via our [[helpdesk|services/contact]]. You may also [[contact us via email|services/contact]] including the respective **group owner in CC**.

Please note: ISG can't decide who gets access to what, the OK must always come from the group owner.

## How to view group membership

You can view group membership on our web interface. To do so, please log in on [account.phys.ethz.ch](https://account.phys.ethz.ch/) with your D-PHYS user account and navigate to **Account Info** or **Show Members** or use the links below:

- [Show your group membership](https://account.phys.ethz.ch/info)
- [Show group members](https://account.phys.ethz.ch/group/info)

## Information for group owners

We have a [new service for group owners to edit group membership](https://nic.phys.ethz.ch/news/2018/04/18/edit-group-share-memberships-yourself/). We are currently in the process of assigning the group owners to groups.

**If you’re the owner of a group (or group share) and would like to be able to perform user management yourself, please get in touch with [[us|services/contact]].**

### How to manage group membership yourself

This service is available only for group owners.

Please log in on [account.phys.ethz.ch](https://account.phys.ethz.ch/) with your D-PHYS user account and navigate to **Edit Group Settings** or follow [this link](https://account.phys.ethz.ch/group/edit). On this page You can:

- add/remove [[users|account]]
- configure [[group reports|services/group report]] and report receivers

Owners of our group shares so far always had to contact us in order to have members added or removed to/from the underlying LDAP group. One of the benefits of the recent [LDAP migration](https://nic.phys.ethz.ch/news/2017/11/13/new-d-phys-ldap-servers/) is that we can now offer a web interface for LDAP group member management.
