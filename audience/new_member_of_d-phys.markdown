I'm a New Member of D-PHYS
==========================

* [[1 h introduction "IT at D-PHYS"|services/Introductory workshop IT at D-PHYS]]
* [[How to get a physics account|account/]]
* [[network/How to get network access]]
* [[mail/How to Use Email]]
* [[How to create an OpenPGP/GnuPG key|documentation/How to create an OpenPGP GnuPG key]] and [[How to sign an OpenPGP/GnuPG key|documentation/How to sign an OpenPGP GnuPG key]]
* [[Storage]]
* [[Printing]]
* [[web/How to get a personal homepage]]
* [[Calendar/]]
* [[calendar/How to use calendar feeds]]
* [[services/Usage Policies]]
