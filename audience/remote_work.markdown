Work from home or abroad
========================

The following guides should help you to do home office or work remotely, while traveling abroad.

### VPN

Many resources (eg. Ethis, scientific journals) are restricted to the ETH network. With [[VPN|services/how to use vpn]] your computer behaves as if its was directly connected to the ETH network.

### Email

Our [webmail](https://webmail.phys.ethz.ch) provides quick access to your D-PHYS mails from anywhere. Consider using a normal [[email client|mail/how to use email]] for longer and more comfortable email access.

### Storage

Your home and group shares can be [[accessed|storage/access]] remotely with VPN.

### Video Conference

We host a [[Jitsi|chat/jitsi]] server for audio and video calls.
ETH provides access to [[Zoom|documentation/zoom]] for virtual meetings and video conferences.

### Chat / collaboration

ISG offers a powerful [[chat solution|chat/]] (Slack replacement).

### Phone

You can [forward your ETH phone](https://unlimited.ethz.ch/pages/viewpage.action?pageId=29236528) to another number in Switzerland or abroad (depending on your call restrictions). Various settings can be viewed and changed over the [go2phone.ethz.ch](https://go2phone.ethz.ch) web interface.

### Remote login

We provide a [[services/windows terminal server]] `winlogin.phys.ethz.ch` and a Linux server `login.phys.ethz.ch` for [[SSH|documentation/ssh]].

You may also request remote RDP access to your managed Windows workstation. If you enable RDP on unmanaged machines, please set strong passwords and keep in mind that you need VPN for access from outside ETH.

In case you don't know the name of your computer, have a look at [registration.phys.ethz.ch](https://registration.phys.ethz.ch), which lists all hosts registered on your name. The `Sent Name` is used for the `sentname.dhcp[-int].phys.ethz.ch` DynDNS, and the `DNS Name` corresponds to static `dnsname.ethz.ch` hostnames.

### Filemaker Databases

After installing of the Filemaker application, you need to configure our server which is described [[here|services/filemaker]]. For access from outside ETH you need [[VPN|services/how to use vpn]].

### Remote support

This is similar to TeamViewer, please visit https://ethz.ch/services/de/it-services/service-desk.html
and download Remote Support Client, start ISLLightClient,
enter code given by the admin.

### Other things to keep in mind

Beware that all files and settings that are stored locally on your ETH computer can not easily be accessed from outside. We recommend to keep all files in your home or group shares. Also consider [[exporting your browser bookmarks|documentation/transfer browser bookmarks]], so that you can transfer them to your computer at home.
