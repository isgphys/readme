Documentation for Administrators at ISG
================================

... and possibly helpful for others, too. :-)

Administrative Tasks
--------------------

* [[NTP|documentation/Troubleshooting NTP]]
* [[documentation/Testing Hardware]]
* [[documentation/Packaging software for Debian]]
* [[How to add tests to our Xymon Monitoring Server|documentation/How to add tests to our Big Brother]]

### System documentation

* [[documentation/ISO to UTF-8 migration notes]]
* [[documentation/Locales]]
* [[services/LVMchart]] - generate LVM usage charts

### How-To

* [[documentation/Setting up a X server for diskless clients]] (X -query)
* [[linux/How to create partitions over 2TB on Linux]]
* [[documentation/How to loopthrough a PCI network card into a Xen DomU]]
* [[documentation/How to fix a broken Xen DomU]] (Tips & Tricks)
* [[documentation/How to setup the serial console]]

Outdated Documentation
----------------------

Outdated notes which may be still useful to others.

* [[attic]]
