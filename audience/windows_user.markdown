I'm a Windows User
==================

* [[How to use the printers|printing]]
* [[Update ETH pia entries with wrong or outdated passwords|printing/change_local_printing_credentials]]
* [How to finish Windows 11 setup without network connection](/windows/windows11/setup_win11)
* [[How to configure your self-manged Windows Installation to access all D-PHYS IT services|windows/self_managed_windows_machines]]
* [[Adding calendar items from ETHIS into MS Outlook|windows/adding_calendar_items_from_ETHIS_into_MS_Outlook]]
* [How you can suppress auto reboot after Windows Update](/windows/windows10/suppress_auto_reboot)
* [Disable the upload from your telemetry data to Microsoft](/windows/windows10/disable_telemetry)
* [[How to configure PyCharm for using Anaconda|windows/configure_pycharm_for_using_anaconda]]
* [Windows 7 end of life FAQ](/windows/Windows_7_EOL)
