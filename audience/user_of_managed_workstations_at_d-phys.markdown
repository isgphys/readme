I'm a User of Managed Workstations at D-PHYS
============================================

* [[Where Do I Find the Workstations|services/workstations]]
* [[printing/How to Use The Scanners]]
* [[web/How to get a personal homepage]]
* [[services/Usage Policies]]

How to with D-PHYS Linux
------------------------

* [[linux/Software on the D-PHYS Linux Computers]]
* [[linux/How to handle long running jobs]]
* [[linux/How to use USB Harddisk or USB Memorysticks]]

How to with D-PHYS Windows
--------------------------

* [[How to connect the printers|printing/how_to_add_an_id_printer_on_windows]]
* [[How to configure preinstalled Thunderbird email client|mail/how_to_use_email_with_thunderbird]]
* [[How to configure preinstalled MS Outlook email client|mail/how_to_use_email_with_outlook]]
* [[windows/Where should I save my files]]
* [[services/Windows Terminal Server]]
* [[windows/How to change the Dropbox location folder]]
* [[How to change the display language in Adobe Reader/Acrobat Professional|windows/How to change the display language in Adobe Reader Professional]]
* [[windows/How to add an MS Excel add-in]]
* [[How to change the display or keyboard language|windows/windows10/change display or keyboard language]]

How to with D-PHYS Mac
----------------------

* [[mail/How to use email with Apple Mail]]
* [[How to use Calendar in iCal|calendar/apple_ical]]
* [[Restore data from the backups|backups/restore]]
