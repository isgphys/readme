I'm a Linux User
================

* [[How to use the printers|printing]]
* [[services/How to Use VPN]] and [[linux/VPN with Linux]]
* [How to report bugs for software in Debian](http://www.debian.org/Bugs/) [or Ubuntu](https://help.ubuntu.com/community/ReportingBugs)
* [[How to propagate your Editor, Language and other Choices to Remote Machines via SSH|documentation/How to propagate your Editor Language and other Choices to Remote Machines via SSH]]
* [[Software on Linux|https://readme.phys.ethz.ch/linux/software_on_the_d-phys_linux_computers/]]

See also
--------

* [[Administrator at ISG]]
