Computing Resources in and for D-PHYS
=====================================

Easy access to powerful computing resources becomes increasingly important in a competitive research field like physics. Since the specific needs between groups differ widely, there's no one-size-fits-all solution but a great range of tailored products ranging from local workstations to [top 10 HPC](https://www.top500.org/lists/top500/) super computers. Here we try to give a general overview, please come talk to us in order to find out what's best for you.

Laptop / Workstation
--------------------

Today's personal computers are amazingly powerful. For rather moderate amounts of money you can get many cores, dozens of gigs of RAM and terabytes of storage. For many people the machine on/under their desk is therefore sufficient to perform all or most of their computing needs.

D-PHYS nodes
------------

A GPU-enabled compute node is available to anyone from D-PHYS: `spaceml4`. Just [SSH](https://readme.phys.ethz.ch/documentation/ssh/) in, but [[respect others|linux/how_to_handle_long_running_jobs/]].

Group / institute nodes
-----------------------

Many research groups and institutes (for example IPA, ITP and IQE) own computation nodes for their members to use. As this is a rather long and fast-changing list, please ask your colleagues or us for the current situation in your group. Most of these machines are operated by ISG.

Please read the MOTD (message of the day) carefully after login.

* IGP users can use:
    * `darkside`

* ITP users can use:
    * `morty` (reserved)
    * `rick` (reserved)
    * `princess` (restricted)
    * `toad`
    * `yoshi`
    * `geno` (restricted)

* IPA users can use:
    * `bluesky`
    * `sunray`
    * `rainbow`
    * `dlnu`

* IQE users can use:
    * `qo-sim`
    * `thebigone`
    * `thebiggerone`
    * `tiqithunder`

* SOLID users can use:
    * `nux-noether`

Clusters
--------

Many members of D-PHYS have access to a range of different clusters, either as shareholders or on a project level. Again: ask your colleagues or us for details. The most important ones are:

* [EULER](https://scicomp.ethz.ch/wiki/Euler) - operated by [Informatikdienste's Scientific IT Services](https://sis.id.ethz.ch), shareholder model, [free guest login](https://scicomp.ethz.ch/wiki/Getting_started_with_clusters) for all ETH users, [FAQ](https://scicomp.ethz.ch/wiki/FAQ)
* [Alps](https://www.cscs.ch/computers/overview/) - operated by [CSCS](http://www.cscs.ch/), for federal projects of highest computing needs


Software support
----------------

It's not always easy to get the most performance out of those machines - the programming challenges can be quite demanding. A lot of material is available online, and also local help is available.

* D-PHYS organized the [Advanced Scientific Computing Workshop](https://indico.phys.ethz.ch/event/5), which given sufficient demand will probably be repeated. The slides are available at the URL above.
* [Informatikdienste's Scientific IT Services](https://sis.id.ethz.ch) offer [training](https://sis.id.ethz.ch/services/index.html#consulting-training) too

General remarks
---------------

In all cases except your personal machine, please be considerate of other users and pay attention to our [[advice on long running jobs|linux/how_to_handle_long_running_jobs/]] and how to [[transfer data|storage/data_transfer]].
