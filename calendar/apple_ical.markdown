Sync Sogo Calendar with Apple Calendar
===================================

Go to `Calendar` -> `Preferences` -> `Accounts`, then click on the plus sign to add a new account and choose `Other CalDAV Account`.

[[!img /media/osx/sogo.png]]

Select `Account Type` -> `Manual` and enter your D-PHYS username and password. For the `Server address` use https://sogo.phys.ethz.ch.

Under `Calendar` -> `Preferences` -> `Accounts` -> `Delegation` you can toggle the view of other D-Phys calendars for which you have access rights. Alternatively you may add them as separate accounts by providing the full path as `Server address` https://sogo.phys.ethz.ch/SOGo/dav/D-PHYS-USERNAME.
