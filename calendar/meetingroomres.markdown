D-PHYS meeting room reservation
===============================


D-PHYS manages the reservations of four meeting rooms listed below.

|  Room      | Web calendar view (public)                                                                                                                                     | Email to invite   |
|------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------|
| HIT E 41.1 | [View](https://outlook.office365.com/owa/calendar/eabb3cac20134ede8ba580d670573cd8@ethz.ch/f7a0681ce2504095a9b1fabed301b35f16253867706019105296/calendar.html) | `hite411@ethz.ch` |
| HIT G 31.1 | [View](https://outlook.office365.com/owa/calendar/b6facb6be03547aa90364ecb4bc9c2a1@ethz.ch/36d80a0dedfd4bbd9f54bb86213fe5119700495526484548606/calendar.html)  | `hitg311@ethz.ch` |
| HPF G 6    | [View](https://outlook.office365.com/owa/calendar/eff5ef25f9d244598643c58d5b0ce5a2@ethz.ch/a07cfabe2b54496f96071ff74d10829515855769092302443142/calendar.html) | `hpfg6@ethz.ch`   |
| HPT H 4.2  | [View](https://outlook.office365.com/owa/calendar/f3af37d4603d4305a50997b47d7b6faf@ethz.ch/74fe7467129a4ceeb9130085913c726a928125480758052941/calendar.html)   | `hpth42@ethz.ch`  |

Additional rooms can be booked via the [ETH Room Request](https://www.lehrbetrieb.ethz.ch/raumanfrage/login.view?lang=en) site.

<!--
### historic, different admin group

| Room     | Email            |
|----------|------------------|
| HPK D 32 | hpk_d_32@ethz.ch |
| HPK G 32 | hpk_g_32@ethz.ch |
-->


Request a new reservation
-------------------------

To submit a room reservation request, an **Calendar invite** (not a normal email) has to be sent to the respective room. This is typically done by creating a new event and inviting the room's email address as attendee.

### Request from the browser

* Open the 'Web Calendar View (Public)' link for the room you want to book.
    - See if your preferred time slot is available.
* Open your ETH Webmail (https://outlook.office.com/mail/)
    - Enter your `username@ethz.ch` in the login form and press `Next`.
    - When you are redirected to the ETH login form, enter your ETH credentials.
    - Go to the calendar and create the desired event and add it with the `New event -> Event` button in the top left corner.
* In the 'New event' window
    - Enter the name of the event as the title, followed by the name of the organiser or research group in brackets.
    - In the 'Invite participants' box, add the email address for the room.
    - Click 'Save' to submit your booking request.
* Your room reservation will be marked 'tentative' and will appear shaded until it has been approved by one of the room planners.
    - You will receive an email notification confirming (or rejecting) your request, usually within one business day or less.

### Request from a desktop app

* Open the 'Web Calendar View (Public)' link for the room you want to book.
    - See if your preferred time slot is available.
* Add a new event in your desktop calendar application.
    - Enter the name of the event as the title, followed in parentheses by the name of the organiser or research group.
    - In the 'Invite attendees' field, add the email address for the room.
    - Save the event to send your request.
* Your room reservation will be marked as 'tentative' and will appear shaded until it has been approved by one of the room planners.
    - You will receive an email notification confirming (or rejecting) your request, usually within one business day or less.
