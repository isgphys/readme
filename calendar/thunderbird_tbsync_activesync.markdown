# Thunderbird TbSync with ActiveSync

Thunderbird also allows syncing calendars with the ActiveSync protocol, like for instance the ETH Exchange.

## Install required add-ons

Install [TbSync](https://addons.thunderbird.net/en-US/thunderbird/addon/tbsync/):

- Select **Tools** > **Add-ons**
- Find more add-ons: `tbsync` > **TbSync** > **Add to Thunderbird**
- Confirm any following popups with **Add** or **OK**

Install [Provider for Exchange ActiveSync](https://addons.thunderbird.net/en-US/thunderbird/addon/eas-4-tbsync/?src=ss)

- Select **Tools** > **Add-ons**
- Find more add-ons: `caldav` > **Provider for Exchange ActiveSync** > **Add to Thunderbird**
- Confirm any following popups with **Add** or **OK**


## Configure TbSync

- Select **Tools** > **Synchronization Settings (TbSync)**
- In the **Account settings** > **Account actions** drop-down menu, select **Add new account** > **Exchange ActiveSync**
- Select **Automatic Configuration** > **Next**
- **Account name**: Choose a name (example: `Exchange`)
- **User name**: Enter your username@ethz.ch
- **Password**: Enter your ETHZ password
- Click **Autodiscover settings and add account** > **Finish**
- Select **Enable and synchronize this account**

When the account sync is finished it will show all available calendars you have access to.

- In **Available resources** select all calendars you would like to show
- Click **Synchronize now**
