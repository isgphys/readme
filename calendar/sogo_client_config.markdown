D-PHYS Calendar (SOGo) Client Config
====================================

Synchronization with external clients
-------------------------------------

SOGo offers both CalDAV and ActiveSync. Use CalDAV for Apple devices and Thunderbird and ActiveSync for Android and Outlook.

### Sync URLs

| CalDav                              | ActiveSync                 |
| ----------------------------------- | -------------------------- |
| https://sogo.phys.ethz.ch/SOGo/dav/ | https://sogo.phys.ethz.ch/ |

Note that if additional calendars don't show up on your sync clients with ActiveSync, you might have to enable "Microsoft Enterprise ActiveSync" in the calendar context menu.

### Add Sogo in Outlook

go to `Control Panel` -> `Mail (Microsoft Outlook)` -> `E-Mail Accounts...` -> `New...` -> `Manual setup or additional server types` -> `Exchange ActiveSync`

| Key            | Value                        |
| -------------- | ---------------------------- |
| Your Name:     | `Your Fullname`              |
| Email Address: | `dphysUsername@phys.ethz.ch` |
| Mail server:   | `sogo.phys.ethz.ch`          |
| User Name:     | `dphysUsername`              |
| Password:      | `yourDphysPassword`          |

### Add Sogo in Apple Calendar

Add a new CalDAV account, depending on your operating system:

* On macOS: open `System Settings` -> `Internet Accounts`
* On iOS: open `Settings` -> `Calendar` -> `Accounts`

Click `Add Account` -> `Other` -> `CalDAV Account` and enter the following information:

| Key            | Value                       |
| -------------- | --------------------------- |
| Account Type   | `Manual`                    |
| User Name      | `dphysUsername`             |
| Password       | `yourDphysPassword`         |
| Server Address | `https://sogo.phys.ethz.ch` |

Once the account is added, you can use the `Delegation` tab to show additional calendars that you have access to.

### Add Sogo in Thunderbird

SOGo and Thunderbird can sync out of the box using the CalDAV protocol. In order to set it up, just right click in the calendar tab of Thunderbird, choose `New calendar`, `On the Network`, and then provide your D-PHYS login and `https://sogo.phys.ethz.ch/SOGo/dav` for `Location`. Thunderbird will then offer you all calendars you have access to.

### Add Sogo in Android

#### ActiveSync

In order to set up your Sogo calendar in Android, go to `Settings` -> `Accounts and Settings` -> `Manage accounts` -> `Add account` -> `Exchange`, then provide your D-PHYS email address and hit `Set up manually`. Provide your D-PHYS password and change the `Server settings` like so:

| Key            | Value                       |
| -------------- | --------------------------- |
| Domain/username| `dphysUsername`             |
| Server         | `sogo.phys.ethz.ch`         |

then hit `Next`. You can then do some fine tuning in the account settings.

**Note** that some people have experienced issues with Exchange sync simply stopping after some time - it seems the ActiveSync client in Android is not very reliable. In these cases you might want to switch to a CalDAV based sync (see below).

#### CalDAV sync

If you would like control over displaying calendars shared with you, you might want to switch to a CalDAV based sync using [DAVx5](https://www.davx5.com/) available via:

Install:

- [F-Droid](https://f-droid.org/en/packages/at.bitfire.davdroid/) (free)
- [Play Store](https://play.google.com/store/apps/details?id=at.bitfire.davdroid) (costs)

Setup:

- Grant calendar permissions
- Grant contacts permissions (optional)
- Add account
- Login with URL and user name
- Base URL: `https://sogo.phys.ethz.ch/SOGo/dav/`
- Enter D-PHYS credentials
- Add account
- CalDAV: Check calendars you would like to sync
- CalDAV: Synchronize now
- CardDAV: Check to sync Users/Groups (optional)

You may have to enable the calendar in your calendar app now.

In Google Calendar:

- ☰ > ⚙️  Settings > Manage Accounts > Enable account with your D-PHYS email
