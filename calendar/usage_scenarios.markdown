Using SOGo calendar in a group
===========================

Basically there's two possible ways to share calendar information in a group.


Shared LDAP user
----------------

Example: NCCR group calendar

This is the preferred choice for most situations, eg group meetings, holiday absences. An impersonal LDAP user is created to host a calendar. One person in the group, for instance the secretary, receives the credentials and manages the calendar. Using the _Access_ settings in the groupware web interface, read and/or write access can be granted to other group members. (Note that users must log in once in groupware to be visible in the list of users.) When granted access, a group member can either consult the shared group calendar from the web interface or sync it to his personal computer or phone. For the sync to work, the group calendar must be checked in the _CalDAV_ or _eSync_ preferences as _calendar to sync in addition to personal calendar_.


User group in SOGo calendar
---------------------------

Example: ISG

Use this if in general the members of the group use their individual calendars but once in a while would like to check availability of a colleague or invite her to a meeting. All group members can specify event visibility (private, public..) according to their needs (`Grant access` in the left pane) and choose which calendar to display (the pull-down with the two heads). Challenge: keeping the membership list up to date.
