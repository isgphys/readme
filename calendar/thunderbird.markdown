Calendar in Mozilla Thunderbird
================================

Mozilla's Thunderbird with the Lightning calendar plugin works very well with SOGo. Here's what you have to do if you want to use them:

* Install the [Lightning plugin](http://www.mozilla.org/projects/calendar/lightning/) for Thunderbird
* In the 'Calendars' tab, right click and choose 'New Calendar'. Next. Select 'On the Network'. Next. CalDAV format, Location: https://sogo.phys.ethz.ch/SOGo/dav - hit next. Choose a name and a color. Next. Finish.

    [[!img /media/sogo/egw_location_url.png]]

* When Lightning asks for username and password, use your D-PHYS login and password.

Support multiple users on the same server
-----------------------------------------

It may happen that you want to include multiple calendars from sogo. Thunderbird requires setting a special preference flag to improve support of several usernames for the same calendar server.

Open the Thunderbird Preferences, select "Advanced"-> "General" and click on the bottom on the "Config Editor" button. A new window should open with a list of available preferences. Enter "multirealm" in the search box, and  double click "calendar.network.multirealm" to set its boolean value to "True".

From now on, the name of the calendar will be appended to server addresses to distinguish them, for instance when saving passwords.

You can then add calendars of another user using

```
https://sogo.phys.ethz.ch/SOGo/dav/D-PHYS-USERNAME
```

as server address, where `D-PHYS-USERNAME` has to be replaced by their username.
