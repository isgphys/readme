Calendar Compatibility Matrix
=============================

### [SOGo](https://sogo.phys.ethz.ch)

|                                                     | Web      | Android  | iOS      | macOS    | Outlook  | Thunderbird |
| --------------------------------------------------- | -------- | -------- | -------- | -------- | -------- | ----------- |
| CalDav Sync                                         | -        | ✔ (1)   | ✔       | ✔       | ✔ (2)   | ✔          |
| Active Sync                                         | -        | ✔       | ✔       | ✔       | ✔       | ✔ (4)      |
| Offline use                                         | ❌       | ✔       | ✔       | ✔       | ✔       | ✔          |
| share calendar with others                          | ✔       | ❌       | ❌       | ❌       | ❌       | ❌          |
| show shared calendar from others                    | ✔       | ✔       | ✔       | ✔       | ✔       | ✔          |
| show free busy state during meeting creation        | ✔       | ❌       | ❌       | ❌       | ❌       | ❌          |
| invite a user for a meeting                         | ✔       | ✔       | ✔       | ✔       | ✔       | ✔          |
| invite a group for a meeting                        | ✔       | ❌       | ❌       | ❌       | ❌       | ❌          |
| add a meeting in a group calendar                   | ✔       | ✔       | ✔       | ✔       | ✔       | ✔          |
| create private meeting                              | ✔       | ✔       | ❌       | ❌       | ✔       | ✔          |
| add a [room](/calendar/meetingroomres) to a meeting | (✔) (5) | (✔) (5) | (✔) (5) | (✔) (5) | (✔) (5) | (✔) (5)    |

### [Exchange](https://mail.ethz.ch)

|                                                     | Web | Android | iOS | macOS | Outlook | Thunderbird |
| --------------------------------------------------- | --- | ------- | --- | ----- | ------- | ----------- |
| CalDav Sync                                         | -   | ❌      | ❌  | ❌    | ❌      | ❌          |
| Active Sync                                         | -   | ✔      | ✔  | ✔    | ✔      | ✔ (4)      |
| Offline use                                         | ✔  | ✔      | ✔  | ✔    | ✔      | ✔          |
| share calendar with others                          | ✔  | ❌      | ❌  | ❌    | ✔      | ❌          |
| show shared calendar from others                    | ✔  | ❌      | ❌  | ✔    | ✔      | ❌          |
| show free busy state during meeting creation        | ✔  | ❌      | ❌  | ❌    | ✔      | ❌          |
| invite a user for a meeting                         | ✔  | ✔      | ✔  | ✔    | ✔      | ✔          |
| invite a group for a meeting                        | ✔  | ✔      | ✔  | ✔    | ✔      | ✔          |
| add a meeting in a group calendar                   | ✔  | ❌      | ❌  | ✔    | ✔      | ❌          |
| create private meeting                              | ✔  | ✔      | ✔  | ✔    | ✔      | ✔          |
| add a [room](/calendar/meetingroomres) to a meeting | ✔  | ✔      | ✔  | ✔    | ✔      | ✔          |

* (1) Only with WebDav Sync app, for example [DAVx5](https://f-droid.org/en/packages/at.bitfire.davdroid/) (see [[instructions|/calendar/sogo_client_config/#caldav-sync]])
* (2) Only with external Plugin like [CalDavSynchronizer](/calendar/outlook_caldav_synchronizer)
* (4) Only with external Plugin like [TBsync (with activesync)](/calendar/thunderbird_tbsync_activesync)
* (5) Via email address of the room, not free/busy
