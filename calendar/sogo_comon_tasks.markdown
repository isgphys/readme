D-PHYS Calendar (SOGo) - Common Tasks
=====================================

## Creating a new event

You need to click-and-drag in the web interface, not just click.

## Inviting an LDAP group to an event

This is only possible in the web front-end (synced clients don't know about groups - but you can always invite an email address).

search for group:
[[!img /media/sogo/sogo-group-1.png size="450x"]]

expand group into users:
[[!img /media/sogo/sogo-group-2.png size="450x"]]

Note that once a group has been expanded to its members, this collection is static and will not be updated when group members change.

enjoy!
[[!img /media/sogo/sogo-group-3.png size="450x"]]

## Sharing a calendar with a user or group

You can easily share your calendars with other D-PHYS users or groups.

Click on the 3 dots of the calendar that you'd like to share. Click 'Sharing...'.
[[!img /media/sogo/sogo-share-group-1.png size="250x"]]

Search for the user or group.
[[!img /media/sogo/sogo-share-group-2.png size="450x"]]

You can identify users by their `@phys.ethz.ch` and groups by their `@localhost` suffixes, respectively.

Note that you cannot directly search for `<groupname>@localhost`, so please only search for the groupname and scroll down in the filtered results to pick the `<groupname>@localhost` item.

If you tick the 'Subscribe user' checkbox, the user(s) will be auto-magically subscribed, otherwise they'll have to do this manually. Be careful with this option when sharing to (large) groups...

Then set the visibility and edit options to your liking.
[[!img /media/sogo/sogo-share-group-3.png size="450x"]]

## Displaying external calendars

In the web interface you can display external calendars in iCal format (.ics). Click the + button next to 'Web Calendars' and fill in the external URL. In the context menu you can then tell SOGo to refresh the calendar on each login.
