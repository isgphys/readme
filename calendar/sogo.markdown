D-PHYS Calendar (SOGo)
======================

SOGo offers sync via both CalDAV and ActiveSync and supports LDAP groups.

Some noteworthy features are:

- multiple calendars per account
- public calendars that can be freely shared or integrated into a web page
- LDAP group support. This means you can address the same group of people that have access to your group share
- integration of web calendars (.ics)

Of course, there's also ID's Exchange email/calendar solution. That one is a valid solution as long as you're in a pure Windows/Outlook environment, but it lacks some features that Sogo offers:

- calendar sync via CalDAV (not just ActiveSync)
- allow shared calendars to be synced


Login
-----

https://sogo.phys.ethz.ch/

Common tasks in the web interface
---------------------------------

Some common tasks in the web interface are described [here](/calendar/sogo_comon_tasks)

Synchronization with external clients
-------------------------------------

SOGo offers both CalDAV and ActiveSync. Use CalDAV for Apple devices, Thunderbird and Android via DAVx5, and ActiveSync for Android and Outlook.

Calendar client setup is described [here](/calendar/sogo_client_config).
