How to Use Calendar Feeds
=========================

We provide iCal-compatible feeds for the talks at D-PHYS. Unfortunately, not all calendar clients take the right action if you just click on these links in your web browser. We have seen programs that just add the current contents of the file to a calendar but do not actually subscribe to the feed. Hence, you will not get updates in the future. Below we list some popular clients and show how to properly subscribe to the ics feed.

Go to http://events.phys.ethz.ch/feeds and choose the feed you want to subscribe to.

On some clients it's also hard to find out in which intervals they update the feed. You might want to manually refresh from time to time.

### Apple Calendar/iCal

In the menu `Calendar` click _Subscribe_ and copy/paste one of the URLs above.

### Thunderbird/Lightning

Right click on calendar pane, _New calendar_ -> _On the network_ -> _iCalendar_, URL goes into _Location_

### Outlook

Outlook 2003 does not support ics subscriptions. Outlook 2007 does. _Tools_ -> _Account settings_ -> _Internet Calendar_ tab -> _New_, provide URL and a name

### iPhone/iPod Touch

Go to _Settings_ -> _Mails, Contacts, Calendars_ -> _Add Account_ -> _Other_ -> _Add Subscribed Calendar_, type in the URL
