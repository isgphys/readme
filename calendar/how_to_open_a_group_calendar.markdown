How to open a group Calendar in Outlook
=======================================

If you want a group or a resource calendar for your group, please ask us we will create one for you.

To add a group calendar follow the steps below.

* Open Outlook
* Click on **File**.
* Under the **Info** header, click on the **Account Settings** button and then on the **Account Settings** in the list.

[[!img /media/windows/connect_to_group_calendar/1.png size="400x"]]

* In the **E-mail** tab, make sure your **ETH Exchange** account is selected, then click on **Change**. If you don't have click on the **New...** button and add it.

[[!img /media/windows/connect_to_group_calendar/2.png size="400x"]]

* Click the **More Settings** button.

[[!img /media/windows/connect_to_group_calendar/3.png size="400x"]]

* Click on the **Advanced** tab and then click on **Add**.

[[!img /media/windows/connect_to_group_calendar/4.png size="400x"]]

* Enter the email address of the delegated mailbox in the **Add mailbox** field. Then click on **OK**.

[[!img /media/windows/connect_to_group_calendar/5.png size="400x"]]

* Click on **Apply** and then on **OK**. Outlook will now be busy accessing the other mailbox’s data, so this may take a while.

* In the calendar view can you see now your group calendar.
