Calendar
========

There are two types of calendars at D-PHYS that we recommend to use: [SOGo](https://sogo.phys.ethz.ch) is our calendar solution and [Microsoft Exchange](https://mail.ethz.ch) is provided by Informatikdienste.

Take a look at the [Calendar compatibility Matrix](/calendar/calendar_compatibility_matrix) to choose the right one for you.

D-PHYS SOGo/Calendar
-------------------------

We run a web-based [calendar](/calendar/sogo) system (SOGo) that allows calendar management and collaboration (sharing/editing) with other D-PHYS users. It provides a personal calendar for every user (by default) and group calendars ([[contact us|/services/contact]]). It also syncs calendar entries to a wide range of desktop and mobile clients.

* [D-PHYS Groupware and Calendar](sogo)

Calendar Feeds
--------------

We also host [events.phys.ethz.ch](http://events.phys.ethz.ch), a web-based event announcement system. Apart from the displays around campus, RSS and PDF downloads, the events are also provided as iCal subscriptions - see the link below.

* [[How to use Calendar Feeds]]

Doodle
------

If you need to coordinate meeting times, have a look at ETH's `doodle`. This time management tool allows to poll collaborators to determine the best time and date to meet.

* [ethz.doodle.com](https://ethz.doodle.com/)

ETH Exchange/Calendar
---------------------

The _Informatikdienste_ of ETH use Microsoft's Exchange for email and calendar.

* [ETH Email and Calendar](https://unlimited.ethz.ch/pages/viewpage.action?pageId=16451437)

If you find yourself receiving a lot of Exchange meeting requests from other people at ETH but are yourself using SOGo, you can sync your calendar with Thunderbird as described [[here|thunderbird]]. If you then read the invitation email in Thunderbird, it detects the event and allows you to accept the invitation.
