# SOGo Calendar Outlook

Connect to the D-PHYS SOGo calendar using Outlook.

## Install required plugins

Install [Outlook CalDav Synchronizer](https://caldavsynchronizer.org/download) ([GitHub](https://github.com/aluxnimm/outlookcaldavsynchronizer/releases)).

## Configure Outlook CalDav Synchronizer

- (Optional) Create a calendar

[[!img /media/sogo/outlook-caldavsynchronizer-0.png alt="D-PHYS SOGo Outlook CaldavSynchronizer" size="200x"]]
[[!img /media/sogo/outlook-caldavsynchronizer-1.png alt="D-PHYS SOGo Outlook CaldavSynchronizer" size="200x"]]

- In the **CalDav Synchronizer** tab select **Synchronization Profiles**
- Click on the `+` (plus) icon to add a new profile

[[!img /media/sogo/outlook-caldavsynchronizer-2.png alt="D-PHYS SOGo Outlook CaldavSynchronizer" size="400x"]]
[[!img /media/sogo/outlook-caldavsynchronizer-3.png alt="D-PHYS SOGo Outlook CaldavSynchronizer" size="400x"]]

- Select **Sogo**
[[!img /media/sogo/outlook-caldavsynchronizer-4.png alt="D-PHYS SOGo Outlook CaldavSynchronizer" size="400x"]]

- **Name**: Choose a name (example: `Sogo`)
- **Outlook folder**: Select the previously created calendar
- **DAV URL**: `https://sogo.phys.ethz.ch/SOGo/dav`
- **User name**: Enter your D-PHYS username
- **Password**: Enter your D-PHYS password

[[!img /media/sogo/outlook-caldavsynchronizer-5.png alt="D-PHYS SOGo Outlook CaldavSynchronizer" size="400x"]]

- Click **Test or discover settings**
- Select the calendar(s) to show
- Adapt Synchronization mode and interval to your liking
- Confirm with **OK**

[[!img /media/sogo/outlook-caldavsynchronizer-6.png alt="D-PHYS SOGo Outlook CaldavSynchronizer" size="400x"]]
