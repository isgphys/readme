Data Transfer
=============

We describe some typical tools to transfer small to large amounts of data from one host to another.


Small transfers
---------------

### SMB / sFTP / FileZilla / Cyberduck

* User-friendly graphical interface instead of command line
* Cross-platform

The easiest solution is to mount the groupshare on one computer and manually copy the files with drag-n-drop. This is only recommended for transfers of up to several gigabytes.


Recommended tool
----------------

### rsync

* Best all-rounder solution that works for small and large transfers.
* Scans files in target location, only copies what is not yet present.
* Allows to resume transfer after interruption, without having to re-copy all files.

Typical usage:

```sh
rsync -avP /path/to/local/folder/ \
dphysuser@login.phys.ethz.ch:/home/groupname/subfolder/
```

See `man rsync` for a full documentation of all available options.


Large transfers
---------------

### Globus Online & GridFTP

* Best solution to transfer several terabytes of data
* Uses the high-performance data transfer protocol GridFTP
* May not be supported by all universities.
    * CSCS support command line (GridFTP with SSH authentication)
    * CSCS support Globus.org, Endpoint: **CSCS Globus Online Endpoint**

#### via Globus web interface

* Make sure to contact us, we need to enable your user
* Open a Browser and go to [globus.org](https://www.globus.org)
* Use your ETH account to login
    * Select **ETHZ - ETH Zurich**
* Search for the collection **ETH Zurich#D-PHYS**
    * Initially will not see any data
    * Type `/USERNAME` or `/SHARENAME` into the path field to access your data, just as if you would access it [via our file servers](https://readme.phys.ethz.ch/storage/access)

#### Sharing via Globus

* Contact us, we disabled most sharing properties
