Storage
=======

All owners of a D-PHYS [[account/]] have a certain amount of disk space on our file servers.

General Advice
--------------

Please follow our [[general advice]] regarding file management on our storage systems.

Access the files
----------------

Read our readme on how to configure your computer to [[access]] your files.

Group drives
------------

Research groups of the Department of Physics can get shared file space or group drives. Feel free to [[contact us|services/contact]]!
Think of a good name for the group drive, up to 8 characters. If
it's to be served by HTTPS, prepend www ahead of the name.

Quota
-----

To protect the users from overfilling the servers, each user has a limited disk space available. You may have a look at your [current disk usage](https://account.phys.ethz.ch/). If you think you need more quota, get [[in touch with us|services/contact]].

Web space
---------

Learn more about [[web/how to get a personal homepage]].

Backups
-------

All file servers are backed up on a regular basis. Read our [[instructions|backups/restore]] on how to restore lost files.

Access permissions
------------------

Access to the files on our file servers is granted by the usual [Unix-type owner-group-other schema](http://en.wikipedia.org/wiki/Filesystem_permissions#Traditional_Unix_permissions). In some cases, when the number of users is very small, we also use [[documentation/ACL]]s (we don't need an extra Unix group in this case).

Transfer files
--------------

Read our tips on how to [[transfer data|storage/data_transfer]].

Share files
-----------

Different services allow you to share large files with other people:

* The [Polybox](http://polybox.ethz.ch) service to share and collaborate with other members at ETH.
* The [SWITCHfilesender](https://www.switch.ch/services/filesender/) service to share one large file with anyone, or even allow someone else to send you a large file.
* Your own [[personal web space|web/how_to_get_a_personal_homepage]] at D-PHYS.

More Information
----------------

* [Research Data at ETH Zurich](https://www.ethz.ch/researchdata)
    - [Research Data Wiki of Library ETH Zurich](https://documentation.library.ethz.ch/display/DD/Research+Data+Management+and+Digital+Curation)
* [[windows/Where should I save my files]]
