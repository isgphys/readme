Access your files
=================

Here we describe how to access our file servers from the different operating systems.

Note that file server access via SMB from outside the ETH network only works via [[VPN|services/how_to_use_vpn]]! Alternatively you can try to access the files via the SSH protocol from everywhere. Scroll down to "Using SSH" for details.

In order to cope with the huge amount of storage at D-PHYS and to optimize for different usage patterns, we have multiple front end servers. Please use the table below to determine which server you need:
<table>
  <caption>File server mounts</caption>
  <tr>
    <td></td>
    <th scope="col">SMB mount</th>
  </tr>
 <tr>
    <th scope="row">Home directory</th>
    <td>smb://home.phys.ethz.ch/<i>USERNAME</i></td>
  </tr>
 <tr>
    <th scope="row">D-PHYS group share</th>
    <td>smb://group-data.phys.ethz.ch/<i>SHARENAME</i></td>
  </tr>
 <tr>
    <th scope="row">Galaxy group share</th>
    <td>smb://galaxy-data.phys.ethz.ch/<i>SHARENAME</i></td>
  </tr>
 <tr>
    <th scope="row">IPA group share</th>
    <td>smb://ipa-data.phys.ethz.ch/<i>SHARENAME</i></td>
  </tr>
 <tr>
    <th scope="row">IGP group share</th>
    <td>smb://igp-data.phys.ethz.ch/<i>SHARENAME</i></td>
  </tr>
 <tr>
    <th scope="row">NUX data group share</th>
    <td>smb://nux-data.phys.ethz.ch/<i>SHARENAME</i></td>
  </tr>
 <tr>
    <th scope="row">web shares (<i>www*</i>)</th>
    <td>smb://misc-data.phys.ethz.ch/<i>SHARENAME</i></td>
  </tr>
 <tr>
    <th scope="row">any share via smbgate in exile network</th>
    <td>smb://smbgate.phys.ethz.ch/<i>SHARENAME</i></td>
  </tr>
</table>

You can also use the `Account Info` tab in our [account portal](https://account.phys.ethz.ch/info) to determine the mounts for all shares you have access to.

On Windows
----------

### How to connect your home drive

* press `🪟 Windows` and the `R` key together
* Type `\\home.phys.ethz.ch\USERNAME`  eg: `\\home.phys.ethz.ch\patrika`
* Enter your D-PHYS username **`AD\USERNAME`** and your D-PHYS password and select OK
* Optionally click with the right mouse button on your home and assign a drive letter

### How to connect to a group share

* press `🪟 Windows` and the `R` key together
* Type `\\SERVERNAME\SHARENAME` eg: `\\group-data.phys.ethz.ch\nano1`, where **SERVERNAME** is take from the table above
* Enter your D-PHYS username **`AD\USERNAME`** and your D-PHYS password and select OK
* Optionally click with the right mouse button on your home and assign a drive letter


On macOS
--------

* Select the **Finder**
* In the menu **Go**, click on **Connect to server...**
* Type in `smb://home.phys.ethz.ch/homes` for your home, respectively `smb://SERVERNAME/SHARENAME` for group shares (**SERVERNAME** taken from the table above) and click **Connect**
* In the following dialog enter your D-Phys username and password
* The mounted share should appear on your desktop or Finder window
* Optionally: configure quick access via the [[Finder sidebar|osx/finder_volumes]]


### Troubleshooting

It may happen that the wrong username or password were saved and you can no longer connect to the network share. You may either remove the entry in the [[Keychain Access|osx/keychain]] application, or provide the correct username when connecting: `smb://username:*@home.phys.ethz.ch/homes`

#### Cosmologists

If you don't see all files inside the `ipa/refreg/data` subfolder, please create a `/etc/nsmb.conf` file, with the following contents

```
[default]
file_ids_off=yes
```

to fix bugs because of nested file systems.


On Linux
--------

There are several ways to access our SMB shares: (**replace "USERNAME" with your D-PHYS user name**).

The package `cifs-utils` must be installed on your system:

```
apt install cifs-utils
```

## Konqueror, Midnight commander and others

You are able to connect directly to:

```
smb://home.phys.ethz.ch/homes
```

and

```
smb://SERVERNAME/SHARENAME
```

(see table at the top)

If you get an error, you may need to configure samba. Check that the following line is in the section `[global]` in your `/etc/samba/smb.conf`:

```
[global]
client max protocol = SMB3
```

It may also be necessary to specify your D-PHYS username:

```
smb://AD;USERNAME@SERVERNAME/SHARENAME
```

## mount

You may mount the files server on your local workstation.

To access your home:

```
mkdir -p /media/dphys
sudo mount -t cifs -o domain=AD,username=USERNAME,vers=3.0 //home.phys.ethz.ch/homes /media/dphys
```

To access the group share `SHARENAME` on server `SERVERNAME`:

```
mkdir -p /media/GROUP
sudo mount -t cifs -o domain=AD,username=USERNAME,vers=3.0 //SERVERNAME/SHARENAME /media/SHARENAME
```


To map `uid` and `gid` to your local user:

```
sudo mount -t cifs -o domain=AD,username=USERNAME,vers=3.0,uid=$(id -u $UID),gid=$(id -g $UID) //home.phys.ethz.ch/homes /media/dphys
```

## smbclient

This will give you a ftp-like interface.

To access your home:

```
smbclient --max-protocol SMB3 -W AD -U USERNAME //home.phys.ethz.ch/homes
```

To access the group share `SHARENAME` on server `SERVERNAME`:

```
smbclient --max-protocol SMB3 -W AD -U USERNAME //SERVERNAME/SHARENAME
```

## Direct access on managed Linux workstations

On our managed Linux workstations, your home and shares will be automounted to

<table>
  <caption>File server mounts</caption>
  <tr>
    <td></td>
    <th scope="col">Linux workstation path</th>
  </tr>
  <tr>
    <th scope="row">Home directory</th>
    <td>/home/<i>USERNAME</i></td>
  </tr>
  <tr>
    <th scope="row">D-PHYS group share</th>
    <td>/home/<i>SHARENAME</i></td>
  </tr>
 <tr>
    <th scope="row">IPA group share</th>
    <td>/home/<i>SHARENAME</i></td>
  </tr>
 <tr>
    <th scope="row">Galaxy group share</th>
    <td>/home/<i>SHARENAME</i></td>
  </tr>
 <tr>
    <th scope="row">IGP group share</th>
    <td>/home/<i>SHARENAME</i></td>
  </tr>
</table>


Using SSH (any operating system)
--------------------------------

Even though SMB access is not possible without VPN from outside ETH, you may access your files from everywhere using tools based on the [[SSH|documentation/ssh]] protocol.

All variants work by connecting to **login.phys.ethz.ch** or one of our student pool workstation using your normal D-PHYS username and password.

### SCP or SFTP

You can access your files also using SCP (Secure Copy) or SFTP (Secure File Transfer Protocol) clients, e.g. [WinSCP](http://winscp.net/) or [Putty](http://www.chiark.greenend.org.uk/~sgtatham/putty/) under Windows or [Cyberduck](http://cyberduck.io/) under macOS. See our Readme [[windows/SFTP Samples Windows Workstation to D-PHYS Workstation]] for how to use SFTP with WinSCP.

### SSHFS

SSHFS on Linux or Windows provide a way to mount shares directly and access files using the default operating system file explorers. See our Readme [[storage/sshfs]] for setup instructions.

### SSHUTTLE

Mac and Linux users may also use [sshuttle](https://github.com/sshuttle/sshuttle) as replacement for VPN and then connect to the storage with the SMB protocol.
