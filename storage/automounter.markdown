Automounter
===========

Common usage of automounting on our Linux workstations
------------------------------------------------------

* `/net/<hostname>/scratch`
* `/backup/`, see [[Backup Restore|backups/restore]]

Explanation of the automountable directories
--------------------------------------------

The directory `/scratch` is always local on the disk of the computer you're logged into (either graphically or via SSH). There is no
backup of data there.

If you want to access these directories from some other managed Linux workstation, you can access them via `/net/<hostname>/<directory-path>`, e.g. to access `/scratch` from `workstation-a` while being logged in on  `workstation-b` go to `/net/workstation-a/scratch/`, e.g. by typing on the command-line:

```
cd /net/workstation-a/scratch
```

(Tab-completion does not work for the hostnames as this is automatically mounted on access and tab completion just works for already mounted stuff.)
