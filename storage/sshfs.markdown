SSHFS
=====

SSHFS on Linux or Windows provide a way to mount group shares directly and access files using the default operating system file explorers.

When using SSHFS to access group shares, watch out to create new files with the proper permissions, so that other group members can edit them as well. See instructions below to set an appropriate `umask`.

In the instructions below, replace `<username>` with your D-PHYS username and `<groupshare>` with the name of the groupshare.

Linux
-----

Installation (Debian/Ubuntu):

```bash
sudo apt install sshfs
```

On Linux you can adapt the default permissions by first running the command `umask 0002` before transferring files:

```bash
umask 0002
sshfs <username>@login.phys.ethz.ch:/home/<groupshare> /local/path
```

Windows
-------

Installation ([GitHub](https://github.com/winfsp/sshfs-win)) or:

```bash
winget install WinFsp.WinFsp
winget install SSHFS-Win.SSHFS-Win
```

To adapt the default permissions, `sshfs.exe` needs to be started using extra parameters:

### Using ad-hoc commands

Open a command prompt (`cmd.exe`) and enter:

```bash
cd C:\Program Files\SSHFS-Win\bin
set CYGFUSE=WinFsp
set PATH=C:\Program Files\SSHFS-Win\bin
sshfs -f -o create_umask=0002,uid=-1,gid=-1 <username>@login.phys.ethz.ch:/home/<groupshare> z:
```

### Using a shortcut on the desktop

Create a txt file with file extension `.bat` (example: `sshfs_groupshare.bat`):

```bash
@echo off

set sshfs-username=<username>
set sshfs-server=login.phys.ethz.ch
set sshfs-share=/home/<groupshare>
set sshfs-letter=Z:

set sshfs-root=C:\Program Files\SSHFS-Win\bin

cd "%sshfs-root%"
set CYGFUSE=WinFsp
set PATH=%sshfs-root%
sshfs -f -o create_umask=0002,uid=-1,gid=-1 %sshfs-username%@%sshfs-server%:%sshfs-share% %sshfs-letter%
```

Double-click the bat file to connect. Close the command prompt window to close the connection.
