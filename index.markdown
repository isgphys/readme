D-PHYS IT Documentation
=======================

Use the sub-navigation on the side to navigate by topic, or the links below for different target audiences and frequently asked questions. A search box can be found on the bottom of the page.

### I'm a

* [[audience/New member of D-PHYS]]
* [[audience/User of Managed Workstations at D-PHYS]]
* [[Work from home or abroad|audience/remote work]]
* [[audience/New laptop owner]]
* [[audience/Windows user]]
* [[audience/Mac user]]
* [[audience/Linux user]]
* [D-BAUG IGP staff member](https://wiki.phys.ethz.ch/!igp/global:it)
* Student working in either [D-BAUG IGP PF-Lab](https://wiki.phys.ethz.ch/!igp/prs:it:pf_lab) or [D-BAUG IGP GSEG-Lab](https://wiki.phys.ethz.ch/!igp/gseg:it:gseg_lab)
* [[audience/Administrator of a lab PC]]
* [[audience/IT coordinator at a D-PHYS research group]]
* [[audience/Administrator at ISG]]

### Learn more

Check out our lecture series [Basics of Computing Environments for Scientists](https://compenv.phys.ethz.ch/)

### Frequently Asked Questions

* [[How to get or keep a physic account|account/policy]]
* [[How to get help from us|services/contact]]
* [[How to recover my lost password|account/]]
* [[How to access backups|backups/restore]]
* [[printing/How to resolve printing issues]]

### Sitemap

* [[List of all documentation topics|attic/sitemap]]
