Microsoft 365 settings on Windows
=================================

There are some settings for Microsoft 365 that make it more usable or strengthen privacy settings on Windows. The similar settings for macOS are documented [[here|osx/ms365_settings]].


## Use the old mail account wizard

The new automatic mail account wizard will not work with the `@phys.ethz.ch` email addresses. Instead you should use the mail setting from Windows Control Panel as described [here](/mail/how_to_use_email_with_outlook).

## Change the default file save location

The default file save location for Microsoft 365 is the OneDrive which is in the Microsoft Cloud. It's recommended to change this to your local, home or group drive by following the instructions [here](https://support.microsoft.com/en-us/topic/customize-the-save-experience-in-office-786200a7-f5f2-4d26-a3ae-b78c60dd5d3b).

## Configure privacy settings

You can find a lot of privacy settings and their description on this
[Microsoft web page](https://docs.microsoft.com/en-us/deployoffice/privacy/manage-privacy-controls).

We recommend the following settings:

* [Policy setting for connected experiences that analyze your content](https://docs.microsoft.com/en-us/deployoffice/privacy/manage-privacy-controls/#policy-setting-for-connected-experiences-that-analyze-your-content) -> set to **Disabled**

* [Policy setting for diagnostic data](https://docs.microsoft.com/en-us/deployoffice/privacy/manage-privacy-controls/#policy-setting-for-diagnostic-data) -> set to **Neither**

To create a `.reg` file for modifying privacy settings, open Notepad and copy the following lines. Adjust the values to suit your needs, and then save the file. Be sure the file name has an extension of `.reg`

```reg
Windows Registry Editor Version 5.00

[HKEY_CURRENT_USER\Software\Policies\Microsoft\office\16.0\common\privacy]
"usercontentdisabled"=dword:00000001

[HKEY_CURRENT_USER\Software\Policies\Microsoft\office\common\clienttelemetry]
"sendtelemetry"=dword:00000003
```

After saving the file, you can just double click it to add the configuration into the registry.

## Use of shared computer activation for lab computers

You can activate up to 5 different computers with your Cloud Subscription. If you manage computers accessed by multiple users, for example in a lab, it's recommended to use the `shared computer activation` feature. With shared computer activation enabled, it doesn't count against that license limit.

To create a `.reg` file for enable shared computer activation, open Notepad and copy the following lines, then save the file. Be sure the file name has an extension of `.reg`

```reg
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\software\policies\microsoft\office\16.0\common\licensing]
"sharedcomputerlicensing"=dword:00000001
```

After saving the file, you can just double click it to add the configuration into the registry (administrator rights needed).
