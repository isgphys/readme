Configuration hints for self-managed Windows Machines
=====================================================

At D-PHYS you can choose between [[managed|services/windowsws]] and self-managed Windows machines. If you decide to manage your Windows installation by your self, please refer to the following few steps to configure your system to be able to access all D-PHYS IT services. On managed Windows machines all these settings are automatically be done and you have not to take care.

Network Configuration
---------------------

Register and configure your computer to the D-PHYS network according to [[this guide|network/how_to_get_network_access]]

Adjust your DNS client settings
-------------------------------

To be able to reach all D-PHYS services by using the short names (i.E. only type _readme_ to reach our documentation instead of the full qualified domain name _readme.phys.ethz.ch_) it is important to configure a search list to allow your DNS client to first seek a computer name within the _.phys.ethz.ch_ zone and if nothing matched, the computer will also be looked up in _.ethz.ch_. This is necessary because D-PHYS provides some services only in the zone _.phys.ethz.ch_ and your Windows would only try to lookup the short names in _.ethz.ch_ with the standard configuration.

To correct this behavior of your self-manged Windows computer you should follow either of the following guides or alternatively always use the full qualified domain name like _readme.phys.ethz.ch_. (If you are using earlier Windows versions than Windows Vista please [[services/contact]] us.


### Configure DNS search list by GUI

* Go to your start menu and type 'view network connections' into the search field and hit enter.
* The following dialog should appear now listing your existing network connections.

[[!img /media/self_managed_windows_machines/win_dns_settings_1.png size="450x"]]

* Double-click your active LAN connection that brings up the following dialog

[[!img /media/self_managed_windows_machines/win_dns_settings_2.png]]

* Click the button _Properties_ (you may now enter your administrator password to reach a higher level of privileges) Now you should reach the following dialog window:

[[!img /media/self_managed_windows_machines/win_dns_settings_3.png]]

* From the item list choose _Internet Protocol Version 4 (TCP/IPv4)_ and click the button _Properties_ the following configuration dialog show up:

[[!img /media/self_managed_windows_machines/win_dns_settings_4.png]]

* Now click the _Advanced..._ button which brings up the following dialog

[[!img /media/self_managed_windows_machines/win_dns_settings_5.png]]

* In this dialog click the tab _DNS_ and choose the option _Append these DNS suffixes (in Order)_ and click the lower one of the tow 'Add...' buttons:
* Now enter the string _phys.ethz.ch_ in the small text box window and click the _add_ botton.

[[!img /media/self_managed_windows_machines/win_dns_settings_6.png]]

* Secondly click the same _Add..._ button, enter _ethz.ch_ and confirm with the _add_ botton.

[[!img /media/self_managed_windows_machines/win_dns_settings_7.png]]

* Finally the list should look as follows. Confirm every open dialog box with _OK_. Done.

[[!img /media/self_managed_windows_machines/win_dns_settings_8.png]]

### Configure DNS search list by importing the registry settings

You can download the following .zip file to your computer unzip the file and double-click the uncovered reg-file afterward to import the DNS settings directly to your registry (You have to be logged in with a user with administrative privileges to complete this action).

[[/media/self_managed_windows_machines/dns_suffix_list.zip]]
