How to add a Microsoft Excel add-in
===================================

If you want to add an additional Excel add-in, you have to copy the file to the AddIns folder on your computer.

Write **%appdata%** into the Windows search box on your start menu and press "ENTER".

[[!img /media/how_to_add_an_ms_excel_add-in/excel_add-in1.png size="300x"]]

Navigate to the Subfolder **Microsoft\AddIns**

[[!img /media/how_to_add_an_ms_excel_add-in/excel_add-in2.png size="400x"]]
[[!img /media/how_to_add_an_ms_excel_add-in/excel_add-in3.png size="400x"]]

and copy the new add-in file into this folder

[[!img /media/how_to_add_an_ms_excel_add-in/excel_add-in4.png size="400x"]]

Now start Excel and navigate to "**Datei**" -> "**Optionen**"

[[!img /media/how_to_add_an_ms_excel_add-in/excel_add-in5.png size="400x"]]

then "**Add-Ins**" and press the button "**Gehe zu...**"

[[!img /media/how_to_add_an_ms_excel_add-in/excel_add-in6.png size="400x"]]

The new add-in (which was copied in the first step) should appear in this list. **Select** it and press the "**OK**" button.

[[!img /media/how_to_add_an_ms_excel_add-in/excel_add-in7.png size="400x"]]

Now there will be a new menu option "**Add-Ins**" displayed in Excel, choose it and the add-in is now available for using.

[[!img /media/how_to_add_an_ms_excel_add-in/excel_add-in8.png size="400x"]]
