How to configure PyCharm for using Anaconda
=================================================

For using "Anaconda Python 3 distribution" in PyCharm just follow this readme.

Prerequisite
------------

- install PyCharm [Pro](https://www.jetbrains.com/pycharm/download/#section=windows) or [EDU](https://www.jetbrains.com/edu-products/download/#section=pycharm-edu) version or for managed Windows workstations, order it by [chic tool](https://chic.phys.ethz.ch/public/windows/packages)

Note for **PyCharm Pro** you can get a **free** Jetbrains account. For more information see [[here|documentation/jetbrains_edu_account]]

- install [Anaconda](https://www.anaconda.com/products/individual "Download") or for managed Windows workstations, order it by [chic tool](https://chic.phys.ethz.ch/public/windows/packages)

**Note:** There is a difference in configuration between **PyCharm Pro** and **PyCharm Edu** versions.

PyCharm Pro
-----------

The following steps describe how to configure **PyCharm Pro** for using Anaconda Python Interpreter.

[[!img media/windows/pycharm/pycharm_pro_anaconda_1.png]]

[[!img media/windows/pycharm/pycharm_pro_anaconda_2.png]]

[[!img media/windows/pycharm/pycharm_pro_anaconda_3.png]]


PyCharm Edu
-----------

The following steps describe how to configure **PyCharm Edu** for using Anaconda Python Interpreter.

[[!img media/windows/pycharm/pycharm_edu_anaconda_1.png]]

[[!img media/windows/pycharm/pycharm_edu_anaconda_2.png]]

[[!img media/windows/pycharm/pycharm_edu_anaconda_3.png]]
