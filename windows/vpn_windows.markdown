How to setup VPN on Windows
===========================

To use the VPN on a Windows computer, you will need the **Cisco Secure Client** (formerly known as Cisco AnyConnect).


## Instructions for the installation and configuration of the Cisco Secure Client

For the login into https://sslvpn.ethz.ch and the configuration of Cisco Secure Client use the example given below (please replace the stars\*\*\* with your **ETH user name** and enter your **ETH network password** )

### Download & Install

* Login [sslvpn.ethz.ch](https://sslvpn.ethz.ch)

    |who      | realm               |
    |---------|---------------------|
    |staff    | staff-net.ethz.ch   |
    |students | student-net.ethz.ch |

[[!img media/windows/vpn/WebVPNStaff.PNG]]

* Download the Cisco Secure Client and install it.

[[!img media/windows/vpn/Download.PNG size="400x"]]

### Connect to ETHZ

* Start the Cisco Secure Client

[[!img media/windows/vpn/open.PNG size="400x"]]

* Cisco AnyConnect configuration.

    |who                                          | realm       |
    |---------------------------------------------|-------------|
    |staff                                        | staff-net   |
    |students                                     | student-net |
    |selected users with managed D-PHYS notebooks | phys-ad     |

[[!img media/windows/vpn/VPNStaff.PNG]]

* Cisco AnyConnect login

    |who                                          | realm               |
    |---------------------------------------------|---------------------|
    |staff                                        | staff-net.ethz.ch   |
    |students                                     | student-net.ethz.ch |
    |selected users with managed D-PHYS notebooks | phys-ad.ethz.ch     |

[[!img media/windows/vpn/VPNStaffLogin.PNG]]

