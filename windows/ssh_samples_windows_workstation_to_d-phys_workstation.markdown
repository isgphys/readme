Access Linux workstations from any Windows computer via ssh
===========================================================

PuTTY is a program to access an ssh server from Windows. You may get PuTTY on the original site http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html. You should get the installer _putty-X.XXX-installer.exe_ to get all tools. For a simple login, _putty.exe_ will be enough.

**Login to a server**
After installing, start PuTTY from Start / Programs / PuTTY. The first dialog appears:

[[!img /media/ssh_samples_windows_workstation_to_d-phys_workstation/dialog1.png alt="dialog1.png"]]

Enter the hostname you like to connect and activate SSH as protocol. Save this session as _Default Settings_ to be sure to not use telnet by error.

The first time you connect to a server, this dialog appears:

[[!img /media/ssh_samples_windows_workstation_to_d-phys_workstation/dialog2.png alt="dialog2.png"]]

The servers host key will be added to your registry and you wont be prompted again. This secures that you are connecting every time to the same machine and protects you from a man in the middle attack.

Now, the terminal window appears and PuTTY asks you about the username:

[[!img /media/ssh_samples_windows_workstation_to_d-phys_workstation/dialog3.png alt="dialog3.png" size="450x"]]

and the password:

[[!img /media/ssh_samples_windows_workstation_to_d-phys_workstation/dialog4.png alt="dialog4.png" size="450x"]]

If everything went OK, you'll get the shell:

[[!img /media/ssh_samples_windows_workstation_to_d-phys_workstation/dialog5.png alt="dialog5.png" size="450x"]]

**File transfer**
In PuTTY's menu you'll find an entry PSFTP. It's the same program as _sftp_ documented [[here|documentation/ssh_samples_d-phys_workstation_to_d-phys_workstation]]. `C:\Program Files\PuTTY\pscp.exe` is the same program as _scp_. Adjust your path from Windows to include PuTTY's directory if you use pscp often.

**X11 forwarding**
To use X11 forwarding, you need to install an X11 server. The best X server for Windows is XMing, which you can download from http://www.straightrunning.com/XmingNotes/. There is at least one Open-Source alternative - you may get Cygwin on http://www.cygwin.com/ and install XFree86. You may also install Cygwin's OpenSSH and use ssh like on [[Linux|documentation/ssh_samples_d-phys_workstation_to_d-phys_workstation]]. Cygwin's X11 server is not as good integrated in the Windows desktop as the commercial X11 servers and should only be installed by people who knows how to use a typical Linux, BSD or UNIX workstation.
After starting your local X11 server, activate X11 forwarding in PuTTY:

[[!img /media/ssh_samples_windows_workstation_to_d-phys_workstation/dialog6.png alt="dialog6.png"]]

After logging in on the server, you could start any X11 application and the output window should appear on your desktop. As on a real ssh, the traffic is tunneled and encrypted by PuTTY.

**Key generation**
To get a password free access to the server, you need to create ssh-keys. PuTTY delivers a nice application called PuTTYgen with do that:

[[!img /media/ssh_samples_windows_workstation_to_d-phys_workstation/dialog8.png alt="dialog8.png"]]

You'll need to do several steps (NOTE: YOU PROBABLY DON'T WANT TO CREATE SSH1 AND SSH2DSA KEYS ANYMORE):

* Select SSH2RSA as Type of key
* Click generate
* Move the mouse until PuTTYgen has enough randomness
* Enter a passphrase twice
* Save public key as _id_rsa.pub_
* Save private key as _id_rsa.ppk_

Load your generated keys to the agent by double-clicking on the files with the hat:

[[!img /media/ssh_samples_windows_workstation_to_d-phys_workstation/dialog10.png alt="dialog10.png"]]

When using PuTTY often, it would be the best to put a shortcut to this key files in your Autorun-Group of the start menu.

Now activate Allow agent forwarding in PuTTY's configuration:

[[!img /media/ssh_samples_windows_workstation_to_d-phys_workstation/dialog9.png alt="dialog9.png"]]

Login to your server and type in the following command:

```
beat@paris:~$ cd .ssh/
beat@paris:~/.ssh$ ssh-add -L > authorized_keys
```

You should be now able to login without entering a password.
