Windows 7 end of life
=====================

The time has come to upgrade your Windows 7 computer to Windows 10
since extended support for Windows 7 ends on January 14, 2020.

- [Windows life-cycle](https://support.microsoft.com/en-us/help/13853/windows-lifecycle-fact-sheet)

### Why can I no longer use Windows 7 on the ETH network after the end of 2019?

Only operating systems with security support by the vendor are allowed to connect to the ETH network.

Unsupported operating systems that no longer receive security updates render the computer vulnerable to threats like viruses, malware or hacker attacks and also pose a threat to other computers on the network.

Check out our FAQ section below:

### Which Windows 10 version should I choose?

We recommend two versions of Windows 10 depending on the use case. All versions be ordered in the IT-Shop [https://idesnx.ethz.ch](https://idesnx.ethz.ch "IT-Shop").

#### Windows 10 Enterprise

Recommended for **almost all users**, for example office workstations, notebooks or tablets

- The support period is 18 months.
- Windows Update will extend the support with automatic feature upgrades, twice a year, as you know from your computer at home.
- This version will support new hardware by including the necessary drivers.

#### Windows 10 LTSC ("Long-term servicing channel")

Recommended for laboratory computers and critical systems

- The support period is 10 years.
- The support can be extended with an installation of a newer version (like Windows 7 to Windows 10).
- This version will only support the hardware available at the release time.
- You will get only security updates and bug fixes, no new features.

### I cannot upgrade because…

- a Windows 10 system reboots automatically and in Windows 7 this does not happen:
- Our readme explains different ways to [suppress an automatic reboot](https://readme.phys.ethz.ch/windows/windows10/suppress_auto_reboot/).
- I don’t want Microsoft to collect any telemetry data:
    - The sending of telemetry data can be [minimized](https://readme.phys.ethz.ch/windows/windows10/disable_telemetry/).
- my software or device driver is not Windows 10 compatible or
- my Windows 7 computer is embedded in a lab instrument and I can’t upgrade it:
    - If you don’t need network access, remove the network cable and everything is OK.
    - If you need network access only to communicate with sensors or something else in the same room, create your own isolated network with a network switch.
    - If you need network access to transfer your data to the server, then please contact us about the [eXile network](https://exile.phys.ethz.ch).

Note that at some point the network security group of Informatikdienste will start scanning for remaining Windows 7 computers at which point we will be forced to disconnect them from the network.
