Python for Windows
==================

Using Python on Windows is different then on Linux and macOS. For a peaceful installation you will need the right Python distribution and the right IDE. This article should help you, to understand what the differences are and which is the right one for you.

## Python distribution

There a few Python distributions on the net. We recommend two of them and always the newest version and 64bit edition.

* [Python.org](https://www.python.org/downloads/windows/)
    * This is a slim Python distribution without any extras.
    * We recommend this for little projects without many dependencies.
    * Also we recommend this for high performance projects. (not for beginners)
* [Anaconda](https://www.anaconda.com/download/)
    * This is a fat Python distribution with many pre-compiled libraries included.
    * We recommend this for beginners in scientific Python or when you don't need the highest performance.

The usage of Python is made much easier, when you include the binaries in the path environment variable.
There are two folders you must add, namely `C:\[PYTHONROOT]` and `C:\[PYTHONROOT]\scripts`.

## IDE

There are many IDEs on the market. We take just two of them, because we have experience with them.

### [Visual Studio Code](https://code.visualstudio.com/)

#### Benefits of VSCode

* This is a slim editor with IntelliSense and built-in Git and can be customized with extensions.
* It's ideal for small projects.
* It has support for Python's virtual environments.
* Cross-platform with support of all major operating systems.

#### First steps with VSCode

* Install Python and Visual Studio Code.
* [Install the Python extension](https://marketplace.visualstudio.com/items?itemName=ms-python.python).
* Create a [virtual environment](https://compenv.phys.ethz.ch/python/ecosystem_1/30_virtual_environments/).
* You always need to activate the environment before starting your work.
* Open VS Code with 'code .' in your command line.
* Press <kbd>CTRL</kbd>+<kbd>Shift</kbd>+<kbd>P</kbd>, type: `Select Workspace Interpreter` and choose your virtual env.
* Create a new file `myScript.py` and write

```python
word_one = "Hello"
word_two = "World"
print(word_one + " " + word_two)
```

* and run it with <kbd>F5</kbd>.
* In the `Debug Console` you should see `Hello World`.

### [Visual Studio Community 2017](https://www.visualstudio.com/downloads/)

#### Benefits

* This is a fat IDE with many built-in features.
* There are C, C++, C#, VB and many more compilers included. This is important for high performance computing.
* You can even add other compilers like CUDA or Fortran.
* Visual Studio provides a better overview for bigger projects.
* IntelliSense and built-in Git is also available.
* Great debugging features.

#### First steps

* Detailed instructions are [here](https://docs.microsoft.com/en-us/visualstudio/python/python-in-visual-studio).
* Install Python and Visual Studio with the Python and the C++ workload.
* Open Visual Studio and create a new Python project.
* Create a [virtual env](https://docs.microsoft.com/en-us/visualstudio/python/python-environments#virtual-environments).
* Edit your Python script

```python
word_one = "Hello"
word_two = "World"
print(word_one + " " + word_two)
```

* and run it with <kbd>F5</kbd>.
* In the `Debug Console` you should see `Hello World`.
