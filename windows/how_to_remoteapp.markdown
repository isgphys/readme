How to use RemoteApp for regular/permanent access
============================

This manual describes the use of RemoteApp for regular/permanent access.

If you only want to use the RemoteApp one-time/temporary access, use this steps [here](/services/using_remoteapp).

* Type in the Windows Search `RemoteApp and Desktop Connections`

[[!img /media/windows/remoteapp/03.png size="300x"]]

* Enter this url `https://winlogin.phys.ethz.ch/RDWeb/Feed/webfeed.aspx`

[[!img /media/windows/remoteapp/04.png size="300x"]]

* Confirm the message

[[!img /media/windows/remoteapp/05.png size="300x"]]

* Enter your username `ad\your_dphys_username` and be sure that you **safe your credentials**

[[!img /media/windows/remoteapp/06.png size="300x"]]

* Now you will find in your start menu the folder `Work Resources (RADC)` which contains all RemoteApps that are available.

[[!img /media/windows/remoteapp/07.png size="300x"]]

* When you start a RemoteApp, you will get message box. Select the checkbox `Dont't ask me again for remote connections from this publisher` and choose `Connect`

[[!img /media/windows/remoteapp/02.png size="300x"]]
