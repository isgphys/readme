How to change the Dropbox location folder
=========================================

After a default installation of Dropbox the location folder is located inside the Windows userprofile and fill up the userprofile quota. Please use this readme to change the location of this folder to avoid problems with the quota space.

Open the "Dropbox preferences.." from Windows systray (right side on the taskbar, near the clock).
Right click with the mouse on this icon.

[[!img /media/how_to_change_the_dropbox_location_folder/dropbox_speicherort_verschieben_0.png]]

Choose "Einstellungen/Preferences"

[[!img /media/how_to_change_the_dropbox_location_folder/dropbox_speicherort_verschieben_1.png]]

Choose "Erweitert/Advanced" tab and change the location with the "Verschieben/Move" Button to "C:\Scratch".

[[!img /media/how_to_change_the_dropbox_location_folder/dropbox_speicherort_verschieben_2.png]]

And save it with the "OK" button.

[[!img /media/how_to_change_the_dropbox_location_folder/dropbox_speicherort_verschieben_3.png]]

The folder is now out of your profile and does affect the quota anymore.
