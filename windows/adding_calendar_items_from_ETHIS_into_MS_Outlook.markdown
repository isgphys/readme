Adding calendar items from ETHIS into MS Outlook
=================================================

You can import an .ics calendar file from ETHIS you received by email.

The following steps describe how to import this calendar .ics file to Microsoft Outlook 2016. Other versions of MS Outlook should be similar.

**Do not open the .ics file or double-click on it**. It will not be added correctly in MS Outlook. Instead
**save** the .ics file to your desktop.

- Start MS Outlook and click on **File** in the menu
- Then choose **Open & Export**

[[!img media/adding_calendar_items_from_ETHIS_into_MS_Outlook/Outlook2016_import_ics_1.png]]

Click on **Import/Export**

[[!img media/adding_calendar_items_from_ETHIS_into_MS_Outlook/Outlook2016_import_ics_2.png size="432x"]]

Select **"Import an iCalendar (.ics) or vCalendar file (.vcs)"** and click **Next**.

[[!img media/adding_calendar_items_from_ETHIS_into_MS_Outlook/Outlook2016_import_ics_3.png size="432x"]]

Browse to your desktop and **select the ETHIS calendar (.ics) file** and click **Open**.

A dialog box will appear. Click the **Import** button to add the data to your calendar.

[[!img media/adding_calendar_items_from_ETHIS_into_MS_Outlook/Outlook2016_import_ics_4.png]]

Your calendar file has now been imported into MS Outlook.
