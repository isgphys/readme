Where should I save my files?
=============================

There are different possibilities to save files (e.g. research data) on the D-PHYS managed Windows workstations. This readme provides best practices and shows the **advantages** and  **disadvantages** of the different locations.

- The **best** idea is to store all data on a **groupshare** `\\group-data.phys.ethz.ch\groupdrivename` e.g. `\\group-data.phys.ethz.ch\picodata` (host name depends on the [[file server|storage/access]])

    - On managed computers, the groupshares will automatically be connected during logon and are therefore visible in the Windows Explorer as  drive **P:**.

    - On the groupshare is a **lot of space** available and it will also be **backed up** every night. You are also able to [[access|storage/access]] them from outside ETH.

    - The data will automatically be accessible to all group members. This also guarantees that research-relevant data remains accessible to the group even after your departure.

- An additional possibility to save files is in the **user home directory** `\\home.phys.ethz.ch\username` e.g. `\\home.phys.ethz.ch\johndoe`

    - On managed computers, the home directory is automatically connected during logon and is visible in the Windows Explorer as drive **H:**.

    - The **space is limited** (quota) for the home directory, so you should use it mainly for **personal files** and not for research or project data. It will also be backed up every night and you are able to [[access|storage/access]] it from outside ETH.

- You can save files on your **desktop** which is part of the **user profile** or in the profile itself. It is located in `C:\Users\%Username` (English installation) or `C:\Benutzer\%Username` (German installation).

    - But the profile **space is limited** (currently 500MB), to avoid long wait times during synchronization. The roaming part of the user profile (e.g. the desktop folder) will be copied during every login and logout process, this is called [roaming user profiles](http://msdn.microsoft.com/en-us/library/bb776897.aspx). Therefore **don't save a lot of data on the desktop** or in `C:\Users\%Username` (a few megabytes are still ok).
    - A good idea is to **put shortcuts on your desktop** to quickly access files or folders you often need, instead of placing the files themselves on the desktop.
    - The local user profile folder will be **deleted automatically** if there was no logon for 270 days. The roaming part of the profile will still be available, saved on the server for 400 days, but the local part of it is lost.


- Sometimes you need to save files **locally** on the computer, for instance to improve the performance of computational jobs. However, you need to be aware of the following **short-comings**:

    - **The files are not safe** in case of a hard-drive failure, a human error or virus which may delete or encrypt them. They are **not** automatically backed up, **you will lose them if you have no backup done by yourself!**
    - This data is lost if the computer has to be reinstalled.
    - You are **not** able to access them remotely from a different computer.


- If you are aware of these risks and still want to save them locally, there are  different possible locations:

    - Probably the best folder is `C:\Scratch`. You have write permissions and there is no space limit, but please **do not fill the hard-drive completely**! There should always be at least **10-15% of free space** to install software updates and ensure the stability of the operating system.
    - On some computers there is a second hard-drive installed, which is visible in the Windows Explorer as drive `D:`. You may use it too and there are no restrictions on free space because it will not be used by the operating system itself.

