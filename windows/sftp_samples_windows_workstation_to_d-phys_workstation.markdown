SFTP Samples Windows Workstation to D-PHYS Workstation
======================================================

WinSCP is a client program to access files on a Linux workstation running ssh. You could use WinSCP to transfer files secure over the internet. You may get WinSCP from the original site http://winscp.sourceforge.net/.

**Login to a server**
After installing, start WinSCP from Start / Programs / WinSCP3 / WinSCP. The first dialog appears:

[[!img /media/sftp_samples_windows_workstation_to_d-phys_workstation/dialog1.png alt="dialog1.png"]]

Enter the hostname you like to connect, your username and your password.

The first time you connect to a server, this dialog appears:

[[!img /media/sftp_samples_windows_workstation_to_d-phys_workstation/dialog2.png alt="dialog2.png" size="450x"]]

The servers host key will be added to your registry and you wont be prompted again. This secures that you are connecting every time to the same machine and protects you from a man in the middle attack.

Now the file transfer window appears and you could drag & drop your files from local to remote and back:

[[!img /media/sftp_samples_windows_workstation_to_d-phys_workstation/dialog3.png alt="dialog3.png" size="450x"]]

The second interface (choosable in the preferences pane while starting WinSCP) looks more like explorer:

[[!img /media/sftp_samples_windows_workstation_to_d-phys_workstation/dialog4.png alt="dialog4.png" size="450x"]]

You could also work with keys to avoid typing your password. See [[SSH Samples Windows Workstation to D-PHYS Workstation]] how to do that.
