Set Default Program in Windows 10
=================================

To change the default Browser or PDF reader, is in Windows 10 little bit different then in previous Windows version.

In Windows 10 you have three different ways to set a Default Program.

- by **[file extension](#set-default-program-by-file-extension)** for example: `.pdf; .jpg; .png; .docx`

- by **[application type](#set-default-programs-by-application-type)** for example: `Browser; Mail; Video Player`

- by **[application](#set-default-programs-by-application)** for example: `Firefox; Outlook; Photoshop`


### Set Default Program by file extension

Right click on the file where you want to change the Default Program and select **Properties**

[[!img media/windows/windows10/set_default_program1.png size="350x"]]

In the general tab click on the **change** button

[[!img media/windows/windows10/set_default_program2.png size="350x"]]

Select the Default Application which you want.

[[!img media/windows/windows10/set_default_program3.png size="350x"]]


### Set Default Programs by application type

Open the **Start Menu** and click on **Settings**

[[!img media/windows/windows10/set_default_program4.png size="350x"]]

In the settings window click on **Apps**

[[!img media/windows/windows10/set_default_program5.png size="450x"]]

Open the **Default apps** menu

[[!img media/windows/windows10/set_default_program6.png size="350x"]]

Change the Default Browser or Mail Application by clicking on the specific App.

[[!img media/windows/windows10/set_default_program7.png size="350x"]]


### Set Default Programs by application

Open the **Start Menu** and click on **Settings**

[[!img media/windows/windows10/set_default_program4.png size="350x"]]

In the settings window click on **Apps**

[[!img media/windows/windows10/set_default_program5.png size="450x"]]

Open the **Default apps** menu

[[!img media/windows/windows10/set_default_program6.png size="350x"]]

At the bottom click on **Set defaults by app**

[[!img media/windows/windows10/set_default_program8.png size="350x"]]

Choose an Application and click **Set this program as default**

[[!img media/windows/windows10/set_default_program9.png size="450x"]]
