Edit Lockscreen on Windows 10
=============================

You can enable or disable the auto lockscreen from your Windows Workstation.

It need just few steps to do this.

Right click on the Desktop Background and select **Personalize**

[[!img media/windows/windows10/edit_lockscreen1.png size="300x"]]

Click in the menu on **Lock Screen**

[[!img media/windows/windows10/edit_lockscreen2.png size="300x"]]

At the bottom of this page, click on **Screen saver settings**

[[!img media/windows/windows10/edit_lockscreen3.png size="300x"]]

### Enable lockscreen

To enable the Lockscreen. Enable the checkbox **"On resume, display logon screen"** and define the wait minutes.

Recommended is **20 minutes**, because the display go to sleep at this time.

[[!img media/windows/windows10/edit_lockscreen4.png size="300x"]]

### Disable lockscreen

To disable the Lockscreen. Disable the checkbox **"On resume, display logon screen"**

[[!img media/windows/windows10/edit_lockscreen5.png size="300x"]]
