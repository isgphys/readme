Change Display or Keyboard language in Windows 10
=================================================

The default user interface for our managed workstations is German with the Swiss German Keyboard layout.
You can change the **display language**, this is the user interface language in Windows to English.
There is also the possibility to change the **keyboard layout** to English-US or German-Switzerland depending on your personal preference. Please follow this steps to change the settings:

## 1. Change the display language

Open **Settings** from the search box.

[[!img media/windows/windows10/change_language/0.1.png size="300x"]]

Choose **Click on Zeit und Sprache**

[[!img media/windows/windows10/change_language/0.2.png size="300x"]]

under **Region und Sprache** can you set the default language

[[!img media/windows/windows10/change_language/0.3.png size="300x"]]

Now you should **logoff or restart** your computer for activate the new settings.

## 2. Change the keyboard language

You can change your keyboard layout in the Systray menu in the right corner
[[!img media/windows/windows10/change_language/2.1.png size="300x"]]

When the keyboard layout is not persistent US-Layout, remove the Swiss German layout under Language Preferences
[[!img media/windows/windows10/change_language/2.2.png size="300x"]]

## 3. Change regional Format

When you change your Display language to English, it will change automatic the Time format and the currency format.

Open **Control Panel**
[[!img media/windows/windows10/change_language/1.1.png size="300x"]]

Chose **Change date, time or number formats**
[[!img media/windows/windows10/change_language/1.2.png size="300x"]]

In the Format panel chose the **German (Switzerland)** Format
[[!img media/windows/windows10/change_language/1.3.png size="300x"]]
