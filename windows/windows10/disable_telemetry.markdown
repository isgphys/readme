Disable Telemetry in Windows 10
===============================

We recommend to disable the telemetry communication to Microsoft, this can you do with few clicks.

* Press **WIN+R** keys together to launch RUN dialog box. Now type **gpedit.msc** in RUN and press Enter. It'll open **Group Policy Editor**.
* Now go to:

 ```
 Computer Configuration -> Administrative Templates -> Windows Components -> Data Collection and Preview Builds
 ```

* In right-side pane, look for "**Allow Telemetry**" option

    [[!img /media/windows/windows10/telemetry/1.png size="400x" alt="Telemetry GPO Rule"]]

* Double-click on "Allow Telemetry" and select **Enabled** option in the new window. Now you can set its value to any of following options from the drop-down list:
    * 0 - Security
    * 1 - Basic
    * 2 - Enhanced
    * 3 - Full

    [[!img /media/windows/windows10/telemetry/2.png size="400x" alt="Telemetry GPO Rule"]]

* We suggest to set the option to "0 - Security" to minimize telemetry and data collection in Windows 10.
