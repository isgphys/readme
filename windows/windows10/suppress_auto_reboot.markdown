Suppress auto reboot on Windows 10
==================================

Windows 10 will automatic reboot after installing Windows Update.

Windows Update are important to secure your system and stable your system.

We **recommend to install the Updates as soon as possible**.

There a few options to suppress it.

Set your active hours
---------------------

You can set your active hours to tell your machine, when you are in your office. Windows will not restart automatically during this time period.

* Open the **Windows Settings** and go to:

```
Update & Security / Windows Update / Change active hours
```

* Edit the time range to the time when you are normally in the office.

[[!img /media/windows/windows10/suppress_reboot/active_hours.png size="400x" alt="Windows Update setting"]]

Temporary pause Windows Update
------------------------------

If you calculating something or you have a measurements that need running over days. You can disable Windows Updates for few days with one click.

* Open the **Windows Settings** and go to:

```
Update & Security / Windows Update / Advanced options / Pause Updates
```

* Enable it with the toggle.

[[!img /media/windows/windows10/suppress_reboot/pause_update.png size="400x" alt="Windows Update setting"]]

* Now can you see it how long the pause status is enabled. After this will your computer install all available updates and reboot when it is out of the active hours

Disable auto installation of Windows Updates
--------------------------------------------

If the options above not enough and you want install the updates by your self. You can disable the auto installation.

### IMPORTANT

```
This option doesn't mean, that you never need install your updates.
Windows Update will receive every second Tuesday evening in Month.
Please make a reminder on Wednesday to Install updates.
```

* Press WIN+R keys together to launch RUN dialog box. Now type gpedit.msc in RUN and press Enter. It'll open Group Policy Editor.

* Now go to:

```
Computer Configuration -> Administrative Templates -> Windows Components -> Windows Update
```

* In right-side pane, look for "**Configure Automatic Updates**" option

    [[!img /media/windows/windows10/suppress_reboot/disable_update_1.png size="400x" alt="Windows Update setting"]]

* Double-click on "Configure Automatic Updates" and select **Enabled** option in the new window. Now you can set its value to any of following options from the drop-down list:

    * 2 - Notify for download and auto install
    * 3 - Auto download and notify for install
    * 4 - Auto download and schedule the install
    * 5 - Allow local admin to choose setting

    [[!img /media/windows/windows10/suppress_reboot/disable_update_2.png size="400x" alt="Windows Update setting"]]

* Choose the options "3 - Auto download and notify for install" to Disable the auto installation of Windows Update.

* Next time when you get a notification to update your computer you must press **Install** in the Windows Update settings menu, in your next maintenance block, to start the Update installation.

[[!img /media/windows/windows10/suppress_reboot/disable_update_3.png size="400x" alt="Windows Update setting"]]
