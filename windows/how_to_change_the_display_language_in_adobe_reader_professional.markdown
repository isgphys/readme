How to change the display language in Adobe Reader/Acrobat Professional
=======================================================================

Adobe Reader and Acrobat Professional has a multi-language user interface. On D-PHYS managed computers you can choose between English and German language.

To change the language, follow these steps:

* Go to "**Bearbeiten**" -> "**Voreinstellungen..**" or press "**Ctrl+K**"

    [[!img /media/how_to_change_the_display_language_in_adobe_reader_professional/adobe_languageselect_1.png size="300x"]]

* Choose "**Sprache**" in the left menu, then "**Wahl bei Anwendungsstart**" on the top menu an confirm with the "**OK**" button and close the program.

    [[!img /media/how_to_change_the_display_language_in_adobe_reader_professional/adobe_languageselect_2.png size="400x"]]

* **Start** Adobe Reader or Acrobat Professional again and you will see a language selector box, which you can select the desired language.

    [[!img /media/how_to_change_the_display_language_in_adobe_reader_professional/adobe_languageselect_3.png size="300x"]]
