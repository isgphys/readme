Connect to Windows Remote Desktop Server
===================================

For more information see [[here|services/windows_terminal_server]]

* Run `mstsc.exe`

    [[!img /media/windows/connect_to_terminalserver/01.png size="300x"]]

* Click on `Show Options`

    [[!img /media/windows/connect_to_terminalserver/02.png size="300x"]]

* Enter the computer name `winlogin.phys.ethz.ch`
* Enter the Username in this format `ad\your_dphys_username`

    [[!img /media/windows/connect_to_terminalserver/03.png size="300x"]]

* Finally enter your D-PHYS password

    [[!img /media/windows/connect_to_terminalserver/04.png size="300x"]]
