First setup Windows 11 @ D-Phys
===============================

When you turn on your Windows 11 computer for the first time, the Setup Assistant, also known as the Out of the Box Experience (OOBE), will appear.

If your computer only makes it to the 'Let’s connect you to a network' step and prompts for an internet connection, it means your device MAC address has not yet been registered on our network. You can skip this menu item and register your computer later by following the steps below.

[[!img /media/windows/windows11/windows_11_network.png size="450x"]]

* At “Let’s connect you to a network” screen, press SHIFT+F10 keys together. It’ll launch Command Prompt window on the top of setup wizard window.
* Enter following command in the command line:

```batch
oobe\bypassnro
```

* Now your computer will restart and you will have an option `I don't have internet` and continue the second screen with the `Continue with limited setup` option.

After installing of Windows 11 you can [register](/network/how_to_get_network_access) the MAC address of your computer.
