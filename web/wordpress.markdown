Wordpress
=========

Requesting a WordPress site
---------------------------

Please see [[general web share instructions|web/webshares]].

Administration and customization
--------------------------------

Open `https://yourdomain.phys.ethz.ch/wp-admin/` in your browser and use the credentials:

- Username: `admin`
- Password: use 'database_password' from `private/database.yml`

Go to **Settings** > **General** and change **Administration Email Address** to your address.

All WordPress instances may now be fully customized using the web interface.
This includes installing plugins and themes.

Updates
-------

All WordPress instances including installed plugins and themes will be automatically updated.
