DokuWiki
========

We offer [DokuWiki](https://www.dokuwiki.org/) based wiki hosting for research and other groups in the Dept. of Physics:

- [Group Wikis](https://wiki.phys.ethz.ch/) (to request a new wiki, [[contact us|/services/contact]])
- [Personal Wikis](https://wiki.phys.ethz.ch/!personal)

Information for Wiki Admins
---------------------------

Admins can configure most settings in the **Configuration Manager**.
Some options are grayed out or red.
Those are forced by our main configuration and cannot be changed.

Plugins can only be (de)activated by [[ISG|/services/contact]].
Some of the installed plugins ([ckgedit](https://www.dokuwiki.org/plugin:ckgedit)) are deactivated by default.

The **sprintdoc** template is set as the default template, which looks good on widescreen desktops and mobile devices.
Admins can choose another template in the **Configuration Manager**.

### ACLs

To change access permissions go to **Access Control List Management** in the **Admin** panel.

Common permissions may include:

- Default permission `* @ALL None` which means nobody has access
- `@ALL` represents all users (the public internet)
- `@user` represents all authenticated users (D-PHYS users)
- Any other `@groupname` will be mapped to our LDAP groups

Example to grant full (`Delete`) access to a group:

- Click on `[root]` to select the whole wiki namespace
- Check that **Permissions for** `Group` is selected
- Type the name of the LDAP group in the empty field
- Click `Select`
- In `Add new Entry` set the permission (`Delete`)
- Click `Save`
