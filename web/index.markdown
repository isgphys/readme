# Web

Our web servers at the D-PHYS use [Apache](https://httpd.apache.org/) + [MariaDB](https://mariadb.org/) ([MySQL](https://en.wikipedia.org/wiki/MySQL)) + [PHP](https://www.php.net/)/[Python](https://www.python.org/)/[Perl](https://www.perl.org/) ("LAMP") and provide the following services:

- [Debian](https://debian.ethz.ch/) (official [ftp.ch.debian.org](http://ftp.ch.debian.org/)), [Ubuntu](https://ubuntu.ethz.ch/) and [Raspbian](https://raspbian.ethz.ch/) mirror currently serving about <div data-host="https://netdata.phys.ethz.ch/host/plattenberg/" data-netdata="apache_local.requests" data-chart-library="textonly" data-update-every="2" style="display: inline;"></div> requests/s

<div data-netdata="apache_local.requests"
    data-host="https://netdata.phys.ethz.ch/host/plattenberg/"
    data-chart-library="sparkline"
    data-legend="no"
    data-width="60%"
    data-height="20"
    data-after="-300"
    data-points="150"
    ></div>

- [[documentation/Grafana]] with [[documentation/InfluxDB]] currently serving about <div data-host="https://netdata.phys.ethz.ch/host/phd-influx/" data-netdata="nginx_local.requests" data-chart-library="textonly" data-update-every="2" style="display: inline;"></div> requests/s

<div data-netdata="nginx_local.requests"
    data-host="https://netdata.phys.ethz.ch/host/phd-influx/"
    data-chart-library="sparkline"
    data-legend="no"
    data-width="60%"
    data-height="20"
    data-after="-300"
    data-points="150"
    ></div>

- Web hosting for group/project sites currently serving about <div data-host="https://netdata.phys.ethz.ch/host/phd-apache/" data-netdata="apache_local.requests" data-chart-library="textonly" data-update-every="2" style="display: inline;"></div> requests/s

<div data-netdata="apache_local.requests"
    data-host="https://netdata.phys.ethz.ch/host/phd-apache/"
    data-chart-library="sparkline"
    data-legend="no"
    data-width="60%"
    data-height="20"
    data-after="-300"
    data-points="150"
    ></div>

- [[Personal homepage|web/how_to_get_a_personal_homepage]] on [people.phys.ethz.ch](https://people.phys.ethz.ch/) for all D-PHYS [[users|account]]
- [[Group homepage|web/how_to_get_a_personal_homepage]] on [share.phys.ethz.ch](https://share.phys.ethz.ch/) for all D-PHYS [[groups|account/groups]]
- [[Dedicated websites (web shares)|web/webshares]] for more demanding applications and a `foo.phys.ethz.ch` or `bar.ethz.ch` URL
- [[Test and staging environments|web/test_webshares]] (test web shares) for development
- [[Calendar]]

## Support and announcement room

If you are a website owner, please join our Matrix room [#web:phys.ethz.ch](https://m.ethz.ch/#/#web:phys.ethz.ch)
to get help and stay informed about upcoming changes on the web server.

## Web hosting overview

We can provide installations and support for various content management systems or wiki engines.

### Fully managed

The services below are fully managed and recommended by ISG. We take care of the initial installation, maintenance (security updates) and possible migrations due to major version updates.

- [[web/Wordpress]] (CMS, Blog)
- [[DokuWiki|/web/dokuwiki]] (Wiki)

### Partially managed

We provide installations with limited maintenance support for:

- [Drupal](https://www.drupal.org/) (CMS)
- [MediaWiki](http://www.mediawiki.org/wiki/MediaWiki) (Wiki)
- [TWiki](https://twiki.org/) (Wiki)
- [[WSGI Apps|/web/wsgi]] (Python 3)

### Unmanaged

You may freely choose to install any other software or even write a dynamic website on your own in your preferred programming language. You are responsible for security updates and compatibility with our hosting environment.

## How to get a site

- To create a personal or group homepage in existing home directory or groupshare (`public_html`), please refer to [[this howto|web/how to get a personal homepage]]
- For a dedicated webshare (with your own domain name, with a database, PHP, Python, Wordpress, etc.), please refer to [[this howto|web/webshares]] and [[request a new website|services/contact]].

For any other services listed or not listed above, please [[contact us|services/contact]].

## More information on web hosting

[[!map pages="./web/*"]]

<script type="text/javascript">var netdataNoBootstrap = true;</script>
<script type="text/javascript" src="https://netdata.phys.ethz.ch/dashboard.js"></script>
