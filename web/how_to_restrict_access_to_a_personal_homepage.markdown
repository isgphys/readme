# How restrict access to a personal or group homepage

You can make a homepage subdirectory password-protected. To do so you can grant access based on existing D-PHYS users/groups or you can define your own self-managed username/password database.

## Using D-PHYS user/group database

As example, we will create a password protected subdirectory "foo", where we want to give access to [[registered D-PHYS users|account]].

Create the directory, if it doesn't exist. It must be somewhere under the `public_html` directory, e.g. with:

```
mkdir ${HOME}/public_html/foo
```

It _cannot_ be a symbolic link to somewhere outside the `public_html` directory.

Create a file with name `.htaccess` in that directory with the following content:

```apache
AuthType Basic
AuthName "ETH D-PHYS Account"
AuthBasicProvider ldap
AuthLDAPURL "ldap://ldap1.phys.ethz.ch ldap2.phys.ethz.ch ldap3.phys.ethz.ch/ou=people,dc=phys,dc=ethz,dc=ch?uid?one?(&(objectClass=dphysUser)(!(blocked=yes)))" TLS
AuthLDAPGroupAttribute memberUid
AuthLDAPGroupAttributeIsDN off
```

To give access to some [[D-PHYS users|account]], add the following line with the space separated usernames to grant access (here we grant access to the two users `rda` and `horeizo`):

```apache
Require ldap-user rda horeizo
```

To give access to [[D-PHYS groups|account/groups]] add a line as follows for every group to grant access (here we grant access to the group `isg`):

```apache
Require ldap-group cn=isg,ou=groups,dc=phys,dc=ethz,dc=ch
```

To grant access to all D-PHYS users, simply add the following line:

```apache
Require valid-user
```

## Using self-managed username/password database

As example, we will create a password protected subdirectory "foo" for the user "johndoe" at http:s//people.phys.ethz.ch/~johndoe/foo/. There will be two users with access: One will be "bar" with password "foobar", the other will be "fnord" with password "gna". Change those values appropriately for your case.

Create the directory, if it doesn't exist. It must be somewhere under the `public_html` directory, e.g. with:

```
mkdir ${HOME}/public_html/foo
```

It _cannot_ be a symbolic link to somewhere outside the `public_html` directory.

Create a file with name `.htaccess` in that directory with the following content:

```apache
AuthType Basic
AuthName "This directory is password protected"
AuthBasicProvider file
AuthUserFile /home/johndoe/public_html/foo/.htpasswd
require valid-user
```

Execute the following command on one of our managed Linux workstations:

```
htpasswd -B -b -c ${HOME}/public_html/foo/.htpasswd bar foobar
```

(You may also omit the `-b` parameter and the password. Then you have to type it twice and blindly afterwards. The `-c` parameter is only needed the first time to create the `.htpasswd` file.)


Make sure, the `.htpasswd` and `.htaccess` files are world-readable, e.g. with:

```
chmod 644 ${HOME}/public_html/foo/.ht*
```

otherwise the web server won't have access to those files. Again, if you have your home directory inaccessible for others, they won't get access, even if they know the location of those files. The web server won't give them out either, even not with password.

If you want to add additional users, additionally execute the following command on one of our managed Linux workstations:

```
htpasswd -B -b ${HOME}/public_html/foo/.htpasswd fnord gna
```

(You may also omit the `-b` parameter and the password. Then you have to type it twice and blindly afterwards.)

See [Authentication and Authorization How-To by Apache](https://httpd.apache.org/docs/2.4/howto/auth.html) for the gory details and further documentation.

## Combined LDAP and self-managed authentication

You can combine the options listed in the LDAP and self-managed sections above and select both authentication providers with the following directive:

```apache
AuthBasicProvider ldap file
```
