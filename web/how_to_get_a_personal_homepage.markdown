# Personal or group homepage

This guide is valid for **personal homepages served from your home directory**
or for **group homepages served from groupshares**. The latter can be a good
way to share large amounts of group data with selected people or the public.

The homepages are available under the following URLs:

- `https://people.phys.ethz.ch/~username/` (Home directory)
- `https://share.phys.ethz.ch/~groupname/` (Groupshare)

Note: This guide uses the shell variable `${HOME}`, which will be evaluated by your shell
and replaced with your home directory (example: `/home/username`).
This is automatically available on the managed workstations (either `${HOME}` on Linux and macOS,
or the `H:` drive on Windows).
For self managed workstations read how to [[access|storage/access]] the fileservers.

Note for group homepages: Simply replace `${HOME}` with `/home/groupname`

## Access from any computer

You can log in from any computer to our login server (`login.phys.ethz.ch`) using [[documentation/SSH]] and from there create the homepage in your home/groupshare (see below).

## Creating the homepage

Here's how to setup your homepage:

### Short description for Linux and Mac command line users

```
mkdir ${HOME}/public_html
chmod 755 ${HOME}/public_html
$EDITOR ${HOME}/public_html/index.html
$BROWSER https://people.phys.ethz.ch/~username/
```

### General description (e.g. for Mac, Windows, KDE or GNOME users)

* Create a directory `public_html` in your home directory (this is the directory `/home/username/` with _username_ replaced by your user name. It is not your desktop neither the directory `/home/`)
* Make it at least world accessible. You home directory does not need to be world accessible or readable though.
* Create a file called `index.html` in that newly created directory and put some HTML in it. See http://de.selfhtml.org/ for a good, easy to understand and though comprehensive HTML documentation in German.
* Point your favorite web browser to `https://people.phys.ethz.ch/~username/`. (Please note that you will get a "Permission denied" error if you didn't create an `index.html` file beforehand.)
* You may optionally register your personal homepage to show up in the ETH phone book. Go to https://passwort.ethz.ch, log in with your ETH account, then navigate to `my services` and `Homepage`.

## Subdirectory password protection

See [[web/How to restrict access to a personal homepage]].

## Show beautiful math on your page

Check out [[documentation/KaTeX]].

## Configuration options

For the default `public_html` configuration and the list of allowed configuration options via `.htaccess` files, please refer to [[web/personal homepage configuration]].

## FAQ

### Can I use symbolic links?

Only relative symbolic links with source and target within `public_html` are supported.
Links to targets residing outside of the `public_html` and furthermore, links to `/net` are not possible.

### Can I use CGI scripts for dynamic content?

Not by default. Please [[contact us|services/contact]] and request CGI execution for your preferred scripting language like PHP, Python, Perl, etc.
