# TLS/SSL Certificates

To secure our services (e.g., HTTPS for web sites or SMTP and IMAP on the mail server) we employ two types of TLS/SSL certificates:

- Commercial certificates signed by [DigiCert Inc](https://www.digicert.com/)
- (Old) Commercial certificates signed by [QuoVadis CA](https://www.quovadisglobal.com/)
- Free and open certificates signed by [Let's Encrypt](https://letsencrypt.org/)

In the past some of our services/sites used [CAcert](http://www.cacert.org/) or self-signed certificates. These should not be in use anymore. If we missed one in our checks or should you encounter any security errors, please [[contact us|services/contact]].

## Directory service (LDAP)

Our LDAP data is only accessible through an encrypted connection using TLS v1.2 or GSSAPI (Kerberos 5) to ensure the security and privacy of our users. Refer to [[account/ldap]] for TLS certificate trust and configuration details.
