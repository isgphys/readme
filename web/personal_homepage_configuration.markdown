# Personal homepage configuration

## Default configuration

By default every personal homepage is configured with the following [apache web server options](https://httpd.apache.org/docs/2.4/mod/core.html#options)

```apache
Options Multiviews Indexes SymLinksIfOwnerMatch
```

which enables [multi-views](https://httpd.apache.org/docs/current/content-negotiation.html), [automatic directory indexing](https://httpd.apache.org/docs/current/mod/mod_autoindex.html) in the absence of an `index.html` file and following of symlinks in your `~/public_html/` directory and all subfolders.

Please note that only relative symbolic links within `public_html` are supported.

By default access is granted to all files and directories within `~/public_html` except for dotfiles (anything starting with a dot `.`) and CGI script files (like `index.php`). Please [[contact our helpdesk|services/contact]] should you require dynamic content provided by CGI scripts in your preferred scripting language like PHP, Python, Perl, etc.

## Modifying your configuration

To modify your configuration, create a file named `.htaccess` in the directory to modify. For example to turn off directory indexing, create a `.htaccess` file with the following content:

```apache
Options -Indexes
```

You may also specify multiple directives to enable/disable options (`+EnableOption`/`-DisableOption`):

```apache
Options +Indexes -SymLinksIfOwnerMatch
```

To disable all directory options:

```apache
Options None
```

For password protected directories, please refer to [[web/How to restrict access to a personal homepage]].

## Directives allowed to override

You may override any directives in the following 3 [override classes](https://httpd.apache.org/docs/current/mod/overrides.html)

- `AuthConfig`
- `Limit`
- `Indexes`

and the additional directives

- `Redirect`
- `RedirectMatch`
- `RedirectPermanent`
- `RedirectTemp`

You may also override directory `Options` limited to the directives

- `None`
- `Multiviews`
- `Indexes`
- `SymLinksIfOwnerMatch`
