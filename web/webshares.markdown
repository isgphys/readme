# Web site hosting at ISG D-PHYS

A web share works like a regular group share but the data is hosted directly on the web server (for performance reasons) and is meant to make the files of your website easily accessible. It is not suitable for storing large amounts of data (more than a few GB) like on regular group shares.

## Requesting a new web site

To request a new website, please [[contact us|services/contact]] and provide the following information:

- desired web share name: `www<name>`
- desired domain name: `<name>.phys.ethz.ch` / `<name>.ethz.ch`
- web share members (administrators): a list of D-PHYS usernames
- web share owner (may [[edit members|account/groups/]]): a list of D-PHYS usernames
- website access restrictions: public or a list of D-PHYS groups

Optional website specifics:

- CMS installation: Wordpress, Drupal, TWiki, Mediawiki, etc?
- SQL database (MariaDB): Yes/No?
- dynamic content: provide CGI file extensions (`.php`, `.pl`, `.py`, `.cgi`, etc)
- Python 3 WSGI web app: provide modules in `private/requirements.txt`
- SSH access (power users): D-PHYS username(s) + [[ssh keys|/documentation/ssh_keys/]]

To check if your desired domain name is still available, you can use any managed Linux workstation:

```bash
host <name>.phys.ethz.ch
```

## Access the files and directory structure

A web share can be accessed like a regular group share and is prefixed with `www`.
See also [[How to access your files|storage/access]].

```bash
cd /home/wwwshare
```

It has the following directory structure:

```
wwwshare
├── public
├── private
├── logs
│   ├── archive
│   ├── goaccess
│   ├── http_access.log
│   ├── http_error.log
│   ├── https_access.log
│   ├── https_error.log
├── stats
│   ├── archive
│   └── current
├── fcgid
│   └── fcgid-wrapper-generic
└── tmp
```

This structure is managed my ISG. Users do not have write permission in the main web share home directory, but in the sub-directories `public`, `private` and `tmp`.

The sub-directories contain the following types of files:

- `public`: files that should be visible on the web (previously `htdocs`)
- `private`: files that should **not** be visible, like PHP DB config files etc [1]
- `logs`: the log files of your web site
- `stats`: the website statistics files
- `fcgid`: only present if you use CGI (Python, PHP, etc)
- `tmp`: for temporary files, unpacking zips etc; may be regularly deleted

[1] note that .htpasswd files do not work in `private/`, but are protected differently

### SSH Access

SSH access is available optionally for power users, typically for WSGI apps or when it is convenient to
be able to execute shell commands directly on the server. Tasks such as `pip install` or GitLab CI/CD
integration may need ssh access.

Access is only possible via [[ssh authorized_keys|/documentation/ssh_keys/]], which are managed by us.
Due to security reasons we automatically disallow ssh-agent forwarding on shares with multiple ssh users.

Connect to the share using: `ssh www<name>@<website_domain>`

Should you need local ssh keys for the webshare user (i.e. for GitLab deploy keys),
create them in `~/private/.ssh/`, since the webshare user does not have write access to `~/.ssh/`:

```bash
ssh-keygen -t ed25519 -C "name" -f ~/private/.ssh/id_ed25519
```

For outgoing ssh connections the key location for ed25519 and RSA keys is preconfigured in `~/.ssh/config`.

Please only use ssh access for the intended purpose, which is website related configuration and tasks.
**No computation** 🙅‍♀️.

## Access website statistics

All websites contain a statistics URL (`/website-statistics`), that are updated on a daily basis. The stats are generated using [goaccess](https://goaccess.io/) and may only be accessed by web share administrators (all members of the respective `wwwshare` group).

The stats may be accessed as follows:

- via web: https://yourdomain.phys.ethz.ch/website-statistics/
- via file: `/home/wwwshare/stats/`

The sub-directory `current` holds the daily website statistics, `archive` holds old archived statistics.

## Access log files

The log files can be used for debugging purposes, example usage [[on linux|documentation/ssh]]:

```bash
ssh <username>@login.phys.ethz.ch
tail -f /home/wwwshare/logs/*.log | ccze
```

We provide separate log files for HTTP and HTTPS requests. Users should normally only care about the `https_*` log files, as all website requests are automatically redirected to HTTPS.

The sub-directory `archive` may contain old archived log files from recent migrations, `goaccess` holds temporary data for website statistics generation.

## Access the database

Some websites are connected to an SQL database, which can be [accessed via web-interface](https://sqlweb.phys.ethz.ch/adminer/).
Select the Server `sqlweb` and connect using the credentials in `private/database.yml`.

For connections from your dynamic website code, use `sqlweb.phys.ethz.ch` as the database hostname.

With [[SSH access|/web/webshares/#ssh-access]] enabled you can connect using the cli `mysql`
after connecting via ssh to the web server:

```sh
wwwuser@server:~$ mysql
MariaDB [wwwshare]>
```

## Dynamic content using CGI

We allow to use dynamic content using CGI, which is enabled on demand only (please [[contact us|services/contact]]). It allows to use various scripting languages like [PHP](https://www.php.net/) or [Perl](https://www.perl.org/).

It is implemented using `libapache2-mod-fcgid` and `mod_suexec` via an ISG managed FCGID wrapper script. This means that code is executed as the `wwwshare` user and only files within the web share may be accessed from CGI scripts.

## Python 3 web applications

See [[/web/wsgi]].

## Test and staging environments

See [[/web/test_webshares]].
