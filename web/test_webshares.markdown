# Test webshares

Sometimes it can be useful to have a clone of your [[website|/web/webshares]] or database
in order to test version upgrades or as a staging environment for development.

## Server setup and conditions

The web and database server are hosted separately from the production servers,
which is also used by us (ISG) as testing environment and for regular OS upgrade tests of the production servers.
This will also enhance synergies between us and users willing to invest in testing
to get their websites up and running more quickly after OS upgrades.

Conditions:

- There is **no backup**. Use git + gitlab to track/store your changes
- The servers can be destroyed/re-deployed at any time without notice
- All untracked changes/data will be lost
- Join Matrix room [#web:phys.ethz.ch](https://m.ethz.ch/#/#web:phys.ethz.ch) and be online
- Announcements only via Matrix room on very short notice

## Requesting a new test webshare

Test webshares use the identical setup as documented for [[webshares|/web/webshares]].

We will create a complete initial clone of your existing production site, with the following changes:

- test web share name: identical to your production `www<name>`
- domain name: `test-<your_production_domain>`
- database server: see `private/database.yml` on test webshare
- no NFS/SMB access possible

Please provide:

- SSH access: D-PHYS username(s) + [[ssh keys|/documentation/ssh_keys/]]
- anything else that should be different?

## SSH Access

The [[webshare data|/web/webshares/#access-the-files-and-directory-structure]]
of the test webshare is accessible by SSH only.
See [[ssh access|/web/webshares/#ssh-access]] but connect using:

```sh
ssh www<name>@test-<your_production_domain>
```

## Access the database

Use the same [[web interface|/web/webshares/#access-the-database]] but select the server `test-sqlweb`
and connect using the credentials in `private/database.yml` on the test webshare.

We recommend directly including the credentials from the file mentioned above in your code,
which will allow to share identical website configuration between production and testing.

The database hostname is `test-sqlweb.phys.ethz.ch`.

To re-sync (dump + restore) the database from production to testing connect to the production site
using [[ssh|/web/webshares/#ssh-access]] and use the cli tools:

```sh
wwwuser@server:~$ mysqldump ${USER} | mysql -h test-sqlweb.phys.ethz.ch
```

This will dump all tables from production and overwrite the test database.
