# Python web applications

Please first read our documentation about [[/web/webshares]].

We allow to host Python 3.9 web applications using [WSGI](https://en.wikipedia.org/wiki/Web_Server_Gateway_Interface),
which is enabled on demand only (please [[contact us|services/contact]]).

## Basic usage

- WSGI app directory: `/home/wwwshare/public`
- WSGI entry point: `/home/wwwshare/public/wwwshare.wsgi`

All new WSGI webshares will come with a dummy app in `wwwshare.wsgi`.
Edit this file to load your app into the `application` variable.

```python
from pathlib import Path
import sys

# Add /home/wwwshare/public to the Python search path
PUBLICDIR = Path(__file__).parents[0]
sys.path.insert(0, str(PUBLICDIR))

# Import your app. Details depend on your code and framework.
from your_custom_app_code import app

# Assign the app to the special wsgi 'application' variable
application = app
```

If you make changes to the source code, you have to tell the web server to
restart the WSGI app by modifying the file time stamp on the WSGI entry point:

```bash
touch /home/<wwwshare>/public/<wwwshare>.wsgi
```

Note: [[SSH access|/web/webshares/#ssh-access]] is recommended for developers.

## Static content

If your app hosts static content, please inform us about the relative path(s)
to the location(s) in `/home/wwwshare/public`, that contains static content.

## Virtual environment

Our WSGI uses a [virtual environment](https://compenv.phys.ethz.ch/python/ecosystem_1/30_virtual_environments/) (venv),
which contains any custom python packages and is activated by our web server configuration.

- Location: `/home/wwwshare/private/venv`
- Requirements: `/home/wwwshare/private/requirements.txt`
- Freeze: `/home/wwwshare/private/freeze.txt`

If your app needs custom python packages, please specify them in the
[requirements file](https://pip.pypa.io/en/stable/reference/requirements-file-format/).
Our deployment will install the venv from the requirements.txt and automatically
generate the freeze.txt file, which contains all package versions installed in the venv.
The freeze.txt file is needed to reproducibly re-generate the exact state of the venv later,
please do not modify this file.

Should there be any changes in the requirements.txt file, we will have to generate a new venv,
so please [[contact us|services/contact]] in that case.

Alternatively you can use [[SSH access|/web/webshares/#ssh-access]] to generate the venv yourself.
After logging in via SSH, follow the exact steps below:

```sh
rm -r private/venv
python3 -m venv private/venv
private/venv/bin/pip --no-cache-dir install --upgrade pip setuptools wheel
private/venv/bin/pip --no-cache-dir install -r private/requirements.txt
private/venv/bin/pip --no-cache-dir freeze --local > private/freeze.txt
touch public/*.wsgi
```

But this should not be needed often, since development should happen on your local machine
and your app should only be deployed to our server once it is stable.

## Database

If your app needs a database connection, please directly include the generated YAML settings from your app:

- Database parameters: `/home/wwwshare/private/database.yml`

## Resource usage

Please optimize your code for minimal resource usage (cpu, memory) due to finite server resources.
By default memory usage is limited to 1 GiB per WSGI process.
