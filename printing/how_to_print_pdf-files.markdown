How to print PDF-Files as Images
================================

The following steps are required to avoid printing problems with PDF files.

* On the print-dialog window, select the **Advanced** button.

[[!img /media/how_to_print_pdf-files/print-pdf_0_s.png]]

Settings of Adobe Reader
------------------------

* Deselect **Print as Image** (step 1), if already selected.
* Select **_Send for Each Page_** on **Font and Resource Policy**. (step 2)
* Press the **OK** button (step 3) and print the document.

[[!img /media/how_to_print_pdf-files/print-pdf_2_reader_s.png]]

Settings of Adobe Acrobat Professional
--------------------------------------

* Deselect **Print as Image** (step 1), if already selected.
* Select **Postscript Options**. (step 2)

[[!img /media/how_to_print_pdf-files/print-pdf_1_s.png]]

* Select **_Send for Each Page_** on **Font and Resource Policy**. (step 3)
* Press the **OK** button (step 4) and print the document.

[[!img /media/how_to_print_pdf-files/print-pdf_2_s.png]]
