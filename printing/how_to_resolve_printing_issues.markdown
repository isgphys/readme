How to resolve printing issues
==============================

Many factors can be relevant to diagnose printing issues.

Could the problem be the computer?
----------------------------------

* Have you installed the printer according to our [[instructions|printing/]]?
* Have you been able to print before?
* Have you changed anything in your computer configuration?
* Do you get any error messages when submitting the print job?

Could the problem be the printer?
---------------------------------

* Is there an error message on the display of the printer?
* Can other people print on the same printer?
* Have you tried to print on a different printer?

Could the problem be the document?
----------------------------------

* Have you tried to print a different document?
* Have you tried to print the same document from a different program?
* Have you tried to repair the pdf with our [converter](http://www.phys.ethz.ch/convert/)?
* Have you tried to print the [[pdf file as images|How to print PDF-files]]?

Need help?
----------

* Contact your IT coordinator or [[ISG|services/contact]].
