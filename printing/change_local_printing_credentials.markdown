Change locally saved password for PIA printers
==============================================

After you change the ETH password, you also have to update the password stored in your credential manager. Otherwise your computer keeps trying to authenticate with the old password and printing will fail.

### macOS

See [[osx/keychain]]

### Linux

See [[printing/authenticate_id_printer]]

### Windows

* For opening the credential manager, open the start menu and enter `credential manager` (in English) or `anmeldeinformationsverwaltung` (in German), into the search box.
[[!img /media/change_local_printing_credentials/win_1.png size="450x"]]
* Choose `Windows credentials`, click on the `pia` entries and select `Edit`.
[[!img /media/change_local_printing_credentials/win_2.png size="450x"]]
* Now enter your new ETH password into the `Password` field and press the `Save` button.
 [[!img /media/change_local_printing_credentials/win_3.png size="450x"]]
* Repeat the two steps above, if you have more then one `pia` entries.
* Important: Restart your computer to activate the new credentials!
