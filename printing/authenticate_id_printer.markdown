Authenticate ID printers on managed Linux workstations
======================================================

When printing on an ID printer for the first time, you have to save your ETH credentials in your keychain (you can manage your keychain with `seahorse`):

* start `system-config-printer` from a terminal

    [[!img /media/authenticate_id_printer/auth_idprinter1.png size="450x"]]

* right click on the printer, choose _Properties_
* hit _Print Test Page_

    [[!img /media/authenticate_id_printer/auth_idprinter2.png size="450x"]]

* in the password dialog, provide `d\LOGIN` in the username field, where LOGIN is your **ETH** login (in lowercase of course), and your password
* check the 'Remember password' box and click _OK_
* fetch the test page from your printer
