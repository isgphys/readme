How to add an ID printer on a Managed Windows Workstation
=========================================================

Search `Connect ETH Printer` in the start menu and click on the icon

[[!img media/how_to_add_an_id_printer_on_windows/1_shortcut.png size="400x"]]

Login with your ETH account.

[[!img media/how_to_add_an_id_printer_on_windows/2_logon.png size="400x"]]

Now the `card-hp on pia01.d.ethz.ch` is installed on your PC. With this printer you can print on every printer from the ETH print-service with your ETH Card.

After installation will open a new window, there you can chose your printers by double clicking on it. With this printer you don't need your ETH Card.

[[!img media/how_to_add_an_id_printer_on_windows/3_printer.png size="400x"]]
