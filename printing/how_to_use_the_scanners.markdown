How to Use The Scanners
=======================

The scanners on Linux workstations have been obsoleted and removed. Please use the new [[ID printers|printing]] for scanning.

* to optically recognize scanned texts or vectorize graphics, there are these tools that you can use
    * [unpaper](https://www.flameeyes.eu/projects/unpaper) remove dark edges, align pages
    * [gocr](http://jocr.sourceforge.net/) OCR software
    * [tesseract](https://github.com/tesseract-ocr) other OCR software
    * [autotrace](http://autotrace.sourceforge.net/) vectorize graphics
    * [convert](http://www.imagemagick.org/) to convert/process images into different formats, for instance to create a multi-page PDF of some scanned pages run: `convert page001.jpg page002.jpg page003.jpg the.pdf`.
