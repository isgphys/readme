ETH Print Service
=================

The D-PHYS uses the [ETH Print Service](https://unlimited.ethz.ch/display/itkb/Printing) of the central IT Services.
Please follow the [official instructions from ID](https://unlimited.ethz.ch/display/itkb/Printing) to configure printers on your personal device. For ISG managed workstations see instructions below.

* [[List of public printers in D-PHYS|printer_list]]

### Installation instructions

* Self managed devices: [Windows](https://unlimited.ethz.ch/display/itkb/Add+a+printer+with+Windows), [[macOS|how_to_add_an_id_printer_on_osx]] and [Linux](https://unlimited.ethz.ch/display/itkb/Add+a+printer+with+Linux)
* [[ISG-Managed Windows Workstation|how_to_add_an_id_printer_on_windows]]
* ISG-Managed Linux Workstation: we pre-install selected printers on all machines. [[Authenticate|authenticate_id_printer]] when printing for the first time. Note that some Linux programs show a password dialog when printing. Just hit _OK_ without entering anything.

### Advantages

* efficient printing with fewer, but better adapted printers
* push- and pull-printing for comfort and privacy
* centralized technical support
* automatic delivery of new toner
* no additional expenses for the printer hardware, toner or paper

### Useful links

* printer web interface at [print.ethz.ch](https://print.ethz.ch)
* web printing for [employees](https://piaweb01.ethz.ch) or [students](https://webprint.ethz.ch)
* [Search form to find a printer](https://ethz.ch/services/en/it-services/catalogue/printing/print-service/printer-locations.html)
* [How to scan to mail](https://unlimited.ethz.ch/display/itwdb/Scan+to+Mail)
* [How to scan to USB stick](https://unlimited.ethz.ch/display/itwdb/Scan+to+USB)
* [[How to Resolve Printing Issues]]
* [[Update ETH pia entries with wrong or outdated passwords|change local printing credentials]] (useful after password change)

### Pull printing via ETH card

The PIA system supports printing to a special `card-hp` queue, which stores your print jobs for 24 hours, and lets you release it on any printer you put your ETH card on.

### Transfer printing quota

Non-employees may need some money on their printing account. Employees can [transfer](https://print.ethz.ch/app?service=page/UserTransfer) small amounts of money to others to increase their printing quota.
