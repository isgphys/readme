How to add an ID printer on macOS
=================================

### Install driver

Download and install the required drivers, depending on the printer model, as explained on the [ID printing page](https://unlimited.ethz.ch/display/itkb/Add+a+printer+with+macOS):

* connect the [smb://nas12.ethz.ch/id_bd_printing_drivers/MacOS/hp/](smb://nas12.ethz.ch/id_bd_printing_drivers/MacOS/hp/) share with your ETH account (`d\username`). This requires VPN when not on campus.
* download and install both printer packages
    - `hp-printer-essentials-UniPS-6_1_0_1.pkg` package for most HP models
    - `hp-printer-essentials-SE-5_19_0_1.pkg` package for the HP X580 model

### Install print queue

* Open **System Preferences** -> **Printers & Scanners** and click on the button to add a new printer.
* Select the **Advanced** tab (see [below](./#customize-toolbar-to-add-the-advanced-icon) if this icon is missing)
* Choose **Type** -> **Internet Printing Protocol (ipps)**.
* Enter the **URL** of the print queue.
    * pull-printing: `ipps://pia01.d.ethz.ch:9164/printers/card-hp`
    * specific printer on Hoengg: `ipps://pia02.d.ethz.ch:9164/printers/p-building-hp0xx`

* Provide a name and location.
* Click **Use** -> **Select Software** and choose
    * **HP Color MFP E87640-50-60** for card-hp
    * the appropriate driver for any specific printer model
    * **Generic Postscript Printer** (universal) if the others are unavailable

[[!img media/osx/id_printer_add_queue.png size="450x"]]

* Once the printer is added, you may optionally configure additional trays by clicking the **Options & Supplies** button and opening the **Options** tab. Then select **Option Tray**: **Tray 2** if your printer has a second paper tray.
* Further [[settings|osx/print_settings]] (Color or Black&White, Paper Tray, etc) can be made when printing.


#### Comment about SMB printing

In the past, we documented to use the `smb://` protocol for printing. This is unfortunately broken with macOS Sonoma with no fix in sight from ID or Apple. We therefore suggest to use the `ipps://` protocol instead.

### Authenticate

On submitting your first print job, you will be required to authenticate with your ETH username and password. It's recommended to prepend your username with `d\`.

[[!img media/osx/id_printer_authenticate.png size="400x"]]

Use the [[Keychain Access|osx/keychain]] application to delete wrong or outdated passwords.

### Customize toolbar to add the Advanced icon

* Right-click on the toolbar and select _Customize Toolbar_

    [[!img media/osx/id_printer_customize_toolbar1.png size="400x"]]

* Drag and drop the **Advanced** icon to your toolbar

    [[!img media/osx/id_printer_customize_toolbar2.png size="400x"]]

### Install print queue with app

ETH offers an application for the printer installation, as explained in the [ID documentation](https://unlimited.ethz.ch/display/itkb/Add+a+printer+with+macOS).

