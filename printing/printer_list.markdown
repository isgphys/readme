# List of public D-PHYS printers

## Printers

| Queue name  | Location   | Model |
| ----------- | ---------- | ----- |
| p-hez-hp001 | HEZ E 17.7 |  A4   |
| p-hit-hp001 | HIT E 21.2 |  A3   |
| p-hit-hp012 | HIT G 32.1 |  A4   |
| p-hit-hp011 | HIT J 21.1 |  A3   |
| p-hit-hp014 | HIT J 33.4 |  A4   |
| p-hit-hp013 | HIT K 21.1 |  A4   |
| p-hpf-hp001 | HPF B 10.1 |  A4   |
| p-hpf-hp004 | HPF C 6    |  A4   |
| p-hpf-hp007 | HPF C 104  |  A3   |
| p-hpf-hp008 | HPF D 7    |  A3   |
| p-hpf-hp005 | HPF E 7    |  A4   |
| p-hpf-hp006 | HPF F 14   |  A4   |
| p-hpf-hp002 | HPF G 7    |  A4   |
| p-hph-hp001 | HPH F 10.5 |  A4   |
| p-hpk-hp003 | HPK D 61   |  A3   |
| p-hpk-hp007 | HPK E 25   |  A3   |
| p-hpk-hp005 | HPK F 27   |  A4   |
| p-hpk-hp006 | HPK G 31   |  A4   |
| p-hpk-hp004 | HPK H 27   |  A4   |
| p-hpp-hp003 | HPP J 12   |  A3   |
| p-hpr-hp001 | HPR E 86.1 |  A4   |
| p-hpt-hp007 | HPT C 5    |  A4   |
| p-hpt-hp006 | HPT C 113.2|  A3   |
| p-hpt-hp001 | HPT D 16   |  A4   |
| p-hpt-hp005 | HPT E 7.1  |  A4   |
| p-hpt-hp004 | HPT F 17.2 |  A4   |
| p-hpt-hp003 | HPT G 14   |  A4   |
| p-hpt-hp002 | HPT H 6    |  A4   |
| p-hpv-hp002 | HPV F 55   |  A3   |
| p-hpv-hp001 | HPV G 50.1 |  A4   |
| p-hpz-hp002 | HPZ G 26.2 |  A3   |

A4 is HP X58045z, A3 is HP E786z
